# coding: utf-8

import os
from setuptools import setup, find_packages
import os
from night_rail_bm18 import __version__

def setup_package():
    doc_requires = [
        "sphinx",
        "cloud_sptheme",
        "myst-parser",
        "nbsphinx",
    ]
    setup(
        name="night_rail_bm18",
        author="Alessandro Mirone",
        version=__version__,
        author_email="mirone@esrf.fr",
        maintainer="Alessandro Mirone",
        maintainer_email="mirone@esrf.fr",
        scripts=['sandbox/night_rail_bm18_collect_batch_tiffs.sh'],
        packages=find_packages(),
        package_data={
        },
        include_package_data=True,
        install_requires=[
            "numpy",
            "readline",
            "namedlist"
        ],
        extras_require={
            "full": [
            ],
            "doc": doc_requires,
        },
        description="application of night_rail ( A rail which guides you when the light faints away) to bm18.",
        entry_points={
            "console_scripts": [
                "night_rail_bm18_setup_pars     = night_rail_bm18.app.setup_pars:main",
                "night_rail_bm18_setup_pars_distortion     = night_rail_bm18.app.setup_pars_distortion:main",
                
                "night_rail_bm18_create_scripts_distortion     = night_rail_bm18.app.create_scripts_distortion:main",
                
                "night_rail_bm18_create_scripts = night_rail_bm18.app.create_scripts:main",
                "night_rail_bm18_modify_json = night_rail_bm18.app.modify_wf_json:main",
                "night_rail_bm18_nexus_juicer = night_rail_bm18.app.nexus_juicer:main",
                "night_rail_bm18_prepare_online = night_rail_bm18.app.prepare_online:main",
                "night_rail_bm18_automatic = night_rail_bm18.app.automatic:main",
                "night_rail_bm18_automatic_gui = night_rail_bm18.app.automatic_gui:main",
                "night_rail_bm18_automatic_gui_revised = night_rail_bm18.app.automatic_gui_revised:main",
                "night_rail_bm18_to_tiff = night_rail_bm18.app.to_tiff:main",
                "night_rail_bm18_join_lines = night_rail_bm18.app.join_lines:main",
                "night_rail_bm18_glue_scans = night_rail_bm18.app.colla:main",
                "night_rail_bm18_mend_scan = night_rail_bm18.app.mend_single_scan:main",
                "cutscan = night_rail_bm18.app.cutscan:main",
                "night_rail_bm18_extract_slit_lines    = night_rail_bm18.app.analizza:main",
                "night_rail_bm18_slits2map    = night_rail_bm18.app.fitpoly2D:main",
                "night_rail_bm18_extract_grid    = night_rail_bm18.app.extract_grid:main",
            ],
        },
        zip_safe=True,
    )


if __name__ == "__main__":
    setup_package()

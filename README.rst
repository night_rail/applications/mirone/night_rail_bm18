night_rail_bm18
===============

Last Installed Environment
~~~~~~~~~~~~~~~~~~~~~~~~~~


The new candidate to stable versions is :

::
   
  source /scisoft/tomotools/activate bm18_24_12_20

alternatively you can use the command ::

  source /scisoft/tomotools/x86_64/cuda11/bm18_24_11_20/bin/activate

The modifications are :
    - distortion map is automatically cropped around the center if it is larger than the radio
    - There is the possibility to modulate the value fractional_thresholding wich triggers the mending of low weight pixels.
      The threshold is relative to teh average weight on each line of the collected wignal. Pixel with smaller weight have their value replaced with the one of the neares valid pixel
    - Improved the cor tracking method.
    - Improved and tested the voxel size calibration method. In principle it is usable

The previous candidate to stable version was ::
  
  source /scisoft/tomotools/x86_64/cuda11/bm18_24_11_20/bin/activate

The modifications are :
   - Center of rotation follow-up along the vertical translation
   -  A bug fix in renomalisation by the current
  
The previous candidate to stable versions was ::
  
  source /scisoft/tomotools/x86_64/cuda11/bm18_24_11_06/bin/activate

The modifications are :
   - A better tuned wobble correction

The previous candidate to stable versions was :

::

  source /scisoft/tomotools/activate bm18_24_10_23

The modifications are :
    - Corrected some remaining failures in the automatic follow-up
    - various acceleration/parallelisations of critically slow remaining python parts ( like a[:] = 0 which takes very long for large arrays )

The previous candidate to stable versions is :

::

  source /scisoft/tomotools/activate bm18_24_09_13


The modifications are :
    - corrected the behaviour of the automatic followup of online when a scan is interrupted ( to be tested).
    - wobble correction ( check box "big sample stage" in the second tab).
    - faster cor finder.
    - skipped z_translation gap mending because bliss files should be OK now.

      
Test version with wobble correction ( based on 24_07_22 plus optional wobble correction ) ::

  source /data/scisofttmp/mirone/environments/bm18_wooble_correction/bin/activate

::  

In the GUI the wobble correction can be activate in the second panel, through its checkbox.


The last stable installation is :

::

  source /scisoft/tomotools/activate bm18_24_07_22


The version 24_07_22 introduces :
   - fast reading and preprocessing, up to 5Gb per second thank to multiprocessing
   - A third gui panel for re-testing:
     - variables can be selected
     - An already existing workflow directory can be choosed and replayed with only the selected variables giving raise to questions.
   - Various improvements in the metadata extractions
   - A new methods for extracting adittive double flats.

::
   
    source /scisoft/tomotools/activate bm18_24_06_19


The version 24_06_19 introduces the online reconstructio of equally spaced slice and, optionally, of the whole volume online 


::
   
   source /scisoft/tomotools/activate bm18_24_06_07

The version 24_06_07 introduces the management of defaults in UI_levels tab of the gui. This is done by dialogues which are prompted in the terminal for which the gui has been launched.

  
::
   
   source /scisoft/tomotools/activate bm18_24_06_04

The version 24_06_04 mends the NaN in the machine current metadata in the bliss file ( coming from pytango)
  
  
Previous installed versions :

::
   
   source /scisoft/tomotools/activate bm18_24_05_30


The version 24_05_30 implements the scintillator light deconvolution in the pipeline 
  

  
::
   
   source /scisoft/tomotools/activate bm18_24_04_24




Installation From Sources
~~~~~~~~~~~~~~~~~~~~~~~~~

First clone the night_rail_bm18 repository
::
   
   git clone  https://gitlab.esrf.fr/night_rail/applications/mirone/night_rail_bm18


then edit *no_surprise_installation_script.sh*, setting the target directory for the to be installed environment, and execute it.


Running night_rail at ESRF
~~~~~~~~~~~~~~~~~~~~~~~~~~

Allocate a resource, if you have access to bm18 ::

  salloc --partition bm18 --ntasks=1  --gres=gpu:1  --cpus-per-task=32 --time=12:00:00 --mem=850G  --nodes=1  --x11    srun   --pty bash  -l


alternatively ::

   salloc --partition gpu-long --ntasks=1  --gres=gpu:1  --cpus-per-task=32 --time=4:00:00 --mem=250G  --nodes=1  --x11    srun   --pty bash  -l


then activate an environment (see above) and run the GUI interface ::

     night_rail_bm18_automatic_gui
  
more documentation on the gui usage to come.

  
Distortion Map Generation
~~~~~~~~~~~~~~~~~~~~~~~~~

  * starting from edf files with horizontal lines and vertical lines ::

      night_rail_bm18_extract_slit_lines --dir /data/bm18/inhouse/PAUL/LAFIP3/distortion_otus85/

    In this directory "horizontal*edf" and "vertical*edf" files are searched.
    In the above example the directory contains ::

         horizontal_0000.edf
         horizontal_0001.edf
         horizontal_0002.edf
         horizontal_0003.edf
         .......

    and  ::
    
         vertical_0000.edf
         vertical_0001.edf
         vertical_0002.edf
         vertical_0003.edf
         .......

    For the horizontal and vertical lines repsectively.
    
    it is supposed that the translation vertical step ( for "horizontal*" )  is equal to the horizontal translation step (for the vertical lines)

    The utility creates a files *crossings.npy* containing the grid of the intersection positions and a file *dect_shape.npy* with the original dimensions, and this in the work directory.

    Remaining in the work directory the map is generated with the command ::

      night_rail_bm18_slits2map

    which generates the distortion map for the original detector. The command has other options that you can see with  ::

     night_rail_bm18_slits2map --help
     usage: night_rail_bm18_slits2map [-h] [--left LEFT] [--right RIGHT] [--bottom BOTTOM] [--top TOP] [--binning BINNING]

     Map creator.

     optional arguments:
      -h, --help         show this help message and exit
      --left LEFT        remove pixels on the left
      --right RIGHT      remove pixels on the right
      --bottom BOTTOM    remove pixels at the bottom
      --top TOP          remove pixels at the top
      --binning BINNING  binning factor

  * Starting from a scan where a small spot change position at each image following a grid with a fast horizontal dimensions ::

      night_rail_bm18_extract_grid  --scan  /data/bm18/inhouse/PAUL/4xOtus/1.04um_4x-optic_distortion_mesh2/1.04um_4x-optic_distortion_mesh2_0001/1.04um_4x-optic_distortion_mesh2_0001.h5  --periodx  56  --safe_margin 10

    The *safe_margin* option is used to discard spot whose maximum is within N pixels from a border. This is done to avoid catching the tail of outer spots.
    The *periodx* argument tells how many images are taken for an horizontal line. use the *help* for a reminder of the arguments

    As with *night_rail_bm18_extract_slit_lines*, the command *night_rail_bm18_slits2map* produces  *crossing.npy*  and *dect_shape.npy*. The final step for the map distortion map creation will be the same.

    
Hands On
~~~~~~~~

   The following documentation is not updated. More to come on the automatic features

.. code-block:: console

   salloc --partition bm18 --ntasks=1  --gres=gpu:1  --cpus-per-task=32 --time=1:00:00 --mem=850G  --nodes=1    srun   --pty bash  -l
   source /scisoft/tomotools/activate bm18_dev
   cd /tmp_14_days/
   mkdir nr_test
   cd nr_test
   night_rail_bm18_setup_pars


This launches the dialog, and the following is a copy of it. Mind that you have, from the readline library, autocompletion, history defaults values. Check the base library night_rail for more details ::
  
  Choose pipeline kind: [N]abu pipeline, [A]spect preliminary phase: 
          :[n]
  Choose wether you start from bliss files or nexus one: from [B]liss files, from [N]exus file: 
          :[b]
  Choose dataset localisation scheme: from [P]ath, from [M]ultiple paths, From multiple path and [R]eference scans: 
          :[m]
  Choose the first bliss dataset of the  series : : 
          /data/visitor/ihes108/bm18/20230503/HA_2050_BS01/HA_2050_BS01_0002_0000/HA_2050_BS01_0002_0000.h5
  Choose the last bliss dataset of the  series : : 
          /data/visitor/ihes108/bm18/20230503/HA_2050_BS01/HA_2050_BS01_0002_0003/HA_2050_BS01_0002_0003.h5
  Choose scheme for reconstructing the whole stack: [S]eparately, [A]fter concatenation: 
          :[a]
  The entry name is in general entry0000 ( default). But you can possibly change it here : (Tab will reset to default value entry0000): 
          entry0000
  Choose detector correction scheme: [N]o correction, from [M]ap file: 
          :[n]
  Choose voxel setting scheme: Voxel size: Use the [B]liss provided value, from [U]ser provided  value, from [A]utomatic estimation by correlations: 
          :[b]
  Choose cor setting scheme: from [U]ser value, from [A]utomatic estimation, from [D]iagnostics: 
          :[a]
  input the near value for the center of rotation(Tab will reset to default value 0.0): 
          2050
  input the width value for the near around the center of rotation(Tab will reset to default value 20): 
          20
  input, in pixel, the size beyond shorter than which signal is filtered(Tab will reset to default value 0.4): 
          0.4
  input, in pixel, the size beyond longer than which signal is filtered(Tab will reset to default value 10): 
          10
  The interval in degree between considered radios(Tab will reset to default value 5): 
          5
  How many lines from each radio are taken(Tab will reset to default value 10): 
          10
  Choose reconstruction: [N]o reconstruction, [H]elical reconstruction: 
          :[h]
  the delta/beta ratio for the phase filter. ZERO to disactivate filtering.(Tab will reset to default value 1000.0): 
          848
  Choose vertical range setting scheme: A slice at a [F]raction of the range,  from slices [I]ndexes, from slice [M]illimiters, [A]ll: 
          :[f]
  Give a fraction between 0.0 and 1.0(Tab will reset to default value 0.5): 
          0.5
  Choose chunk size: [N]o chunk limitation, [C]hunk limited size: 
          :[c]
  Give the max chunk size for Nabu reconstruction: (Tab will reset to default value 100): 
          200
  Give a lenght > 2.0 for the vertical transition at the map borders  (Tab will reset to default value 15.0): 
          100
  Unsharp: [N]o unsharp, [U]nsharp: 
          :[u]
  Unsharp coefficient(Tab will reset to default value 3.0): 
          4.0
  Unsharp sigma(Tab will reset to default value 1.2): 
          1.2
  chose post treatement mylti-phase:  [N]o PagOPag,  [P]agOPag: 
          :[n]
  Give a name for this session ( must be contiguous string)(Tab will reset to default value unnamed_session): 
          testA
  
Now you have, in the current working directory, this file ::

  nrbm18_default.json

unless otherwise specified on the command line (--help for help).
From such structured description of the user choices the workflow is generated
.. code-block:: console

   night_rail_bm18_create_scripts -i nrbm18_default.json

And we have the workflow ::

  -rw-r--r-- 1 mirone soft 10912 Sep  8 16:13 nrbm18_default.json
  -rwxr--r-- 1 mirone soft   602 Sep  8 16:18 testA_000_Bliss2NxMultiZScript.nrs
  -rwxr--r-- 1 mirone soft   702 Sep  8 16:18 testA_001_CompositeCorSimpleScript.nrs
  -rwxr--r-- 1 mirone soft   629 Sep  8 16:18 testA_002_ConcatenateToHeliScanScript.nrs
  -rwxr--r-- 1 mirone soft   409 Sep  8 16:18 testA_003_CreateWeightMapScript.nrs
  -rwxr--r-- 1 mirone soft  1732 Sep  8 16:18 testA_004_CreateNabuFinalConfScript.nrs
  -rwxr--r-- 1 mirone soft   340 Sep  8 16:18 testA_005_NabuFinalRunScript.nrs
  -rw-r--r-- 1 mirone soft   809 Sep  8 16:18 testA.nrd
  -rw-r--r-- 1 mirone soft  1492 Sep  8 16:18 testA.nri
  -rw-r--r-- 1 mirone soft  1411 Sep  8 16:18 testA.nro
  -rw-r--r-- 1 mirone soft  2947 Sep  8 16:18 testA_ewoks_wf.nrw
  -rw-r--r-- 1 mirone soft 10912 Sep  8 16:18 testA_configuration.json
  -rw-r--r-- 1 mirone soft  5339 Sep  8 16:18 testA_aiguillage.py

All what is prefixed by the session name is part of the workflow. Notably all the files with nrs extensions are scripts (bash or python), json keeps track of the user choices ( will be equal to the nrbm18_default.json that we have used), the nrw extension if for an ewoks workflow, the py extension is a machine generated module with useful information for the utility by which on can optionally modify the parameters from the command line. The others three files with extensions nrd, nri, nro are not used so far but contain nonetheless for somebody which might want to use them programmatically, the dependencies, the inputs and the output names for each script.

Now let us run the workflow
.. code-block:: console

   ewoks execute testA_ewoks_wf.nrw  -p"_raise_on_error=true" 
  
Alternatively you can run one by one a selected choice of script.
Let us see instead how to change the parameters of a expanded workflow ( one already created from json) using in the current case
the aiguillage file 


.. code-block:: console
		
    night_rail_bm18_modify_json --aiguillage testA_aiguillage.py \
    --change_par FirstS 0000 0001\
    --change_par LastS 0003 0001\
    --change_par Delta 800\
    --change_par Session testB \
    --target_file new_wf.json

Remember the '--help' argument for information about the argument.
Now the obtained workflow description can be expanded to obtain a workflow named TestB with modified parameters.
To come an utility to activate or desactivate a part of the ewoks workflow.


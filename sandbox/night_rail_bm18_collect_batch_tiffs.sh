#!/bin/bash

source_dir=$1
target_dir=$2
mkdir -p $target_dir
for resdir in ${source_dir}/batch_dir/batch_*_*_online_file ; do
    lista=$(find $resdir -maxdepth 1 -name "*.tiff")
    for tifffile in $lista ; do
	base_tifffile=$(basename $tifffile)
        shortname=$(echo ${base_tifffile} | sed -E 's/^[^0-9]*([0-9]+_){2}//')
	echo " $tifffile =======>>>  ${target_dir}/$shortname"
        cp $tifffile ${target_dir}/$shortname
    done
done    

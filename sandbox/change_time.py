from h5py import *
import sys
import datetime
import dateutil.parser
from dateutil.parser import parser as TimeParser
import pytz 


time_parser = TimeParser()


f=File(sys.argv[1],"r+")

print(  f["5.1/end_time"][()].decode() )

t=(time_parser.parse(f["5.1/end_time"][()].decode()))



s =  t.timestamp() -1

date = datetime.datetime.fromtimestamp(s,tz=pytz.timezone('Europe/Rome'))

s = date.isoformat()

del f["5.1/end_time"]
f["5.1/end_time"] = s

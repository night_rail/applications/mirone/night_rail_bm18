import os
import fnmatch
import shutil

raw_dir="/data/visitor/md1389/bm18/20240202/RAW_DATA/"
fragment = "HA1150_20.022um_K336_kidney_complete-organ"
raw_target_dir="/tmp_14_days/mirone/test_online/RAW_DATA"


for source_dir, dirs, files in os.walk(raw_dir):
    
    if("scan" in os.path.basename(source_dir) ):
        continue

    dir_name = os.path.basename(source_dir)

    if not fragment in dir_name:
        continue

    target_dir = source_dir[len(raw_dir):]

    target_dir =  os.path.join( raw_target_dir, target_dir   )
    
    os.makedirs( target_dir, exist_ok = True  )
    
    for tok in fnmatch.filter(files, "*"):
        print( "................" + tok)

        target =  os.path.join( target_dir,  tok    )
        source = os.path.join( source_dir,  tok    )
        print(" Copierei ", source , " su ", target )

        shutil.copy(   source ,  target   )
        
    for tok in dirs :
        print(tok)
        if tok[:4]=="scan":
            source = os.path.join( source_dir,  tok    )
            target =  os.path.join( target_dir,  tok    )
            print(" LINKEREI ", source, " SU ", target)
            os.symlink(source, target)
    # print("")

import numpy as np
import glob
import pickle
from scipy.optimize import curve_fit

# the ones below come from a fit with sinus
alto = [7,6.97, 6.97, 6.88, 7.02, 6.93, 6.79, 6.85]
basso = [1.45, 1.42, 1.439, 1.39, 1.4363, 1.374, 1.4616, 1.413, 1.393]


z_high = 2300.0
z_low  = 133.0

amp_alto = np.array(alto).mean()
amp_basso = np.array(basso).mean()
print(amp_alto, amp_basso)

fl=glob.glob("?.txt")
print(fl)

media = 0
pixel_size = 0.471
for fn in fl:
    media += np.loadtxt(fn)[:,0] *pixel_size
    
media = media / len(fl)


if True:
    
    def cosine_fit(i, amplitude, phase, offset):
        N = len(media)
        return offset + amplitude * np.sin(3 * i * 2 * np.pi / N + phase)

    # Crea l'array degli indici i
    i = np.arange(len(media))

    # Esegui il fit non lineare usando curve_fit
    popt, _ = curve_fit(cosine_fit, i, media , p0=[np.max(media), 0, 0.0 ])

    # Ottieni i valori ottimali per l'ampiezza e la fase
    amplitude_opt, phase_opt, offset = popt

    print(" FIT  wobble", " AMPLITUDE ", amplitude_opt, " PHASE ", phase_opt, " OFFSET ", offset ) 

    # Calcola il risultato del fit
    x_fitted = cosine_fit(i, amplitude_opt, phase_opt, offset )


    media = x_fitted

shift_high = media
shift_low = media *  amp_basso / amp_alto

wobble_infos = {
    "z_high" : z_high,
    "z_low" : z_low,
    "shift_high" : shift_high,
    "shift_low" : shift_low
}


with open("wobble_infos.pkl", "wb") as f:
    pickle.dump(wobble_infos, f)
np.savetxt("res.txt", np.array([shift_high, shift_low]).T)

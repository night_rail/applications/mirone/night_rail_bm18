import h5py
import numpy as np
import sys
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit



def find_pyramid_tip(image_array):
    # Verifica che l'immagine sia nel formato corretto (float32 2D)
    if image_array.dtype != np.float32:
        raise ValueError("L'immagine deve essere di tipo float32.")
    
    if len(image_array.shape) != 2:
        raise ValueError("L'immagine deve essere 2D.")

    # Normalizzazione dell'immagine tra 0 e 1
    min_val = np.min(image_array)
    max_val = np.max(image_array)
    if max_val > min_val:  # Evita divisione per zero
        normalized_img = (image_array - min_val) / (max_val - min_val)
    else:
        normalized_img = np.zeros_like(image_array)  # Immagine uniforme se max == min

    # Applicazione del filtro Gaussiano direttamente sull'immagine normalizzata (ancora float32)
    blurred = cv2.GaussianBlur(normalized_img, (5,5), 0)

    # Conversione a uint8 dopo il filtro Gaussiano
    img_uint8 = (blurred * 255).astype(np.uint8)

    # Rilevamento dei bordi con l'algoritmo di Canny
    edges = cv2.Canny(img_uint8, 5, 60)


    
    # Trova le linee nell'immagine usando la trasformata di Hough
    lines = cv2.HoughLinesP(edges, 1, np.pi / 180, threshold=30, minLineLength=30, maxLineGap=2)



    
    # Creiamo un'immagine di output per visualizzare le linee
    line_img = np.copy(img_uint8) * 0  # Immagine nera per tracciare le linee

    if lines is not None:
        # Inizializza una lista per memorizzare i punti delle linee
        points = []

        for line in lines:
            for x1, y1, x2, y2 in line:
                # Disegna le linee trovate
                cv2.line(line_img, (x1, y1), (x2, y2), (255, 0, 0), 2)
                points.append((x1, y1))
                points.append((x2, y2))

        # Trova il punto di convergenza (la punta della piramide)
        points = np.array(points)
        # Calcola il baricentro dei punti delle linee
        tip_x = int(np.mean(points[:, 0]))
        tip_y = int(np.mean(points[:, 1]))

        # Disegna il punto della punta della piramide sull'immagine
        cv2.circle(line_img, (tip_x, tip_y), 10, (255, 255, 255), -1)

        # Visualizza l'immagine con le linee e la punta della piramide
        plt.figure(figsize=(10, 10))
        plt.subplot(1, 2, 1)
        plt.imshow(edges, cmap='gray')
        plt.title('Bordi rilevati')

        plt.subplot(1, 2, 2)
        plt.imshow(line_img, cmap='gray')
        plt.title('Punta della piramide rilevata')

        plt.show()

        return tip_x, tip_y
    else:
        print("Nessuna linea rilevata.")
        return None




etf_dir = sys.argv[1]

dark = h5py.File(os.path.join(etf_dir,"dark.h5"),"r")["data"][()]
flat = h5py.File(os.path.join(etf_dir,"flats.h5"),"r")["data"][0]
flat_current = h5py.File(os.path.join(etf_dir,"flats.h5"),"r")["framewise/flats_currents"][0]

print(" flat_current ", flat_current ) 

stack=[]

radio_raw = h5py.File(os.path.join(etf_dir,"projections.h5"),"r")["data"][()]
radio_current = h5py.File(os.path.join(etf_dir,"projections.h5"),"r")["framewise/control"][()]


xpos = []
for i, (radio, current)  in enumerate(zip(radio_raw,radio_current  ) ):

    # print(i)
    radio = ( radio[1000:-1000,1380:3600] - dark[1000:-1000,1380:3600]      )/(  flat[1000:-1000,1380:3600] - dark[1000:-1000,1380:3600]   )

    radio = -np.log(radio)
    # radio = radio - radio[:700].mean()

    xs = np.arange(radio.shape[1])
    
    # radio = (radio - radio.min())/( radio.max() - radio.min() ) * flat_current / current
    
    # x,y = find_pyramid_tip( radio.astype("f") )

    # radio[ y-2:y+3,x-2:x+3] = radio.max()
    xpos.append( ( radio[:]*xs ).sum() / radio[:].sum()   )

    stack.append(radio)

h5py.File("flatted_radios.h5","w")["data"] =stack
h5py.File("flatted_radios.h5","r+")["xpos"] =xpos




x_smoothed = np.array(xpos ) 

# Definisci la funzione di fitting: a * cos(i * 2 * pi / N + fase)
def cosine_fit(i, amplitude, phase, offset):
    N = len(x_smoothed)
    return offset + amplitude * np.cos(i * 2 * np.pi / N + phase)

# Crea l'array degli indici i
i = np.arange(len(x_smoothed))

# Esegui il fit non lineare usando curve_fit
popt, _ = curve_fit(cosine_fit, i, x_smoothed, p0=[np.max(x_smoothed), 0, 0.0 ])

# Ottieni i valori ottimali per l'ampiezza e la fase
amplitude_opt, phase_opt, offset = popt

print(" FIT ", " AMPLITUDE ", amplitude_opt, " PHASE ", phase_opt, " OFFSET ", offset ) 

# Calcola il risultato del fit
x_fitted = cosine_fit(i, amplitude_opt, phase_opt, offset )


res = np.array([ x_smoothed, x_fitted , x_smoothed - x_fitted ]).T

np.savetxt(f"alignement_x.txt",  res)

###########################################


error = x_smoothed - x_fitted

# Definisci la funzione di fitting: a * cos(i * 2 * pi / N + fase)
def cosine_fit(i, amplitude, phase, offset):
    N = len(x_smoothed)
    return offset + amplitude * np.sin(3 * i * 2 * np.pi / N + phase)

# Crea l'array degli indici i
i = np.arange(len(x_smoothed))

# Esegui il fit non lineare usando curve_fit
popt, _ = curve_fit(cosine_fit, i, error , p0=[np.max(x_smoothed), 0, 0.0 ])

# Ottieni i valori ottimali per l'ampiezza e la fase
amplitude_opt, phase_opt, offset = popt

print(" FIT  wobble", " AMPLITUDE ", amplitude_opt, " PHASE ", phase_opt, " OFFSET ", offset ) 

# Calcola il risultato del fit
x_fitted = cosine_fit(i, amplitude_opt, phase_opt, offset )


res = np.array([ error, x_fitted , error - x_fitted ]).T

np.savetxt(f"wobble_fit.txt",  res)







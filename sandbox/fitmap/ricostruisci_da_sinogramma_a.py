import json
import os
import sys
import h5py
import numpy as np
from scipy import sparse


from pycuda.elementwise import ElementwiseKernel
from nabu.cuda.utils import get_cuda_context
from nabu.reconstruction.projection import Projector
from nabu.reconstruction.fbp import Backprojector
from nabu.app.create_distortion_map_from_poly import create_maps_x_and_z


def main():
    conf = json.load(open(sys.argv[1]))
    print(conf)
    if "cor_data_file" in conf:
        rotation_axis_position = json.load(open(conf["cor_data_file"], "r"))["rotation_axis_position"]
    elif "rotation_axis_position" in conf:
        rotation_axis_position = float(conf["rotation_axis_position"])
    else:
        raise RuntimeError("Failed to retrieve the informations about center of rotation")

    n_stripes = conf["n_stripes"]

    data_stripes = []
    starts_z = []
    ends_z = []

    total_shape = conf["radio_height"], conf["radio_width"]

    recs = []
    for i_stripe in list(map(str, range(n_stripes))):
        if i_stripe != "2":
            continue

        starts_z.append(conf[i_stripe]["start_z"])
        ends_z.append(conf[i_stripe]["end_z"])
        sino_file = conf[i_stripe]["sino_file"]

        entry = "entry0000"
        process_name = "sinogram"
        h5_path = os.path.join(entry, *[process_name, "results", "data"])

        print(" Reading ", sino_file)
        data_stripes.append(h5py.File(sino_file, "r")[h5_path][()])

    mapper = MapParametersToRec(total_shape, starts_z, ends_z, data_stripes, rotation_axis_position)

    recsss = []
    sinos = []
    paramsss = []
    slice_shape = None
    c2 = np.linspace(-0.002, 0.002, 11 )[3]
    for c2 in  np.linspace(-0.005, 0.005, 11 ):

        recss = []
        paramss = []

        for c4 in  np.linspace(-0.002, 0.002, 5 )[2:3]:
            
            recs = []
            params = []

            for DX in  np.linspace(-200,  200, 11 )  :

                pp = [c2, c4, DX]
                
                print(pp)
                
                sinograms, new_rot_pos = mapper.get_new_sinograms_and_new_rot(
                   DX +  conf["radio_width"] / 2, conf["radio_height"] / 2, c2, c4
                )
        
                for sino in sinograms:
                    # sinos.append(sino)
        
                    n_angles = sino.shape[0]
    
                    print(n_angles)
                    angles = np.arange(n_angles) * 2 * np.pi / n_angles
        
                    B = Backprojector(
                        sino.shape,
                        slice_shape = slice_shape,
                        rot_center=new_rot_pos,
                        angles=angles,
                        filter_name="ramlak",
                        halftomo=True,
                        padding_mode="edge",
                        extra_options={"centered_axis": True, "clip_outer_circle": True},
                    )
        
                    rec = B.filtered_backprojection(sino)
                    slice_shape = rec.shape
                    mosaic = np.zeros([2*700,2*700],"f")
    
                    for (i,j),(X,Y) in { (0,0):(2000,4000), (0,1):( 3626,2500), (1,0):(2600,1000), (1,1):(1200,3100) }.items():
                        print ( (i,j),(X,Y)  ) 
                        mosaic[i*700:(i+1)*700,j*700:(j+1)*700] = rec[ Y:Y+700, X:X+700    ]
    
                        
                    recs.append(mosaic)                    
                    params.append( pp )
                    
            recss.append(recs)
            paramss.append( params )
                    
        recsss.append(recss)
        paramsss.append(paramss)
                # P = Projector(rec.shape, angles, rot_center=new_rot_pos, detector_width=sino.shape[1])
                # sinos.append(P(rec))
                # print(angles)

    print(" finished")
    f = h5py.File("recs.h5", "w")
    f["rec"] = np.array(recsss)
    f["pps"] = np.array(paramsss)
    # f["sinos"] = sinos


class MapParametersToRec:
    def __init__(self, total_shape, starts_z, ends_z, data_stripes, rot_pos):
        self.total_shape = total_shape
        self.starts_z = starts_z
        self.ends_z = ends_z
        self.data_stripes = data_stripes
        self.rot_pos = rot_pos

    def get_new_sinograms_and_new_rot(self, center_x, center_z, c2, c4):
        map_x, map_z, new_rot_pos = self.get_mapsxz_and_new_rot_pos(center_x, center_z, c2, c4)

        sinograms = []

        for stripe, s_z, e_z in zip(self.data_stripes, self.starts_z, self.ends_z):
            line_map_x = map_x[(s_z + e_z - 1) // 2]
            line_map_z = map_z[(s_z + e_z - 1) // 2] - s_z

            csr_sparse_matrix = self.build_full_transformation(line_map_x, line_map_z, s_z, e_z)

            new_sino = np.zeros([stripe.shape[0], stripe.shape[2]], "f")

            for new_line, old_image in zip(new_sino, stripe):
                new_line[:] = csr_sparse_matrix @ (old_image.flat)

            sinograms.append(new_sino)

        return sinograms, new_rot_pos

    def build_full_transformation(self, line_map_x, line_map_z, stripe_s_z, stripe_e_z):
        """ """
        stripe_shape_vh = (stripe_e_z - stripe_s_z), self.total_shape[1]

        coordinates = np.array([line_map_z, line_map_x])

        # padding
        sz, sx = stripe_shape_vh

        xs = np.clip(np.array(coordinates[1].flat), [0], [sx - 1])
        zs = np.clip(np.array(coordinates[0].flat), [0], [sz - 1])

        ix0s = np.floor(xs)
        ix1s = np.ceil(xs)
        fx = xs - ix0s

        iz0s = np.floor(zs)
        iz1s = np.ceil(zs)
        fz = zs - iz0s

        I_tmp = np.empty([4 * sx], np.int64)
        J_tmp = np.empty([4 * sx], np.int64)
        V_tmp = np.ones([4 * sx], "f")

        I_tmp[:] = np.arange(sx * 4) // 4

        J_tmp[0::4] = iz0s * sx + ix0s
        J_tmp[1::4] = iz0s * sx + ix1s
        J_tmp[2::4] = iz1s * sx + ix0s
        J_tmp[3::4] = iz1s * sx + ix1s

        V_tmp[0::4] = (1 - fz) * (1 - fx)
        V_tmp[1::4] = (1 - fz) * fx
        V_tmp[2::4] = fz * (1 - fx)
        V_tmp[3::4] = fz * fx

        coo_tmp = sparse.coo_matrix((V_tmp.astype("f"), (I_tmp, J_tmp)), shape=(sx, sz * sx))

        csr_sparse_matrix = coo_tmp.tocsr()

        return csr_sparse_matrix

    def get_mapsxz_and_new_rot_pos(self, center_x, center_z, c2, c4):
        args = dict(
            center_x=center_x,
            center_z=center_z,
            c2=c2,
            c4=c4,
            nz=self.total_shape[0],
            nx=self.total_shape[1],
            axis_pos=self.rot_pos,
        )

        map_x, map_z, new_rot_pos = create_maps_x_and_z(args)
        return map_x, map_z, new_rot_pos


main()

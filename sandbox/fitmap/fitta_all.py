import json
import os
import sys
import h5py
import numpy as np
from scipy import sparse
from scipy.ndimage import gaussian_filter
from scipy.optimize import minimize



from pycuda.elementwise import ElementwiseKernel
from nabu.cuda.utils import get_cuda_context
from nabu.reconstruction.projection import Projector
from nabu.reconstruction.fbp import Backprojector
from nabu.app.create_distortion_map_from_poly import create_maps_x_and_z


def main():


    n_stripes = len(sys.argv) -1
    data_stripes = []
    starts_z = []
    ends_z = []
    rots = []
    total_shapes = []

    for cc, i_stripe in zip(sys.argv[1::2], sys.argv[2::2]):

        i_stripe = i_stripe
        
        conf = json.load(open(cc))
        print(conf)
    
        if "cor_data_file" in conf:
            rotation_axis_position = json.load(open(os.path.join( os.path.dirname(  cc     ) ,conf["cor_data_file"]), "r"))["rotation_axis_position"]
        elif "rotation_axis_position" in conf:
            rotation_axis_position = float(conf["rotation_axis_position"])
        else:
            raise RuntimeError("Failed to retrieve the informations about center of rotation")
    
        rots.append(rotation_axis_position)

        total_shapes.append(  (conf["radio_height"], conf["radio_width"]) )


        starts_z.append(conf[i_stripe]["start_z"])
        ends_z.append(conf[i_stripe]["end_z"])
        sino_file = conf[i_stripe]["sino_file"]


        sino_file = os.path.join( os.path.dirname(  cc     ) , sino_file ) 
        
        entry = "entry0000"
        process_name = "sinogram"
        h5_path = os.path.join(entry, *[process_name, "results", "data"])

        data_stripes.append(h5py.File(sino_file, "r")[h5_path][()])

    mapper = MapParametersToRec(total_shapes, starts_z, ends_z, data_stripes, rots)


    #free_variables = {"dx":None, "dz":0, "drx":None, "drz":0, "c2":None, "c4":None}
    
    free_variables = {"dx":None, "dz":None, "drx":0,   "c2":None,"c3":0,  "c4":None,  "c5":0,  "c6":0}
    bounds =  ( (-3000,3000),)*2 +  ( (-0.016, 0.016), )*2   # , (-0.016, 0.016), (-0.016, 0.016), (-0.016, 0.016), (-0.016, 0.016)   )

    
    loss_function = Loss( mapper , free_variables = free_variables, bounds = bounds)

    x0 = 235.43732994298423, -205.345716318568, 0.00035402431391774963,  0.0023802432378675093
    
    X0 = []
    for xx, bb in zip(x0, bounds):
        X0.append(  ( xx - np.array(bb).mean()   )/ ( (bb[1] - bb[0])/2   ) /600  )
    
    res = minimize(loss_function, X0 , bounds = [(-1,1)]*len(bounds) ,method='Nelder-Mead')

    print( res.x ) 
    
                
class MapParametersToRec:
    def __init__(self, total_shapes, starts_z, ends_z, data_stripes, rots_pos):
        self.total_shapes = total_shapes
        self.starts_z = starts_z
        self.ends_z = ends_z
        self.data_stripes = data_stripes
        self.rots_pos = rots_pos
        
        
    def get_new_sinograms_and_new_rot(self, d_center_x, d_center_z, c2, c3, c4, c5, c6):

        center_x = self.total_shapes[0][1] / 2 + d_center_x
        center_z = self.total_shapes[0][0] / 2 + d_center_z

        new_rots = []


        for rr in self.rots_pos:
            map_x, map_z, nr = self.get_mapsxz_and_new_rot_pos(rr, center_x, center_z, c2, c3, c4, c5, c6)
            new_rots.append(nr)
            
        sinograms = []

        for stripe, s_z, e_z in zip(self.data_stripes, self.starts_z, self.ends_z):
            line_map_x = map_x[(s_z + e_z - 1) // 2]
            line_map_z = map_z[(s_z + e_z - 1) // 2] - s_z

            csr_sparse_matrix = self.build_full_transformation(line_map_x, line_map_z, s_z, e_z)

            new_sino = np.zeros([stripe.shape[0], stripe.shape[2]], "f")

            for new_line, old_image in zip(new_sino, stripe):
                new_line[:] = csr_sparse_matrix @ (old_image.flat)

            sinograms.append(new_sino)

        return sinograms, new_rots

    def build_full_transformation(self, line_map_x, line_map_z, stripe_s_z, stripe_e_z):
        """ """
        stripe_shape_vh = (stripe_e_z - stripe_s_z), self.total_shapes[0][1]

        coordinates = np.array([line_map_z, line_map_x])

        # padding
        sz, sx = stripe_shape_vh

        xs = np.clip(np.array(coordinates[1].flat), [0], [sx - 1])
        zs = np.clip(np.array(coordinates[0].flat), [0], [sz - 1])

        ix0s = np.floor(xs)
        ix1s = np.ceil(xs)
        fx = xs - ix0s

        iz0s = np.floor(zs)
        iz1s = np.ceil(zs)
        fz = zs - iz0s

        I_tmp = np.empty([4 * sx], np.int64)
        J_tmp = np.empty([4 * sx], np.int64)
        V_tmp = np.ones([4 * sx], "f")

        I_tmp[:] = np.arange(sx * 4) // 4

        J_tmp[0::4] = iz0s * sx + ix0s
        J_tmp[1::4] = iz0s * sx + ix1s
        J_tmp[2::4] = iz1s * sx + ix0s
        J_tmp[3::4] = iz1s * sx + ix1s

        V_tmp[0::4] = (1 - fz) * (1 - fx)
        V_tmp[1::4] = (1 - fz) * fx
        V_tmp[2::4] = fz * (1 - fx)
        V_tmp[3::4] = fz * fx

        coo_tmp = sparse.coo_matrix((V_tmp.astype("f"), (I_tmp, J_tmp)), shape=(sx, sz * sx))

        csr_sparse_matrix = coo_tmp.tocsr()

        return csr_sparse_matrix

    def get_mapsxz_and_new_rot_pos(self, rr, center_x, center_z, c2, c3, c4, c5, c6):
        args = dict(
            center_x=center_x,
            center_z=center_z,
            c2=c2,
            c3=c3,
            c4=c4,
            c5=c5,
            c6=c6,
            nz=self.total_shapes[0][0],
            nx=self.total_shapes[0][1],
            axis_pos=rr,
        )

        print(" ARGS ", args)
        map_x, map_z, new_rot_pos = create_maps_x_and_z(args)
        with h5py.File(f"""map_{args["nz"]}_{args["nx"]}.h5""", "w") as f:
            f["coords_source_x"] = map_x
            f["coords_source_z"] = map_z

        return map_x, map_z, new_rot_pos


class Loss:
    def __init__(self, mapper, free_variables = {"dx":0, "dz":0,  "c2":None,"c3":None,"c4":None,"c5":None, "c6":None}, bounds = None):
        self.mapper = mapper
        self.free_variables = free_variables
        self.slice_shape = None
        self.bounds = bounds
    def __call__(self,X):
        
        i = 0
        
        for var_name in self.free_variables.keys():
            
            if self.free_variables[var_name] is None:
                x = X[i]
                l,h = self.bounds[i]
                x = (l+h)/2 + 600*x*(h-l)/2

                print (" for ", var_name,"  ", x, "  in ", l,h)
                setattr(self, var_name, x)
                i+=1
            else:
                setattr(self, var_name, self.free_variables[var_name])
                

        sinograms, new_rots = self.mapper.get_new_sinograms_and_new_rot(
            self.dx,
            self.dz,
            self.c2,
            self.c3,
            self.c4,
            self.c5,
            self.c6,
        )


        res = 0.0
        all = []
        for iset , (sino, rr) in enumerate(zip(sinograms, new_rots )) :
            n_angles = sino.shape[0]
            angles = np.arange(n_angles) * 2 * np.pi / n_angles
            print(" back")
            B = Backprojector(
                sino.shape,
                slice_shape = self.slice_shape,
                rot_center=  rr  + self.drx ,
                angles=angles,
                filter_name="ramlak",
                halftomo=True,
                padding_mode="edge",
                extra_options={"centered_axis": True, "clip_outer_circle": False},
            )

            rec = B.filtered_backprojection(sino)

            all.append(rec)
        
            if self.slice_shape is None:
                self.slice_shape = rec.shape
                R = (self.slice_shape[0]-1)/2
                coords = np.arange( self.slice_shape[0] ) -R
                coords2 = coords*coords
                rs = np.sqrt(   coords2 + coords2[:, None] )
                dump = np.maximum( 0 , (rs+40 - R)/10 )
                self.dump = np.exp( - dump * dump ).astype("f")

            P = Projector(
                rec.shape,
                angles,
                rot_center=rr,
                detector_width=sino.shape[1]
            )
            print(" back")

            pbp_sino = P( rec * self.dump )
            print(" OK")
            sigma = [0,5]
            diff = gaussian_filter((sino -  gaussian_filter(sino, sigma)) - (pbp_sino -  gaussian_filter(pbp_sino, sigma)), 0)
            diff = diff.astype("d")
            add = (diff*diff).sum()
            res += add
            print(" add ", add )


        h5py.File("all.h5", "w")["all"] = np.array(all)
        print( self.dx, self.dz,   self.c2, self.c3, self.c4, self.c5, self.c6 , " ==>   " , res)

        return res


    
main()

import json
import os
import sys
import h5py
import numpy as np
from scipy import sparse
from scipy.ndimage import gaussian_filter
from scipy.optimize import minimize



from pycuda.elementwise import ElementwiseKernel
from nabu.cuda.utils import get_cuda_context
from nabu.reconstruction.projection import Projector
from nabu.reconstruction.fbp import Backprojector
from nabu.app.create_distortion_map_from_poly import create_maps_x_and_z


def main():
    conf = json.load(open(sys.argv[1]))
    print(conf)
    if "cor_data_file" in conf:
        rotation_axis_position = json.load(open(conf["cor_data_file"], "r"))["rotation_axis_position"]
    elif "rotation_axis_position" in conf:
        rotation_axis_position = float(conf["rotation_axis_position"])
    else:
        raise RuntimeError("Failed to retrieve the informations about center of rotation")

    n_stripes = conf["n_stripes"]

    data_stripes = []
    starts_z = []
    ends_z = []

    total_shape = conf["radio_height"], conf["radio_width"]

    recs = []
    for i_stripe in list(map(str, range(n_stripes))):
        if i_stripe != "2":
             continue

        starts_z.append(conf[i_stripe]["start_z"])
        ends_z.append(conf[i_stripe]["end_z"])
        sino_file = conf[i_stripe]["sino_file"]

        entry = "entry0000"
        process_name = "sinogram"
        h5_path = os.path.join(entry, *[process_name, "results", "data"])

        print(" Reading ", sino_file)
        data_stripes.append(h5py.File(sino_file, "r")[h5_path][()])

    mapper = MapParametersToRec(total_shape, starts_z, ends_z, data_stripes, rotation_axis_position)


    free_variables = {"dx":None, "dz":0, "drx":None, "drz":0, "c2":None, "c4":None}
    bounds = (( -2000, 2000) , (-5 , 5), (-0.006, 0.006), (-0.006, 0.006)   )

    
    loss_function = Loss( mapper , free_variables = free_variables, bounds = bounds)
    f=0
    x0 = (0,)*4
    ## x0 =  ( 343.7225682572873, -0.20886285188531073, -0.005896103664742856, 0.0033351834308964785 )
    ## x0 =  ( 260.6525394885296 , -0.15729100746233216 , -0.0021980125145716865,  0.002 )
    x0 =  ( 254.49268047372607 , -0.1965901666665284 , -0.002030844718897185 , 1.8228414885308996e-06  )
    ## x0 = (-349.5602579911802 , -0.23666878880445696 , -0.0018947898970143658 , 0.0011693529795456894  )
    ## x0 = ( 196.28060393493558 , -0.17568624188395926, -0.0017942675398650083, -1.4979329632405966e-06 )

    X0 = []
    for xx, bb in zip(x0, bounds):
        X0.append(  ( xx - np.array(bb).mean()   )/ ( (bb[1] - bb[0])/2   ) /100  )
    
    res = minimize(loss_function, X0 , bounds = [(-1,1)]*len(bounds) ,method='Nelder-Mead')

    print( res.x ) 
    
                
class MapParametersToRec:
    def __init__(self, total_shape, starts_z, ends_z, data_stripes, rot_pos):
        self.total_shape = total_shape
        self.starts_z = starts_z
        self.ends_z = ends_z
        self.data_stripes = data_stripes
        self.rot_pos = rot_pos

    def get_new_sinograms_and_new_rot(self, d_center_x, d_center_z, c2, c4):

        center_x = self.total_shape[1] / 2 + d_center_x
        center_z = self.total_shape[0] / 2 + d_center_z
        
        map_x, map_z, new_rot_pos = self.get_mapsxz_and_new_rot_pos(center_x, center_z, c2, c4)

        sinograms = []

        for stripe, s_z, e_z in zip(self.data_stripes, self.starts_z, self.ends_z):
            line_map_x = map_x[(s_z + e_z - 1) // 2]
            line_map_z = map_z[(s_z + e_z - 1) // 2] - s_z

            csr_sparse_matrix = self.build_full_transformation(line_map_x, line_map_z, s_z, e_z)

            new_sino = np.zeros([stripe.shape[0], stripe.shape[2]], "f")

            for new_line, old_image in zip(new_sino, stripe):
                new_line[:] = csr_sparse_matrix @ (old_image.flat)

            sinograms.append(new_sino)

        return sinograms, new_rot_pos

    def build_full_transformation(self, line_map_x, line_map_z, stripe_s_z, stripe_e_z):
        """ """
        stripe_shape_vh = (stripe_e_z - stripe_s_z), self.total_shape[1]

        coordinates = np.array([line_map_z, line_map_x])

        # padding
        sz, sx = stripe_shape_vh

        xs = np.clip(np.array(coordinates[1].flat), [0], [sx - 1])
        zs = np.clip(np.array(coordinates[0].flat), [0], [sz - 1])

        ix0s = np.floor(xs)
        ix1s = np.ceil(xs)
        fx = xs - ix0s

        iz0s = np.floor(zs)
        iz1s = np.ceil(zs)
        fz = zs - iz0s

        I_tmp = np.empty([4 * sx], np.int64)
        J_tmp = np.empty([4 * sx], np.int64)
        V_tmp = np.ones([4 * sx], "f")

        I_tmp[:] = np.arange(sx * 4) // 4

        J_tmp[0::4] = iz0s * sx + ix0s
        J_tmp[1::4] = iz0s * sx + ix1s
        J_tmp[2::4] = iz1s * sx + ix0s
        J_tmp[3::4] = iz1s * sx + ix1s

        V_tmp[0::4] = (1 - fz) * (1 - fx)
        V_tmp[1::4] = (1 - fz) * fx
        V_tmp[2::4] = fz * (1 - fx)
        V_tmp[3::4] = fz * fx

        coo_tmp = sparse.coo_matrix((V_tmp.astype("f"), (I_tmp, J_tmp)), shape=(sx, sz * sx))

        csr_sparse_matrix = coo_tmp.tocsr()

        return csr_sparse_matrix

    def get_mapsxz_and_new_rot_pos(self, center_x, center_z, c2, c4):
        args = dict(
            center_x=center_x,
            center_z=center_z,
            c2=c2,
            c4=c4,
            nz=self.total_shape[0],
            nx=self.total_shape[1],
            axis_pos=self.rot_pos,
        )

        map_x, map_z, new_rot_pos = create_maps_x_and_z(args)
        return map_x, map_z, new_rot_pos


class Loss:
    def __init__(self, mapper, free_variables = {"dx":0, "dz":0, "drx":0, "drz":0, "c2":None, "c4":None}, bounds = None):
        self.mapper = mapper
        self.free_variables = free_variables
        self.slice_shape = None
        self.bounds = bounds
    def __call__(self,X):
        
        i = 0
        
        for var_name in self.free_variables.keys():
            
            if self.free_variables[var_name] is None:
                x = X[i]
                l,h = self.bounds[i]
                x = (l+h)/2 + 100*x*(h-l)/2

                print (" for ", var_name,"  ", x, "  in ", l,h)
                setattr(self, var_name, x)
                i+=1
            else:
                setattr(self, var_name, self.free_variables[var_name])
                

        sinograms, new_rot_pos = self.mapper.get_new_sinograms_and_new_rot(
            self.dx,
            self.dz,
            self.c2,
            self.c4
        )

        res = 0.0
        for sino in sinograms:
            n_angles = sino.shape[0]
            angles = np.arange(n_angles) * 2 * np.pi / n_angles
            print(" back")
            B = Backprojector(
                sino.shape,
                slice_shape = self.slice_shape,
                rot_center=new_rot_pos + self.drx,
                angles=angles,
                filter_name="ramlak",
                halftomo=True,
                padding_mode="edge",
                extra_options={"centered_axis": True, "clip_outer_circle": False},
            )
    
            rec = B.filtered_backprojection(sino)
            h5py.File("slice.h5","w")["slice"] = rec

            
            if self.slice_shape is None:
                self.slice_shape = rec.shape
                R = (self.slice_shape[0]-1)/2
                coords = np.arange( self.slice_shape[0] ) -R
                coords2 = coords*coords
                rs = np.sqrt(   coords2 + coords2[:, None] )
                dump = np.maximum( 0 , (rs+40 - R)/10 )
                self.dump = np.exp( - dump * dump ).astype("f")

            P = Projector(
                rec.shape,
                angles,
                rot_center=new_rot_pos + self.drx,
                detector_width=sino.shape[1]
            )
            print(" back")

            pbp_sino = P( rec * self.dump )
            print(" OK")
            sigma = 5.0
            diff = (sino -  gaussian_filter(sino, sigma)) - (pbp_sino -  gaussian_filter(pbp_sino, sigma))
            diff = diff.astype("d")
            add = (diff*diff).sum()
            res += add

            print(" add ", add )
        print( self.dx, self.dz, self.drx, self.drz, self.c2, self.c4 , " ==>   " , res)
        return res


    
main()

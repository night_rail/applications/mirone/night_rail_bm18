#!/bin/bash
# set -e


TAG=25_03_10

# INST_DIR=/scisoft/tomotools/x86_64/cuda11
# INST_DIR=/tmp_14_days/mirone/
INST_DIR=/data/scisofttmp/mirone/environments
ENV_DIR=${INST_DIR}/bm18_$TAG

# ENV_DIR=${INST_DIR}/venv_memory_limit

python3 -m venv $ENV_DIR

sed -i '3s/^/module load cuda\/11 \n/' $ENV_DIR/bin/activate

sed -i '3s/^/export SKIP_TOMOSCAN_CHECK=\"1\"  \n/' $ENV_DIR/bin/activate
sed -i '3s/^/export TOMOTOOLS_SKIP_DET_CHECK=\"1\"  \n/' $ENV_DIR/bin/activate

source $ENV_DIR/bin/activate
pip install pip --upgrade

pip install -r freeze_no_surprise.txt

# pip install pyopencl
# python3 -m pip install git+https://github.com/lebedov/scikit-cuda.git


# git clone https://gitlab.esrf.fr/mirone/nabu nabu_tmp
# cd nabu_tmp
# git checkout $TAG
# pip install . --no-deps
# cd ..

python3 -m pip install git+https://gitlab.esrf.fr/mirone/nabu@${TAG} --no-deps --force

python3 -m pip install git+https://gitlab.esrf.fr/mirone/nxtomomill@${TAG} --no-deps --force
python3 -m pip install git+https://gitlab.esrf.fr/mirone/nxtomo@${TAG} --no-deps --force
python3 -m pip install git+https://gitlab.esrf.fr/mirone/tomoscan@${TAG} --no-deps --force

python3 -m pip install git+https://gitlab.esrf.fr/night_rail/night_rail@${TAG} --no-deps --force
python3 -m pip install git+https://gitlab.esrf.fr/night_rail/applications/mirone/night_rail_bm18@${TAG} --no-deps --force
python3 -m pip install git+https://gitlab.esrf.fr/tomotools/nabuxx@${TAG} --no-deps --force

source $ENV_DIR/bin/activate

python3 -m pip install git+https://gitlab.esrf.fr/mirone/aspect_phase_comppag@$TAG   --no-deps --force
python3 -m pip install git+https://gitlab.esrf.fr/mirone/get_doubles@$TAG --no-deps --force

pip install git+https://gitlab.esrf.fr/icat/pyicat-plus@main

pip install openpyxl

# this is used in the getdoubles package
pip install opencv-python
pip install numexpr

chmod a+x $ENV_DIR/bin/*

## MAKING TAG AVAILABLE

# Get the installation directory of the Python module night_rail_bm18
module_dir=$(python -c "import night_rail_bm18, os; print(os.path.dirname(night_rail_bm18.__file__))")

# Check that the directory was found
if [ -z "$module_dir" ]; then
  echo "Unable to locate the directory for the module night_rail_bm18."
  exit 1
fi

echo "Module directory: $module_dir"

# Create (or overwrite) tag.py in the module's directory and set the variable tag
echo "tag = '$TAG'" > "${module_dir}/tag.py"

echo "Created file ${module_dir}/tag.py with tag = '$TAG'"


### MAKING TAG AVAILABLE

# 0. go into a clean directory
unique_dir=$(mktemp -d -p /tmp)
cd $unique_dir

# 1. Correctly get the installation directory of the installed night_rail_bm18 module
module_dir=$(python -c "import sys, night_rail_bm18, os; sys.path = list(filter(None, sys.path)); print(os.path.dirname(night_rail_bm18.__file__))")

if [ -z "$module_dir" ]; then
  echo "Unable to locate the directory for the module night_rail_bm18."
  exit 1
fi

echo "Module directory: $module_dir"

# 2. Create (or overwrite) tag.py in the module's directory, setting the tag variable
echo "tag = '$TAG'" > "${module_dir}/tag.py"



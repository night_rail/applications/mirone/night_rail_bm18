import re
import os

class DictToObj(object):
    """utility class to transform a dictionary into an object with dictionary items as members.
    Example:

    >>> a=DictToObj( dict(i=1,j=1))
    ... a.j+a.i

    """

    def __init__(self, dictio):
        self.__dict__ = dictio


def get_nabu_outputs( my_dir,  output_per_script, session_name ):

    keys = list(output_per_script.keys())
    my_key=None
    
    for k in keys:
        if re.fullmatch(f"{session_name}_[0-9]{{3}}_NabuFinalRunScript.nrs",k):
            outputs = output_per_script[k]
            my_key = k
            break

    if my_key is None:
        raise ValueError(f"Cannot find outputs in the list of keys {keys}")
        
    for fn in outputs:
        if  os.path.splitext(fn)[1] != ".hdf5":
            raise ValueError(f"Cannot find the list of outputs. The file {fn} in the list {outputs} of {my_key} has not the hdf5 extension" )
    if len(outputs) == 1:
        root_name = os.path.splitext(fn)[0]
    else:
        root_name = os.path.splitext(fn)[0]
        root_name = root_name[: root_name.rfind("_")]


    outputs = [os.path.join(my_dir, fn) for fn in outputs]
    return outputs, root_name 

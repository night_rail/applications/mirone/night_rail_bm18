import sys
from PyQt5.QtCore import QSize,  Qt, pyqtRemoveInputHook
from PyQt5.QtGui import QImage, QPalette, QBrush
from PyQt5.QtWidgets import *

from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtGui import QColor

import pyperclip
import os
import subprocess
import shutil
from ..decisional_trees import rec_tree 
import json

class MainWindow(QTabWidget):
    def __init__(self):
        QTabWidget.__init__(self)
        self.w,self.h = None, None
        self.resize(QSize(1200, 800))
        self.show()

        
        self.widget_1 = ParamsWindow( self)
        self.addTab(self.widget_1, "Parameters")
        self.tabBar().setTabTextColor(0, QColor("yellow"))

        self.widget_2 = LevelWindow( self)
        self.addTab(self.widget_2, "UI_Levels")
        self.tabBar().setTabTextColor(1, QColor("yellow"))

    def paintEvent(self, event):
        
        w,h = self.frameGeometry().width()  , self.frameGeometry().height()
        if (w,h) != (self.w, self.h):
            self.w, self.h = w,h

            background_image_file = "/data/scisofttmp/mirone/train_by_night.jpg"

            if os.path.isfile(background_image_file):
                
                oImage = QImage(background_image_file)
                sImage = oImage.scaled(QSize( self.w, self.h  )  )  # resize Image to widgets size
                palette = QPalette()
                palette.setBrush(QPalette.Window, QBrush(sImage))                        
                self.setPalette(palette)

    def to_clipboard(self):
        print(" CLIP ")
        command = self.compose_command()
        if ( command ):
            pyperclip.copy(command)
        else:
            print(" was not able to compose a command line. probably a problem with the parameters")
            

    def run(self):
        command = self.compose_command()
        if ( command ):
            activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
            os.system(f"""xterm -e bash -l  -c "source {activation_script} ;   {command} ; bash "  &""")
        else:
            print(" was not able to compose a command line. probably a problem with the parameters")
            
    def compose_command(self):

        command ="night_rail_bm18_automatic "

        rawdata_dir = self.widget_1.rawdata_dir.text().strip()
        if not rawdata_dir:
            message= "WARNING : rawdata_dir is not set. But it is mandatory"
            print(message)
            return None
        else:
            command = command + f"--rawdata_dir {rawdata_dir} "
        
        name_root = self.widget_1.name_root.text().strip()
        if name_root:
            command = command + f"--name_root {name_root} "

        exclude_fragment = self.widget_1.exclude_fragment.text().strip()
        if exclude_fragment:
            command = command + f"--exclude_frag {exclude_fragment} "
 
        nobackup_dir = self.widget_1.nobackup_dir.text().strip()
        if nobackup_dir:
            command = command + f"--nobackup_dir {nobackup_dir} "

        distortion_map_file = self.widget_1.distortion_map_file.text().strip()
        if distortion_map_file:
            command = command + f"--distortion_map_file {distortion_map_file} "
            
        # after = self.widget_1.after.text().strip()
        # if after:
        #     command = command + f"--after {after} "
            
        target = self.widget_1.target.text().strip()
        if target:
            command = command + f"--target {target} "
            
        max_c = self.widget_1.max_concurrent_spawns.value()
        command = command + f"--max_concurrent_spawns {max_c} "
        
        max_c = self.widget_1.visu_timeout.value()
        command = command + f"--visu_timeout {max_c*60} "
        
        if self.widget_1.do_spawn.isChecked():
            command = command + f"--do_spawn "

        every_value = self.widget_1.batch_reconstruction_every.get_value()
        if every_value >0:
            command = command + f"--online_preview_step {every_value} "
            
        if self.widget_1.do_reconstruct.isChecked():
            command = command + f"--do_reconstruct "

        if self.widget_1.do_show.isChecked():
            command = command + f"--do_show "
            
        if self.widget_1.online.isChecked():
            command = command + f"--online "   
        if self.widget_1.keep_going.isChecked():
            command = command + f"--keep_going "

        if self.widget_1.degroup.isChecked():
            command = command + f"--degroup "


        command = command + f"--ui_level_unsharp {self.widget_2.combobox_unsharp.currentText()} "
        command = command + f"--ui_level_data_selection {self.widget_2.combobox_data_selection.currentText()} "
        command = command + f"--ui_level_paganin_phase {self.widget_2.combobox_paganin_phase.currentText()} "
        command = command + f"--ui_level_cor_search {self.widget_2.combobox_cor_search.currentText()} "
        
        command = command + f"--ui_level_generic {self.widget_2.combobox_generic.currentText()} "
        command = command + f"--ui_level_calculation {self.widget_2.combobox_calculation.currentText()} "
        command = command + f"--ui_level_stripes {self.widget_2.combobox_stripes.currentText()} "
        command = command + f"--ui_level_scintillator {self.widget_2.combobox_scintillator.currentText()} "

        if self.widget_2.auto_absorption.isChecked():
            command = command + f" --auto_absorption "
        
        if self.widget_2.use_modified_defaults.isChecked():
            filename = "_tmp_by_gui_modified_defaults.json"
            print("SAVING DEFAULTS TO FILE ", filename)
            fd=open(filename,"w")
            json.dump(self.widget_2.global_dict_for_defaults, fd, indent=4)
            command = command + f" --defaults_file {filename} "
        
        
        return command

                
        
class ParamsWindow(QWidget):

                
    def __init__(self, parent):
       QWidget.__init__(self)

       self.parentw = parent

       hlayout = QHBoxLayout()

       hdum = QWidget()
       glayout = QGridLayout() # QVBoxLayout()

       ql0 = QLabel()
       ql0.setText("TEST0")
       
       glayout.setRowStretch(0,5)
       
       self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }");

       lab = QLabel("name_root")
       lab.setToolTip("the name root of the scan for which reconstruction is wished")
       glayout.addWidget( lab  , 1,0 )
       self.name_root = QLineEdit()
       glayout.addWidget( self.name_root  , 1,1 )

       
       lab = QLabel("rawdata_dir")
       lab.setToolTip("The directory which will be searched recursively for scans")
       lab.setStyleSheet("color: red")
       glayout.addWidget( lab  , 2,0 )
       self.rawdata_dir = QLineEdit()
       self.rawdata_dir.setFixedWidth(450);
       glayout.addWidget( self.rawdata_dir  , 2,1 )

       search_command = QPushButton("Search rawdata dir")
       glayout.addWidget( search_command  , 2,2 )
       search_command.clicked.connect(self.search_raw)
      
       lab = QLabel("target")
       lab.setToolTip("The name of the subdirectory, relatively to the nobackup directory, under which the processed scan will be placed. If not give it will be derived from date/time")
       glayout.addWidget( lab  , 3,0 )
       self.target = QLineEdit()
       glayout.addWidget( self.target  , 3,1 )
 
       lab = QLabel("nobackup_dir")
       lab.setToolTip("if not given it will be NOBACKUP subdir, placed at the same level as the root directory (typically RAWDATA). Optionally you can give the full path of a directory of your choice")
       glayout.addWidget( lab  , 4,0 )
       self.nobackup_dir = QLineEdit()
       glayout.addWidget( self.nobackup_dir  , 4,1 )

       search_command = QPushButton("nobackup dir")
       glayout.addWidget( search_command  , 4,2 )
       search_command.clicked.connect(self.search_nobackup)
       
       # lab = QLabel("after")
       # lab.setToolTip("the date after which scans are considered. Format is (for this moment) in isoformat 2023-12-03T05:42:54.922828")
       # glayout.addWidget( lab  , 5,0 )
       # self.after = QLineEdit()
       # glayout.addWidget( self.after  , 5,1 )

       lab = QLabel(f"batch every ")
       lab.setToolTip("If zero then no batch reconstruction, otherwise every N slices wih N given here. If 1 then reconstruct all slices.")
       self.batch_reconstruction_every = QSpinBox()
       self.batch_reconstruction_every.setMinimum(0)  
       self.batch_reconstruction_every.setMaximum(10000) 
       self.batch_reconstruction_every.setValue(0) 
       glayout.addWidget( lab  , 5,0 )
       glayout.addWidget( self.batch_reconstruction_every  , 5,1 )
       
       lab = QLabel("do_spawn")
       lab.setToolTip("Spawn non-online scans on slurm")
       glayout.addWidget( lab  , 6,0 )
       self.do_spawn = QCheckBox()
       glayout.addWidget( self.do_spawn  , 6,1 )

       lab = QLabel("max_concurrent_spawns")
       lab.setToolTip("limits the maximum number of slurm processes that are running concurrently at any moment")
       glayout.addWidget( lab  , 7,0 )
       self.max_concurrent_spawns = QSlider(Qt.Horizontal)
       self.max_concurrent_spawns.setMinimum(1)
       self.max_concurrent_spawns.setMaximum(16)
       self.max_concurrent_spawns.setValue(1)
       glayout.addWidget( self.max_concurrent_spawns  , 7,1 )
       self.max_concurrent_spawns_label = QLabel("1")
       self.max_concurrent_spawns.valueChanged.connect(self.max_concurrent_spawns_label.setNum)
       glayout.addWidget( self.max_concurrent_spawns_label  , 7,2 )





       
       lab = QLabel("do_reconstruct")
       lab.setToolTip("Already implicit for online scans. Launch automatically the reconstructions")
       glayout.addWidget( lab  , 8,0 )
       self.do_reconstruct = QCheckBox()
       glayout.addWidget( self.do_reconstruct  , 8,1 )

       lab = QLabel("do_show")
       lab.setToolTip("Already implicit online scans. Launch automatically the visualation for the reconstructions if any")
       glayout.addWidget( lab  , 9,0 )
       self.do_show = QCheckBox()
       glayout.addWidget( self.do_show  , 9,1 )

       lab = QLabel("only_with_reference")
       lab.setToolTip("Considers only scan with reference")
       glayout.addWidget( lab  , 10,0 )
       self.only_with_reference = QCheckBox()
       glayout.addWidget( self.only_with_reference  , 10,1 )
       
       lab = QLabel("keep_going")
       lab.setToolTip("Automatically relaunches itself with --after time set to the last end time")
       glayout.addWidget( lab  , 11,0 )
       self.keep_going = QCheckBox()
       glayout.addWidget( self.keep_going  , 11,1 )


       lab = QLabel("visu_timeout")
       lab.setToolTip("limits the time ( in minutes) during which the popped up visualisation remains on scree")
       glayout.addWidget( lab  , 12,0 )
       self.visu_timeout = QSlider(Qt.Horizontal)
       self.visu_timeout.setMinimum(1)
       self.visu_timeout.setMaximum(32)
       self.visu_timeout.setValue(1)
       glayout.addWidget( self.visu_timeout  , 12,1 )
       self.visu_timeout_label = QLabel("1")
       self.visu_timeout.valueChanged.connect(self.visu_timeout_label.setNum)
       glayout.addWidget( self.visu_timeout_label  , 12,2 )


       lab = QLabel("online")
       lab.setToolTip("follow online scans")
       glayout.addWidget( lab  , 13,0 )
       self.online = QCheckBox()
       glayout.addWidget( self.online  , 13,1 )


       lab = QLabel("exclude_fragment")
       lab.setToolTip("name fragments which must not appear in the scan name")
       glayout.addWidget( lab  , 14,0 )
       self.exclude_fragment = QLineEdit()
       glayout.addWidget( self.exclude_fragment  , 14,1 )

       
       lab = QLabel("degroup")
       lab.setToolTip("z-series will be considered as separated scans. Useful for online follow-up.")
       glayout.addWidget( lab  , 15,0 )
       self.degroup = QCheckBox()       
       glayout.addWidget( self.degroup  , 15,1 )

       self.copy_command = QPushButton("Copy to ClipBoard")
       glayout.addWidget( self.copy_command  , 16,3 )
       self.copy_command.clicked.connect(self.parentw.to_clipboard)

       
       self.run_command = QPushButton("Run")
       glayout.addWidget( self.run_command  , 16,4 )
       self.run_command.clicked.connect(self.parentw.run)


       lab = QLabel("distortion_map_file")
       lab.setToolTip("if given an hint will be passed to the dialog to avoid searching for it.")
       glayout.addWidget( lab  , 16,0 )
       self.distortion_map_file = QLineEdit()
       glayout.addWidget( self.distortion_map_file  , 16,1 )
       search_command = QPushButton("dist. map")
       glayout.addWidget( search_command  , 16,2 )
       search_command.clicked.connect(self.search_distortion_map_file)

       
       
       # self.label_1.setStyleSheet("background-color: lightgreen")

       hdum.setLayout(glayout)

       # self.label_1.setFont(QFont('Arial', 10))

       
       # widget = QWidget()

       hlayout.addWidget( hdum )
       hlayout.insertStretch(1, 4) 
       
       self.setLayout(hlayout)

       # self.resize(QSize(1200, 800))
       
       self.show()

    def search_raw(self ) :
        starting_dir = self.rawdata_dir.text()
        if not len(starting_dir):
            starting_dir = os.getcwd()
            if "/data/visitor" in starting_dir:
                starting_dir = starting_dir[ starting_dir.rfind("/data/visitor"):  ]
       
        # search_command = QPushButton("Search rawdata dir")
        # glayout.addWidget( search_command  , 2,2 )
        # self.search_command.clicked.connect(self.search_raw)
        
        result = QtWidgets.QFileDialog.getExistingDirectory(self, caption='Choose Directory',
                                                            directory=starting_dir)
        self.rawdata_dir.setText(result)

    def search_nobackup(self ) :
        
        starting_dir = self.nobackup_dir.text()
        if not len(starting_dir):
            starting_dir = os.getcwd()
            if "/data/visitor" in starting_dir:
                starting_dir = starting_dir[ starting_dir.rfind("/data/visitor"):  ]
        
        result = QtWidgets.QFileDialog.getExistingDirectory(self, caption='Choose Directory',
                                                            directory=starting_dir)
        self.nobackup_dir.setText(result)


    def search_distortion_map_file(self ) :
        
        starting_dir = self.distortion_map_file.text()
        if not len(starting_dir):
            starting_dir = os.getcwd()
            if "/data/visitor" in starting_dir:
                starting_dir = starting_dir[ starting_dir.rfind("/data/visitor"):  ]
        
        result = QtWidgets.QFileDialog.getOpenFileName(self, caption='Choose File',
                                                 directory=starting_dir)
        try:
          self.distortion_map_file.setText(result[0])
        except:
          print(" Was not able to retrieve the file name")



class Ui_TestQFileDialog(object):
    def _open_file_dialog(self):
        directory = str(QtWidgets.QFileDialog.getExistingDirectory())
        self.lineEdit.setText('{}'.format(directory))

    def _set_text(self, text):
        return text

    def setupUi(self, TestQFileDialog):
        TestQFileDialog.setObjectName("TestQFileDialog")
        TestQFileDialog.resize(240, 320)

        self.toolButtonOpenDialog = QtWidgets.QToolButton(TestQFileDialog)
        self.toolButtonOpenDialog.setGeometry(QtCore.QRect(210, 10, 25, 19))
        self.toolButtonOpenDialog.setObjectName("toolButtonOpenDialog")
        self.toolButtonOpenDialog.clicked.connect(self._open_file_dialog)

        self.lineEdit = QtWidgets.QLineEdit(TestQFileDialog)
        self.lineEdit.setEnabled(False)
        self.lineEdit.setGeometry(QtCore.QRect(10, 10, 191, 20))
        self.lineEdit.setObjectName("lineEdit")

        self.retranslateUi(TestQFileDialog)
        QtCore.QMetaObject.connectSlotsByName(TestQFileDialog)

    def retranslateUi(self, TestQFileDialog):
        _translate = QtCore.QCoreApplication.translate
        TestQFileDialog.setWindowTitle(_translate("TestQFileDialog", "Dialog"))
        self.toolButtonOpenDialog.setText(_translate("TestQFileDialog", "..."))


       
def main():

    pyqtRemoveInputHook() # to avoid event loop warning when interacting with terminal input 

    app = QApplication(sys.argv)
    oMainwindow = MainWindow()
    sys.exit(app.exec_())

class LevelWindow(QWidget):

    def __init__(self, parent):
        QWidget.__init__(self)
        self.parentw = parent


        self.global_dict_for_defaults = {}

        
        hlayout = QHBoxLayout()
        
        hdum = QWidget()
        glayout = QGridLayout() # QVBoxLayout()
    
        glayout.setRowStretch(0,5)
       
        self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }");
       
        choices=["predefined","simple","advanced"]
    
        lab = QLabel("unsharp")
        lab.setToolTip("punctiliousness level for unsharp")
        self.combobox_unsharp = QComboBox()
        for item in choices:
            self.combobox_unsharp.addItem( item )
        self.combobox_unsharp.setCurrentIndex(0)
        glayout.addWidget( lab  , 1,0 )
        glayout.addWidget( self.combobox_unsharp  , 1,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 1,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "unsharp"))

        
        lab = QLabel("data selection")
        lab.setToolTip("punctiliousness level for data selection")
        self.combobox_data_selection = QComboBox()
        for item in choices[:]:
            self.combobox_data_selection.addItem( item )
        self.combobox_data_selection.setCurrentIndex(0)
        glayout.addWidget( lab  , 2,0 )
        glayout.addWidget( self.combobox_data_selection  , 2,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 2,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "data_selection"))


        
        lab = QLabel("paganin_phase")
        lab.setToolTip("punctiliousness level for paganin phase")
        self.combobox_paganin_phase = QComboBox()
        for item in choices:
            self.combobox_paganin_phase.addItem( item )
        self.combobox_paganin_phase.setCurrentIndex(0)
        glayout.addWidget( lab  , 3,0 )
        glayout.addWidget( self.combobox_paganin_phase  , 3,1 )


        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 3,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "paganin_phase"))



        
        lab = QLabel("cor_search")
        lab.setToolTip("punctiliousness level for cor search")
        self.combobox_cor_search = QComboBox()
        for item in choices:
            self.combobox_cor_search.addItem( item )
        self.combobox_cor_search.setCurrentIndex(0)
        glayout.addWidget( lab  , 4,0 )
        glayout.addWidget( self.combobox_cor_search  , 4,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 4,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "cor_search"))


        
        lab = QLabel("generic")
        lab.setToolTip("punctiliousness level for various items : entry name, double_flats averaging pagopag, bliss_voxel_size  redefinition( for big problems in binning)")
        self.combobox_generic = QComboBox()
        for item in choices:
            self.combobox_generic.addItem( item )
        self.combobox_generic.setCurrentIndex(0)
        glayout.addWidget( lab  , 5,0 )
        glayout.addWidget( self.combobox_generic  , 5,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 5,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "generic"))

        
        lab = QLabel("calculation")
        lab.setToolTip("punctiliousness level for calculation : backprojection algorithm, chunk size")
        self.combobox_calculation= QComboBox()
        for item in choices:
            self.combobox_calculation.addItem( item )
        self.combobox_calculation.setCurrentIndex(0)
        glayout.addWidget( lab  , 6,0 )
        glayout.addWidget( self.combobox_calculation  , 6,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 6,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "calculation"))

        
        lab = QLabel("stripes")
        lab.setToolTip("punctiliousness level for stripes")
        self.combobox_stripes= QComboBox()
        for item in choices:
            self.combobox_stripes.addItem( item )
        self.combobox_stripes.setCurrentIndex(0)
        glayout.addWidget( lab  , 7,0 )
        glayout.addWidget( self.combobox_stripes  , 7,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 7,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "stripes"))

        lab = QLabel("scintillator")
        lab.setToolTip("punctiliousness level for scintillator")
        self.combobox_scintillator= QComboBox()
        for item in choices:
            self.combobox_scintillator.addItem( item )
        self.combobox_scintillator.setCurrentIndex(0)
        glayout.addWidget( lab  , 8,0 )
        glayout.addWidget( self.combobox_scintillator  , 8,1 )

        modify_command = QPushButton("Modify defaults")
        glayout.addWidget(modify_command    , 8,2 )
        modify_command.clicked.connect(lambda : self.modify_defaults( "scintillator"))
        
        lab = QLabel("auto_absorption")
        lab.setToolTip("use the scan itself as a reference")
        glayout.addWidget( lab  , 9,0 )
        self.auto_absorption = QCheckBox()
        glayout.addWidget( self.auto_absorption  , 9,1 )


        
        default_collector = rec_tree.DefaultCollector()


        
        lab = QLabel("Use modified defaults values")
        lab.setToolTip("Such defaults can be explored with the predialog buttons")
        glayout.addWidget( lab  , 10,0 )
        self.use_modified_defaults = QCheckBox()
        glayout.addWidget( self.use_modified_defaults  , 10,1 )

        store_current_defaults_command = QPushButton("store current defaults")
        glayout.addWidget(store_current_defaults_command    , 10,2 )
        store_current_defaults_command.clicked.connect(self.store_current_defaults)
        
        load_previous_defaults_command = QPushButton("load previous defaults")
        glayout.addWidget(load_previous_defaults_command    , 10,3 )
        load_previous_defaults_command.clicked.connect( self.load_previous_defaults )
        

        hdum.setLayout(glayout)

        hlayout.addWidget( hdum )
        hlayout.insertStretch(1, 4) 
       
        self.setLayout(hlayout)
       
        self.show()


    def modify_defaults(self, handle):
        default_collector = rec_tree.DefaultCollector()
        self.global_dict_for_defaults = default_collector.get_handle_defaults( handle, global_dict = self.global_dict_for_defaults )
        rec_tree.install_defaults(self.global_dict_for_defaults)
 
    def load_previous_defaults(self ) :
        
        starting_dir = os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[ starting_dir.rfind("/data/visitor"):  ]
        
        result, _ = QtWidgets.QFileDialog.getOpenFileName(self, caption='Chose name for the to be written json file with defaults values',
                                                 directory=starting_dir)
        if result:
            print("LOADING DEFAULTS FROM FILE ", result)
            rec_tree.install_defaults(result)



    def store_current_defaults(self ) :
        
        starting_dir = os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[ starting_dir.rfind("/data/visitor"):  ]
        

        filename, _ = QFileDialog.getSaveFileName(self, "Chose name for the to be written json file with defaults values", starting_dir, "Json Files (*.json)")
        if filename:
            print("SAVING DEFAULTS TO FILE ", filename)
            fd=open(filename,"w")
            json.dump(self.global_dict_for_defaults, fd, indent=4)

            

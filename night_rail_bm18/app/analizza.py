import fabio
import glob
import numpy as np
import scipy.signal
from typing import NamedTuple
from matplotlib import pyplot as plt
import scipy.optimize
from namedlist import namedlist
import pickle
import sys
import argparse

def get_arguments():
    parser = argparse.ArgumentParser(description="Distortion analyser.")
    
    parser.add_argument(
        "--dir",
        required=True,
        type=str,
        help="a directory containing horizontal*.edf and vertical*.edf files ",
    )
    
    return parser.parse_args()




def main():
     args = get_arguments()
     line_data_h = extract_line_data(args,  transl_axis=0  )
     line_data_v = extract_line_data(args,  transl_axis=1  )

     # print(" Horizontal lines ")
     # for l in line_data_h:
     #       print( l.p_pos)
          
     # print(" Vertical lines ")
     # for l in line_data_v:
     #       print( l.p_pos, l.data_inset.shape)
     
     for i, linfo in enumerate(line_data_h + line_data_v):
          print(f" Doing polynomial fitting for {linfo.file_name}")
          set_poly_fit(linfo)
          linfo.data_inset=None
     result = {
          "horizontal_lines":line_data_h,
          "vertical_lines":line_data_v,
     }
     print("building the grid")
     build_grid( line_data_h, line_data_v   )



          
LineInformations    = namedlist("LineInformations",
                                ["idx",
                                 "p_pos",
                                 "p_height",
                                 "p_start",
                                 "data_inset",
                                 "p_pos_relative",
                                 "p_width",
                                 "poly_fit",
                                 "file_name"
                                 ],
                                default=None
                                )

def peak_func(x, amp, cen, sigma, bckgrnd, amp_l, w_l):
     d = (x - cen)/sigma
     dl = (x - cen)/ w_l
     res = 0*amp*(1/sigma*np.sqrt(2*np.pi)) * np.exp( (-1.0/2.0)*d*d ) + bckgrnd + amp_l * 1. /( dl*dl +1 )   
     return res
    
def set_poly_fit(linfo):
     
     ny, nx = linfo.data_inset.shape # can be transposed. For vertical lines it is.


     x_list = []
     y_list = []
     for ix in range(0, nx, 10):
          # print( ix , " / ", nx)
          curve_y = linfo.data_inset[:, ix]
          curve_x = linfo.p_start + np.arange(ny)

          initial_width = linfo.p_width/2 * 10

          try:
              popt_func, pcov_gauss = scipy.optimize.curve_fit( peak_func,
                                                                curve_x ,
                                                                curve_y ,
                                                                p0 = [ linfo.p_height/2,  linfo.p_pos, initial_width , 0 , 0,  initial_width  ],
                                                                #                                                            bounds = ( 0, np.inf)
              )
              
              amp, cen, sigma, bckgrnd, amp_l, w_l = popt_func
              
              x_list.append(ix)
              y_list.append(cen )


          except (RuntimeError) as e:
              print("Curve fitting non converging:", e)

              
     coeffs = np.polyfit(x_list, y_list, 5)
     linfo.poly_fit = np.poly1d(coeffs)
          
     # fit_y = peak_func(curve_x,    amp, cen, sigma, bckgrnd,  amp_l, w_l)
     # # print(  f"  {amp:15.10f}  {cen:15.10f} {sigma:15.10f} {bckgrnd:15.10f} {amp_l:15.10f}  {w_l:15.10f}   {ix}/{nx} " )
     # fig = plt.figure()
     # timer = fig.canvas.new_timer(interval = 600)
     # timer.add_callback(plt.close)
     # plt.plot( curve_x, curve_y )
     # plt.plot( curve_x, fit_y )
     # timer.start()
     # plt.show()
          
def extract_line_data(args,  transl_axis, prefix=None):

     
     if prefix is None:
          prefix = {0:"horizontal", 1:"vertical"}[transl_axis]
         
     fn_list = glob.glob(args.dir+f"/{prefix}*edf")
     fn_list.sort()
    
     line_data = []

     dect_shape = None
     
     for fn in fn_list:
          data = fabio.open(fn).data
          if dect_shape is None:
               dect_shape = np.array(data.shape)
               np.save("dect_shape.npy", dect_shape)
               
          
          max_median = np.median(data.max(axis=transl_axis))
          if max_median < 1600:
               continue
     
          idx = int("".join([char  for char in fn if char.isdigit()]))
         
          profile = data.sum( axis = (transl_axis+1) %2 )
         
          peaks, _ = scipy.signal.find_peaks(profile, height = 600 * data.shape[ (transl_axis+1) %2 ], rel_height=0.5, distance = 150)
         
          widths   = scipy.signal.peak_widths(profile, peaks)

          if len(peaks)!=1 :
               print(" Was not able to find an unique line ")
               print(f"Discarding {fn}")
               line_data.append(None)
          else:               
               p_pos = peaks[0]
               p_width = int(np.ceil(widths[0]))
              
               p_start = max(0, p_pos - 4*p_width )
               p_end   = min( data.shape[ transl_axis ] -1 , p_pos + 4*p_width )
     
               if transl_axis == 0:
                    data_inset = data[p_start:p_end]
               else:
                    data_inset = data.T[p_start:p_end]
                    
               line_data.append( LineInformations( idx=idx,
                                                   p_pos=p_pos,
                                                   p_height = max_median,
                                                   p_start=p_start,
                                                   data_inset=data_inset,
                                                   p_pos_relative=p_pos - p_start,
                                                   p_width=p_width,
                                                   poly_fit = None,
                                                   file_name = fn
               ) )
     
     assert len(line_data)>1, f"Not enough lines found for {prefix}"

     line_data_not_none = []
     for tok in line_data:
          if tok is not None:
               line_data_not_none.append(tok)

     assert(len(line_data_not_none)>1), f"After discarding the non usable lines for prefix {prefix} too few remained (just {len(line_data_not_none)})"
     
     

               
     if line_data_not_none[0].p_pos > line_data_not_none[-1].p_pos:
          line_data.reverse()
          
     for idx, tok in enumerate(line_data):
          if tok is not None:
               tok.idx = idx
               
     line_data_not_none = []
     print(f" Collecting valid lines for {prefix}")
     for tok in line_data:
          if tok is not None:
               line_data_not_none.append(tok)
               print(f"Collecting {prefix} line  numeral {tok.idx} typical position {tok.p_pos} typical width {tok.p_width} from {tok.file_name} ")

     return line_data_not_none



def build_grid(horizontal_lines, vertical_lines):

    ni = len(horizontal_lines)
    nj = len(vertical_lines)

    crossings = np.zeros( [ni,nj,2],"d")

    x,y=0,0
    for i in range(ni):
        for j in range(nj):

            diff = ni+nj
            while diff > 1.0e-20:
                xold, yold = x,y

                assert   i == horizontal_lines[i].idx-horizontal_lines[0].idx , f" Problem in horizontal lines sequence. it seems that there are holes in the sequences (A line in the middle of the line has been discarded in the analysis process)"
                assert   j == vertical_lines[j].idx - vertical_lines[0].idx, f" Problem in vertical lines sequence. it seems that there are holes in the sequences (A line in the middle of the line has been discarded in the analysis process)"
                
                y = horizontal_lines[i].poly_fit(x)
                x = vertical_lines[j].poly_fit(y)
                diff = abs(x-xold)+abs(y-yold)
                # print( xold, yold , "          ", x,y ) 
                
            # print(f" {i:2d},{j:2d} -> {y:15.10f}, {x:15.10f} ")

            crossings[i,j] = y,x

    np.save("crossings.npy", crossings)


# main()

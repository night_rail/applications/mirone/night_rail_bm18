import h5py
import glob
import sys
import os
import json
import re
import numpy as np
import shutil
import h5py
import fabio
import  multiprocessing 

import tifffile

import os
import copy

import argparse
from ..utils import get_nabu_outputs

def get_arguments():
    parser = argparse.ArgumentParser(description="night_rail for bm18. to tiff conversion")

    parser.add_argument('dir', help="the directory for which the tiff will be generated")
    


    parser.add_argument(
        "--session_name",
        required=False,
        type=str
    )
    parser.add_argument(
        "--all_directories",
        required=False,
        action='store',  nargs="*"
    )
    
    parser.add_argument(
        "--overwrite",
        required=False,
        action='store',
    )
    
    parser.add_argument(
        "--data_min",
        required=False,
        type=float,
    )
    
    parser.add_argument(
        "--data_max",
        required=False,
        type=float,
    )

    return parser.parse_args()




def main():
    args = get_arguments()
    directory_path = args.dir
    if args.session_name:
        session_name = args.session_name
    else:
        print( "directory_path   ", directory_path) 
        candidates = glob.glob(   os.path.join(directory_path,"*_aiguillage.py")  )

        if len( candidates ) > 1:
            raise ValueError( f" session name not provided and I cannot univocally determine it.  Please provide a session name. I Found many candidates : {candidates}")
        else:
            candidate = candidates[0]
            session_name = os.path.basename(candidate[ : -len("_aiguillage.py")])
    print( " session name = ", session_name)

    output_per_script = json.load(  open(os.path.join(directory_path,f"{session_name}.nro"),"r")  )

    outputs, root_name = get_nabu_outputs(directory_path,  output_per_script, session_name )

    print("outputs ", outputs )
    print("root_name for tiffs", root_name)
    
    if "online" in root_name:
        print("The reconsruction seems to come from an online copy." )
        root_name = os.path.basename( os.path.abspath(directory_path) )
        print(" Now setting root_name for tiffs  to ", root_name)


    all_outputs = set(outputs)

    if args.all_directories:
        for d in args.all_directories:
            output_per_script = json.load(  open(os.path.join(d,f"{session_name}.nro"),"r")  )
            extra_outputs, root_name_tmp = get_nabu_outputs( d, output_per_script, session_name )
            all_outputs.update(extra_outputs)

    data_min, data_max = get_min_max_percentage(all_outputs)

    if args.data_min is not None:
        data_min = args.data_min

    if args.data_max is not None:
        data_max = args.data_max


    
    target_dir = os.path.join(directory_path, root_name + "_tiff_16bits")
    if os.path.isdir(target_dir) and args.overwrite is True:
        shutil.rmtree(target_dir)
    os.makedirs(target_dir, exist_ok=True)
    
    line_tif_temp_dir = os.path.join(target_dir, "line_tif_temp")
    os.makedirs(line_tif_temp_dir, exist_ok=True)
        
    partial_volumes = []

    
    partial_volumes = []
    for fn in outputs:
        with h5py.File(fn,"r") as f:
            d = f["/entry0000/reconstruction/results/data"]
            for tok in d.virtual_sources():
                partial_volumes.append(tok.file_name)

    print(" PARTIALS VOLUMES",  partial_volumes  )

    indexes = []
    for p in partial_volumes:
        p=os.path.splitext(p)[0]
        index = int( p[p.rfind("_") + 1 :]  )
        indexes.append(index)
    order = np.argsort(indexes)

    indexes.sort()
    partial_volumes = np.array(partial_volumes)[order].tolist()

    starting_slice= 0
    starts = []
    for p, i in zip(partial_volumes, indexes):
        with h5py.File(  os.path.join(directory_path, p) ,"r") as f:
            nslices = f["entry0000/reconstruction/results/data"].shape[0]
            starts.append(starting_slice)
            starting_slice += nslices
            print( p , i, i+nslices, starting_slice)


    pool = multiprocessing.Pool( processes=multiprocessing.cpu_count() )

    args_list = []
    for start, fn in zip(starts, partial_volumes):
        args_list.append(  (directory_path,  fn, data_min, data_max, target_dir, root_name, start ) )

    pool.starmap(convert, args_list)
    
    concatenate_vertical_slices(target_dir, root_name)
      
    metadata_files = sorted(glob.glob(f"{session_name}*_nexus_metadata.json"))
    
    with open(metadata_files[0], 'r') as f:
        metadata = json.load(f)
        print(f"Read metadata from {metadata_files[0]}")
    
    metadata['tomo']['processing']['postprocessing']["32BitsConversion"] = dict(data_min=float(data_min), data_max = float(data_max) )

    metadata_output = os.path.join( target_dir, "metadata.json" )
    with open(metadata_output, 'w') as f:
        f.write(json.dumps(metadata, indent=4))
        print(f"Write metadata file to {metadata_output}")

    print("Now creating the  vertical slice")
    
    return 0
        
def convert( directory_path, fn, data_min, data_max, target_dir, root_name, start ) :
    
    fn=os.path.join(directory_path, fn)

    # edf=fabio.edfimage.edfimage()
    with h5py.File(fn,"r") as f:
        d = f["entry0000/reconstruction/results/data"]
        volume_shape = d.shape
        slice_positions = {"1_4": volume_shape[1] // 4, "1_2": volume_shape[1] // 2, "3_4": 3 * volume_shape[1] // 4}
        for i in range(d.shape[0]):
            data = np.clip(d[i], data_min, data_max)
            data = ((data - data_min) / (data_max-data_min) * float(0xFFFF) ) 
            data = data.astype("uint16")


            tn = os.path.join(target_dir, root_name + f"_{start + i :06d}.tif")            
            if not os.path.exists(tn):
                tifffile.imwrite(   tn  , data, metadata={'data_min': str(data_min), 'data_max':str(data_max) }) # , compression ='zlib')
                print(f"{os.path.basename(tn)} - Writting file")
            else:
                print(f"{os.path.basename(tn)} - File already existing")
        
        
            # #EDF lines creation
            # tn = os.path.join(target_dir, "line" +   f"_{start + i :06d}.edf")
            # if not os.path.exists(tn):
            #     line = data[data.shape[0]//2 ]
            #     line.shape = 1, -1
            #     edf.data = line
            #     edf.write( tn  )
                
            #TIF lines creation
            for label, pos in slice_positions.items():
                
                tn_x = os.path.join(target_dir, "line_tif_temp", f"x_{label}_{start + i:06d}.tif")
                tn_y = os.path.join(target_dir, "line_tif_temp", f"y_{label}_{start + i:06d}.tif")
                
                if not os.path.exists(tn_x):
                    x_slice = data[pos, :]               
                    tifffile.imwrite(tn_x, x_slice)
                    
                if not os.path.exists(tn_y):
                    y_slice = data[:, pos]                    
                    tifffile.imwrite(tn_y, y_slice)
                


def concatenate_vertical_slices(target_dir, root_name):
    print("Concatenation of vertical slices")
    
    line_temp_dir = os.path.join(target_dir, "line_tif_temp")
    vertical_slices_dir = os.path.join(target_dir, "vertical_slices")
    os.makedirs(vertical_slices_dir, exist_ok=True)
        
    for pos in ["1_4", "1_2", "3_4"]:
        x_files = sorted(glob.glob(os.path.join(line_temp_dir, f"x_{pos}_*.tif")))
        y_files = sorted(glob.glob(os.path.join(line_temp_dir, f"y_{pos}_*.tif")))
        if x_files:            
            x_images = [tifffile.imread(f) for f in x_files]
            tifffile.imwrite(os.path.join(vertical_slices_dir, f"{root_name}_concatenated_x_{pos}.tif"), np.stack(x_images, axis=0))
        if y_files:
            y_images = [tifffile.imread(f) for f in y_files]
            tifffile.imwrite(os.path.join(vertical_slices_dir, f"{root_name}_concatenated_y_{pos}.tif"), np.stack(y_images, axis=0))
        
    if os.path.isdir(line_temp_dir):
        shutil.rmtree(line_temp_dir)
        
def get_min_max_percentage(outputs):

    histograms = []
    for fn in outputs:
        histograms.append(  h5py.File(fn,"r")["entry0000/histogram/results/data"][()]  )

    histograms =  np.array(histograms)
    histograms.shape  = -1, histograms.shape[-1]

    ordinate = histograms[0::2,:].astype("d")
    abscissa =  histograms[1::2,:]


    
    total = float(ordinate.sum())
     
    tot_min = abscissa.min()
    tot_max = abscissa.max()

    print( "tot min max", tot_min, tot_max ) 
    
    ## left
    pointers_y = list(range( ordinate.shape[0]))
    pointers_x = [0]*ordinate.shape[0]

    sum_partial = 0

    i_min=0

    while  sum_partial < 0.00001 * total  :
        xs = abscissa[pointers_y, pointers_x]
        i_min = np.argmin(xs)

        sum_partial += float(ordinate[  pointers_y[i_min], pointers_x[i_min]    ])

        pointers_x[i_min] += 1

    data_min = abscissa[  pointers_y[i_min], max( 0, pointers_x[i_min]-1)    ]
    print( " data_min ", data_min) 
    

    ## right
    pointers_y = list(range( ordinate.shape[0]))
    pointers_x = [ordinate.shape[1]-1]*ordinate.shape[0]

    sum_partial = 0

    i_max=0
    while( sum_partial < 0.00001 * total ) :
        xs = abscissa[pointers_y, pointers_x]
        i_max = np.argmax(xs)

        sum_partial += float(ordinate[  pointers_y[i_max], pointers_x[i_max]    ])

        pointers_x[i_max] -= 1

    data_max = abscissa[  pointers_y[i_max], min( ordinate.shape[1]-1, pointers_x[i_max]+1)    ]
    print( " data_max ", data_max) 
    
        
    return data_min, data_max

    

    

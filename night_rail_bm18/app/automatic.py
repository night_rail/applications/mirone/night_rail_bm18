import traceback
import os
import subprocess
import shutil
from datetime import datetime
import time
import argparse
import logging
import re
from silx.io.h5py_utils import File as SilxFile
import dateutil.parser
from dateutil.parser import parser as TimeParser
from contextlib import contextmanager
import uuid

import night_rail
from night_rail_bm18.utils import DictToObj
from ..decisional_trees import rec_tree as bm18_rec_tree
from .create_scripts import main as create_scripts
from .setup_pars import main as setup_pars
from .modify_wf_json import main as modify_json
from .prepare_online import main as prepare_online, safe_copy, scan_is_finished

import json
from collections import namedtuple
from namedlist import namedlist
import textwrap

import multiprocessing as mp
from termios import tcflush, TCIFLUSH
import sys
import importlib
import glob


if __name__ == '__main__' :
    mp.set_start_method('spawn')

logger = logging.getLogger(__name__)
logger.setLevel("INFO")

time_parser = TimeParser()

RecInfoClass = namedlist("RecInfoClass", ["aiguillage", "det_name", "det_shape", "kind", "short_key", "with_ref"], default = None)
TodoClass    = namedlist("TodoClass",["ewok_workflow", "wdir", "session_name", "do_show", "spawn", "max_concurrents", "original_full_path", "full_path", "online_wdir"], default = None)

global keep_previous_for_all
global iter_id
keep_previous_for_all = False


# import sys
# import traceback

# class TracePrints(object):
#   def __init__(self):    
#     self.stdout = sys.stdout
#   def write(self, s):
#     self.stdout.write("Writing %r\n" % s)
#     traceback.print_stack(file=self.stdout)

# sys.stdout = TracePrints()

iter_id=0


def get_arguments():
    parser = argparse.ArgumentParser(description="Automatic reconstruction.")
    parser.add_argument(
        "-n","--name_root",
        required=False,
        help="the name root of the scan for which reconstruction is wished",
        default="",
    )
    parser.add_argument(
        "--exclude_frag",
        required=False,
        nargs="+",
        help="One or more frags. If the scan name contains the fragment ( or one of the given fragments), the scan will be skipped",
    )
    parser.add_argument(
        "-d","--rawdata_dir",
        required=False,
        help="The directory which will be searched recursively for scans",
    )
    parser.add_argument(
        "--target",
        required=False,
        help="The name of the subdirectory, relatively to the nobackup directory, under which the processed scan will be placed. If not give it will be derived from date/time",
        type = str,
    )
    parser.add_argument(
        "--nobackup_dir",
        required=False,
        help="if not given it will be NOBACKUP subdir, placed at the same level as the root directory (typically RAWDATA). Optionally you can give the full path of a directory of your choice",
    )
    parser.add_argument(
        "-a","--after",
        required=False,
        help=f"""the date after which scans are considered. Format is (for this moment) in isoformat {time_parser.parse("2022").today().isoformat()} """,
    )
    
    parser.add_argument(
        "--visu_timeout",
        required=False,
        default=600,
        help=f"""The timeout for visualisation windows in seconds. Default is 600 """,
        type=int
    )
    parser.add_argument(
        "--ui_level_unsharp",
        required=False,
        default="predefined",
        help=f"""punctilousness level for unsharp  """,
        choices=["predefined","simple","advanced"]
    )

    parser.add_argument(
        "--ui_level_data_selection",
        required=False,
        default="simple",
        help=f"""punctilousness level for data_selection  """,
        choices=["predefined", "simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_paganin_phase",
        required=False,
        default="predefined",
        help=f"""punctilousness level for paganin_phase """,
        choices=["predefined","simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_cor_search",
        required=False,
        default="predefined",
        help=f"""punctilousness level for cor search """,
        choices=["predefined","simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_generic",
        required=False,
        default="predefined",
        help=f"""punctilousness level for:  entry name, double_flats averaging pagopag, bliss_voxel_size redefinition( for big problems in binning)""",
        choices=["predefined","simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_calculation",
        required=False,
        default="predefined",
        help=f"""punctilousness level for : ackprojection algorithm, chunk size """,
        choices=["predefined","simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_stripes",
        required=False,
        default="predefined",
        help=f"""punctilousness level for : scintillator gluing stripes """,
        choices=["predefined","simple","advanced"]        
    )
    parser.add_argument(
        "--ui_level_scintillator",
        required=False,
        default="predefined",
        help=f"""punctilousness level for : scintillator gluing scintillator """,
        choices=["predefined","simple","advanced"]        
    )
    
    parser.add_argument(
        "--auto_absorption",
        action="store_true",
        help="Use the scan itself as reference scan",
    )

    parser.add_argument(
        "--cor_followup",
        action="store_true",
        help="Use the scan itself as reference scan",
    )

    parser.add_argument(
        "--wobble_correction",
        action="store_true",
        help="Use the scan itself as reference scan",
    )

    parser.add_argument(
        "--batch_offline",
        action="store_true",
        help="spawn the intermediate slices even offline",
    )

    parser.add_argument(
        "--defaults_file",
        required=False,
        type=str,
        help="A file created by the automatic gui predialoguing tool. In short it is a json file, that could easily be edited by hand. ( see a generated one if you search a template) ",
    )


    parser.add_argument(
        "--overwrite_previous_batches",
        action="store_true",
        help="By default, when batch_reconstruction_every is set and batches are launched, results from previous batches are not overwritten",
    )
    
    parser.add_argument('-o',"--online",   action='store_true', help="activate selection of online scans")
    parser.add_argument('-s',"--batch_reconstruction_every", type=int, required=False, help="If specified, during the online follow-up of a scan, one intermediateslices every batch_reconstruction_every will be reconstructed by spawning the calculation")
    parser.add_argument("--do_spawn",   action='store_true', help="Spawn non-online scans on slurm")
    parser.add_argument("--max_concurrent_spawns",   required=False,  help="limits the maximum number of slurm processes that are running concurrently at any moment", type = int)

    parser.add_argument("--do_reconstruct",   action='store_true', help="Already implicit online scans. Launch automatically the reconstructions")
    parser.add_argument("--do_show",   action='store_true', help="Already implicit online scans. Launch automatically the visualation for the reconstructions if any")

    parser.add_argument("--only_with_reference",   action='store_true', help="Considers only scan with reference")

    parser.add_argument("--keep_going",   action='store_true', help="Automatically relaunches itself with --after time set to the last end time")
    
    parser.add_argument("--degroup",   action='store_true', help="This desactivate the grouping of z-scans together: they will be considered as separated scans")


    parser.add_argument("--distortion_map_file", type=str, required=False, help="hint for the distortion map file. Can be recalled in the history")
    
    parser.add_argument("--test_dir", type=str, required=False, help="used by automatic_gui. If given it assumes that the directory exists already with the workflow, it will reask the questions but only for thos parameters which are set to true in the file automaticSession_tested_variables.json provided by the automatic_gui script")

    return parser.parse_args()

def main():
    
    last_end_time = None

    session_name = "automaticSession"
    
    args = get_arguments()


    if args.defaults_file is not None:
        bm18_rec_tree.install_defaults(args.defaults_file)
        
    if args.cor_followup:
        bm18_rec_tree.install_defaults({"cor_search":{"CorSchemePar":"d"}})



        
    args.session_name = session_name

    previous_rec_info = RecInfoClass()
    actual_rec_info = RecInfoClass()
    my_pars = bm18_rec_tree.WorkFlowScheme()

    if args.test_dir is not None:
        repeat_wf_selected_questions_and_(args, my_pars,   args.test_dir)        
    else:

        args.rawdata_dir = os.path.abspath( args.rawdata_dir )                  

        launch_time_now = time_parser.parse("2022").today().isoformat().replace(":","_")
        args.launch_time_now =  launch_time_now[ : launch_time_now.rfind(".")]

        while (not last_end_time) or args.keep_going  or args.online :

            if last_end_time:
                previous_last_end_time = last_end_time 
                args.after = last_end_time + 0.01
            else:
                previous_last_end_time = None
                if args.after:
                    if not isinstance(args.after, float ) :
                        ## last_end_time can be None at the end of a online loop
                        ## With "keep going" the flow will pass here again with unset last_end_time
                        args.after = time_parser.parse(args.after).timestamp()
                else:
                    args.after = None

            last_end_time = process_rawdata(args, my_pars, previous_rec_info, actual_rec_info)


            if args.keep_going or last_end_time is None  or args.online:
                if (previous_last_end_time and last_end_time == previous_last_end_time) or (last_end_time is None):
                    wait_time = 10
                    print(f" Nothing found todo. Now waiting {wait_time} seconds")
                    time.sleep(wait_time)


def repeat_wf_selected_questions_and_(args, my_pars,   test_dir):

        
    my_wdir = test_dir


    done = False
    while not done:
        tb = None
        try:
            re_deploy_workflow( args, my_wdir,  session_name = args.session_name )
        except:
            tb = traceback.format_exc()
        else:
            tb = ""
        finally:
            if tb:
                print("!!!!!!!!!!!!!!!!! EXCEPTION !!!!!!!!!!!!!!!!!!!")
                print(tb)
                print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
                    
                message = f"""
                The deployment of a workflow in directory {my_wdir}
                has failed.
                This may be due to multiple reasons : 
                     -- an inconsistency in the user options (like "build [S]eparately" for an helical scan which is just one scan)
                     -- An user interrupt, like ctl-C
                     -- finally a more serious problem in the code that must be reported
                 you have now the choice of wether retrying to generate the workflow in {my_wdir}
                 or skip to the next scan
                 """
                tcflush(sys.stdin, TCIFLUSH)
                print(message)
                answer = night_rail.simple_input_from_question_answer(" Do you wish to  [M]odify again the parameters, [S]kip to the next workflow?", default="m")
                if answer == "s":
                    done = True


        # print(" aggiungo todo perche' ",  args.online , args.do_reconstruct )        
            
        todo = TodoClass(
            ewok_workflow = os.path.join(my_wdir, f"{args.session_name}_ewoks_wf.nrw" ),
            wdir = my_wdir,
            session_name = args.session_name,
            do_show  = args.online or args.do_show,
            spawn = (not args.online) and args.do_reconstruct and args.do_spawn,
            original_full_path = "",   # a non null original means that the full path has been transformed by prepara_online
            full_path = None,   # @@@ sorgente di problemi
            max_concurrents = args.max_concurrent_spawns,
        )        
        process_the_required_reconstructions(  [todo], args)


def process_rawdata(args, my_pars, previous_rec_info, actual_rec_info):

    try:
        online_scan, lonelys, grouped_scans, refs_for_scans_before, refs_for_scans_after, scan_by_time, last_end_time = get_scans(  args.rawdata_dir, args.name_root, args.online, args.exclude_frag, args )
    except:
        traceback.print_exc()
        print(" something went wrong ")
        return

    if not args.online:
        online_scan = None
    print(" ONLINE SCAN is ", online_scan, last_end_time)

    if online_scan is not None:
        full_path = online_scan.full_path
        dirname = os.path.dirname(full_path)


        a_thing = online_scan
        short_key = a_thing.name

        online_wdir = get_wdir( a_thing  , short_key, args ) 

        global iter_id

        try:
            print("  preparing online for the first time ")
            if args.batch_reconstruction_every :
                iter_id += 1
            prepare_online(full_path, online_wdir, iter_id=iter_id)
        except:
            if args.batch_reconstruction_every :
                iter_id -= 1
            traceback.print_exc()
            print(" something went wrong ")
            return
        
        ## the presence of original_full_path is a sign for online
        online_scan.original_full_path = online_scan.full_path
        online_scan.online_wdir = online_wdir
        online_scan.full_path = os.path.join(online_wdir, f"online_nobackup.h5") 
    


    # reconstruction list that will be performed
    todo_list = []
    
    for scan_time, the_thing in scan_by_time.items():

        if isinstance(the_thing, DictToObj):
            a_thing = the_thing
            short_key = a_thing.name

            if args.only_with_reference and    refs_for_scans_before[short_key] is None:
                logger.info(f"skipping {short_key} because no ref, but user parameter only_with_reference is true " ) 
                continue
            
            actual_rec_info.kind = "single"
            bliss_first = a_thing.full_path
            bliss_last = a_thing.full_path
            
        else: # group
            a_thing = the_thing[ list(the_thing.keys())[0] ]
            short_key = get_group_short_key( a_thing.name, args )

            if args.only_with_reference and  refs_for_scans_before[short_key] is None:
                logger.info(f"skipping {short_key} because no ref, but user parameter only_with_reference is true " ) 
                continue
            
            actual_rec_info.kind = "multiple"

            bliss_first =  the_thing[ list(the_thing.keys())[ 0] ] .full_path
            bliss_last  =  the_thing[ list(the_thing.keys())[-1] ] .full_path
        
        actual_rec_info.det_name = a_thing.det_name
        actual_rec_info.det_shape = a_thing.det_shape
        actual_rec_info.short_key = short_key

        if args.after and scan_time < args.after :
            logger.info(f"Skipping scan {a_thing.name} because the required minimum start time {datetime.fromtimestamp(args.after).isoformat()} is older than its time {datetime.fromtimestamp(scan_time).isoformat()}")
            continue
    
        my_wdir = get_wdir( a_thing  , short_key, args ) 

        logger.info(f" Building directory :   {my_wdir}")

        os.makedirs( my_wdir  , exist_ok = True )

        done = False
        while not done:
            tb = None
            try:
                deploy_workflow( args, my_wdir, refs_for_scans_before[short_key], refs_for_scans_after[short_key], bliss_first, bliss_last,  previous_rec_info, actual_rec_info, session_name = args.session_name, wf_pars = my_pars )
                done = True
                
                # write into a file, in the current directory, which is supposed to be also the work directory of the gui,
                # a file containing the path to the directory with the to be deployed workflow
                # so that the gui may optionally know which variables have been activated. This is used in the selective activation from the gui in the test phase
                
                file_with_path = ".path_to_the_deployed_workflow_for_night_rail_automatic_gui.txt"
                try:
                    with open(file_with_path,"w") as fw:
                        fw.write(my_wdir)
                except:
                    logger.warning(f" could not write {my_wdir} to {os.path.abspath(file_with_path)}")
            
            except:
                tb = traceback.format_exc()
            else:
                tb = ""
            finally:
                if tb:
                    print("!!!!!!!!!!!!!!!!! EXCEPTION !!!!!!!!!!!!!!!!!!!")
                    print(tb)
                    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
                    
                    message = f"""
                    The deployment of a workflow in directory {my_wdir}
                    has failed.
                    This may be due to multiple reasons : 
                              -- an inconsistency in the user options (like "build [S]eparately" for an helical scan which is just one scan)
                              -- An user interrupt, like ctl-C
                              -- finally a more serious problem in the code that must be reported
                    you have now the choice of wether retrying to generate the workflow in {my_wdir}
                    or skip to the next scan
                    """
                    tcflush(sys.stdin, TCIFLUSH)
                    print(message)
                    answer = night_rail.simple_input_from_question_answer(" Do you wish to  [M]odify again the parameters, [S]kip to the next workflow?", default="m")
                    if answer == "s":
                        done = True


        if args.online or args.do_reconstruct:

            # print(" aggiungo todo perche' ",  args.online , args.do_reconstruct )        
            
            todo = TodoClass(
                ewok_workflow = os.path.join(my_wdir, f"{args.session_name}_ewoks_wf.nrw" ),
                wdir = my_wdir,
                session_name = args.session_name,
                do_show  = args.online or args.do_show,
                spawn = (not args.online) and args.do_reconstruct and args.do_spawn,
                original_full_path = a_thing.original_full_path,   # a non null original means that the full path has been transformed by prepara_online
                full_path = a_thing.full_path,  
                max_concurrents = args.max_concurrent_spawns,
                online_wdir = a_thing.online_wdir
            )
            todo_list.append(todo)

        
    process_the_required_reconstructions(  todo_list, args)
    return last_end_time



n_launchs = 0
def process_the_required_reconstructions(  todo_list, parsed_args):
    global n_launchs
    global iter_id
    for todo in todo_list:
        if not todo.spawn or todo.original_full_path:  # a non null original means that the full path has been transformed by prepara_online
            with change_path(todo.wdir):
                ewoks_runner = f"./{todo.session_name}_run_ewoks.sh"
                ewoks_visualise = f"./{todo.session_name}_visualise_check.sh"

                count = 1
                next_loop_stop_it = False

                z_to_start_at = 0
                
                while True:
                    args = [ewoks_runner]
                    return_object =  subprocess.run(args)
                    if return_object.returncode:
                        logger.warning(f" Ewoks exited with error {return_object}")
                    else:
    
                        if iter_id > 0  or parsed_args.batch_offline  :
                            z_to_start_at = batch_volume_reconstruction(todo.session_name, parsed_args, z_to_start_at)
                            
                        if todo.do_show:
                            args = [ewoks_visualise, str(parsed_args.visu_timeout)]
                            subprocess.Popen(args)
                        
                    count += 1
                    if not todo.original_full_path:
                        break
                    else:
                        if  next_loop_stop_it:
                            print("A scan is finished, terminating the previous online scan following-up ")
                            break

                        print(" preparing again online ", todo.original_full_path )
                        
                        todo.full_path = os.path.join(todo.online_wdir, f"online_nobackup.h5") 

                        if parsed_args.batch_reconstruction_every :
                            iter_id += 1 # @@@@@@@
                        
                        prepare_online( todo.original_full_path, todo.online_wdir, iter_id=iter_id )

                        print(" checking if it is finished ")
                        if scan_is_finished(todo.full_path):
                            next_loop_stop_it = True
        else:
            with change_path(todo.wdir):
                ewoks_runner = f"./{todo.session_name}_run_ewoks.sh"
                ewoks_run_visualise = f"./{todo.session_name}_run_visualise_check.sh"


                if not todo.do_show:
                    args = ["sbatch", ewoks_runner]
                    if todo.max_concurrents is not None:
                        args =  args[:1] +   ["--job-name", f"night_rail_{n_launchs % todo.max_concurrents}"  ] + args[1:]
                else:
                    args = [ewoks_run_visualise]
                    if todo.max_concurrents is not None:
                        args =  args[:1] +   [ f"night_rail_{n_launchs % todo.max_concurrents}", str(parsed_args.visu_timeout) ]
                        
                subprocess.Popen(args)
                
                n_launchs+=1


def batch_volume_reconstruction(session_name, parsed_args, z_to_start_at):
    global iter_id
    
    with  open(f"{session_name}_ewoks_wf.nrw","r") as f:
        ew = json.load(f)
        last_script_name = ew["nodes"][-1]["id"]

    conf_file = None
    with open(f"{session_name}.nri","r") as f:
        my_inputs = json.load(f)[last_script_name]
        for inp_name in my_inputs:            
            if inp_name.endswith(".conf"):
                conf_file = inp_name


    my_start_z = z_to_start_at
    my_end_z = get_span(conf_file)-1


    print(" DA FARE ",  my_start_z, my_end_z , parsed_args.batch_reconstruction_every, "iter_id", iter_id)
                
    batch_dir = os.path.join(os.getcwd(), "batch_dir")
    os.makedirs(batch_dir, exist_ok=True)
    with change_path( batch_dir ):
        shutil.copy(f"../{session_name}_configuration.json"    ,f"{session_name}_configuration.json")
        shutil.copy(f"../{session_name}_aiguillage.py"    ,f"{session_name}_aiguillage.py")

        AiguillageInfo = namedtuple("AiguillageInfo", ["aiguillage","change_par","target_file","automatic"])
        
        SetupParsInfo = namedtuple("SetupParsInfo", ["i","o","automatic", "m", "skip_to","go_to"])
    
        change_par = [
            ("VerticalRangeSchemePar$","i"), # regex expression: $ stands for end of line
        ]


        online_file = f"online_{iter_id:04d}_nobackup.h5"

        
        if os.path.isfile(online_file):
            is_online = True
        else:
            is_online = False
            
        if is_online:
            change_par.extend( [
                ("BlissFirstScan", online_file),
                ("BlissLastScan", online_file),
            ])
            
        
        # Check if there is a directory with extracted darks flats, it is expensive, and we reuse it
        spec = importlib.util.spec_from_file_location("aiguillage", f"./{session_name}_aiguillage.py")
        aiguillage = importlib.util.module_from_spec(spec)
        sys.modules["aiguillage"] = aiguillage
        spec.loader.exec_module(aiguillage)
        # if aiguillage.Parameters.has_variable("RefExtractionTargetDir$" ): 
        #     change_par.extend( [
        #         ("RefExtractionTargetDir$", f"../EXTRACTED_DARKS_FLATS"),
        #     ])
        aiguillage_info = AiguillageInfo(
            aiguillage= f"{session_name}_aiguillage.py",
            target_file = "./nrbm18_default.json",
            change_par = change_par,
            automatic = True
        )

        
        modify_json(aiguillage_info )

        setuppars_info = SetupParsInfo(
            i ="./nrbm18_default.json" , 
            o = "./nrbm18_default.json",
            automatic = True,
            m=None,
            skip_to=None,
            go_to=None
        )

        setup_pars(setuppars_info)
        
        create_scripts("nrbm18_default.json")
        
        ## Final change(s) (because there could be more than one slice to do)
        print( " ESTREMI " , my_start_z, my_end_z)

        if parsed_args.batch_reconstruction_every==1:
            step=200
        else:
            step = parsed_args.batch_reconstruction_every

        my_start_z = my_start_z - (my_start_z  ) % step  + step * ( (my_start_z % step )  > 0 ) 
        
        if  parsed_args.batch_reconstruction_every>1:
            
            todo_list = [ ( iz,iz) for iz in range( my_start_z   , my_end_z, step ) ]
            
            last_z = my_end_z
            
            output_format = "t"
            
        else:
            z_list = list(  range( my_start_z , my_end_z, step) )  
            todo_list = [ ( z_list[i], z_list[i+1]-1) for i in range(len(z_list)-1)  ]
            
            last_z = z_list[-1] - 1
            
            output_format = "h"
            
        for  iz_start, iz_end in todo_list:
            new_session_name = f"batch_{iz_start:04d}_{iz_end:04d}"

            change_par = [
                ("SessionName", new_session_name),
                ("StartZPar", iz_start),
                ("EndZPar", iz_end),
                ("OutputFormatPar",output_format),
            ]

            if is_online:
                new_online_file = f"{new_session_name}_online_file.nx" 
                shutil.copy( online_file, new_online_file ) 
                change_par.extend( [
                    ("BlissFirstScan", new_online_file),
                    ("BlissLastScan" , new_online_file),
                ])

            
            if aiguillage.Parameters.has_variable("RefExtractionTargetDir$" ): 
                change_par.extend( [
                    ("RefExtractionTargetDir$", f"./{new_session_name}_EXTRACTED_DARKS_FLATS"),
                ])
        
            spec.loader.exec_module(aiguillage)
            
            aiguillage_info = AiguillageInfo(
                aiguillage= f"{session_name}_aiguillage.py",
                target_file = "./nrbm18_default.json",
                change_par = change_par,
                automatic = True
            )
            modify_json(aiguillage_info )
            create_scripts("nrbm18_default.json")

            if not parsed_args.overwrite_previous_batches:
                # check if the outpts have already been produced

                job_already_runned = True
                        
                fl = glob.glob(f"{new_session_name}_*_nabu.log")
                if len(fl) == 0:
                    job_already_runned = False
                else:
                    s = open(fl[0],"r").read()
                    if not ("Merging histograms" in s):
                        job_already_runned = False
                    
                if job_already_runned:
                    print(f" OMITTING SBATCH LAUNCH because already done. (see {fl[0]})")
                    continue

            script_name = f"{new_session_name}_run_ewoks.sh"
            os.system(f"sbatch ./{script_name}")

    return last_z



    
def set_vertical_range(conf_file, my_start_z, my_end_z) :
    
    s=open(conf_file,"r").read()
    sl=s.split("\n")

    new_sl = []
    
    for l in sl:
        
        if "file_format =" in l:
            l="file_format = hdf5"
            
        if "start_z_fract =" in l:
            l="start_z_fract = 0"
            
        if "end_z_fract =" in l:
            l="end_z_fract = 0"
            
        if "start_z =" in l:
            l=f"start_z = {my_start_z}"
            
        if "end_z =" in l:
            l=f"end_z = {my_end_z}"
            
        if "file_prefix =" in l:
            l=f"file_prefix = partial_online_reconstructions"

        new_sl.append(l)

    s="\n".join(new_sl)

    open(conf_file,"w").write(s)
    
            
    
def get_span(conf_file):    
    args = [ "nabu-helical",  conf_file, "--dry_run", "1"]
    return_object =  subprocess.run(args, capture_output=True)
    if return_object.returncode:
        raise RuntimeError(f" Could not perform a dry run to check reconstruction limits. Using args : {args}")
    span = None
    for l in return_object.stdout.decode("utf-8").split("\n"):
        needle = "In voxel,the vertical doable span measures:"
        if needle in l:
            l=l[ l.rfind(needle) + len(needle):]
            span = int(l)
            break
    if span is None:
        print(f" Could not get the vertical span by running {conf_file}")
        return -10
        # raise RuntimeError(f" Could not get the vertical span by running {conf_file}")
    return span


                
def deploy_workflow( args, my_wdir, my_ref_for_scans_before, my_ref_for_scans_after, bliss_first, bliss_last, previous_rec_info, actual_rec_info, session_name = "automaticSession", wf_pars= None ):

    if wf_pars:
        my_pars = wf_pars
    else:
        my_pars = bm18_rec_tree.WorkFlowScheme()

    with change_path( my_wdir):

        if my_ref_for_scans_before:
            ref_b = my_ref_for_scans_before.full_path
            
            if my_ref_for_scans_after:
                ref_e = my_ref_for_scans_after.full_path
            else:
                ref_e = ref_b
            actual_rec_info.with_ref = "with ref"
        else:
            ref_b, ref_e = None, None
            actual_rec_info.with_ref = "without ref"

        if (
                (None in [ previous_rec_info.aiguillage, previous_rec_info.det_name, previous_rec_info.det_shape, previous_rec_info.kind  ]) or
                (previous_rec_info.det_name != actual_rec_info.det_name ) or ( previous_rec_info.det_shape!= actual_rec_info.det_shape) or (previous_rec_info.kind != actual_rec_info.kind)  or ( actual_rec_info.with_ref != previous_rec_info.with_ref )  
                or (not confirm_automatic_aiguillage(previous_rec_info, actual_rec_info))     
        ) :
            

            # on the long term the predefined_sequence should be a dictionary, with the parameters class objects as the keys
            if ref_b:
                predefined_sequence = ["n","b","r", bliss_first, bliss_last, ref_b, ref_e ]
            else:
                predefined_sequence = ["n","b","m", bliss_first, bliss_last ]


            predefined_dict = {}
            if ref_b==ref_e:
                predefined_dict[bm18_rec_tree.MultiZRecScheme]="a"


            if args.distortion_map_file is not None:
                predefined_dict[bm18_rec_tree.MapXzPathPar]=args.distortion_map_file
                predefined_dict[bm18_rec_tree.DetectorCorrectionSchemePar]="m"
          
            print(" =======================  ", os.path.basename(bliss_first) , " ========================")
            print("DIALOG because : ",
                  previous_rec_info.det_name , "<>",  actual_rec_info.det_name ,
                  previous_rec_info.det_shape , "<>",   actual_rec_info.det_shape ,
                  previous_rec_info.kind , "<>",  actual_rec_info.kind ,
                  previous_rec_info.with_ref , " <> ", actual_rec_info.with_ref 
            )
            print(" and also ",  [ previous_rec_info.aiguillage, previous_rec_info.det_name, previous_rec_info.det_shape, previous_rec_info.kind  ])


            question_punctiliousness_controller = {
                "paganin_phase" : args.ui_level_paganin_phase,
                "data_selection" : args.ui_level_data_selection,
                "unsharp" :args.ui_level_unsharp,
                "cor_search" :args.ui_level_cor_search,
                "calculation" :args.ui_level_calculation,
                "generic" :args.ui_level_generic,
                "stripes" :args.ui_level_stripes,
                "scintillator" :args.ui_level_scintillator,
                "wobble_correction":"predefined"
            }  

            
            # serialiser with predefined
            user_serialiser = night_rail.FromUser(
                predefined_sequence =predefined_sequence,
                predefined_dict =predefined_dict,
                predefined_session_name = session_name,
                question_punctiliousness_controller = question_punctiliousness_controller
            )
            # user interaction
            my_pars.configure_scheme( user_serialiser = user_serialiser )

            my_serialiser = night_rail.SerialiseForward()
            my_pars.serialise_forward(my_serialiser)
            with open("nrbm18_default.json", "w") as fp:
                json.dump(my_serialiser, fp, indent=4)
            create_scripts("nrbm18_default.json")
            
            previous_rec_info.aiguillage = os.path.abspath( f"./{session_name}_aiguillage.py" )                  
        else:

            print( "Doing aiguillage       ",    os.path.basename(bliss_first)  )

            AiguillageInfo = namedtuple("AiguillageInfo", ["aiguillage","change_par","target_file"])

            change_par = [
                    ("BlissFirstScan", bliss_first),
                    ("BlissLastScan", bliss_last),
            ]
            if ref_b:
                change_par.extend([
                    ("BlissRefBeginScan",ref_b),
                    ("BlissRefEndScan",ref_e)      
                ])

            
            aiguillage_info = AiguillageInfo(
                aiguillage= previous_rec_info.aiguillage,
                target_file = "./nrbm18_default.json",
                change_par = change_par
            )
            
            modify_json(aiguillage_info )
            create_scripts("nrbm18_default.json")
            
        previous_rec_info.det_name = actual_rec_info.det_name
        previous_rec_info.det_shape = actual_rec_info.det_shape
        previous_rec_info.kind = actual_rec_info.kind
        previous_rec_info.short_key = actual_rec_info.short_key
        previous_rec_info.with_ref = actual_rec_info.with_ref

def re_deploy_workflow( args, my_wdir,  session_name = "automaticSession" ):

    with change_path( my_wdir):

        previous_dict = json.load(open("automaticSession_configuration.json", "r"))
        my_serialiser_tmp = night_rail.SerialiseBackward(previous_dict, top_dict = previous_dict)
        my_pars = bm18_rec_tree.WorkFlowScheme(my_serialiser_tmp)
        # user interaction
        test_variables_dict = json.load(open("automaticSession_tested_variables.json", "r"))

        user_serialiser = night_rail.FromUser(
            test_variables_dict = test_variables_dict
        )
        my_pars.configure_scheme(user_serialiser = user_serialiser )
        
        my_serialiser = night_rail.SerialiseForward()
        my_pars.serialise_forward(my_serialiser)
        
        with open("nrbm18_default.json", "w") as fp:
            json.dump(my_serialiser, fp, indent=4)
        create_scripts("nrbm18_default.json")
        
    return my_pars
            
def get_wdir( a_thing , short_key, args) :
    
    full_path = os.path.splitext(a_thing.full_path)[0]
    
    
    # appendix = full_path[full_path.rfind("RAW_DATA") + len("RAW_DATA") + 1: ]
    appendix = short_key


    if args.nobackup_dir is None:

        if "RAW_DATA" in full_path:
            root = full_path[:full_path.rfind("RAW_DATA")]
        else:
            root = os.path.dirname(args.rawdata_dir)

        args.nobackup_dir = os.path.join(root, "NOBACKUP")
    
    if args.target is None:
        if not args.online:
            wdir = os.path.join( args.nobackup_dir,  args.launch_time_now, appendix )
        else:
            wdir = os.path.join( args.nobackup_dir,  "online_" + args.launch_time_now, appendix )
    else:
        wdir = os.path.join( args.nobackup_dir,  args.target, appendix )


    while wdir[:-5].endswith("_fc_") or wdir[:-5].endswith("_bc_"):
        wdir = wdir[:-9] + wdir[-5:]
        
    return wdir

def get_scans( tree_root, name_root , online, exclude_frag, args ):

    ### time collected during a first pass. From them we deduce the most recent one
    ### which will be treated with care
    found_timestamps = set()

    ## all the positional informations
    found_scans = {}
    
    for root,d_names,f_names in os.walk(tree_root): # , followlinks=True):

        if contains_scan( d_names ) or root.endswith("glued"):
            
            name = os.path.basename( root )
            if not name.startswith(name_root):
                continue
            
            if  exclude_frag :
                contained = False
                for frag in exclude_frag:
                    # print(frag, name)
                    if frag in name:
                        contained = True
                if contained:
                    continue
            
            nameh5 = name+".h5"
            if nameh5 not in f_names:
                continue
            else:
                timestamp = os.path.getmtime(os.path.join(root,nameh5) )

                found_timestamps.update([timestamp])

                if name in found_scans:
                    raise RuntimeError(f" Two scans with the same name {name}")

                found_scans[name] = DictToObj(dict(
                    timestamp = timestamp,
                    root = root,
                    nameh5 = nameh5,
                    name = name,
                    full_path = os.path.join(root, nameh5),
                    is_latest = False,
                    original_full_path = None,
                    online_wdir = None,
                ))
    timestamps = list(found_timestamps)
    timestamps.sort()

    info_latest = None
    for key, info in found_scans.items():
        if info.timestamp == timestamps[-1]:
            info.is_latest = True
            info_latest = info


    # find online scan
    online_scan_info = None
    if info_latest and "_REF_" not in info_latest.name:
        # instead of the below line
        # online_file = online_copy(info_latest.full_path)
        # use the one below: then relying on swmr
        online_file = info_latest.full_path
        
        if scan_is_tomo(online_file, must_be_helical=True)  :
            if not scan_is_finished(online_file):
                online_scan_info = info_latest
                tm,dn,sh = get_bliss_time_detname_shape( online_file  )
                online_scan_info.bliss_time = tm
                online_scan_info.det_name = dn
                online_scan_info.det_shape = sh
                
    if online_scan_info:
        del found_scans[online_scan_info.name]

   # purge broken scans and non-tomo
    for key in list(found_scans.keys()):
        if not scan_is_finished(found_scans[key].full_path) or not scan_is_tomo(found_scans[key].full_path):
            del found_scans[key]
        else:
            tm,dn,sh  = get_bliss_time_detname_shape( found_scans[key].full_path  )
            found_scans[key].bliss_time = tm
            found_scans[key].det_name = dn
            found_scans[key].det_shape = sh

    groups = {}

    for key in list(found_scans.keys()):
        if "_REF_"  in key:
            continue

        short_key = get_group_short_key( key, args)
        if short_key:
            this_group = groups.get( short_key,    {} )
            this_group[key] = found_scans[key]
            groups[short_key] = this_group

            del found_scans[key]
    # consider lonely scans
    lonelys = {}
    for key in list(found_scans.keys()):
        if "_REF_"  in key:
            continue
        lonelys[key] = found_scans[key]

        del found_scans[key]

    all_keys = list( lonelys.keys() ) + list( groups.keys()  )
    if online_scan_info:
        all_keys.append( online_scan_info.name  )
    refs_keys_for_scans= dict(  [  (k,[]) for k in all_keys  ]   )


    if not args.auto_absorption:
        for key in list(found_scans.keys()):
            if "_REF_" not  in key:
                del(found_scans[key] )
                continue
    
            root = key[   :key.rfind("_REF_") ]
    
            for sk in all_keys:
                if sk.startswith(root):
                    refs_keys_for_scans[sk].append( key )
    else: # autoabsorption
        for key in all_keys:
            if key in lonelys:
                # use the scan itself as reference
                refs_keys_for_scans[key].append( key )
                # readd the scan so that the reference scan is present in found_scans
                found_scans[key] = lonelys[key]
        

    ref_times = [tok.timestamp for tok in found_scans.values()]
    ref_by_time = dict( [ (tok.timestamp, tok) for tok in  found_scans.values() ]  )
    ref_times.sort()
    ref_by_time = dict( [ (timestamp, ref_by_time[timestamp]) for timestamp in  ref_times ]  )

    refs_for_scans_before= dict(  [  (k,None) for k in all_keys  ]   )
    refs_for_scans_after= dict(  [  (k,None) for k in all_keys  ]   )
            
    if online_scan_info:
        refs_for_scans_before[ online_scan_info.name ] = extract_ref(  online_scan_info, refs_keys_for_scans[online_scan_info.name ], found_scans, "before", online_scan_info.name  )
        refs_for_scans_before[ online_scan_info.name ]  = mend_none_ref(refs_for_scans_before[  online_scan_info.name ], online_scan_info,   ref_by_time   )
        
    for k in lonelys.keys():


        refs_for_scans_before[ k ] = extract_ref(  lonelys[k]  , refs_keys_for_scans[k ], found_scans , "before", k ) 
        refs_for_scans_after[ k ]  = extract_ref(  lonelys[k]  , refs_keys_for_scans[k ], found_scans , "after", k ) 

        refs_for_scans_before[ k ]  = mend_none_ref(refs_for_scans_before[ k ], lonelys[k],   ref_by_time   )
    for k in groups.keys():

        ordered_keys = list(  groups[k].keys() )
        ordered_keys.sort()

        groups[k] = dict( [(ok, groups[k][ok]) for ok in ordered_keys ]  )


        if not args.auto_absorption:
            
            refs_for_scans_before[ k ] = extract_ref(  groups[k][ordered_keys[0]]  , refs_keys_for_scans[k ], found_scans , "before", k) 
            refs_for_scans_after[ k ]  = extract_ref(  groups[k][ordered_keys[-1]]  , refs_keys_for_scans[k ], found_scans, "after" , k) 
            
            refs_for_scans_before[ k ]  = mend_none_ref(refs_for_scans_before[ k ],  groups[k][ordered_keys[0]]  ,   ref_by_time   )

        else:

            refs_for_scans_before[ k ] =   groups[k][ordered_keys[0]] 
            refs_for_scans_after[ k ]  =   groups[k][ordered_keys[-1]] 

            
        


    # SKIP NON ONLINE HERE
    if(not online):
        scan_by_time = dict(
            [ (tok.timestamp, tok) for tok in  lonelys.values() ] +
            [ (tok[list(tok.keys())[0]].timestamp, tok) for tok in  groups.values() ]
        )
    else:
        scan_by_time = {}
        
        if online_scan_info:
            scan_by_time.update(
                dict(
                    [ (online_scan_info.timestamp, online_scan_info)  ] 
                )
            )

            
    scan_times = list(scan_by_time.keys())
    scan_times.sort()
    scan_by_time = dict( [( t, scan_by_time[t] ) for t in scan_times]   )

    if len(scan_times):
        last_end_time = scan_times[-1]
    else:
        last_end_time = None
        
    return  online_scan_info, lonelys, groups, refs_for_scans_before, refs_for_scans_after, scan_by_time, last_end_time

def contains_scan( d_names ):
    p=re.compile("scan[0-9]{4}")
    count = 0
    for d in d_names:
        m = p.match( d)
        if m:
            if len(d) == (m.end() - m.start()):
                count += 1
                
    return  (count > 0)


def scan_is_tomo(scan_file, must_be_helical=False):
    return True
    with SilxFile(scan_file,"r") as f:
        for k in list(f.keys())[:1]:
            if "title" not in f[k]:
                return False

            if f[k]["title"][()].decode().startswith("tomo"):
                if must_be_helical:
                    if "helical" not in f[k]["title"][()].decode():
                        return False
                return True
            else:
                return False
    return False


def get_bliss_time_detname_shape(scan_file):
    end_time = None
    detname = None
    shape = None
    with SilxFile(scan_file,"r") as f:
        for k in list(f.keys())[:]:
            if "end_time" in f[k]:
                end_time = f[k]["end_time"][()].decode()
                end_time = dateutil.parser.parse(end_time)
                end_time =  end_time.timestamp()
            if detname is None:
                if "measurement" in f[k]:
                    g = f[k]["measurement"]
                    for key in list(g.keys()):
                        # print(" ICI il devrait avoir shape ", dir(g[key]))
                        try:
                            if len( hasattr( g[key]  ,"shape")   and g[key].shape) == 3:
                                detname = key
                                shape = g[key].shape[1:]
                        except:
                            print(" WARNING: could not read key ", key, " at ", __file__, " in    f len( hasattr( g[key]  ,'shape')   and g[key].shape) == 3:  ")
                            pass
    return end_time, detname, shape

            
def extract_ref(  tomo_scan  , refs_keys , found_scans, when = "before", scan_name=None ) :
    result = None
    found_by_time = {}

    for k in refs_keys:
        ref_scan = found_scans[k]

        print(f" for {k }  ref time is {ref_scan.bliss_time}   data {tomo_scan.bliss_time} ")

        if when == "before":
            if ref_scan.bliss_time > tomo_scan.bliss_time:
                continue
            else:
                if result is None or result.bliss_time < ref_scan.bliss_time:
                    if result is None:
                        print(f" Scan  {k} is the first found that could go as a Before-reference for {scan_name}. Its time is {(tomo_scan.bliss_time-ref_scan.bliss_time)} seconds before " )
                    else:
                        print(f" Scan  {k} replaces the previously found for the before-reference {scan_name} . Its time is {-(result.bliss_time-ref_scan.bliss_time)} seconds after the previously found " )
                        
                    result = ref_scan
                else:
                    if result:
                        print(f" Scan  {k} cannot replace the previously found for the before-reference {scan_name} . Its time is {(result.bliss_time-ref_scan.bliss_time)} seconds before the previously found " )
        else:
            if ref_scan.bliss_time < tomo_scan.bliss_time:
                continue
            else:
                if result is None or result.bliss_time > ref_scan.bliss_time:
                    
                    if result is None:
                        print(f" Scan  {k} is the first found that could go as a after-reference for {scan_name}. Its time is {-(tomo_scan.bliss_time-ref_scan.bliss_time)} seconds after " )
                    else:
                        print(f" Scan  {k} replaces the previously found for the before-reference {scan_name} . Its time is {(result.bliss_time-ref_scan.bliss_time)} seconds before the previously found " )
                    result = ref_scan
                else:
                    print(f" Scan  {k} cqnnot replace the previously found for the before-reference {scan_name} . Its time is {-(result.bliss_time-ref_scan.bliss_time)} seconds after the previously found " )
               

                    
    return result
            
def mend_none_ref(ref,  tomoscan  ,   ref_by_time   ):
    if ref is not None:
        return ref
    
    ref = None
    for timestamp, old_ref in ref_by_time.items():
        
        if timestamp < tomoscan.timestamp:
            ref = old_ref
    if ref is None:
        return None
    
    if ref.det_name == tomoscan.det_name and ref.det_shape == tomoscan.det_shape:
        return ref
    else:
        return None
    
@contextmanager
def change_path(newdir):
    old_path = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(old_path)


def get_group_short_key(key, args):    
    p=re.compile("\_[0-9]{4}\_[0-9]{4}")
    m = p.search( key )
    if m and m.end() == len(key):
        if not args.degroup:
            short_key = key[:m.start()]
        else:
            short_key = key[:m.start()+5]
            # short_key = key[:(m.start()-5)]
        return short_key
    else:
        return None

def confirm_automatic_aiguillage(previous_rec_info, actual_rec_info):
    
    global keep_previous_for_all
    
    if keep_previous_for_all :
        return True

    tcflush(sys.stdin, TCIFLUSH)
    print( f"Scan {actual_rec_info.short_key} has same detector name, and shape as previous_rec_info {previous_rec_info.short_key}")
    answer = night_rail.simple_input_from_question_answer(" Do you wish [M]odify the parameter,  [K]eep the previous dialog or keep it for [A]ll?", default="a")
    
    if answer == "a":
        
        keep_previous_for_all = True
        answer="k"
        
    return answer == "k"



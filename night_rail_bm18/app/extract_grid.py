import h5py
import sys
import math
import numpy as np
import argparse
import fabio
import os

def get_arguments():
    parser = argparse.ArgumentParser(description="Extract spot from a sequence where the spots follow a grid")
    parser.add_argument(
        "--periodx",
        required=True,
        type=int,
        help="The leading period in X : horizontal. ",
    )
    
    parser.add_argument(
        "--scan",
        required=True,
        type=str,
        help="The hdf5 file containing the scan",
    )
    
    parser.add_argument(
        "--safe_margin",
        required=True,
        type=int,
        help="requirement for the distance between the spot and the border",
    )

    parser.add_argument(
        "--path",
        required=False,
        type=str,
        help="If not given it will be searched and possibly found. If given: the path to the dataset containing the images, like entry0000/1.1/measurement/det2  ",
    )

    parser.add_argument(
        "--target",
        required=False,
        type=str,
        default = "crossings.npy",
        help="The target file where the coordinates will be written ",
    )

    return parser.parse_args()


def find_spots(dataset, args, f, path ):

    NY = int( math.ceil( dataset.shape[0] / args.periodx))
    NX = args.periodx
    
    crossings = np.zeros([NY, NX, 2],"f")
    crossings[:] = np.NAN


    nimages, sy, sx = dataset.shape


    args.dect_shape = sy, sx


    count = 0
    count_iy = 0
    count_ix = 0
    count_py = 0
    count_px = 0
    count_piy = 0
    count_pix = 0
    
    for nradio  in range(nimages):
        f.close()
        f = h5py.File(args.scan)
        dataset = f[path]
        ima=dataset[nradio]
        
        iy = nradio // NX
        ix = nradio % NX

        ima[:args.safe_margin//2]=0
        ima[-args.safe_margin//2:]=0
        ima[:, :args.safe_margin//2]=0
        ima[:, -args.safe_margin//2:]=0
        
        pmax = np.argmax(ima)
        

        p_y = pmax // sx
        p_x = pmax  % sx

        if p_y > args.safe_margin and p_x > args.safe_margin and p_y < sy - args.safe_margin and p_x < sx - args.safe_margin :

            
            crossings[iy, ix, 0 ] = p_y
            crossings[iy, ix, 1 ] = p_x

            count +=1
            count_iy += iy
            count_ix += ix
            count_py += p_y
            count_px += p_x
            count_piy += p_y*iy
            count_pix += p_x*ix

            print(" Adding      ", iy, ix , "  - > ", p_y, p_x)
            
        else:

            if (iy, ix) == (18,12):
                edf=fabio.edfimage.edfimage()
                edf.data = ima
                edf.write("problem.edf")
                edf.close()
                raise
            print(" not adding  ", iy, ix , "  - > ", p_y, p_x)


    count_piy = count_piy - count_iy*count_py / count
    count_pix = count_pix - count_ix*count_px / count

    if count_piy < 0:
        crossings = crossings[::-1, : ]
        
    if count_pix < 0:
        crossings = crossings[:,  ::-1 ]
    

    return crossings
        

def search3Dstack(g, path):
    res = None
    if "measurement" in g:
        g=g["measurement"]
        
        for k in g.keys():
            d = g[k]
            if isinstance(d, h5py.Dataset):
                if(len(d.shape))==3:
                    res =  d
                    break
    if res is not None:
        return res, os.path.join(path, "measurement", k)
    
    return res, None

def main():

    args = get_arguments()

    with h5py.File(args.scan,"r") as f:


        f = h5py.File(args.scan,"r") 
        if args.path:
            dataset, path  = f[args.path], args.path
        else:
            g=f
            keys = list(g.keys())
            keys.sort()
            found = None
            for k in keys:
                found, path = search3Dstack(g[k], k )
                if found :
                    break
            if not found:
                raise RuntimeError(f" Could not find a 3D stack, try to specify the full path to the radios stack")
            print(f" Found dataset at path {found}")
            dataset, path = found, path

        crossings = find_spots(dataset , args, f, path)
        
        for i in range(crossings.shape[0]):
            limit0=None
            for k0, isgood  in enumerate( ~np.isnan(crossings[i])):
                if np.any(isgood):
                    limit0 = k0
                    break
                else:
                    pass
            
            limit1=None
            for k1, isgood in enumerate( ~np.isnan(crossings[i, ::-1])):

                if np.any(isgood):

                    limit1 = k1
                    break
                else:
                    pass

            if limit1 is not None:
                limit1=-limit1
                if limit1==0:
                    limit1=None

            if np.any(np.isnan(crossings[i, limit0:limit1])):
                if i*2 < crossings.shape[0]:
                    crossings[:i+1]=np.NAN
                else:
                    crossings[i:]=np.NAN
                    

        for i in range(crossings.shape[1]):
            limit0=None
            for k0, isgood  in enumerate( ~np.isnan(crossings[:,i])):
                if np.any(isgood):
                    limit0 = k0
                    break
                
            limit1 = None
            for k1, isgood in enumerate( ~np.isnan(crossings[::-1,i])):
                if np.any(isgood):
                    limit1=k1
                    break
                
            if limit1 is not None:
                limit1=-limit1
                if limit1==0:
                    limit1=None
                
            if np.any(np.isnan(crossings[limit0:limit1, i] )):
                if i*2 < crossings.shape[1]:
                    crossings[:, :i+1]=np.NAN
                else:
                    crossings[:, i:]=np.NAN
              

        np.save("crossings.npy", crossings)
        np.save("dect_shape.npy", args.dect_shape)
if __name__ == "__main__"        :
    main()

import argparse
import re
import logging

import json

import night_rail
from ..decisional_trees import rec_tree as bm18_rec_tree

logger = logging.getLogger(__name__)
logger.setLevel("INFO")


def get_arguments():
    parser = argparse.ArgumentParser(description="night_rail demo for Nabu")
    parser.add_argument(
        "-i", "-input_conf", required=False, help="json file containing a previous configuration", default=None
    )
    parser.add_argument(
        "-m",
        required=False,
        help="reads global answer histories (memories ->m) json from a previous configuration without setting the parameters",
        default=None,
    )
    parser.add_argument(
        "--automatic",
        action = "store_true",
        help="Will run throug the whole dialogue, validating proposed default values. Expecially useful if used together with the -m option",
    )
    parser.add_argument(
        "-o",
        "-output_conf",
        required=False,
        help="json file containing a previous configuration",
        default="nrbm18_default.json",
    )
    parser.add_argument(
        "-s",
        "--skip_to",
        required=False,
        type=str,
        help="Skip all question till the question containing the provided string",
        default=None,
    )

    parser.add_argument(
        "-g",
        "--go_to",
        required=False,
        type=str,
        help="Directeth the flow to the singular query containing the given string, thereafter to complete its task",
        default=None,
    )
    return parser.parse_args()


def main(args=None):
    if args is None:
        args = get_arguments()

    ## Loading a previous configuration

    global_histories = None

    if args.i is not None:
        previous_dict = json.load(open(args.i, "r"))

        my_serialiser_tmp = night_rail.SerialiseBackward(previous_dict, top_dict = previous_dict, automatic=args.automatic)
        my_pars = bm18_rec_tree.WorkFlowScheme(my_serialiser_tmp)
    elif args.m is not None:
        previous_dict = json.load(open(args.m, "r"))

        my_pars = bm18_rec_tree.WorkFlowScheme()
        global_histories = previous_dict["global_histories"]

    else:
        my_pars = bm18_rec_tree.WorkFlowScheme()

    # user interaction
    my_pars.configure_scheme(skip_to=args.skip_to, go_to=args.go_to, global_histories=global_histories, automatic=args.automatic)

    # saving parameters
    my_serialiser = night_rail.SerialiseForward()
    my_pars.serialise_forward(my_serialiser)
    with open(args.o, "w") as fp:
        json.dump(my_serialiser, fp, indent=4)

    return 0

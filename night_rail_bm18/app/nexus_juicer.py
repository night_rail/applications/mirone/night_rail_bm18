import sys
from configparser import ConfigParser
from pyicat_plus.client.main import IcatClient
import json
import getpass
import argparse
import os
import h5py
import numpy as np
import glob
import traceback
from nabuxx.etfscan import EtfScan

def special_case_filtering(key, value):
    
    if "scanRadix" in key:
        value = value.rstrip('0')
        
    if "sensorPixelSize" in key:
        value = [round(x*1000,6) for x in value]
        if value[0] == value[1]:
            value = value[0]
            
    if "sensorBinning" in key:
        if value[0] == value[1]:
            value = value[0]
            
    if "optical_magnification" in key:
        value = round(value,4)
        
    if 'samplePixelSize' in key:
        value = value /1000  # put in mm for consistency
            
    return value

def _safe_hdf5_read(hdf_file, key, default=None):
    """
    Read a value from an HDF5 file and return a safe representation of the value.
    """
    try:
        value = hdf_file[key][()]
        if isinstance(value, bytes):
            return value.decode("utf-8")
        elif isinstance(value, (float, np.float64, np.float32, np.int64)):
            return float(value)
        elif isinstance(value, np.ndarray):
            return value.tolist()
        else:
            return str(value)
    except KeyError:
        return default

def safe_update_dictionary(item, data, dic_h5, f):
    """
    Update the dictionary with values read from the HDF5 file.
    """
    key, group, hdf_key, *args = item
    value = _safe_hdf5_read(f, dic_h5[group] + hdf_key)
    if args:  # args for special cases
        if args[0] == -1:  # Handling for special cases where last element is needed
            value = value[-1]
        elif args[0] == 'lower':
            value = value.lower()
        elif args[0] == 'int':
            value = int(value)
           
    value = special_case_filtering(key, value)
            
    if isinstance(value, list):
        value = ''.join(str(value))    #For better readability in the json

    #Improve readability of the json
    try:    
        if value.lower() == "true":
            value = True
        elif value.lower() == "false":
            value = False
    except AttributeError:
        pass
    keys = key.split('/')

    # Traverse and create nested dictionaries
    dd = data["tomo"]["acquisition"]
    for k in keys[:-1]:
        if k not in dd:
            dd[k] = {}
        dd = dd[k]
    
    # Set the value at the final key
    dd[keys[-1]] = value
        

def metadata_dict_init():
    data = {
        "tomo": {
            "acquisition": {},
            "processing": {
                "general": None,
                "preprocessing": None,
                "reconstruction": None,
                "postprocessing": None,
            }
        }
    }
    return data


def extract_metadata_from_h5(h5_name: str) -> None:
    """
    Create a JSON file from an HDF5 file.
    """
    dark = []
    flat = []
    static = []

    with h5py.File(h5_name, "r") as f:
        values = [i for i in f]
        dic_h5 = {value.split("_")[-1]: value for value in values}
        
        scanType, desc, fast_acq, slow_acq = None, None, None, None
        tmp = sorted(dic_h5.keys(), key=lambda x: float(x))

        hel_acq_slow_list = []
        for i in tmp:
            title = str(f[dic_h5[i]]["title"][()])
            if "fullturn" in title:
                scanType = "single scan"
            elif "zseries" in title:
                scanType = "zseries"
            elif "helical" in title:
                scanType = "helical"
            if "tomo" in title:
                desc = i
            if "dark" in title:
                dark.append(i)
            if "flat" in title:
                flat.append(i)
            if "projections" in title:
                fast_acq = i.split(".")[0] + ".1"
                slow_acq = i.split(".")[0] + ".2"
                if scanType == "helical" :
                    hel_acq_slow_list.append( slow_acq)
                    
            if "static images" in title:
                static.append(i)
                
        if None in [scanType, desc, fast_acq, slow_acq]:
            raise RuntimeError("Did not find the necessary scan")

        detector = list(f[dic_h5[desc] + "/technique/detector"])[0]
        sampleDetectorDistance = round(float(_safe_hdf5_read(f, dic_h5[desc] + "/technique/scan/sample_detector_distance")), 2)
        sourceSampleDistance = _safe_hdf5_read(f, dic_h5[desc] + "/technique/scan/source_sample_distance")
        
        try:
            nb_scans = int(_safe_hdf5_read(f, dic_h5[desc] + "/technique/scan/nb_scans"))
        except:
            nb_scans = 1
        

        data = {
            "tomo": {
                "acquisition": {
                    "scanRadix": None,
                    "scanType": scanType,
                    "nb_scans": nb_scans,
                    "date": None,
                    "beamline": None,
                    "machineMode": None,
                    "machineCurrentStart": None,
                    "machineCurrentStop": None,
                    "sampleDetectorDistance": sampleDetectorDistance,
                    "sourceSampleDistance": sourceSampleDistance,
                    "xray_magnification": round((sourceSampleDistance + sampleDetectorDistance) / sourceSampleDistance, 6),
                    "energy": None,
                    "scanRange": None,
                    "rotationMode": None,
                    "half_acquisition": None,
                    "tomo_N": None,
                    "ref_N": None,
                    "dark_N": None,
                    "z_start": None,
                    "z_end": None,
                    "z_step": None,
                    "detector": {
                        "sensor": None,
                        "sensorPixelSize": None,
                        "roi_size": None,
                        "sensorBinning": None,
                        "opticsName": None,
                        "opticsType": None,
                        "optical_magnification": None,
                        "samplePixelSize": None,
                        "scintillator": None,
                    },
                    "acq_mode": None,
                    "expo_time": None,
                    "subframe": None,
                    "latency_time": None,
                    "accumulation": None,
                    "scan_time": None,
                    "comment": None,
                }
            }
        }

        to_add = [
            ["scanRadix", desc, "/sample/name"],
            ["date", desc, "/end_time"],
            ["beamline", desc, "/technique/saving/beamline"],
            ["machineMode", desc, "/instrument/machine/filling_mode"],
            ["machineCurrentStart", desc, "/instrument/machine/current"],
            ["machineCurrentStop", slow_acq, "/measurement/current", -1],
            ["energy", desc, "/technique/scan/energy"],
            ["scanRange", desc, "/technique/scan/scan_range"],
            ["z_start", desc, "/instrument/positioners_start/sz"],
            ["z_end", desc, "/instrument/positioners_end/sz"],
            ["rotationMode", desc, "/technique/scan/scan_type", 'lower'],
            ["half_acquisition", desc, "/technique/scan/half_acquisition"],
            ["tomo_N", desc, "/technique/scan/tomo_n", 'int'],
            ["ref_N", desc, "/technique/scan/flat_n", 'int'],
            ["dark_N", desc, "/technique/scan/dark_n", 'int'],
            ["comment", desc, "/technique/scan/comment"],
            ["z_step", desc, "/technique/scan/delta_pos"],
            ["detector/sensor", desc, f"/technique/detector/{detector}/type"],
            ["detector/sensorPixelSize", desc, f"/technique/detector/{detector}/pixel_size"],
            ["detector/roi_size", desc, f"/technique/detector/{detector}/size"],
            ["detector/sensorBinning", desc, f"/technique/detector/{detector}/binning"],
            ["detector/opticsName", desc, "/technique/optic/name"],
            ["detector/opticsType", desc, "/technique/optic/type"],
            ["detector/optical_magnification", desc, "/technique/optic/magnification"],
            ["detector/samplePixelSize", desc, "/technique/optic/sample_pixel_size"],
            ["detector/scintillator", desc, "/technique/optic/scintillator"],
            ["acq_mode", fast_acq, f"/instrument/{detector}/acq_parameters/acq_mode"],
            ["expo_time", fast_acq, f"/instrument/{detector}/acq_parameters/acq_expo_time"],
            ["subframe", fast_acq, f"/instrument/{detector}/ctrl_parameters/acc_max_expo_time"],
            ["latency_time", fast_acq, f"/technique/proj/latency_time"],
            ["scan_time", fast_acq, f"/measurement/timer_trig", -1]
        ]

        for item in to_add:
            try:
                safe_update_dictionary(item, data, dic_h5, f)
            except:
                print(f"{item[0]} not found")

        if data['tomo']['acquisition']['scanType'] == 'helical':
            my_z_step=0
            for hel_acq_slow in hel_acq_slow_list:
                if hel_acq_slow in dic_h5:
                    my_url = dic_h5[hel_acq_slow] + "/instrument/fscan_parameters/slave_step_size"
                    if my_url in f:
                        my_z_step = max(my_z_step, round(float(_safe_hdf5_read(f, my_url)) * data['tomo']['acquisition']['tomo_N'], 3) )
            data['tomo']['acquisition']['z_step'] = my_z_step
        
        
        data["tomo"]["acquisition"]["accumulation"] = int(round(data["tomo"]["acquisition"]["expo_time"] / data["tomo"]["acquisition"]["subframe"],1))
        
        attenuators = {}
        att_list = [att_axis for att_axis in f[dic_h5[desc] + "/instrument/"] if "wba" in att_axis]
        for att in att_list:
            attenuators[att] = {name: _safe_hdf5_read(f, dic_h5[desc] + f"/instrument/{att}/{name}") for name in f[dic_h5[desc] + f"/instrument/{att}/"]}
        data["tomo"]["acquisition"]["attenuators"] = attenuators

        motors = {name: round(_safe_hdf5_read(f, dic_h5[desc] + f"/instrument/positioners/{name}"), 2) for name in f[dic_h5[desc] + "/instrument/positioners/"]}
        data["tomo"]["acquisition"]["motors"] = motors

        motors_dial = {"yrot_dial": round(_safe_hdf5_read(f, dic_h5[desc] + f"/instrument/positioners_dial_start/yrot"), 2)}
        data["tomo"]["acquisition"]["motors_dial"] = motors_dial

        return data
    
    
    
def traverse_and_collect(config_json, collected_answers):
    for key, value in config_json.items():
        if isinstance(value, dict):
            if "question" in value:
                collected_answers[key] = value["answer"]

            for child_value in value.values():
                if isinstance(value, dict):
                    traverse_and_collect(value, collected_answers)
    return collected_answers
       
def extract_from_nr_metadata(metadata_file):
    """ Extract the metadata from the nr_metadata.json file and add it to the metadata dictionary. """
    
    def filter_general(key, value, metadata):
        metadata['tomo']['processing']['general'][key] = value

    def filter_preprocessing(key, value, metadata):
        keys = key.split('.')
        current_level = metadata['tomo']['processing']['preprocessing']
        
        for part in keys[:-1]:
            if part not in current_level:
                current_level[part] = {}
            current_level = current_level[part]
        
        current_level[keys[-1]] = value
        
    def filter_reconstruction(key, value, metadata):
        metadata['tomo']['processing']['reconstruction'][key] = value
   
    metadata = {
        'tomo': {
            'processing': {
                'general': {},
                'preprocessing': {},
                'reconstruction': {},
                'postprocessing': {}
            }
        }
    }
    
    data = json.load(open(metadata_file, "r"))
    collected_answers = traverse_and_collect(data, {})

    task_list = [
        [filter_general, [
            ('WorkFlowScheme', 'nabu' if collected_answers.get('WorkFlowScheme') == 'n' else collected_answers.get('WorkFlowScheme')),
            ('BlissFirstScan', collected_answers.get('BlissFirstScan')),
            ('BlissLastScan', collected_answers.get('BlissLastScan')),
            ('FlatField', 'From reference scan' if collected_answers.get('DatasetLocation') == 'r' else 'Standard'),
            ('DoubleFlatsScheme', True if collected_answers.get('DoubleFlatsScheme') == 'd' else False),
            ('DetectorDistortionCorrection', True if collected_answers.get('DetectorCorrectionSchemePar') == 'm' else False)
        ]]
    ]

    if collected_answers.get('DatasetLocation') == "r":
        task_list.append(
            [filter_preprocessing, [
            ('virtual_flat_field.N_scan_ref', 2 if collected_answers.get('BlissRefBeginScan') != collected_answers.get('BlissRefEndScan') else 1),
            ('virtual_flat_field.BlissRefBeginScan', collected_answers.get('BlissRefBeginScan')),
            ('virtual_flat_field.BlissRefEndScan', collected_answers.get('BlissRefEndScan')),
            ('virtual_flat_field.ReferenceBySectorScheme', collected_answers.get('ReferenceBySectorScheme')),
            ('virtual_flat_field.NumAngularSectorsRefPar', int(collected_answers.get('NumAngularSectorsRefPar'))),
            ('virtual_flat_field.FrequencySeparationRadiusPar', float(collected_answers.get('FrequencySeparationRadiusPar')))
            ]]
        )
    
    if collected_answers.get('DoubleFlatsScheme') == 'd':

        par_keys = [key for key in collected_answers if "DoubleFlats" in key]
        par_keys.remove("DoubleFlatsScheme")
        pars = []
        for key in par_keys:
            value = collected_answers.get(key)
            if value.isdigit():
                value = int(value)
            pars.append(value)

        par_keys = [f"double_flat_fields.{k}" for k in par_keys]
        task_list.append([filter_preprocessing, list(zip(par_keys, pars))])       

    if collected_answers.get('DetectorCorrectionSchemePar') == 'm':
        task_list.append(
            [filter_preprocessing, [
            ('DetectorDistortionCorrection.MapXzPathPar', collected_answers.get('MapXzPathPar'))
            ]]
        )
        
    task_list.append(
        [filter_reconstruction, [
            ('center_rotation_search.CorSchemePar', 'automatic' if collected_answers.get('CorSchemePar') == 'a' else 'user value'),
            ('center_rotation_search.NearWidthValuePar', collected_answers.get('NearWidthValuePar')),
            ('center_rotation_search.CorWeightSchemePar', 'True' if collected_answers.get('CorWeightSchemePar') == 'w' else 'False'),
            ('center_rotation_search.LowPassValuePar', collected_answers.get('LowPassValuePar')),
            ('center_rotation_search.HighPassValuePar', collected_answers.get('HighPassValuePar')),
            ('center_rotation_search.CompCorThetaIntervalValuePar', collected_answers.get('CompCorThetaIntervalValuePar')),
            ('center_rotation_search.NSubsamplingValuePar', collected_answers.get('NSubsamplingValuePar')),
            ('ChebychevStraighteningPar', collected_answers.get('ChebychevStraighteningPar')),
            ('output_format', collected_answers.get('OutputFormatPar'))
            ]]
    )
    
    for filter_func, tasks in task_list:
        for key, value in tasks:
            try:
                filter_func(key, value, metadata)
            except:
                pass
            
    print("Metadata from night_rail file retrieved")
    return metadata
       
        

def parse_args():

    my_parser = argparse.ArgumentParser()

    # my_parser.add_argument('--output',  type=str, required=False, default="glued.h5")
    my_parser.add_argument('input', action='store', type=str, nargs="+", help="metadata_file nabu_conffile and extracted (target) file")
    my_parser.add_argument("--log_on_book",   action='store_true', help="to activate logging")

    args = my_parser.parse_args()

    return args

# Update metadata instead of replacing it
def dict_update(d, u):
    """Recursively update dictionary `d` with values from `u`."""
    for k, v in u.items():
        if isinstance(v, dict) and isinstance(d.get(k), dict):
            dict_update(d[k], v)
        else:
            d[k] = v

def main():    


    args = parse_args()

    nabu_conf_file = args.input[0]
    extracted_file = args.input[1]
    if len(args.input) == 3:
        metadata_file = args.input[2]
    else:
        metadata_file = None
        
    c = ConfigParser()
    c.read(nabu_conf_file)
    scan_dir = c["dataset"]["etf_location"]

    print(scan_dir)

    scan = EtfScan(scan_dir)

    print("\n🤖 - Creation of the .json file")

    h5_file_list = scan.get_dataset_info().original_bliss_files
    print(" My calibrated voxel is " , scan.get_dataset_info().x_pixel_size     )
    print(" My calibrated voxel is " , scan.get_dataset_info().y_pixel_size     )


    metadata = metadata_dict_init()
    
    try:
        data = extract_metadata_from_h5(h5_file_list[0])
        
        dict_update(metadata, data)  # Apply extracted data to metadata
        
        metadata_last_scan = extract_metadata_from_h5(h5_file_list[-1])
        metadata['tomo']['acquisition']['z_end'] = metadata_last_scan['tomo']['acquisition']['z_end']
        print("Metadata from h5 retrieved")
    except:
        print("Couldn't retrieve h5 metadata")

    if metadata_file:
        try:            
            processing_metadata = extract_from_nr_metadata(metadata_file)    
            metadata['tomo']['processing'] = processing_metadata['tomo']['processing']
        except Exception as e:
            print("Couldn't retrieve nightrail metadata")
            print(f"Error: {e}")
    else:
        print("No nr_metadata.json file provided as third argument")
    try:
        # Extract metadata from nabu conf file
        phase_processing = {
            "method": c["phase"]["method"],
            "delta_beta": float(c["phase"]["delta_beta"]),
            "unsharp_coeff": float(c["phase"]["unsharp_coeff"]),
            "unsharp_sigma": float(c["phase"]["unsharp_sigma"]),
            "unsharp_method": c["phase"]["unsharp_method"],
            "padding_type": c["phase"]["padding_type"]
        }
        reconstruction_processing = {
            "method": c["reconstruction"]["method"],
            "prefix": c["output"]["file_prefix"],
            "rotation_axis_position": c["reconstruction"]["rotation_axis_position"]
        }
        
        # Add list of all rotation axis positions
        CompositeCor_file = glob.glob("*CompositeCorSimpleScript_cors.json")[0]
        CompositeCor_file
        CompositeCor_data = json.load(open(CompositeCor_file, "r"))    
        reconstruction_processing['rotation_axis_position'] = CompositeCor_data['rotation_axis_position_list']
        print('List of rotation axis positions added')
                
        metadata['tomo']['processing']['preprocessing']['phase'] = phase_processing
        metadata['tomo']['processing']['reconstruction'] = reconstruction_processing

        print("Metadata from nabu conf file retrieved")
    except Exception as e:
        print("Couldn't retrieve metadata from conf file")
        print(f"Error: {e}")

    
    try:
        # Add overlap
        overlap = []
        z_translation_m = scan.get_dataset_info().z_translation 
        
        for idx, z in enumerate(z_translation_m[:-1]):
            if z == 0:
                pixel_size = metadata['tomo']['acquisition']['detector']['sensorPixelSize'] 
                value = round((z_translation_m[idx+1] - z_translation_m[idx-1])*1000/pixel_size,2) # Pour Joseph: il serait bien de avoir les unitees dans le nom pixel_size_mm
                                                                                                         # Moi aussi je devrais un jour faire la meme chose dans dataset_info ( z_translation_m"
                overlap.append(value)
        if overlap:
            if 'z_concatenation' not in  metadata['tomo']['processing']['postprocessing']:
                metadata['tomo']['processing']['postprocessing']['z_concatenation']={}
            metadata['tomo']['processing']['postprocessing']['z_concatenation']['overlap'] = overlap
        print("Overlap metadata added")
    except Exception as e:
        print("Couldn't add overlap to metadata")
        print(f"Error: {e}")


    try:
        with open(extracted_file, "w") as f:
            f.write(json.dumps(metadata, indent=4))
            print(f"🤖 - Metadata saved to {extracted_file}\n")
    except Exception as e:
        # Print the exception message
        print(f"An error occurred: {e}")
    
        # Print the stack trace
        traceback.print_exc()

        
        
    if scan_dir.startswith("online"):
        return 0

    try:
        api_key = open("/scisoft/night_rail/bm18_certificate.txt").read()
    except:
        print(" COULD NOT OPEN /scisoft/night_rail/bm18_certificate.txt FOR LOGBOOK CERTIFICATE")
        return 0
    
    work_dir = os.getcwd()
     
    endpoint_url = "https://icatplus.esrf.fr"

    client = IcatClient(
        elogbook_url=endpoint_url,
        elogbook_token=api_key
    )

    beamline = "BM18"
    msg_type = "comment"
    tags = ["night_rail_run"]  # put the tags you want here

    print(" MESSAGE TO LOGBOOK DISACTIVATED FOR THE MOMENT ( waiting for some more features on the logbook ) ")
    
    # try:
    #     # send a text message
    #     client.send_message(
    #         msg=f"""
    #         Night_rail run:
    #            user       : {getpass.getuser()}
    #            work_dir   : {work_dir}
    #            beam_energy: {scan.energy},
    #            voxel_size : {scan.get_pixel_size(unit="um")},
    #            detector_distance_m:  {scan.get_distance(unit="m")}
    #         """,
    #         msg_type=msg_type,
    #         beamline=beamline,
    #         beamline_only=True,
    #         tags=tags,
    #     )

    #     # file_bytes = open( metadata_file ,"rb").read()
    #     # # send all user parameters
    #     # client.send_binary_data(
    #     #     data=file_bytes,  # bytes
    #     #     mimetype="text/plain",
    #     #     msg_type=msg_type,
    #     #     beamline=beamline,
    #     #     beamline_only=True,
    #     #     tags=tags,
    #     # ) 
    # except Exception as e:
    #     print("Error!" + e)
        
    return 0
        

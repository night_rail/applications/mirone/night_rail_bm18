import sys
import os
import json
import shutil
import subprocess
import logging
import traceback

from PyQt5.QtCore import QSize, Qt, QDir, pyqtRemoveInputHook
from PyQt5.QtGui import QImage, QPalette, QBrush, QColor
from PyQt5.QtWidgets import (
    QApplication, QTabWidget, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton,  QTreeWidget, QTreeWidgetItem,
    QCheckBox, QSpinBox, QSlider, QFileDialog, QComboBox, QHBoxLayout, QGridLayout, QMessageBox, QCompleter, QDirModel
)

import pyperclip
from ..decisional_trees import rec_tree 

logger = logging.getLogger(__name__)
logger.setLevel("INFO")



class MainWindow(QTabWidget):
    def __init__(self):
        super().__init__()
        self.w, self.h = None, None
        self.resize(QSize(1200, 800))
        self.show()

        self.widget_1 = ParamsWindow(self)
        self.addTab(self.widget_1, "Parameters")
        self.tabBar().setTabTextColor(0, QColor("yellow"))

        self.widget_2 = LevelWindow(self)
        self.addTab(self.widget_2, "UI_Levels")
        self.tabBar().setTabTextColor(1, QColor("yellow"))
        
        self.widget_3 = TestVarsWindow(self)
        self.addTab(self.widget_3, "Tests")
        self.tabBar().setTabTextColor(2, QColor("yellow"))

    def paintEvent(self, event):
        w, h = self.frameGeometry().width(), self.frameGeometry().height()
        if (w, h) != (self.w, self.h):
            self.w, self.h = w, h
            self.set_background_image()

    def set_background_image(self):
        background_image_file = "/data/scisofttmp/mirone/train_by_night.jpg"
        if os.path.isfile(background_image_file):
            oImage = QImage(background_image_file)
            sImage = oImage.scaled(QSize(self.w, self.h))
            palette = QPalette()
            palette.setBrush(QPalette.Window, QBrush(sImage))
            self.setPalette(palette)

    def to_clipboard(self):
        print("CLIP")
        command = self.compose_command()
        if command:
            pyperclip.copy(command)
        else:
            print("Was not able to compose a command line. Probably a problem with the parameters.")

    def run(self):
        command = self.compose_command()

        if self.needs_gpu():
               # Controlla se siamo su un nodo con GPU provando a eseguire "nvidia-smi"
               try:
                   subprocess.check_output(["nvidia-smi"], stderr=subprocess.STDOUT)
               except (subprocess.CalledProcessError, FileNotFoundError) as e:
                   QMessageBox.warning(
                       self,
                       "Warning no  GPU",
                       "You are on a node without GPU. Your reconstruction might fail",
                       QMessageBox.Ok
                   )
                   # return
        
        if command:
            activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
            os.system(f"""xterm -e bash -l -c "source {activation_script} ; {command} ; bash " &""")
        else:
            print("Was not able to compose a command line. Probably a problem with the parameters.")
            
    def run_test(self):
        command = self.compose_command()
        command = command + ""
        if command:
            variables_activation, test_wf_dir = self.widget_3.read_tree()
            print(" Variables Activation", variables_activation)

            command +=  f" --test_dir {test_wf_dir} "
            for option in ["do_reconstruct", "do_show"]:
                if option not in command:
                    command +=f" --{option} "
                    
            try :
                json_file_name = os.path.join(test_wf_dir, "automaticSession_tested_variables.json")
                with open(json_file_name,"w") as fw:
                    json.dump(variables_activation, fw, indent=4)
            except Exception as e:
                logger.warning(f" could not write the variables activation dictionary to {json_file_name} ")
                traceback.print_exc() 
            else:
                activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
                os.system(f"""xterm -e bash -l -c "source {activation_script} ; {command} ; bash " &""")
        else:
            print("Was not able to compose a command line. Probably a problem with the parameters.")

    def compose_command(self):
        command = "night_rail_bm18_automatic "

        params = {
            "rawdata_dir": self.widget_1.rawdata_dir.text().strip(),
            "name_root": self.widget_1.name_root.text().strip(),
            "exclude_fragment": self.widget_1.exclude_fragment.text().strip(),
            "nobackup_dir": self.widget_1.nobackup_dir.text().strip(),
            "distortion_map_file": self.widget_1.distortion_map_file.text().strip(),
            "target": self.widget_1.target.text().strip(),
            "max_concurrent_spawns": self.widget_1.max_concurrent_spawns.value(),
            "visu_timeout": self.widget_1.visu_timeout.value() * 60,
            "batch_reconstruction_every":self.widget_1.batch_reconstruction_every.value(),
        }

        for param, value in params.items():
            if value:
                command += f"--{param} {value} "

        options = [
            (self.widget_1.do_spawn.isChecked(), "do_spawn"),
            (self.widget_1.do_reconstruct.isChecked(), "do_reconstruct"),
            (self.widget_1.do_show.isChecked(), "do_show"),
            (self.widget_1.online.isChecked(), "online"),
            (self.widget_1.keep_going.isChecked(), "keep_going"),
            (self.widget_1.degroup.isChecked(), "degroup"),
            (self.widget_1.check_overwrite_batch.isChecked(), "overwrite_previous_batches"),
            (self.widget_1.check_batch_offline.isChecked(), "batch_offline"),
        ]

        for condition, flag in options:
            if condition:
                command += f"--{flag} "

        ui_levels = [
            ("ui_level_unsharp", self.widget_2.combobox_unsharp.currentText()),
            ("ui_level_data_selection", self.widget_2.combobox_data_selection.currentText()),
            ("ui_level_paganin_phase", self.widget_2.combobox_paganin_phase.currentText()),
            ("ui_level_cor_search", self.widget_2.combobox_cor_search.currentText()),
            ("ui_level_generic", self.widget_2.combobox_generic.currentText()),
            ("ui_level_calculation", self.widget_2.combobox_calculation.currentText()),
            ("ui_level_stripes", self.widget_2.combobox_stripes.currentText()),
            ("ui_level_scintillator", self.widget_2.combobox_scintillator.currentText())
        ]

        for param, value in ui_levels:
            command += f"--{param} {value} "

        if self.widget_2.auto_absorption.isChecked():
            command += " --auto_absorption "

        if self.widget_2.cor_followup.isChecked():
            command += " --cor_followup "
            
        if self.widget_2.wobble_correction.isChecked():
            command += " --wobble_correction "

        if self.widget_2.use_modified_defaults.isChecked():
            filename = "_tmp_by_gui_modified_defaults.json"
            print("SAVING DEFAULTS TO FILE", filename)
            try:
                with open(filename, "w") as fd:
                    json.dump(self.widget_2.global_dict_for_defaults, fd, indent=4)
                command += f" --defaults_file {filename} "
            except IOError as e:
                print(f"Error saving defaults to file: {e}")

        return command


    def needs_gpu(self):
        do_spawn =  self.widget_1.do_spawn.isChecked()
        do_reconstruct = self.widget_1.do_reconstruct
        online = self.widget_1.online.isChecked()

        return (not do_spawn) and (  do_reconstruct or online)

    
class ParamsWindow(QWidget):
    def __init__(self, parent):
        super().__init__()

        self.parentw = parent

        hlayout = QHBoxLayout()
        hdum = QWidget()
        glayout = QGridLayout()

        glayout.setRowStretch(0,5)

        self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }")

        self.setup_params_ui(glayout)
        hdum.setLayout(glayout)

        hlayout.addWidget(hdum)
        hlayout.insertStretch(1, 4)

        self.setLayout(hlayout)
        self.show()

    def setup_params_ui(self, layout):
        labels_and_tooltips = [
            ("name_root", "the name root of the scan for which reconstruction is wished", False),
            ("rawdata_dir", "The directory which will be searched recursively for scans", True),
            ("target", "The name of the subdirectory, relatively to the nobackup directory, under which the processed scan will be placed. If not given, it will be derived from date/time", False),
            ("nobackup_dir", "If not given it will be NOBACKUP subdir, placed at the same level as the root directory (typically RAWDATA). Optionally you can give the full path of a directory of your choice", False),
            ("batch_reconstruction_every", "If zero then no batch reconstruction, otherwise every N slices with N given here. If 1 then reconstruct all slices.", False),
            ("do_spawn", "Spawn non-online scans on slurm", False),
            ("max_concurrent_spawns", "limits the maximum number of slurm processes that are running concurrently at any moment", False),
            ("do_reconstruct", "Already implicit for online scans. Launch automatically the reconstructions", False),
            ("do_show", "Already implicit for online scans. Launch automatically the visualization for the reconstructions if any", False),
            ("only_with_reference", "Considers only scan with reference", False),
            ("keep_going", "Automatically relaunches itself with --after time set to the last end time", False),
            ("visu_timeout", "limits the time (in minutes) during which the popped-up visualization remains on screen", False),
            ("online", "Follow online scans", False),
            ("exclude_fragment", "Name fragments which must not appear in the scan name", False),
            ("degroup", "Z-series will be considered as separated scans. Useful for online follow-up.", False),
            ("distortion_map_file", "If given a hint will be passed to the dialog to avoid searching for it.", False)
        ]

    
        for i, (label_text, tooltip, add_file_completer) in enumerate(labels_and_tooltips, start=1):
            # i=i+1
            lab = QLabel(label_text)
            lab.setToolTip(tooltip)
            layout.addWidget(lab, i, 0)
            if label_text in ["batch_reconstruction_every", "max_concurrent_spawns", "visu_timeout"]:
                widget = QSpinBox() if label_text == "batch_reconstruction_every" else QSlider(Qt.Horizontal)
                widget.setMinimum(0)
                widget.setMaximum(10000 if label_text == "batch_reconstruction_every" else 16)
                widget.setValue(0)
                layout.addWidget(widget, i, 1)
                if label_text != "batch_reconstruction_every":
                    label = QLabel("1")
                    widget.valueChanged.connect(label.setNum)
                    layout.addWidget(label, i, 2)
                else:
                    self.check_overwrite_batch = LabeledCheckbox("Overwite Previous")
                    layout.addWidget( self.check_overwrite_batch, i, 2)
                    self.check_batch_offline = LabeledCheckbox("batch also offline")
                    layout.addWidget( self.check_batch_offline, i, 3)
            else:
                widget = QCheckBox() if label_text.startswith("do_") or label_text.startswith("only_") or label_text in ["online","keep_going","degroup"] else QLineEdit()
                layout.addWidget(widget, i, 1)
                if label_text == "rawdata_dir" or label_text == "nobackup_dir" or label_text == "distortion_map_file":
                    search_button = QPushButton(f"Search {label_text}")
                    layout.addWidget(search_button, i, 2)
                    widget.setFixedWidth(450)
                    search_button.clicked.connect(getattr(self, f"search_{label_text}"))
                if label_text == "rawdata_dir":
                    assert isinstance(widget, QLineEdit)
                    completer = QCompleter()
                    folder_model = QDirModel(completer)
                    folder_model.setFilter(QDir.Filter.AllDirs)  # display only folders
                    completer.setModel(folder_model)
                    widget.setCompleter(completer)

                if label_text == "rawdata_dir":
                    lab.setStyleSheet("color: red")

            setattr(self, label_text, widget)
                    
        self.copy_command = QPushButton("Copy to ClipBoard")
        layout.addWidget(self.copy_command, len(labels_and_tooltips) + 2, 3)
        self.copy_command.clicked.connect(self.parentw.to_clipboard)

        self.run_command = QPushButton("Run")
        layout.addWidget(self.run_command, len(labels_and_tooltips) + 2, 4)
        self.run_command.clicked.connect(self.parentw.run)

    def search_rawdata_dir(self):
        self.search_directory(self.rawdata_dir)

    def search_nobackup_dir(self):
        self.search_directory(self.nobackup_dir)

    def search_distortion_map_file(self):
        self.search_file(self.distortion_map_file)

    def search_directory(self, line_edit):
        starting_dir = line_edit.text() or os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[starting_dir.rfind("/data/visitor"):]
        result = QFileDialog.getExistingDirectory(self, caption='Choose Directory', directory=starting_dir)

        if result:
            line_edit.setText(result)

    def search_file(self, line_edit):
        starting_dir = line_edit.text() or os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[starting_dir.rfind("/data/visitor"):]
        result, _ = QFileDialog.getOpenFileName(self, caption='Choose File', directory=starting_dir)
        if result:
            line_edit.setText(result)


class LevelWindow(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.parentw = parent

        self.global_dict_for_defaults = {}

        hlayout = QHBoxLayout()
        hdum = QWidget()
        glayout = QGridLayout()

        glayout.setRowStretch(0,5)

        
        self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }")

        self.setup_level_ui(glayout)
        hdum.setLayout(glayout)

        hlayout.addWidget(hdum)
        hlayout.insertStretch(1, 4)

        self.setLayout(hlayout)
        self.show()

    def setup_level_ui(self, layout):
        choices = ["predefined", "simple", "advanced"]
        labels_and_tooltips = [
            ("unsharp", "punctiliousness level for unsharp"),
            ("data_selection", "punctiliousness level for data selection"),
            ("paganin_phase", "punctiliousness level for paganin phase"),
            ("cor_search", "punctiliousness level for cor search"),
            ("generic", "punctiliousness level for various items: entry name, double_flats averaging pagopag, bliss_voxel_size redefinition (for big problems in binning)"),
            ("weights", "punctiliousness level fractional_threshold ( borders)...."),
            ("calculation", "punctiliousness level for calculation: backprojection algorithm, chunk size"),
            ("stripes", "punctiliousness level for stripes"),
            ("scintillator", "punctiliousness level for scintillator")
        ]

        layout.setRowStretch(0,5)

        for i, (label_text, tooltip) in enumerate(labels_and_tooltips, start=1):
            #i=i+1
            lab = QLabel(label_text)
            lab.setToolTip(tooltip)
            layout.addWidget(lab, i, 0)
            combobox = QComboBox()
            combobox.addItems(choices)
            combobox.setCurrentIndex(0)
            layout.addWidget(combobox, i, 1)
            modify_button = QPushButton("Modify defaults")
            layout.addWidget(modify_button, i, 2)
            modify_button.clicked.connect(lambda _, handle=label_text: self.modify_defaults(handle))


            assert not hasattr(self, label_text )
            setattr(self, "combobox_"+label_text, combobox)

        print(" QUA ", self.combobox_data_selection)


            
        lab = QLabel("auto_absorption")
        lab.setToolTip("use the scan itself as a reference")
        layout.addWidget(lab, len(labels_and_tooltips) + 2, 0)
        self.auto_absorption = QCheckBox()
        layout.addWidget(self.auto_absorption, len(labels_and_tooltips) + 2, 1)

        lab = QLabel("cor_followup")
        lab.setToolTip("retrieve the cor drift")
        layout.addWidget(lab, len(labels_and_tooltips) + 3, 0)
        self.cor_followup = QCheckBox()
        layout.addWidget(self.cor_followup, len(labels_and_tooltips) + 3, 1)

        
        lab = QLabel("big sample stage")
        lab.setToolTip("activates the wobble correction")
        layout.addWidget(lab, len(labels_and_tooltips) + 4, 0)
        self.wobble_correction = QCheckBox()
        layout.addWidget(self.wobble_correction, len(labels_and_tooltips) + 4, 1)

        lab = QLabel("Use modified defaults values")
        lab.setToolTip("Such defaults can be explored with the predialog buttons")
        layout.addWidget(lab, len(labels_and_tooltips) + 5, 0)
        self.use_modified_defaults = QCheckBox()
        layout.addWidget(self.use_modified_defaults, len(labels_and_tooltips) + 5, 1)

        store_button = QPushButton("store current defaults")
        layout.addWidget(store_button, len(labels_and_tooltips) + 3, 2)
        store_button.clicked.connect(self.store_current_defaults)

        load_button = QPushButton("load previous defaults")
        layout.addWidget(load_button, len(labels_and_tooltips) + 3, 3)
        load_button.clicked.connect(self.load_previous_defaults)

    def modify_defaults(self, handle):
        default_collector = rec_tree.DefaultCollector() if rec_tree else None
        if default_collector:
            self.global_dict_for_defaults = default_collector.get_handle_defaults(handle, global_dict=self.global_dict_for_defaults)
            rec_tree.install_defaults(self.global_dict_for_defaults)

    def load_previous_defaults(self):
        starting_dir = os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[starting_dir.rfind("/data/visitor"):]
        result, _ = QFileDialog.getOpenFileName(self, caption='Chose name for the to be written json file with defaults values', directory=starting_dir)
        if result and rec_tree:
            print("LOADING DEFAULTS FROM FILE", result)
            rec_tree.install_defaults(result)

    def store_current_defaults(self):
        starting_dir = os.getcwd()
        if "/data/visitor" in starting_dir:
            starting_dir = starting_dir[starting_dir.rfind("/data/visitor"):]
        filename, _ = QFileDialog.getSaveFileName(self, "Chose name for the to be written json file with defaults values", starting_dir, "Json Files (*.json)")
        if filename:
            print("SAVING DEFAULTS TO FILE", filename)
            try:
                with open(filename, "w") as fd:
                    json.dump(self.global_dict_for_defaults, fd, indent=4)
            except IOError as e:
                print(f"Error saving defaults to file: {e}")

class TestVarsWindow(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parentw=parent
        # Main horizontal layout
        hlayout = QHBoxLayout()

        # Grid layout for the tree and buttons
        self.glayout = QGridLayout()
        self.glayout.setRowStretch(0, 5)

        self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }")

        self.setLayout(hlayout)

        # Tree widget
        self.tree = QTreeWidget(self)
        self.tree.setHeaderHidden(True)

        self.tree.setStyleSheet("background: transparent;")

        # Set tree widget transparency
        self.tree.setStyleSheet("""
            QTreeWidget {
                background: rgba(255, 255, 255, 0.4);  /* Adjust the alpha value as needed */
            }
            QTreeWidget::indicator {
                width: 20px;
                height: 20px;
            }
            QTreeWidget::indicator:unchecked {
                border: 1px solid black;
                background-color: white;
            }
            QTreeWidget::indicator:checked {
                border: 1px solid black;
                background-color: black;
            }
            QTreeWidget::indicator:checked:hover {
                border: 2px solid black;
                background-color: gray;
            }
            QTreeWidget::indicator:unchecked:hover {
                border: 2px solid black;
                background-color: lightgray;
            }
        """)
        
        self.glayout.addWidget(self.tree, 0, 0)

        # Load and display button
        self.load_display_button = QPushButton('Load and Display JSON', self)
        self.load_display_button.clicked.connect(lambda: self.load_and_display_json(self.directory_selector.lineEdit.text()))
        self.glayout.addWidget(self.load_display_button, 1, 0)

        self.directory_selector = DirectorySelector()
        self.glayout.addWidget(self.directory_selector, 2, 0)
        
        # Test button
        self.test_button = QPushButton('Run Test', self)
        self.glayout.addWidget(self.test_button, 3, 0)
        self.test_button.clicked.connect(self.parentw.run_test)

        # Add the grid layout to the horizontal layout
        hlayout.addLayout(self.glayout)

        # Add a stretch to the right
        hlayout.addStretch()
        
        # super().__init__(parent)
        
        
        # hlayout = QHBoxLayout()
        # hdum = QWidget()
        # self.glayout = QGridLayout()
        # self.glayout.setRowStretch(0,5)

        # self.setStyleSheet("QLabel { color: blue; background-color: rgb(255, 255, 255); font-weight: bold; font-size: 200% }")

        # self.setLayout(hlayout)
        
        # self.glayout.setRowStretch(0,5)
        # hlayout.insertStretch(0, 1)

        # # self.layout =  QVBoxLayout(self)
        
        # self.tree = QTreeWidget(self)
        # self.tree.setHeaderHidden(True)
        # self.glayout.addWidget(self.tree,0,0)
        
        # self.load_display_button = QPushButton('Load and Display JSON', self)
        # self.load_display_button.clicked.connect(lambda: self.load_and_display_json())
        # self.glayout.addWidget(self.load_display_button,1,0)
        
        # self.test_button = QPushButton('Run Test', self)
        # ## self.test_button.clicked.connect(lambda: print_and_return(self.read_tree(file_name)))
        # self.glayout.addWidget(self.test_button,2,0)

        self.json_data = None
    
    def load_and_display_json(self, dir_path):
        try:
            wf_dir = dir_path
                
            file_name = os.path.join(wf_dir, "automaticSession_nr_metadata.json")
            self.test_wf_dir = wf_dir
            
            with open(file_name, 'r') as file:
                self.json_data = json.load(file)
            
            self.populate_tree(self.json_data)
            self.tree.expandAll()

            
        except Exception as e:
            QMessageBox.critical(self, "Error", f"Failed to read wf file: {e}")
            self.tree.clear()
            
    def populate_tree(self, data, parent_item=None):
        if parent_item is None:
            self.tree.clear()
            parent_item = self.tree.invisibleRootItem()
        
        for key, value in data.items():
            if isinstance(value, dict):
                item = QTreeWidgetItem([key])
                parent_item.addChild(item)
                if "question" in value and len(value)>=2:
                    item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
                    item.setCheckState(0, Qt.Unchecked)
                if "question" in value:
                    item.setToolTip(0, f'Question:{value["question"]}')
                self.populate_tree(value, item)
                
    def populate_tree_old(self, data, parent_item=None):
        if parent_item is None:
            self.tree.clear()
            parent_item = self.tree.invisibleRootItem()
        
        for key, value in data.items():
            if isinstance(value, dict):
                if "question" not in data:
                    item = QTreeWidgetItem([key])
                    parent_item.addChild(item)
                    self.populate_tree(value, item)
                else:
                    item = QTreeWidgetItem([key])
                    item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
                    item.setCheckState(0,  Qt.Unchecked)
                    parent_item.addChild(item)
    
    def read_tree(self, item=None):
        if item is None:
            item = self.tree.invisibleRootItem()
        
        return self.read_tree_item(item),  self.directory_selector.lineEdit.text()

    
    def read_tree_item(self, item):
        data = {}
        for i in range(item.childCount()):
            child = item.child(i)
            if child.childCount() > 0:
                data[child.text(0)] = self.read_tree_item(child)
            else:
                data[child.text(0)] = child.checkState(0) == Qt.Checked
        return data


def main():

    
    pyqtRemoveInputHook()  # to avoid event loop warning when interacting with terminal input

    app = QApplication(sys.argv)
    oMainwindow = MainWindow()
    sys.exit(app.exec_())

    
class DirectorySelector(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        # Create horizontal layout
        hbox = QHBoxLayout()

        # Create QLineEdit
        self.lineEdit = QLineEdit(self)
        self.lineEdit.setToolTip('Input here the directory with the work-flow to be retested')  # Set tooltip
        hbox.addWidget(self.lineEdit)

        # Create QPushButton
        self.button = QPushButton('Select Directory', self)
        self.button.setToolTip('select the directory with work-flow to be retested')  # Set tooltip
        self.button.clicked.connect(self.showDialog)
        hbox.addWidget(self.button)

        # Set main layout
        self.setLayout(hbox)

    def showDialog(self):
        # Get the current text from QLineEdit
        current_dir = self.lineEdit.text()
        
        # Open directory selection dialog starting from current_dir if it exists
        if current_dir:
            dir_path = QFileDialog.getExistingDirectory(self, 'Select Directory', current_dir)
        else:
            dir_path = QFileDialog.getExistingDirectory(self, 'Select Directory')

        # Set the directory path in the QLineEdit
        if dir_path:
            self.lineEdit.setText(dir_path)



class LabeledCheckbox(QWidget):
    def __init__(self, label_text, parent=None):
        super().__init__(parent)

        self.initUI(label_text)

    def initUI(self, label_text):
        # Create a QHBoxLayout instance for the checkbox and label
        layout = QHBoxLayout()

        # Create a QLabel
        self.label = QLabel(label_text, self)

        # Create a QCheckBox
        self.checkbox = QCheckBox('', self)

        # Add the label and checkbox to the horizontal layout
        layout.addWidget(self.label)
        layout.addWidget(self.checkbox)

        # Set the layout for this widget
        self.setLayout(layout)

    def isChecked(self):
        return self.checkbox.isChecked()

    
if __name__ == "__main__":
    main()

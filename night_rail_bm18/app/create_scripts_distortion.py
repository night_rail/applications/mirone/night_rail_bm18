import argparse
import re
import logging

import readline
import json
import glob
import os
import shutil
import glob
import textwrap

import night_rail
import night_rail_bm18
from ..decisional_trees import (
    distortion_calibration_rec_tree as bm18_rec_tree,
    distortion_calibration_scripts as bm18_scripts,
    utils,
)


logger = logging.getLogger(__name__)
logger.setLevel("INFO")


def get_arguments():
    parser = argparse.ArgumentParser(
        description="night_rail for bm18.  Scripts generation for distortion calibration."
    )
    parser.add_argument(
        "-i",
        "-input_conf",
        required=False,
        help="json file containing a previous configuration",
        default="nabu_rail_default.json",
    )

    return parser.parse_args()


def main():
    args = get_arguments()

    ## Loading a previous configuration

    previous_dict = json.load(open(args.i, "r"))
    version = previous_dict["__version__"]

    my_serialiser_tmp = night_rail.SerialiseBackward(previous_dict, top_dict = previous_dict)

    
    my_pars = bm18_rec_tree.WorkFlowScheme(my_serialiser_tmp)

    # script generation
    all_scripts = bm18_scripts.get_all_scripts()

    my_pars.b_workflow_build(all_scripts)

    session_name = my_pars.b_get_session_name()

    ## remove old scripts
    old_scripts = glob.glob(session_name + "_*.nrs")
    for fn in old_scripts:
        print(" removing old script ", fn)
        os.remove(fn)

    all_scripts.b_dump_scripts(session_name=session_name, version=version)

    utils.save_night_rails_for_compability(session_name)

    vintage_installation_relative_dir = session_name + "_night_rail_vintage"
    night_rail.BaseScript.to_be_moved.extend([vintage_installation_relative_dir])

    night_rail.save_conf_and_glossary(previous_dict, session_name, os.getcwd())

    # # saving parameters
    # my_serialiser = night_rail.SerialiseForward()
    # my_pars.serialise_forward(my_serialiser)
    # with open(args.o, 'w') as fp:
    #     json.dump(my_serialiser, fp, indent=4)

    return 0

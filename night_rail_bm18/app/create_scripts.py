import argparse
import re
import logging

import readline
import json
import glob
import os
import shutil
import glob
import textwrap
import time

import night_rail
import night_rail_bm18
from ..decisional_trees import rec_tree as bm18_rec_tree, scripts as bm18_scripts, utils

logger = logging.getLogger(__name__)
logger.setLevel("INFO")


def get_arguments():
    parser = argparse.ArgumentParser(description="night_rail for bm18. Scripts generation.")
    parser.add_argument(
        "-i",
        "-input_conf",
        required=False,
        help="json file containing a previous configuration",
        default="nabu_rail_default.json",
    )

    return parser.parse_args()


def main(conf_file = None):

    if not conf_file:
        args = get_arguments()
        conf_file = args.i
    
    ## Loading a previous configuration

    previous_dict = json.load(open(conf_file, "r"))
    version = previous_dict["__version__"]
    my_serialiser = night_rail.SerialiseBackward(previous_dict, top_dict = previous_dict)

    my_pars = bm18_rec_tree.WorkFlowScheme(my_serialiser)

    # script generation
    all_scripts = bm18_scripts.get_all_scripts()

    my_pars.b_workflow_build(all_scripts)

    session_name = my_pars.b_get_session_name()

    ## remove old scripts
    old_scripts = glob.glob(session_name + "_*.nrs")
    for fn in old_scripts:
        print(" removing old script ", fn)
        os.remove(fn)

    script_by_name = all_scripts.b_dump_scripts(session_name=session_name, version=version)

    write_custom_automatic_scripts(session_name, script_by_name)
    
    utils.save_night_rails_for_compability(session_name)

    vintage_installation_relative_dir = session_name + "_night_rail_vintage"
    night_rail.BaseScript.to_be_moved.extend([vintage_installation_relative_dir])

    night_rail.save_conf_and_glossary(previous_dict, session_name, os.getcwd())

    # # saving parameters
    # my_serialiser = night_rail.SerialiseForward()
    # my_pars.serialise_forward(my_serialiser)
    # with open(args.o, 'w') as fp:
    #     json.dump(my_serialiser, fp, indent=4)

    return 0



def write_custom_automatic_scripts(session_name, script_by_name):


    my_outputs = []
    for my_name, my_script in script_by_name.items():
        if my_name.endswith("NabuFinalRunScript.nrs"):
            my_outputs = my_script.b_provided_outputs


    with open(f"{session_name}_run_ewoks.sh", "w") as fp:
        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
        s_reconstruct = "#!/bin/bash\n" + textwrap.dedent(
            rf"""
            #SBATCH --nodes=1
            #SBATCH --tasks-per-node=1
            #SBATCH --cpus-per-task=32
            #SBATCH --mem=850G
            #SBATCH --time=2000
            #SBATCH --output=phase-%A.%a.out
            #SBATCH --dependency=singleton
            #SBATCH --job-name="night_rail_{time.time()}"
            #SBATCH --gres=gpu:1
            #SBATCH -C a40
            #SBATCH -p bm18
            #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
            source {activation_script}
            
            ewoks execute {session_name}_ewoks_wf.nrw -p"_raise_on_error=true --inputs=all "
            error_code=$?
            if (( $error_code != 0 )) ; then 
                echo  " ERROR running the ewoks workflow "
                exit 1
            fi
            """ )
        fp.write(s_reconstruct)
            
    with open(f"{session_name}_visualise_check.sh", "w") as fp:
        s_visualise = "#!/bin/bash\n" + textwrap.dedent(
            rf"""          
            if [ "$#" -eq 0 ]; then   
               tmout=100000                                                                                                                                                                                           tmout=10000
            else
               tmout=$1
            fi
            output_dir_list=( {" ".join( my_outputs  )} )
            if [ ${{#output_dir_list[@]}} != 0 ]; then
              for outdir in $output_dir_list ; do
                 file_list=($(ls -t $outdir | grep -v histogram ))
                 if [ ${{#file_list[@]}} != 0 ]; then
                     file=${{file_list[0]}}
                    if [[ $file != *5 ]] ; then
                       timeout -k9 $tmout  imagej "${{outdir}}/${{file}}" >& /dev/null &
                    else
                       timeout -k9 $tmout  silx view "${{outdir}}/${{file}}" >& /dev/null &
                    fi
                 fi
              done                    
            fi
            """)
        fp.write(s_visualise)
    with open(f"{session_name}_run_visualise_check.sh", "w") as fp:
        s_visualise = "#!/bin/bash\n" + textwrap.dedent(
            rf"""                
            if [ "$#" -eq 0 ]; then
                 sbatch --wait ./{session_name}_run_ewoks.sh
                 timeout=10000
            else
                 sbatch  --job-name $1  --wait ./{session_name}_run_ewoks.sh
                 timeout=$2
            fi
            error_code=$?
            if (( $error_code != 0 )) ; then 
                echo  " cannot visualise results in directory $PWD because sbatch of the ewoks workflow return a non null error code "
                exit 1
            else
               ./{session_name}_visualise_check.sh $timeout
            fi
            
            """)
        fp.write(s_visualise)
      
    with open(f"{session_name}_run_to_tiff.sh", "w") as fp:
        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
        s_conversion = "#!/bin/bash\n" + textwrap.dedent(
            rf"""
            #SBATCH --nodes=1
            #SBATCH --tasks-per-node=1
            #SBATCH --cpus-per-task=32
            #SBATCH --mem=850G
            #SBATCH --time=2000
            #SBATCH --output=to_tiff-%A.%a.out
            #SBATCH --dependency=singleton
            #SBATCH --job-name="night_rail_{time.time()}"
            #SBATCH --gres=gpu:1
            #SBATCH -C a40
            #SBATCH -p bm18
            #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
            source {activation_script}
            
            night_rail_bm18_to_tiff . --session_name {session_name}
            
            error_code=$?
            if (( $error_code != 0 )) ; then 
                echo  " ERROR running the to_tiff "
                exit 1
            fi
            """ )
        fp.write(s_conversion)

    
    os.chmod( f"{session_name}_visualise_check.sh"   , 0o744)
    os.chmod( f"{session_name}_run_ewoks.sh"   , 0o744)
    os.chmod( f"{session_name}_run_visualise_check.sh"   , 0o744)
    os.chmod( f"{session_name}_run_to_tiff.sh"   , 0o744)


    print(f"FINSHED CREATING SCRIPTS IN : {os.getcwd()}")
    


import os
import sys
import shutil
import h5py
import random

def main():

    if len(sys.argv)==1:
        usage = """
        Usage: cutscan directory_with_scanxxxx  max_int_part
        Usage: cutscan directory_with_scanxxxx  max_int_part cut_to
        """
        print(usage)
        return 1
    
    target_dir = os.path.abspath(sys.argv[1])

    nome = os.path.basename(target_dir)

    source_file = os.path.join( target_dir, "_" + nome + ".h5" )

    target = os.path.join( target_dir,  nome + ".h5" )

    if len(sys.argv) == 2:
        shutil.copy(source_file, target)
        return
    
    max_scan_int_part = int(sys.argv[2])
    
    shutil.copy(source_file, "tmp.h5")
    with h5py.File("tmp.h5","r+") as f:
        for k in f.keys():
            if is_float(k):
                num = int(float(k))
                if num > max_scan_int_part:
                    del(f[k])

    with h5py.File("tmp.h5","r+") as f:
        scan = f[ f"{max_scan_int_part}.1"]                    
        visitor_func = VisitorFuncEstimated(0)
        scan.visititems(visitor_func)
        scan.visititems(visitor_func)
        print("dimensione estimated is ", visitor_func.n_estimated)
        if len(sys.argv) == 3:
            return 0
        visitor_func = VisitorFuncCut( visitor_func.n_estimated  , int(sys.argv[3]) )
        scan.visititems(  visitor_func)
        
    with h5py.File("tmp.h5","r+") as f:
        g = f[ f"{max_scan_int_part}.1/instrument/det4"]                    
        del g["image"]
        del g["data"]
        del g["acq_parameters"]
        g = f[ f"{max_scan_int_part}.1"]
        del g["end_time"]
        
class VisitorFuncEstimated:

    def __init__(self, n_estimated ):
        self.n_estimated = n_estimated
        
    def __call__(self, name, node):

        if "period" in name:
            return
        
        if isinstance(node, h5py.Dataset):
            
            if len(node.shape) ==1:


                if self.n_estimated ==0:
                    
                    self.n_estimated = node.shape[0]
                
                print(   name,  node.shape[0] ) 
                if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :

                    
                    if node.shape[0] < self.n_estimated:
                        
                        self.n_estimated = node.shape[0]
                        
                elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                    
                    if node.shape[0] < self.n_estimated * 2:
                        
                        self.n_estimated = node.shape[0]// 2
        else:
            pass

class VisitorFuncCut:
    def __init__(self, n_estimated,  cut_to ):
        self.cut_to = cut_to
        self.n_estimated = n_estimated
    def __call__(self, name, node):
        if isinstance(node, h5py.Dataset):
            if len(node.shape) ==1:
                if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :
                    node.resize((self.cut_to - random.randint(0,200)   ,))
                elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                    node.resize((2*(self.cut_to - random.randint(0,200)),))
        else:
            pass
            # print(node, " is a group ")


def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False




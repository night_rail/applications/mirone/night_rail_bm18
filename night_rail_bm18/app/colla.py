import h5py
from silx.io.h5py_utils import File as SilxFile
from datetime import datetime

import numpy as np
from collections import namedtuple
import sys
import os
import argparse


def parse_args():

    my_parser = argparse.ArgumentParser()


    # my_parser.add_argument('--output',  type=str, required=False, default="glued.h5")
    my_parser.add_argument('input', action='store', type=str, nargs="+")
    
    my_parser.add_argument("--output_dir", type=str,   help="the glued scan will be created there")

    my_parser.add_argument("--integer_cut",   action='store_false', help="Try to cut at zero degree ( include only multiples of 360 turns)")

    args = my_parser.parse_args()

    if len(args.input) < 2:
        message = f"you must specify two or more files to be glued together. The input contained only {len(args.input)} items"
        raise ValueError(message)

    args = my_parser.parse_args()

    target_dir = os.path.commonprefix( args.input  ) +"_glued"
    output_name = os.path.basename(target_dir)+".h5"

    
    args.output = os.path.join(args.output_dir, os.path.splitext(os.path.basename(output_name))[0] , output_name)

    print("I would write on ", args.output )
    
    os.makedirs(  os.path.dirname(args.output)  , exist_ok=True)

    return args

def main(args=None):

    if args is None:
        args = parse_args()

    file_list = args.input

    pieces = []

    all_scan_numbers=[]
    
    past_helical = False  # will become true after first helical scan
    
    for fn in file_list:
        is_first = (fn == file_list[0 ]) 
        is_last  = (fn == file_list[-1])

        if is_first:
            pieces.append(   [ fn, f"1.1", False, None, None ])
            
        with SilxFile(fn,"r") as fr:

            if False and "1.1/measurement/scan_numbers" in fr:
                # this was the way to deal with useful scans at the time of bliss1
                scan_numbers = fr["1.1/measurement/scan_numbers"][()]
            else:
                # this works with both bliss1 and bliss2
                keys = list(fr.keys())
                keys.sort()
                scan_numbers = []
                print(keys)
                for k in keys:
                    print(k)
                    if 1:
                        print("?")
                        val = float(k)
                        val2 = val - int(val)
                        if val2> 0.15:
                            continue
                        print("K ", k)
                        val=int(val)
                        print(" === ",  val)
                        
                        if val > 1:
                            
                            scan_numbers.append(val)
                    # except:
                    #     continue


            print(f" SCAN NUMBERS FOR FILE {fn} : {scan_numbers}        .  The check on the file : it is  {is_last}  that it is the last file)")
        
            for sn in scan_numbers:
                is_helical =  (f"{sn}.1/measurement"  in fr)  and  ("mrsz_center" in fr[f"{sn}.1/measurement"]  or "acs_sz_center" in fr[f"{sn}.1/measurement"])

                if is_helical:
                    if "mrsz_center" in fr[f"{sn}.1/measurement"]:
                        VisitorPrepareCut.sz_center_name =  "mrsz_center"
                    elif "acs_sz_center" in fr[f"{sn}.1/measurement"]:
                        VisitorPrepareCut.sz_center_name =  "acs_sz_center"

                
                # skip non helical failures

                print(f" CONSIDERING SCAN NUM {sn} of  {fn} is_helical = {is_helical}")
                
                if (not is_helical) and ( (f"{sn}.1/end_reason"  in fr) and ( fr[f"{sn}.1/end_reason"][()].decode("utf-8") == "FAILURE") ):
                    # otherwise past_helical will become false and the following non helical scan will be discarded if not helical
                    print(f" ~~~~~~~~~~~~~~~~~~~ SKIPPING SCAN {sn} BECAUSE NOT HELICAL AND FAILURE ")
                    continue
                

                print(f"........... CHECKS is_first, is_helical, past_helical, is_last ====> {is_first, is_helical, past_helical, is_last}         ")
                if (is_first  or is_helical) or ( past_helical):  # and is_last ):
                    print(f".................... this scan SCAN {sn}  WILL BE CONSIDERED")
                    if f"{sn}.2" in fr:
                        extra = f"{sn}.2"
                    else:
                        extra = None
                    pieces.append(   [ os.path.realpath(fn), f"{sn}.1", is_helical, extra, None ]) # the last entry will be filled with the
                    # first z of the following helical scan if any
                else:
                    print(f" ~~~~~~~~~~~~~~~~~~~~~~~~ SCAN {sn} IS DISCARDED (considering the above checks)")

                if not past_helical:
                    # after the first helical this becomes true
                    past_helical = is_helical
                    
    for i in range(1, len( pieces) ):
        fn, tag, is_helical, extra, _ = pieces[-i]
        if is_helical:
            with SilxFile(fn,"r") as fr:
                first_z = fr[f"{tag}/measurement/" + VisitorPrepareCut.sz_center_name][0]
                if pieces[ -(i+1)][1]: # is_helical
                    pieces[ -(i+1)][-1] = first_z
                

    with SilxFile( args.output ,   "w") as fw:
        sn = 0

        for fn, scan_tag, is_helical, extra, next_first_z in pieces:
            sn+=1
            with SilxFile(os.path.realpath(fn),"r") as fr:
                fr.copy( scan_tag, fw, f"{sn}.1" )

                if sn > 1:
                    all_scan_numbers .append(sn)

                if is_helical:
                    applicandum_next_first_z = next_first_z
                else:
                    applicandum_next_first_z = None
                    

                fix_links( args, fw[f"{sn}.1"],  scan_tag,  f"{sn}.1" , fn , fw, fr,applicandum_next_first_z)
                
                if extra is not None:
                    
                    fr.copy( extra, fw, f"{sn}.2" )
                    
                    fix_links(args,  fw[ f"{sn}.2" ],  extra,  f"{sn}.2", fn , fw, fr )
        # del fw["1.1/instrument/scan_numbers/data"]
        # fw["1.1/instrument/scan_numbers/data"] = all_scan_numbers

class VisitorPrepareCut:

    sz_center_name = "mrsz_center"
    
    def __init__(self,  next_first_z):
        
        self.next_first_z = next_first_z
        self.dataset_3D = []
        self.dataset_1D = []
        
    def __call__(self, name, object):
        
        # print(" Visitor nome ", name, "  object ", object )
        
        if isinstance(object, h5py.Dataset):
            #### if name 
            if len(object.shape) == 3:
                self.dataset_3D.append(name)
                
            if len(object.shape) == 1:
                self.dataset_1D.append(name)
                if os.path.basename(os.path.dirname(name)) == self.sz_center_name:
                    z_s = object[()]
                    if z_s[-1] > z_s[0] :
                        self.cut_at = np.searchsorted( z_s, self.next_first_z)
                    else:
                        self.cut_at = np.searchsorted( -z_s, -self.next_first_z)
                        
                if os.path.basename(os.path.dirname(name)) == "mrsrot_cen360":
                    self.all_angles = object[()]
    def retune_cut(self):
        if self.cut_at < len(self.all_angles):
            for i in range(self.cut_at, len(self.all_angles)-1):
                if self.all_angles[i+1] < self.all_angles[i]:
                    self.cut_at = i+1
                    break
        
class VisitorChangeTag:
    def __init__(self, oldtag, newtag, source_file, h5file_source):
        self.oldtag = oldtag
        self.newtag = newtag
        self.source_file = source_file
        self.h5file_source = h5file_source
        self.virtuals_to_be_recreated= {}
        
    def __call__(self, name, obj):
        # print(" Visitor nome ", name, "  obj ", obj )
        if not isinstance(obj, h5py.Dataset):
            keys =  list(obj.keys()) 
            for k in keys:
                link = obj.get( k, getlink=True)
                if isinstance( link, h5py.SoftLink) :
                    path = link._path
                    if self.oldtag in path:
                        path = path.replace(self.oldtag, self.newtag)
                        link._path = path
                        del obj[k]
                        obj[k] = link
        else:

            if obj.is_virtual:
                
                new_layout = h5py.VirtualLayout(shape=obj.shape, dtype=obj.dtype)

                start = 0
                shape = None
                
                ll = len( obj.virtual_sources()  )
                for itmp, (vdsmap , filename, dset_name, src_space) in enumerate(obj.virtual_sources()):

                    if filename ==".":
                        print(" LOCALE ")
                        # it is n the same file. Will not reopen it because we are already there
                        islocal=True
                    else:
                        islocal=False

                    if  islocal:
                        
                        shape =  self.h5file_source [dset_name].shape
                        
                        end = start + shape[0]
                        if (end<= new_layout.shape[0] ):
                            vsource = h5py.VirtualSource(filename, dset_name, shape   )
                        else:
                            too_much = end - new_layout.shape[0]
                            vsource = h5py.VirtualSource(filename, dset_name, (shape[0] - too_much, ) + shape[1:]  )
                            end -= too_much
                        new_layout[start:end] = vsource
    
                        
                        start = end
                        self.virtuals_to_be_recreated[name] = new_layout
                        

                    else:
                        
                        filename = os.path.realpath(os.path.join( os.path.dirname(self.source_file),  filename ))
    
                        if shape is None:
                            # here we suppose that the detector files have all the same lenght
                            # It would take too much time to reopen each on just to get the shape
                            with SilxFile(filename, "r") as f:
                                shape = f[dset_name].shape
                                print(name, self.oldtag)
                                print(shape)
                        end = start + shape[0]
                        if (end<= new_layout.shape[0] ):
                            vsource = h5py.VirtualSource(filename, dset_name, shape   )
                        else:
                            too_much = end - new_layout.shape[0]
                            vsource = h5py.VirtualSource(filename, dset_name, (shape[0] - too_much, ) + shape[1:]  )
                            end -= too_much
                        new_layout[start:end] = vsource
    
                        
                        start = end
                        self.virtuals_to_be_recreated[name] = new_layout

                        
# for n in range(1, 5):
#     filename = "{}.h5".format(n)
#     vsource = h5py.VirtualSource(filename, 'data', shape=(100,))
#     layout[n - 1] = vsource

# # Add virtual dataset to output file
# with SilxFile("VDS.h5", 'w', libver='latest') as f:
#     f.create_virtual_dataset('data', layout, fillvalue=-5)
       
def fix_links(args,  node, oldtag, newtag , source_file , h5file, h5file_source, next_first_z=None)  :    
    visitor = VisitorChangeTag( oldtag, newtag, source_file, h5file_source)
    node.visititems( visitor )

    for name, layout in visitor.virtuals_to_be_recreated.items():
        new_name= os.path.join(newtag,  name) 
        del h5file[new_name]
        h5file.create_virtual_dataset(new_name, layout, fillvalue=0)

    if next_first_z is not None:        
        visitor = VisitorPrepareCut(  next_first_z)
        node.visititems( visitor )

        if args.integer_cut:
            visitor.retune_cut()


        old_lenght = None

        time_has_been_fixed = False
        
        for  dname in       visitor.dataset_3D :
            
            node[dname+"_original_for_gluing"] =  node[dname]
            original_len = node[dname].shape[0]

            old_lenght = node[dname].shape[0]
            
            del node[dname]
            obj = node[dname+"_original_for_gluing"]
            new_shape = (visitor.cut_at,) + obj.shape[1:]

            new_lenght = new_shape[0]

            if not time_has_been_fixed:
                start_time_str = node['start_time'][()].decode('utf-8')
                end_time_str = node['end_time'][()].decode('utf-8')
    
                start_time = datetime.fromisoformat(start_time_str)
                end_time = datetime.fromisoformat(end_time_str)
    

                duration = end_time - start_time
                print(" Shortening the (end_time - start_time) duration by a factor ",     (  new_lenght / old_lenght  )  )
                new_duration = duration * (  new_lenght / old_lenght  )       
                new_end_time = start_time + new_duration
                new_end_time_str = new_end_time.isoformat()
    
                # write the new end_time in the dataset
                node['end_time'][()] = new_end_time_str.encode('utf-8')

            
            new_layout = h5py.VirtualLayout(shape=new_shape  , dtype=obj.dtype)
            new_layout[:] = h5py.VirtualSource(  node.file.filename  , f"/{newtag}/"+dname+"_original_for_gluing"   , new_shape)
            node.create_virtual_dataset(dname, new_layout, fillvalue=0)
        
        for  dname in       visitor.dataset_1D :
            my_len = node[dname].shape[0]

            if abs( my_len - original_len  ) < abs( my_len - 2*original_len  ):
                new_len = visitor.cut_at
            else:
                new_len = 2*visitor.cut_at + 1
                
            data = node[dname][ : new_len]
            del node[dname]
            node[dname] = data
            print("1D ", node,  dname)

if __name__ == "__main__":    
    main()

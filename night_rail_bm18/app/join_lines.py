import numpy as np
import glob
import fabio
from PIL import Image
import io
import os
import getpass


from pyicat_plus.client.main import IcatClient


def main(target_dir="./"):

    fl=glob.glob(os.path.join( target_dir, "line_*.edf"))
    fl.sort()
    
    res=[]
    for fn in fl:
        print(fn)
        data=fabio.open(fn).data
        data.shape=-1
        res.append(data)
    
    res=np.array(res)
    
    edf=fabio.edfimage.edfimage()
    edf.data = res
    edf.write("slice.edf")

    # disabling for the moment the posting of the image
    # return 0
    
    try:
        api_key = open("/scisoft/night_rail/bm18_certificate.txt").read()
    except:
        print(" COULD NOT OPEN /scisoft/night_rail/bm18_certificate.txt FOR LOGBOOK CERTIFICATE")
        return 0


    work_dir = os.getcwd()
     
    endpoint_url = "https://icatplus.esrf.fr"

    client = IcatClient(
        elogbook_url=endpoint_url,
        elogbook_token=api_key
    )

    beamline = "BM18"
    msg_type = "comment"
    tags = ["night_rail_run"]  # put the tags you want here

    print(" MESSAGE TO LOGBOOK DISACTIVATED FOR THE MOMENT ( waiting for some more features on thje logbook ) ")

    
    # try:
    #     # send a text message
    #     beamline = "BM18"
    #     msg_type = "comment"
    #     client.send_message(
    #         msg=f"""
    #         # Producing tiff images ( joining lines for vertical slice ) 
    #         Night_rail run:
    #            user       : {getpass.getuser()}
    #            work_dir   : {work_dir}
    #         """,
    #         msg_type=msg_type,
    #         beamline=beamline,
    #         beamline_only=True,
    #         tags=tags,
    #     )

    #     original_size = max( *(edf.data.shape)  )
    #     shrink_factor = 300/original_size

    #     new_size = ( int(edf.data.shape[0]*shrink_factor), int(edf.data.shape[1]*shrink_factor) )
        
    #     data8 = (edf.data/2**8).astype(np.uint8)

    #     image_pil = Image.fromarray(data8 )

    #     image_pil = image_pil.resize( new_size )
        
    #     f = io.BytesIO()
    #     image_pil.save(f, format="png")
    #     # image_pil.save(f, format="png")

    #     # send all user parameters
    #     client.send_binary_data(
    #         data=f.getbuffer().tobytes(),  # bytes
    #         mimetype="image/png",
    #         msg_type=msg_type,
    #         beamline=beamline,
    #         beamline_only=True,
    #         tags=tags,
    #     )
        
    # except Exception as e:
    #     print("Error!" + e)
        

    
    return 0

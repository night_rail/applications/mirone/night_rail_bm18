import json
import importlib
import argparse
import os
import importlib.util
import sys


def get_arguments():
    parser = argparse.ArgumentParser(description="night_rail for bm18. Modifier of parameters json  file.")
    parser.add_argument("--aiguillage", required=True, help="aiguillage file", default="nabu_rail_default.json")

    parser.add_argument(
        "--change_par",
        action="append",
        nargs="+",
        required=True,
        help=""" This argument can be repeated for every parameter that needs to be changed. If two parameters are provided, 
                 the first is a fragment of the variable name, the second it value.
                 If three parameters are provided, the first is still the fragment, the second a part of the current value, and the third its wished replacement.
        """,
    )

    parser.add_argument("--target_file", required=True, help="aiguillage file", default="nabu_rail_default.json")

    return parser.parse_args()


def main(args=None):
    
    if args is None:
        args = get_arguments()

    aiguillage_module_path = args.aiguillage
    aiguillage_module_name = os.path.basename(os.path.splitext(args.aiguillage)[0])
    aiguillage_module_dir = os.path.dirname(args.aiguillage)

    spec = importlib.util.spec_from_file_location(aiguillage_module_name, aiguillage_module_path)
    aiguillage = importlib.util.module_from_spec(spec)

    sys.modules[aiguillage_module_name] = aiguillage
    spec.loader.exec_module(aiguillage)

    # load an existing configuration into a dictionary
    with open(os.path.join(aiguillage_module_dir, aiguillage.Parameters.json_filename), "r") as fp:
        json_conf = json.load(fp)

    for replacement in args.change_par:
        if len(replacement) == 2:
            name_fragment, value = replacement
            # if not found or several matches (>1) are found an exception will be raised
            aiguillage.Parameters.search_and_change(name_fragment=name_fragment, value=value)
        elif len(replacement) == 3:
            name_fragment, fragment, value = replacement
            # if not found or several matches (>1) are found an exception will be raised
            aiguillage.Parameters.search_and_change(name_fragment=name_fragment, fragment=fragment, value=value)

    # set the json dictionary with updated parameters
    aiguillage.Parameters.change_dic_by_parameters(json_conf)

    # write another ( or possible the same) json file with an update conf

    with open(args.target_file, "w") as fp:
        json.dump(json_conf, fp, indent=4)  # without indent it will not be very readable

    return 0

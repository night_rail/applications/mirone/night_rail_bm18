import numpy as np
import h5py
import argparse


def get_arguments():
    parser = argparse.ArgumentParser(description="Map creator.")
    parser.add_argument(
        "--left",
        required=False,
        type=int,
        default=0,
        help="remove pixels on the left",
    )
    
    parser.add_argument(
        "--right",
        required=False,
        type=int,
        default=0,
        help="remove pixels on the right",
    )

    parser.add_argument(
        "--bottom",
        required=False,
        type=int,
        default=0,
        help="remove pixels at the bottom",
    )

    parser.add_argument(
        "--top",
        required=False,
        type=int,
        default=0,
        help="remove pixels at the top",
    )
    
    parser.add_argument(
        "--binning",
        required=False,
        type=int,
        default=1,
        help="binning factor",
    )
    
    return parser.parse_args()




max_order_i = 3
max_order_j = 5


def main():
    args = get_arguments()
    
    coeffs_i, coeffs_j = get_coeffs_ij(args)
    dect_shape = np.load("dect_shape.npy")
    Ny, Nx = dect_shape

    if args.binning != 1:

        if (Ny%args.binning) or (Nx%args.binning) :
            print(" WARNING "*4)
            print( f" The original shape {Ny},{Nx} contains number which are not all multiple of the binning size {args.binning})")

        Ny = Ny // args.binning
        Nx = Nx // args.binning
    

    punti_allineamento_verticale  = [
        (0  + args.bottom ,
         (Nx-1 + args.left - args.right  )/2
        ),
        (Ny-1 -args.top ,
         (Nx-1  + args.left - args.right)/2
        )
    ]

    IJ_vert = []
    for punto in punti_allineamento_verticale:
        I,J = retrieve_IJ( coeffs_i, coeffs_j, punto , 0  )
        IJ_vert.append((I,J))
        print(punto , "  -> ", I,J)

    (I0,J0),(I1,J1) = IJ_vert
    alpha = -(J1-J0)/(I1-I0)
    
    for punto in punti_allineamento_verticale:
        I,J = retrieve_IJ( coeffs_i, coeffs_j, punto , alpha  )
        print(punto , "  -> ", I,J)

    
    punti_allineamento_orrizontale  = [
        ( (Ny-1   +args.bottom - args.top )/2 ,
          args.left + (Nx - args.left - args.right )  * 0.1
        ),
        (
            (Ny-1  +args.bottom - args.top )/2 ,
            Nx -1 -args.right - 0.1 * (Nx - args.left - args.right )  
        )
    ]
    
    IJ_hor = []
    for punto in punti_allineamento_orrizontale:
        I,J = retrieve_IJ( coeffs_i, coeffs_j, punto , alpha  )
        IJ_hor.append((I,J))
        print(punto , "  -> ", I,J)

    (I0,J0),(I1,J1) = IJ_hor
    calib_factor = ((Nx -args.left - args.right  )*0.8 -1) /(J1 -J0)

    print(" CALIB ", calib_factor)
    
    IJ_hor = []
    for punto in punti_allineamento_orrizontale:
        I,J = retrieve_IJ( coeffs_i, coeffs_j, punto , alpha , calib_factor )
        IJ_hor.append((I,J))
        print(punto , "  -> ", I,J)


    print(" CENTRO")
    centro  = ( (Ny-1 + args.bottom - args.top )/2 , (Nx-1 + args.left - args.right)/2 )
    I,J = retrieve_IJ( coeffs_i, coeffs_j, centro , alpha , calib_factor )
    print( centro , "  -> ", I,J)

    shiftIJ = (np.array(centro) - np.array([I,J])).astype("d")

    print( " SHIFT ", shiftIJ)
    I,J = retrieve_IJ( coeffs_i, coeffs_j, centro , alpha , calib_factor, shiftIJ )
    print( centro , "  -> ", I,J)


    shiftIJ[0] -= args.bottom
    shiftIJ[1] -= args.left
    
    # FINALE

    new_Nx = Nx  - args.left - args.right
    new_Ny = Ny  - args.bottom - args.top
    
    j = np.arange(new_Nx).astype("d")
    i = np.arange(new_Ny).astype("d")


    print("Gnerating final map .......")
    J, I = np.meshgrid(j, i, copy=False)
    J = J.flatten()
    I = I.flatten()    
    components, components_der_I , components_der_J = get_components_der(I,J, alpha  , calib_factor = calib_factor, shiftIJ =  shiftIJ)

    iis = np.dot( components, coeffs_i )
    jjs = np.dot( components, coeffs_j )

    
    iis.shape = new_Ny, new_Nx
    jjs.shape = new_Ny, new_Nx

    with h5py.File(f"mapfromslit_{new_Ny}_{new_Nx}.h5","w") as f:
        f["coords_source_x"] = jjs - args.left
        f["coords_source_z"] = iis - args.bottom

    print("Build phase distortion")
    
    X_nominal, Y_nominal = np.meshgrid( np.arange(new_Nx), np.arange(new_Ny), copy=False)
    
    shift_y = iis - args.left - Y_nominal
    shift_x = jjs - args.bottom - X_nominal
    

    middle = (new_Nx)//2 
    yder = shift_y[:, middle]
    column_values = np.cumsum(yder)

    row_values = np.cumsum( shift_x , axis = 1 )

    phase = column_values[:,None] + row_values - (row_values[:, middle])[:,None]


    
    with h5py.File(f"mapfromslit_{new_Ny}_{new_Nx}.h5","r+") as f:
        f["shift_x"] =  shift_x
        f["shift_y"] =  shift_y
        f["phase"] =  phase
        
    print("DONE")

def retrieve_IJ( coeffs_i, coeffs_j, punto , rot_alpha=0 , calib_factor = 1 , shiftIJ= np.array([0,0])) :

    I,J = 0,0

    target_i, target_j = punto

    diff = 100    

    while diff > 1.0e-10:

        oldI, oldJ = I,J
        
        components, components_der_I , components_der_J = get_components_der(I,J, rot_alpha  , calib_factor = calib_factor, shiftIJ =  shiftIJ)

        i = np.dot(components, coeffs_i)    
        error_i = target_i - i

        i_der_I = np.dot(components_der_I, coeffs_i)
        I = I + error_i / i_der_I
    
        components, components_der_I , components_der_J = get_components_der(I,J, rot_alpha , calib_factor = calib_factor, shiftIJ =  shiftIJ)
        j = np.dot(components, coeffs_j)    
        error_j = target_j - j

        j_der_J = np.dot(components_der_J, coeffs_j)
        J = J + error_j / j_der_J

        diff = abs(I - oldI)+abs(J - oldJ)

    return I,J
        
def get_components_old(I,J, ):
    components = []
    IP = I*0 + 1.0
    for order_i in range( max_order_i ):   
        JP = J*0 + 1.0
        for order_j in range( max_order_j ):
            components.append( JP*IP )
            JP *= J
        IP *= I
    components = np.array(components).T
    return components

def get_components_der_old(I,J, derI=0, derJ=0):
    components = []
    for order_i in range( max_order_i ):   
        for order_j in range( max_order_j ):
            if derI + derJ == 0:
                
                term = (I** order_i) * (J ** order_j )
                
            elif derJ:

                if order_j == 0:
                    term =  I * 0
                else:
                    term = order_j * ( I ** order_i  ) * ( J ** ( order_j - 1 ) )
                    
            elif derI:

                if order_i == 0:
                    term =  I * 0
                else:
                    term = order_i * ( I ** (order_i - 1 )  ) * ( J **  order_j  )    
            components.append(term)
            
    components = np.array(components).T
    
    return components



def PP( x, order):
    if order == -1:
        return x*0
    else:
        return x**order
    

def get_components_der(Iarg,Jarg, rot_alpha = 0,  calib_factor = 1, shiftIJ= np.array([0,0])):

    cc = np.cos(rot_alpha) 
    ss = np.sin(rot_alpha) 


    Iarg = Iarg - shiftIJ[0]
    Jarg = Jarg - shiftIJ[1]
    
    I = (   Iarg * cc + Jarg * ss ) / calib_factor 
    J = ( - Iarg * ss + Jarg * cc ) / calib_factor 

    I_der_I =  cc / calib_factor
    I_der_J =  ss / calib_factor
    J_der_I = -ss / calib_factor
    J_der_J =  cc / calib_factor
        
    components = []
    components_der_I = []
    components_der_J = []
    
    for order_i in range( max_order_i ):   
        for order_j in range( max_order_j ):
            
            term00 = (I** order_i) * (J ** order_j )
                
            term10  = order_i * PP(I , order_i - 1 ) * PP(J , order_j     )  * I_der_I
            term10 += order_j * PP(I , order_i     ) * PP(J , order_j - 1 )  * J_der_I

            term01  = order_i * PP(I , order_i - 1 ) * PP(J , order_j     )  * I_der_J
            term01 += order_j * PP(I , order_i     ) * PP(J , order_j - 1 )  * J_der_J

            components.append(term00)
            components_der_I.append(term10)
            components_der_J.append(term01)
            
    components = np.array(components).T
    components_der_I = np.array(components_der_I).T
    components_der_J = np.array(components_der_J).T
    
    return components, components_der_I , components_der_J




def get_coeffs_ij(args):
    
    crossings = np.load("crossings.npy")
    ni,nj, _ = crossings.shape

    j = np.arange(nj).astype("d")/args.binning
    i = np.arange(ni).astype("d")/args.binning
    
    J, I = np.meshgrid(j, i, copy=False)
    
    # Z = J**2 + I**2 + np.random.rand(*J.shape)*0.01

    
    J = J.flatten()
    I = I.flatten()

    
    i_vals = crossings[:,:,0].flatten()
    j_vals = crossings[:,:,1].flatten()

    
    sumij = i_vals + j_vals

    mask = ~np.isnan(sumij)

    i_vals = i_vals[mask ]
    j_vals = j_vals[mask ]
    I = I[mask ]
    J = J[mask ]

    
    components, components_der_I, components_der_J = get_components_der(I,J)
    
    coeffs_i, r, rank, s = np.linalg.lstsq(components, i_vals)

    simulati = np.dot(components, coeffs_i)

    coeffs_j, r, rank, s = np.linalg.lstsq(components, j_vals)

    simulati = np.dot(components, coeffs_j)


    return coeffs_i, coeffs_j
    
# main()

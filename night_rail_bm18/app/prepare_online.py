import glob
import sys
import h5py
from silx.io.h5py_utils import File as SilxFile
from .colla import main as colla

import os
import shutil
from contextlib import contextmanager
import logging
import traceback
from collections import namedtuple

import multiprocessing as mp
mp_manager = mp.Manager()


logger = logging.getLogger(__name__)
logger.setLevel("INFO")


@contextmanager
def change_path(newdir):
    old_path = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(old_path)


def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

class VisitorFunc:

    def __init__(self, n_estimated, cut_to = None):
        self.n_estimated = n_estimated
        self.cut_to = cut_to
        
    def __call__(self, name, node):
        if isinstance(node, h5py.Dataset) and node.dtype != '|O':
            if len(node.shape) ==1:

                if not name.startswith("instrument"):
                    return

                if self.cut_to is  None:
                    if self.n_estimated > node.shape[0]:
                        self.n_estimated = node.shape[0]

                    if node.shape[0]:
                        if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :
                            if node.shape[0] < self.n_estimated:
                                self.n_estimated = node.shape[0]
                        elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                            if node.shape[0] < self.n_estimated * 2:
                                self.n_estimated = node.shape[0]// 2
                else:
                    if node.shape[0]:
                        if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :
                            #print (self.cut_to )
                            #print(" Corrige ici ")
                            node.resize((self.cut_to,))
                        elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                            node.resize((2*self.cut_to,))
                        
                # node is a dataset
        else:
            pass
            # print(node, " is a group ")

class VisitorChecker:

    def __init__(self):
        pass
        
    def __call__(self, name, node):
        if isinstance(node, h5py.Dataset):
            if len(node.shape) ==1:
                print( " NAME ", name)
                if node.shape[0]:
                    test = node[()][-1]
            elif len(node.shape) ==3:
                test = node[-1,-1,-1]
                test = node[0,0,0]
        else:
            pass
            # print(node, " is a group ")
            
            

def analyse_chunks(chunk_list, directory):    
    # all_chunks = open(chunk_list_file,"r").read().split()

    all_chunks = sorted(chunk_list, key=lambda x: int(''.join(filter(str.isdigit, x))))

    
    n_chunks = len(all_chunks)
    all_chunks = [ directory + "/" + os.path.basename(tok) for tok in all_chunks]


    #print(" all_chunks size. Si il y en a qu'un attends ( rais exception ... ) ", len(all_chunks) )
    
    one_file = all_chunks[0]

    detector_name = os.path.basename(one_file[ : one_file.rfind("_")  ])
    
    with h5py.File(one_file,"r") as f:
        entry_name = list(f.keys())[0]
        if "entry" not in entry_name:
            raise RuntimeError("I am expecting that chunks from the detector have one entry key and only one key at root" )
        g = f[entry_name]
        shape = g["measurement/data"].shape
        dtype = g["measurement/data"].dtype
    return n_chunks, shape, all_chunks, entry_name, dtype, detector_name

def safe_copy_only_swmr(source_name,target_name) :
    
    print(" copy ", source_name, " to ", target_name )        
    os.makedirs( os.path.dirname(target_name), exist_ok=True )

    success = False
    while not success:
        try:    
            with SilxFile(source_name, "r") as source, SilxFile(target_name, "w") as dest:
                dest.attrs.update(source.attrs)
                for key in source.keys():
                    source.copy(key, dest)

            success = True
        except:
            print(traceback.format_exc())
            success = False
        

def safe_copy_old(source,target) :

    initial_mtime_ns, final_mtime_ns = 0,1
    initial_size, final_size = 0,1
    
    while (initial_mtime_ns != final_mtime_ns)  or ( initial_size   != final_size ):

        with SilxFile(source,"r") as f:
            print(f"opening {source} for safe_copy. Keys are {f.keys()}") 
        file_stats = os.stat(source)
        initial_mtime_ns = file_stats.st_mtime_ns
        initial_size = file_stats.st_size

    
        print(" copy ", source, " to ", target )
        
        os.makedirs( os.path.dirname(target), exist_ok=True )
        
        shutil.copy(  source ,  target  )
        


        with SilxFile(source,"r") as f:
            print(f"Saying bye to {source} after safe_copy. Keys are {f.keys()}") 

        file_stats = os.stat(source)
            
        final_mtime_ns = file_stats.st_mtime_ns
        final_size = file_stats.st_size
            
        if (initial_mtime_ns != final_mtime_ns)  or ( initial_size != final_size ):
            print("File has changed during copy. Retrying...")

def safe_copy(source,target_file) :

    initial_mtime_ns, final_mtime_ns = 0,1
    initial_size, final_size = 0,1
    success = False
    
    while (not success) or (initial_mtime_ns != final_mtime_ns)  or ( initial_size   != final_size ):

        file_stats = os.stat(source)
        initial_mtime_ns = file_stats.st_mtime_ns
        initial_size = file_stats.st_size
    
        print(" copy ", source, " to ", target_file )
        
        os.makedirs( os.path.dirname(target_file), exist_ok=True )
        
        shutil.copy(  source ,  target_file  )
        
        file_stats = os.stat(source)            
        final_mtime_ns = file_stats.st_mtime_ns
        final_size = file_stats.st_size
            
        if (initial_mtime_ns != final_mtime_ns)  or ( initial_size != final_size ):
            print("File has changed during copy. Will retry..")
            success = False
        else:
            return_dict =  mp_manager.dict(success=False)

            q = mp.Queue()
            p = mp.Process(target=h5_read_into_sandbox, args=(target_file,return_dict))
            p.start()
            p.join()
            success = return_dict["success"]

        if success:

            max_scan_int_part = -1
            
            has_end_time = True
            with h5py.File(target_file,"r") as f:
                for k in f.keys():
                    if is_float(k):
                        num = int(float(k))
                        if num > max_scan_int_part:                            
                            max_scan_int_part = num
                            if not("end_time" in f[f"{max_scan_int_part}.1"]):
                                has_end_time = False

                            
            # target_dir = os.path.dirname(online_file)
            # scan_files = glob.glob(os.path.join(source_dir,"scan*"))
            # for scan_source in scan_files:
            #     scan_target = os.path.join(target_dir, os.path.basename(scan_source))
            #     if  os.path.islink(scan_target):
            #         if os.path.realpath(scan_source) != os.path.realpath(scan_target):
            #             os.unlink(scan_target)
            #             os.symlink(scan_source, scan_target)
            #     else:
            #         os.symlink(scan_source, scan_target)

            if not has_end_time:
            
                if max_scan_int_part == -1 :
                    success = False
                else:
                    source_dir = os.path.dirname(os.path.abspath(source))

                    chunk_pattern = os.path.join(source_dir, f"scan{max_scan_int_part:04d}", "*.h5")
                    chunk_list = glob.glob(chunk_pattern)
                    if len(chunk_list) < 2:
                        success = False
        
            

            

def h5_read_into_sandbox(target_file, return_dict): 
    try:  
        with h5py.File(target_file, "r+") as f:
        #     scan_numbers = f["1.1/measurement/scan_numbers"][()]
        #     for k in f.keys():
        #         if is_float(k):
        #             num = int(float(k))
        #             if num > 1 and num not in scan_numbers:
        #                 del f[k]
                        
            visitor_func = VisitorChecker()
            f.visititems(visitor_func)
        success = True
    except:
        print(f"The check of the online copied file failed for dataset :")
        print(traceback.format_exc())
        success = False
    return_dict["success"] = success
            
def main(source=None, online_wdir=None, iter_id=0):
    if source is None:
        source = sys.argv[1]
        online_wdir = "./"
        
    online_scan_full_path = os.path.join(online_wdir, f"online_nobackup.h5")

    source_dir = os.path.dirname(os.path.abspath(source))

    _source_name = os.path.basename(source)
    _dir_name = os.path.dirname(source)
    previous_brokens = []
    while _source_name[:-8].endswith("_fc_") or _source_name[:-8].endswith("_bc_"):
        _source_name = _source_name[:-12] + _source_name[-8:]
        _dir_name = _dir_name[:-9] + _dir_name[-5:]
        previous_brokens.append( os.path.join(_dir_name,_source_name)   )
        
    if len(previous_brokens)==0:
        is_finished = prepare_online_file( source_dir, source, online_scan_full_path    )
    else:
        last_online_scan_full_path = os.path.join(online_wdir,"last_scan", f"online_nobackup.h5")
        is_finished = prepare_online_file( source_dir, source, last_online_scan_full_path    )


        GluingInfo = namedtuple("GluingInfo", ["input","integer_cut","output"])
        gluing_info = GluingInfo(
            input = previous_brokens + [ last_online_scan_full_path ],
            integer_cut = True,
            output = online_scan_full_path
        )
        print(" GLUING ", gluing_info.input )

        colla(gluing_info)
        
    prepare_batch_dir(iter_id, online_wdir, online_scan_full_path)

    
def prepare_batch_dir(iter_id, online_wdir, online_scan_full_path):
    if iter_id:
        batch_dir = os.path.join(online_wdir, "batch_dir")
        scan_files = glob.glob(os.path.join(os.path.dirname(online_scan_full_path), "scan*"))
 
        os.makedirs( batch_dir, exist_ok=True )
        for scan_source in scan_files:
            scan_target = os.path.join(batch_dir, os.path.basename(scan_source))
            if  os.path.islink(scan_target):
                if os.path.realpath(scan_source) != os.path.realpath(scan_target):
                    os.unlink(scan_target)
                    os.symlink(scan_source, scan_target)
            else:
                os.symlink(scan_source, scan_target)
                
        batch_scan_full_path = os.path.join(batch_dir, f"online_{iter_id:04d}_nobackup.h5")
        shutil.copy(  online_scan_full_path ,  batch_scan_full_path  )


def prepare_online_file(source_dir, source, online_scan_full_path  ):
    
    with change_path( source_dir):
        online_file = online_scan_full_path
        safe_copy(source, online_file ) 
        is_finished = scan_is_finished(online_file)
        print("Copy successful")
        print(" now editing ", online_file )
        max_scan_int_part = -1
        has_end_time = True
        with h5py.File(online_file,"r+") as f:
            for k in f.keys():
                if is_float(k):
                    num = int(float(k))
                    if num > max_scan_int_part:
                        
                        max_scan_int_part = num
                        if not("end_time" in f[f"{max_scan_int_part}.1"]):
                            has_end_time = False

        target_dir = os.path.dirname(online_file)
        scan_files = glob.glob(os.path.join(source_dir,"scan*"))
        for scan_source in scan_files:
            scan_target = os.path.join(target_dir, os.path.basename(scan_source))
            if  os.path.islink(scan_target):
                if os.path.realpath(scan_source) != os.path.realpath(scan_target):
                    os.unlink(scan_target)
                    os.symlink(scan_source, scan_target)
            else:
                os.symlink(scan_source, scan_target)

        if has_end_time:
            logger.info("Online scan has completed" )
            return 1

        chunk_pattern = os.path.join(source_dir, f"scan{max_scan_int_part:04d}", "*.h5")
        chunk_list = glob.glob(chunk_pattern)
        

        if max_scan_int_part == -1:
            raise RuntimeError(" No scan with numerical prefix h.l was found " )
        if max_scan_int_part < 3:
            raise RuntimeError(f" I need at least to go down till scan 2 to copy some meta data like pixel size, but here the max scan number is {max_scan_int_part} " )
        n_chunks, chunk_shape, all_chunks, entry_name, dtype, detector_name = analyse_chunks(chunk_list, f"scan{max_scan_int_part:04d}")
        n_estimated = n_chunks * chunk_shape[0]
        
        print(  n_chunks, "  chunks with chunk_shape ", chunk_shape )
        
        with h5py.File(online_file,"r+") as f:
            scan = f[ f"{max_scan_int_part}.1"]                    
            visitor_func = VisitorFunc(n_estimated)
            scan.visititems(visitor_func)
            # to be sure that n_estimated is fixed
            scan.visititems(visitor_func)
          
            n_estimated = visitor_func.n_estimated
            n_round = visitor_func.n_estimated - ( visitor_func.n_estimated %  chunk_shape[0]  )
            
        print(" I need to cut everything at ", n_round," rounded to multipled of ",  chunk_shape[0])
        
        with h5py.File(online_file,"r+") as f:
            scan = f[ f"{max_scan_int_part}.1"]                    
            visitor_func = VisitorFunc(n_estimated, n_round)
            scan.visititems(visitor_func)
            
        data_shape = n_round, chunk_shape[1], chunk_shape[2]
        print(" Now I create a layout of size  ", data_shape,"  from ", n_chunks , "  chunks  of individual shape ", chunk_shape)
        with h5py.File(online_file,"r+") as f:
            g = f[ f"{max_scan_int_part}.1/instrument"]
            
            g = f[ f"{max_scan_int_part}.1/instrument/{detector_name}"]                    
            layout = h5py.VirtualLayout(shape=data_shape, dtype=dtype)
            for nc, chunk_file_name  in enumerate(all_chunks[: n_round // chunk_shape[0] ]):
                vsource = h5py.VirtualSource(chunk_file_name, entry_name + "/measurement/data", shape=chunk_shape)

                layout[nc*chunk_shape[0]:(nc+1)*chunk_shape[0]] = vsource
                # print(  layout[nc*chunk_shape[0]:(nc+1)*chunk_shape[0]].shape   )

            if "image" in g:
                logger.warning("WARNING: dataset image already existing in {online_file,}. Now removing it and proceeding")
                del g["image"]
                
            g.create_virtual_dataset('image', layout, fillvalue=0)
            if "data" not in g:
                g["data"]=g["image"]
            
        print(" Now I copy some metadata from scan ", max_scan_int_part-1 , " into scan ", max_scan_int_part)
        with h5py.File(online_file,"r+") as f:
            g_nm1 = f[ f"{max_scan_int_part-1}.1/instrument/{detector_name}"]
            g_n = f[ f"{max_scan_int_part}.1/instrument/{detector_name}"]
            if "acq_parameters" not in g_n:
                g_n["acq_parameters"] = g_nm1["acq_parameters"]


        return 0

            
        
# main()

def scan_is_finished(scan_file):
    with SilxFile(scan_file,"r") as f:
        for k in list(f.keys()):
            if "end_time" not in f[k]:
                return False
    return True

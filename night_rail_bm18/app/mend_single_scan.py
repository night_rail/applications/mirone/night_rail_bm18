import os
import sys
import shutil
import h5py
import random
import argparse

"""
   For now this script is used in the following way, which requires a couple of manual steps ( will be possibly simplified in the future):

 - create a target directory
 - link symbolically the scanXXXX directory from the interrupted scan into the target directory ( example for directory name : fixed_toto)
 - copy the bliss file ( the one which is at the same level of the scanxxxx directories) into the target 
   directory with name _fixed_toto.hdf5. NOTE : note the _ at the beginning of the name. It is there to distinguish from the final name of the final name
   that we are going to produce.
 - if the scan has been interrupted during scan 5  (corresponding to subdirectory scan0005 )  run the following command
       night_rail_bm18_mend_scan name_for_target_directory --int_path 5
    this command will tell you the maximal lenght that can be given to the to be fixed scan. let us say N
 - Rerun again the command, specify a lenght of N ( or shorter if you wish, but not longer)
       night_rail_bm18_mend_scan name_for_target_directory --int_path 5 --cut_to N

   this will create the file tmp.h5
   Now copy ( or move) tmp.h5 to fixed_toto.h5
   Remember that night_rail recongnise a bliss file named toto.h5 as a bliss file when it is in directory /some/path/toto
"""


def parse_args():

    my_parser = argparse.ArgumentParser()
    my_parser.add_argument("--dir", type=str, required=True, help="a directory with the scan to mend")
    my_parser.add_argument("--int_part", type=str, required=True, help="To mend scan N.1 give N")
    my_parser.add_argument("--cut_to", type=int, required=False, help="If given the new resized lenght. If not the maximal lenght will be shown")
                           
    args = my_parser.parse_args()

    return args

def main():


    args = parse_args()
    
    target_dir = os.path.realpath(args.dir)

    nome = os.path.basename(target_dir)

    source_file = os.path.join( target_dir, "_" + nome + ".h5" )

    target = os.path.join( target_dir,  nome + ".h5" )
    
    max_scan_int_part = int(args.int_part)
    
    shutil.copy(source_file, "tmp.h5")
    with h5py.File("tmp.h5","r+") as f:
        for k in f.keys():
            if is_float(k):
                num = int(float(k))
                if num > max_scan_int_part:
                    pass
                    # del(f[k])

    with h5py.File("tmp.h5","r+") as f:
        scan = f[ f"{max_scan_int_part}.1"]                    
        visitor_func = VisitorFuncEstimated(0)
        scan.visititems(visitor_func)
        scan.visititems(visitor_func)
        print("dimensione estimated is ", visitor_func.n_estimated)
        if args.cut_to is None:
            return 0
        visitor_func = VisitorFuncCut( visitor_func.n_estimated  , int(args.cut_to) )
        scan.visititems(  visitor_func)

        print(visitor_func.name_to_mend)
        print(visitor_func.new_shape_mended)

        name_original = visitor_func.name_to_mend+"_original"
        f[name_original] = f[visitor_func.name_to_mend]
        del f[visitor_func.name_to_mend]
        new_layout = h5py.VirtualLayout(shape=visitor_func.new_shape_mended, dtype=f[name_original].dtype)
        vsource = h5py.VirtualSource("tmp.h5", name_original, visitor_func.new_shape_mended  )
        new_layout[:] = vsource

        f.create_virtual_dataset(visitor_func.name_to_mend, new_layout, fillvalue=0)

        
        
class VisitorFuncEstimated:

    def __init__(self, n_estimated ):
        self.n_estimated = n_estimated
        
    def __call__(self, name, node):

        if "period" in name:
            return
        
        if isinstance(node, h5py.Dataset):
            
            if len(node.shape) ==1:


                if self.n_estimated ==0:
                    
                    self.n_estimated = node.shape[0]
                
                print(   name,  node.shape[0] ) 
                if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :

                    
                    if node.shape[0] < self.n_estimated:
                        
                        self.n_estimated = node.shape[0]
                        
                elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                    
                    if node.shape[0] < self.n_estimated * 2:
                        
                        self.n_estimated = node.shape[0]// 2
        else:
            pass

class VisitorFuncCut:
    def __init__(self, n_estimated,  cut_to ):
        self.cut_to = cut_to
        self.n_estimated = n_estimated
    def __call__(self, name, node):
        if isinstance(node, h5py.Dataset):
            if len(node.shape) ==1:
                print(" resize " , self.cut_to )
                if abs(   node.shape[0]/self.n_estimated  -1  ) < 0.2 :
                    node.resize((self.cut_to   ,))
                elif abs(   node.shape[0]/self.n_estimated  -2  ) < 0.2 :
                    node.resize((2*(self.cut_to )+1,))
            if len(node.shape) ==3:
                self.name_to_mend = node.name
                self.new_shape_mended = (self.cut_to, ) + node.shape[1:]

        else:
            pass
            # print(node, " is a group ")


def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False




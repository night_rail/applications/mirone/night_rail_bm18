import re
import numpy as np
import os
import night_rail
import shutil
import textwrap
from tomoscan.esrf.scan.nxtomoscan import NXtomoScan
import h5py


def get_radio_shape(possible_scan_files):
    file_name = None
    for file_name in possible_scan_files:
        if file_name:
            break
    if str(file_name)[-3:] == ".nx":
        my_scan = NXtomoScan(file_name)
        return (my_scan.dim_2, my_scan.dim_1)

    else:
        with h5py.File(file_name, "r") as f:
            for key_block in f.keys():
                block = f[key_block]
                if "measurement" in block:
                    meas = block["measurement"]
                    for key_meas in meas.keys():
                        dataset = meas[key_meas]
                        if len(dataset.shape) == 3:
                            return dataset.shape[1:]
    message = f""" I was not able to deduce the radio shape from file {file_name} """
    raise ValueError(message)


def save_night_rails_for_compability(session_name):
    # what we are using now
    installation_dir = os.path.abspath(os.path.dirname(os.path.dirname(night_rail.__file__)))
    vintage_installation_dir = os.path.os.path.join(
        os.path.abspath(os.path.curdir), session_name + "_night_rail_vintage"
    )

    # what might be used in the future

    if installation_dir == vintage_installation_dir:
        # it is already there
        return

    for package in ["night_rail", "night_rail_bm18"]:
        shutil.copytree(
            os.path.join(installation_dir, package), os.path.join(vintage_installation_dir, package), dirs_exist_ok=True
        )

    inset = textwrap.dedent(
        """
    #!/usr/bin/env python
    import sys
    import os
    sys.path=[ os.path.dirname(__file__)    ] + sys.path
    """
    ).strip()

    for entry_point in [
        "night_rail_bm18_setup_pars",
        "night_rail_bm18_setup_pars_distortion",
        "night_rail_bm18_create_scripts",
        "night_rail_bm18_modify_json",
        "night_rail_bm18_nexus_juicer",
    ]:
        source = shutil.which(entry_point)
        s = open(source, "r").read()
        target = os.path.join(vintage_installation_dir, entry_point)

        with open(target, "w") as f:
            f.write(inset + s)
        os.chmod(target, 0o755)


def count_z_stage(first_scan, second_scan):
    if first_scan == second_scan:
        template = first_scan
        return template, 0, 0, 1

    pattern = re.compile("[0-9]+")
    first_s, second_s = first_scan, second_scan

    changing = []
    changing_part_first = None
    changing_part_second = None

    len_part = 0
    while pattern.search(first_s) is not None and pattern.search(second_s) is not None:
        # print(first_s)
        match_first = pattern.search(first_s)
        match_second = pattern.search(second_s)

        # print(match_first)

        if None in [match_first, match_second]:
            first_s, second_s = "_" * len(first_s), "_" * len(first_s)
        else:
            if tuple(match_first.span()) != tuple(match_second.span()):
                message = f"The numerical parts of first_scan and second_scan do not match in position. they are   {first_scan}  and {second_scan}"
                raise ValueError(message)

            part_first = first_s[slice(*match_first.span())]
            part_second = second_s[slice(*match_second.span())]

            # print(part_first , part_second   )

            if part_first != part_second:
                if changing_part_first is None:
                    changing_part_first = part_first
                    changing_part_second = part_second
                    first_num = int(part_first)
                    last_num = int(part_second)
                    len_part = len(part_first)
                else:
                    if changing_part_first != part_first or changing_part_second != part_second:
                        message = f"Non sincronicity in the change of the numerical parts"
                        raise ValueError(message)
                changing.append(slice(*match_first.span()))
                # print(changing)

            first_s = "_" * (match_first.span()[1]) + first_s[match_first.span()[1] :]
            second_s = "_" * (match_first.span()[1]) + second_s[match_second.span()[1] :]

    template = first_scan

    inset = "X" * len_part
    # print(changing)
    for match_slice in changing:
        template = template[: match_slice.start] + inset + template[match_slice.stop :]

    icheck = last_num

    n_tot_avail_z = last_num + 1
    while icheck < 100000:
        file_check = template.replace(inset, f"{icheck:0{len_part}}")
        # print( file_check)
        if os.path.exists(file_check) and os.path.isfile(file_check):
            n_tot_avail_z = icheck + 1
        else:
            break
        icheck += 1

    return template, first_num, last_num, n_tot_avail_z


def template_to_format_string(template, literal=False):
    pattern = re.compile("[X]+")
    # X represent the variable part of the 'template'
    # for example if we want to treat scans HA_2000_sample_0000.nx, ..., HA_2000_sample_9999.nx then
    # we expect the template to be HA_2000_sample_XXXX.nx
    # warning: If the dataset base names contains several X substrings the longest ones will be taken.
    ps = pattern.findall(template)
    ls = list(map(len, ps))
    if len(ls) == 0:
        return template
    idx = np.argmax(ls)
    if len(ps[idx]) < 2:
        message = f""" The argument template should contain  a substring  formed by at least two 'X'
        The template was {template}
        """
        raise ValueError(message)
    if not literal:
        template = template.replace(ps[idx], "{i_stage:" + "0" + str(ls[idx]) + "d}")
    else:
        template = template.replace(ps[idx], "{what}")

    return template


if __name__ == "__main__":
    first = "/data/visitor/md1290/bm18/20221101/HA-1100_27.73um_sheep-head_ethanol_W__0000/md1290_HA-1100_27.73um_sheep-head_ethanol_W__0000.h5"
    last = "/data/visitor/md1290/bm18/20221101/HA-1100_27.73um_sheep-head_ethanol_W__0003/md1290_HA-1100_27.73um_sheep-head_ethanol_W__0003.h5"

    print(count_z_stage(first, last))


def find_top_level_output_file_from_classes(cls_list, must_exist=True):
    for cls in cls_list:
        if cls.b_class_get_relevant_output_file(must_be_set=False) is not None:
            return cls.b_class_get_relevant_output_file(), cls
    else:
        if must_exist:
            message = f""" No class with setted output_file was found in the list {cls_list} """
            raise RuntimeError(message)
        return None, None


def find_top_level_key_val_from_classes(key, cls_list, must_exist):
    for cls in cls_list:
        if cls.b_class_retrieve_val(key) is not None:
            return cls.b_class_retrieve_val(key), cls
    else:
        if must_exist:
            message = f""" No class with setted key was found in the list {cls_list} for key {key}"""
            raise RuntimeError(message)
        return None, None




def get_input_filename_and_output_prefix_from_scan_names(  bliss_first_scan=None,
                                                           bliss_last_scan=None,
                                                           nexus_first_scan=None,
                                                           nexus_last_scan=None,
                                                           entry_name_par = "entry0000",
                                                           multi_z_rec_scheme =None,
                                                           bliss_unique_scan=None,
                                                           nexus_unique_scan=None,
                                                           fractions_number = 1,
                                                           
):
        from_many_separately = False

        fractions_number = int(fractions_number)
        slurm_extra_split_n = 1

        if bliss_first_scan or nexus_first_scan:
            if bliss_first_scan:
                first_scan = bliss_first_scan
                last_scan = bliss_last_scan
                first_scan_bliss = first_scan
                last_scan_bliss = last_scan
            else:
                first_scan = nexus_first_scan
                last_scan = nexus_last_scan
                first_scan_bliss = first_scan
                last_scan_bliss = last_scan

                url = "/" + entry_name_par + "/bliss_original_files"
                with h5py.File(first_scan, "r") as f:
                    if url in f:
                        first_scan_bliss = str(f[url][0])

                with h5py.File(last_scan, "r") as f:
                    if url in f:
                        last_scan_bliss = str(f["/" + entry_name_par + "/bliss_original_files"][-1])

            filename_template_bliss, first_num_bliss, last_num_bliss, _ = count_z_stage(
                first_scan_bliss, last_scan_bliss
            )  # the original total number
            
            filename_template, first_num, last_num, _ = count_z_stage(
                first_scan, last_scan
            )  # just the ones that are found there when nexus is used

            if multi_z_rec_scheme != "s":  # [A]fter concatenation or None ( we exclude the excplicit case of "s"

                slurm_extra_split_n = fractions_number

                if slurm_extra_split_n ==1 :
                    output_prefix = (
                        os.path.splitext(os.path.basename(first_scan_bliss))[0]   ### c'etait trop long + f"_stages_{first_num:03d}_{last_num:03d}"
                    )
                else:
                    output_prefix = (
                        os.path.splitext(os.path.basename(first_scan_bliss))[0]  
                    )

                    output_prefix = [  output_prefix + f"_{i_split:03d}" for i_split in range(slurm_extra_split_n) ]
            else:
                from_many_separately = True
                original_file_template_numeric = template_to_format_string(filename_template)
                output_prefix = [
                    os.path.splitext(os.path.basename(original_file_template_numeric.format(i_stage=i_z)))[0]
                    for i_z in range(first_num, last_num + 1)
                ]

        else:
            if bliss_unique_scan:
                unique_scan = bliss_unique_scan
            else:
                unique_scan = nexus_unique_scan
            output_prefix = os.path.splitext(os.path.basename(unique_scan))[0]
            slurm_extra_split_n = fractions_number
            if slurm_extra_split_n > 1:
                output_prefix = (
                    os.path.splitext(os.path.basename(output_prefix))[0]  
                )
                output_prefix = [  output_prefix + f"_{i_split:03d}" for i_split in range(slurm_extra_split_n) ]
        from . import scripts # import from within a function, not very elegant: it could be avoided by passing as argument the list of candidate classes
                                  # but it works
        if not from_many_separately:
            # let us see who is producing the nexus for us
            input_nx_filename, _ = find_top_level_output_file_from_classes(
                [  scripts.ApplyRotCorrectionScript, scripts.CorrectPixFromDiagScript, scripts.ConcatenateToHeliScanScript, scripts.Bliss2NxScript]
            )
        else:
            input_nx_filename = scripts.Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")

        if (slurm_extra_split_n>1):
            assert ( not from_many_separately   ), "this should not happen "
            input_nx_filename = [input_nx_filename] * slurm_extra_split_n
        return input_nx_filename, output_prefix

    

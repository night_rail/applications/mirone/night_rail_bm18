import night_rail
import night_rail_bm18


class BlissUniqueScan(night_rail.LocationByPath):
    question = "Choose bliss dataset  by input and autocompletion: "
    path_is_file = True


class BlissFirstScan(night_rail.LocationByPath):
    question = "Choose the first bliss dataset of the  series : "
    path_is_file = True


class BlissLastScan(night_rail.LocationByPath):
    question = "Choose the last bliss dataset of the  series : "
    path_is_file = True


class NexusUniqueScan(night_rail.LocationByPath):
    b_my_histories_add_to = ["bliss_path"]
    b_my_histories_take_from = ["bliss_path"]
    question = "Choose nexus dataset  by input and autocompletion: "
    path_is_file = True


class NexusFirstScan(NexusUniqueScan):
    question = "Choose the first nexus dataset of the  series : "
    path_is_file = True


class NexusLastScan(NexusUniqueScan):
    question = "Choose the last nexus dataset of the  series : "
    path_is_file = True


class BlissRefBeginScan(night_rail.LocationByPath):
    question = "Choose the Reference bliss dataset for the  series begin: "
    path_is_file = True


class BlissRefEndScan(night_rail.LocationByPath):
    question = "Choose the Reference bliss dataset for the  series end: "
    path_is_file = True


class RefExtractionTargetDir(night_rail.LocationByPath):
    question = "Chose the target directory for extracted flats/darks"
    path_is_dir = True
    create_dir_on_demand = True
    default = "./EXTRACTED_DARKS_FLATS"


class OutputFormatPar(night_rail.BasePar):
    possible_choices = ["output will be [H]df5 file", "[T]iff files"]
    value = "h"
    tacit = True

    def proper_init(self, serialisation_object):
        question = "Choose output format: " + (", ".join(self.possible_choices))
        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class ComesFromAspectScheme(night_rail.BasePar):
    possible_choices = ["[Y]es", "[N]o"]
    value = "y"

    def proper_init(self, serialisation_object):
        question = "You are reusing Nexus files for a reconstruction. Do They come from Aspect? " + " ".join(
            self.possible_choices
        )

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class DatasetLocationScheme(night_rail.BasePar):
    possible_choices = ["from [B]liss files", "from [N]exus file"]
    value = "b"
    wf = "nabu"

    def proper_init(self, serialisation_object):
        question = "Choose wether you start from bliss files or nexus one: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "b":
            self.b_set_or_create_member(inode=0, cls=DatasetLocation, parent_serialiser=serialisation_object)
        if self.value == "n":
            if self.wf == "nabu":
                self.b_set_or_create_member(inode=0, cls=NexusLocation, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ComesFromAspectScheme, parent_serialiser=serialisation_object)


class DatasetLocation(night_rail.BasePar):
    value = "p"
    my_workflow = "nabu"
    from_nexus = False

    def proper_init(self, serialisation_object):
        if not self.from_nexus:
            possible_localisation_schemes = [
                "from [P]ath",
                "from [M]ultiple paths",
                "From multiple path and [R]eference scans",
            ]
        else:
            possible_localisation_schemes = ["from [P]ath", "from [M]ultiple paths"]

        question = "Choose dataset localisation scheme: " + (", ".join(possible_localisation_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if not self.from_nexus:
            UniqueScan, FirstScan, LastScan, RefBeginScan, RefEndScan = (
                BlissUniqueScan,
                BlissFirstScan,
                BlissLastScan,
                BlissRefBeginScan,
                BlissRefEndScan,
            )
        else:
            UniqueScan, FirstScan, LastScan = NexusUniqueScan, NexusFirstScan, NexusLastScan

        if self.value == "p":
            self.b_set_or_create_member(inode=0, cls=UniqueScan, parent_serialiser=serialisation_object)

        elif self.value in ["m", "r"]:
            self.b_set_or_create_member(inode=0, cls=FirstScan, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=LastScan, parent_serialiser=serialisation_object)

            if self.value == "r":
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=RefBeginScan, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=RefEndScan, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(),
                    cls=RefExtractionTargetDir,
                    parent_serialiser=serialisation_object,
                )

        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """
        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=EntryNamePar, parent_serialiser=serialisation_object
        )

        if not self.from_nexus:
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(),
                cls=RoughVoxelCorrectionPar,
                parent_serialiser=serialisation_object,
            )


class NexusLocation(DatasetLocation):
    from_nexus = True


class EntryNamePar(night_rail.TypedPar):
    value = "entry0000"
    b_my_histories_add_to = ["entry_name"]
    b_my_histories_take_from = ["entry_name"]
    asked_question = "The entry name is in general entry0000 ( default). But you can possibly change it here : "
    typed_default = "entry0000"
    my_type = str


class VoxelSchemePar(night_rail.BasePar):
    possible_determination_schemes = [
        "Voxel size: Use the [B]liss provided value",
        "from [U]ser provided  value",
    ]
    value = "b"

    def proper_init(self, serialisation_object):
        question = "Choose voxel setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "b":
            pass
        elif self.value == "u":
            self.b_set_or_create_member(inode=0, cls=UserVoxelSizePar, parent_serialiser=serialisation_object)
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class UserVoxelSizePar(night_rail.TypedPar):
    value = "1.0"
    b_my_histories_add_to = ["voxel"]
    b_my_histories_take_from = ["voxel"]
    asked_question = "Voxel size in micron"
    typed_default = "1.0"
    my_type = float


class RoughVoxelCorrectionPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["voxel"]
    b_my_histories_take_from = ["voxel"]
    asked_question = "Let this value to zero if you want to start from the bliss voxel size. If not zero it will be the initial size in the nexus files. In microns:"
    typed_default = "0"
    my_type = float


class CorValuePar(night_rail.TypedPar):
    value = "0.0"
    b_my_histories_add_to = ["cors"]
    b_my_histories_take_from = ["cors"]
    asked_question = "input the center of rotation"
    typed_default = "0.0"
    my_type = float

    def proper_init(self, serialisation_object):
        # this is a derivation of TypedPar which already is
        # a specialisation of BasePar.
        # No need to define the proper_init in this case.
        # If the one defined in TypedPar is enough we
        # simple call the parent class proper_init
        # This method could be removed. Left here as a template
        super().proper_init(serialisation_object)


class CorSchemePar(night_rail.BasePar):
    possible_determination_schemes = ["from [U]ser value", "from [A]utomatic estimation"]
    value = "u"

    def proper_init(self, serialisation_object):
        question = "Choose cor setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "u":
            self.b_set_or_create_member(inode=0, cls=CorValuePar, parent_serialiser=serialisation_object)
        elif self.value in ["a"]:
            self.b_set_or_create_member(inode=0, cls=CompositeCorFinderPar, parent_serialiser=serialisation_object)
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class CorInterpolationSchemePar(night_rail.BasePar):
    possible_determination_schemes = ["apply [L]inear interpolation", "[N]o interpolation "]
    value = "n"
    tacit = True

    def proper_init(self, serialisation_object):
        question = "Choose cor setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class AngularToleranceInStepsPar(night_rail.TypedPar):
    value = "5"
    b_my_histories_add_to = ["angular_tolerance"]
    b_my_histories_take_from = ["angular_tolerancel"]
    asked_question = "How may steps may be missing from the scan that can be tolerated? ( sometimes scan stop short of making a full turn ) "
    typed_default = "5"
    my_type = night_rail.limited_typed_class(int, 0, None)


class NearCorValuePar(night_rail.TypedPar):
    value = "0.0"
    b_my_histories_add_to = ["cors"]
    b_my_histories_take_from = ["cors"]
    asked_question = "input the near value for the center of rotation"
    typed_default = "0.0"
    my_type = float


class NearWidthValuePar(night_rail.TypedPar):
    value = "20"
    b_my_histories_add_to = ["near_widths"]
    b_my_histories_take_from = ["near_widths"]
    asked_question = "input the width value for the near around the center of rotation"
    typed_default = "20"
    my_type = int


class VerticalErrorWindowValuePar(night_rail.TypedPar):
    value = "20.0"
    b_my_histories_add_to = ["vertical_turn_error_width"]
    b_my_histories_take_from = ["vertical_turn_error_width"]
    asked_question = "The maximum error, in pixels,  that you expect after one turn in vertical translation due to the error in the value voxel size"
    typed_default = "20.0"
    my_type = float


class LowPassValuePar(night_rail.TypedPar):
    value = "0.4"
    b_my_histories_add_to = ["low_pass"]
    b_my_histories_take_from = ["low_pass"]
    asked_question = "input, in pixel, the size beyond shorter than which signal is filtered"
    typed_default = "0.4"
    my_type = float


class HighPassValuePar(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["high_pass"]
    b_my_histories_take_from = ["high_pass"]
    asked_question = "input, in pixel, the size beyond longer than which signal is filtered"
    typed_default = "10"
    my_type = int


class CompCorThetaIntervalValuePar(night_rail.TypedPar):
    value = "5"
    b_my_histories_add_to = ["theta_interval"]
    b_my_histories_take_from = ["theta_interval"]
    asked_question = "The interval in degree between considered radios"
    typed_default = "5"
    my_type = float


class NSubsamplingValuePar(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["n_subsampling_y"]
    b_my_histories_take_from = ["n_subsampling_y"]
    asked_question = "How many lines from each radio are taken"
    typed_default = "10"
    my_type = int


class DeltaBetaPar(night_rail.TypedPar):
    value = "1000.0"
    b_my_histories_add_to = ["deltabeta"]
    b_my_histories_take_from = ["deltabeta"]
    asked_question = "the delta/beta ratio for the phase filter. ZERO to disactivate filtering."
    typed_default = "1000.0"
    my_type = float



class WeightHorizontalTransitionLengthPar(night_rail.TypedPar):
    value = 300
    b_my_histories_add_to = ["weight_horizontal_transition_length"]
    b_my_histories_take_from = ["weight_horizontal_transition_length"]
    asked_question = "Give a lenght >=0 for the horizontal transition at the map borders. Analogous of PENTE_ZONE. For full tomo a zero value may be pertinent  "
    typed_default = 300
    my_type = night_rail.limited_typed_class(float, 0.0, 100000)
    tacit = False

    
class WeightVerticalTransitionLengthPar(night_rail.TypedPar):
    value = 15.0
    b_my_histories_add_to = ["weight_vertical_transition_length"]
    b_my_histories_take_from = ["weight_vertical_transition_length"]
    asked_question = "Give a lenght > 2.0 for the vertical transition at the map borders  "
    typed_default = 15.0
    my_type = night_rail.limited_typed_class(float, 2.0, 100000)
    tacit = False

class StripesShearMin(night_rail.TypedPar):
    value = "-1.0"
    b_my_histories_add_to = ["stripes_tan"]
    b_my_histories_take_from = ["stripe_tan"]
    asked_question = "Minimum Tangent value of the shear"
    typed_default = "-1.0"
    my_type = float
    
class StripesShearMax(night_rail.TypedPar):
    value = "1.0"
    b_my_histories_add_to = ["stripes_tan"]
    b_my_histories_take_from = ["stripe_tan"]
    asked_question = "Maximum Tangent value of the shear"
    typed_default = "1.0"
    my_type = float

class NumberOfScintillatorStripes(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["n_stripes"]
    b_my_histories_take_from = ["n_stripes"]
    asked_question = "How many stripes in the scintillator need to be covered? ( 0 no covering ) "
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)

class StripeWidth(night_rail.TypedPar):
    value = "10.0"
    b_my_histories_add_to = ["stripes_width"]
    b_my_histories_take_from = ["stripe_width"]
    asked_question = "stripe width"
    typed_default = "10.0"
    my_type = float


class ScintillatorStripesWeightingScheme(night_rail.BasePar):


    def proper_init(self, serialisation_object):

        self.b_set_or_create_member(inode=0, cls=WeightVerticalTransitionLengthPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=WeightHorizontalTransitionLengthPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NumberOfScintillatorStripes, parent_serialiser=serialisation_object)

        if int(self.all_members[2].value) > 0:
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripeWidth, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripesShearMin, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripesShearMax, parent_serialiser=serialisation_object)


class NearCorValueScheme(night_rail.BasePar):
    possible_determination_schemes = [
        "[U]ser setted near value", "from [y]rot in bliss file"
    ]
    value = "y"

    def proper_init(self, serialisation_object):
        question = "Choose near value scheme (center of the search region): " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "u":
            self.b_set_or_create_member(inode=0, cls=NearCorValuePar, parent_serialiser=serialisation_object)

    

class CompositeCorFinderPar(night_rail.BasePar):
    def proper_init(self, serialisation_object):
        self.b_set_or_create_member(inode=0, cls=NearCorValueScheme, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NearWidthValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=LowPassValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=HighPassValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CompCorThetaIntervalValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NSubsamplingValuePar, parent_serialiser=serialisation_object)


class VerticalRangeSchemePar(night_rail.BasePar):
    possible_determination_schemes = [
        "A slice at a [F]raction of the range,  from slices [I]ndexes",
        "from slice [M]illimiters, [A]ll",
    ]
    value = "a"
    tacit = True

    def proper_init(self, serialisation_object):
        question = "Choose vertical range setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "a":
            # the whole is done. So this is all we need to know
            pass
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class ReconstructionSchemePar(night_rail.BasePar):
    possible_schemes = ["[N]o reconstruction", "[H]elical reconstruction"]
    value = "h"
    tacit = True

    def proper_init(self, serialisation_object):
        question = "Choose reconstruction: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "n":
            pass
        elif self.value in ["h", "u"]:
            self.b_set_or_create_member(inode=0, cls=DeltaBetaPar, parent_serialiser=serialisation_object)

            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=VerticalRangeSchemePar, parent_serialiser=serialisation_object)

            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ChunkSchemePar, parent_serialiser=serialisation_object)
            if self.value == "h":

                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(), cls=ScintillatorStripesWeightingScheme , parent_serialiser=serialisation_object
                )

        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class ChunkSizePar(night_rail.TypedPar):
    value = "41"
    b_my_histories_add_to = ["chunk_size"]
    b_my_histories_take_from = ["chunk_size"]
    asked_question = "Give the max chunk size for Nabu reconstruction: "
    typed_default = "41"
    my_type = int
    tacit = True


class ChunkSchemePar(night_rail.BasePar):
    possible_schemes = ["[N]o chunk limitation", "[C]hunk limited size"]
    value = "c"
    tacit = True

    def proper_init(self, serialisation_object):
        question = "Choose chunk size: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "n":
            pass
        elif self.value == "c":
            self.b_set_or_create_member(inode=0, cls=ChunkSizePar, parent_serialiser=serialisation_object)
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class WorkFlowScheme(night_rail.BasePar):
    __version__ = night_rail_bm18.__version__
    b_is_root = True

    possible_schemes = ["[D]etector corrections"]
    value = "d"

    def proper_init(self, serialisation_object):
        if self.value == "d":
            self.b_set_or_create_member(inode=0, cls=DatasetLocationScheme, parent_serialiser=serialisation_object)

            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=VoxelSchemePar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CorSchemePar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ReconstructionSchemePar, parent_serialiser=serialisation_object)

            self.b_set_or_create_member(
                inode=self.b_next_inode_num(),
                cls=AngularToleranceInStepsPar,
                parent_serialiser=serialisation_object,
            )
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=OutputFormatPar, parent_serialiser=serialisation_object
            )

        self.b_set_or_create_member(
            inode=self.b_next_inode_num(),
            cls=night_rail.SessionNamePar,
            parent_serialiser=serialisation_object,
        )

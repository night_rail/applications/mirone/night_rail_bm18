import night_rail
import night_rail_bm18
import sys
import inspect
import json
import os

class BlissFilePath(night_rail.LocationByPath):
    punctiliousness_handle = "data_selection"


class BlissUniqueScan(BlissFilePath):
    question = "Choose bliss dataset  by input and autocompletion: "
    path_is_file = True


class BlissFirstScan(BlissFilePath):
    question = "Choose the first bliss dataset of the  series : "
    path_is_file = True

class BlissLastScan(BlissFilePath):
    question = "Choose the last bliss dataset of the  series : "
    path_is_file = True


class NexusUniqueScan(night_rail.LocationByPath):
    b_my_histories_add_to = ["bliss_path"]
    b_my_histories_take_from = ["bliss_path"]
    question = "Choose nexus dataset  by input and autocompletion: "
    path_is_file = True


class NexusFirstScan(NexusUniqueScan):
    question = "Choose the first nexus dataset of the  series : "
    path_is_file = True


class NexusLastScan(NexusUniqueScan):
    question = "Choose the last nexus dataset of the  series : "
    path_is_file = True


class BlissRefBeginScan(BlissFilePath):
    question = "Choose the Reference bliss dataset for the  series begin: "
    path_is_file = True


class BlissRefEndScan(BlissFilePath):
    question = "Choose the Reference bliss dataset for the  series end: "
    path_is_file = True


class RefExtractionTargetDir(night_rail.LocationByPath):
    question = "Chose the target directory for extracted flats/darks"
    path_is_dir = True
    create_dir_on_demand = True
    default = "./EXTRACTED_DARKS_FLATS"
    value = default


class OutputFormatPar(night_rail.BasePar):
    possible_choices = ["output will be [H]df5 file", "[T]iff files","[E]df files"]
    value = "t"

    def proper_init(self, serialisation_object):
        question = "Choose output format: " + (", ".join(self.possible_choices))
        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )



class WobbleCorrectionScheme(night_rail.BasePar):
    possible_choices = ["[W]obble correction", "[N]o wobble correction"]
    value = "n"
    punctiliousness_handle = "wobble_correction"
    punctiliousness_offset =  3
    def proper_init(self, serialisation_object):
        question = "Should I use:" + " ".join(
            self.possible_choices
        )

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        

class HierarchicalOrStandardBackprojectionPar(night_rail.BasePar):
    possible_choices = ["[B]ackprojection", "[G]eneralised hierarchical backprojection"]
    value = "g"
    punctiliousness_handle = "calculation"
    punctiliousness_offset = 0
    def proper_init(self, serialisation_object):
        question = "Your are running a reconstruction Should I use:" + " ".join(
            self.possible_choices
        )

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class ExtraPaddingFactorPar(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["padding_factor"]
    b_my_histories_take_from = ["padding_factor"]
    asked_question = "How much do you want to stretch the padding width"
    typed_default = "1"
    my_type = int 
    punctiliousness_handle = "calculation"
    punctiliousness_offset = 0

        
class ScintillatorDiffusionFractionPar(night_rail.TypedPar):
    value = "0.1"
    b_my_histories_add_to = ["diffusion_fraction"]
    b_my_histories_take_from = ["diffusion_fraction"]    
    asked_question = "The fraction of diffused light"
    typed_default = "0.1"
    my_type = float
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 0

class ScintillatorDiffusionLenghtPar(night_rail.TypedPar):
    value = "100"
    b_my_histories_add_to = ["diffusion_lenght"]
    b_my_histories_take_from = ["diffusion_lenght"]
    asked_question = "The damping  lenght of diffused light"
    typed_default = "100"
    my_type = float
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 0

class ScintillatorDiffusionBinProjectionPar(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["diffusion_bin_proj"]
    b_my_histories_take_from = ["diffusion_bin_proj"]
    asked_question = "The sampling over projections"
    typed_default = "10"
    my_type = night_rail.limited_typed_class(int, 1, None)
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 1

class ScintillatorDiffusionBinPixelPar(night_rail.TypedPar):
    value = "4"
    b_my_histories_add_to = ["diffusion_bin_pixel"]
    b_my_histories_take_from = ["diffusion_bin_pixel"]
    asked_question = "The sampling over pixels ( both axis)"
    typed_default = "4"
    my_type = night_rail.limited_typed_class(int, 1, None)
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 1
    
class ScintillatorDiffusionBorderInPixelsPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["diffusion_border"]
    b_my_histories_take_from = ["diffusion_border"]
    asked_question = "The illuminated scintillator border around the roi in pixels"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 0

class WeightHorizontalMendingFractionalThreshold(night_rail.TypedPar):
    value = "0.0001"
    b_my_histories_add_to = ["fractional_threshold"]
    b_my_histories_take_from = ["fractional_threshold"]    
    asked_question = "The pixel whose collected weight is less that the average weight times this fraction will be horizontally mended with the nearest good pixel value"
    typed_default = "0.0001"
    my_type = float
    punctiliousness_handle = "weights"
    punctiliousness_offset = 0


    
class StripesShearMin(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["stripes_tan"]
    b_my_histories_take_from = ["stripe_tan"]
    asked_question = "Minimum Tangent value of the shear"
    typed_default = "0"
    my_type = float    
    punctiliousness_handle = "stripes"
    
class StripesShearMax(night_rail.TypedPar):
    value = "0.33"
    b_my_histories_add_to = ["stripes_tan"]
    b_my_histories_take_from = ["stripe_tan"]
    asked_question = "Maximum Tangent value of the shear"
    typed_default = "0.33"
    my_type = float
    punctiliousness_handle = "stripes"

class NumberOfScintillatorStripes(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["n_stripes"]
    b_my_histories_take_from = ["n_stripes"]
    asked_question = "How many stripes in the scintillator need to be covered? ( 0 no covering ) "
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)

class StripeWidth(night_rail.TypedPar):
    value = "10.0"
    b_my_histories_add_to = ["stripes_width"]
    b_my_histories_take_from = ["stripe_width"]
    asked_question = "stripe width"
    typed_default = "10.0"
    my_type = float
    punctiliousness_handle = "stripes"
    punctiliousness_offset = -1
    


    
class WeightHorizontalTransitionLengthPar(night_rail.TypedPar):
    value = "1000"
    b_my_histories_add_to = ["weight_horizontal_transition_length"]
    b_my_histories_take_from = ["weight_horizontal_transition_length"]
    asked_question = "Give a lenght >=0 for the horizontal transition at the map borders. Analogous of PENTE_ZONE. For full tomo a zero value may be pertinent  "
    typed_default = "1000"
    my_type = night_rail.limited_typed_class(float, 0.0, 100000)
    tacit = False

class ScintillatorStripesWeightingScheme(night_rail.BasePar):

    def proper_init(self, serialisation_object):

        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=WeightVerticalTransitionLengthPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=WeightHorizontalTransitionLengthPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=WeightHorizontalMendingFractionalThreshold, parent_serialiser=serialisation_object)
        
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NumberOfScintillatorStripes, parent_serialiser=serialisation_object)

        if int(self.all_members[3].value) > 0: # this is the NumberOfScintillatorStripes above ( position 3, after 0 ( first)  , 1 and 2
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripeWidth, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripesShearMin, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripesShearMax, parent_serialiser=serialisation_object)

class ComesFromAspectScheme(night_rail.BasePar):
    possible_choices = ["[Y]es", "[N]o"]
    value = "y"

    global_value = "n"

    def proper_init(self, serialisation_object):
        question = "You are reusing Nexus files for a reconstruction. Do They come from Aspect? " + " ".join(
            self.possible_choices
        )

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        ComesFromAspectScheme.global_value = self.value


class DatasetLocationScheme(night_rail.BasePar):
    possible_choices = ["from [B]liss files", "from [N]exus file"]
    value = "b"
    wf = "nabu"

    def proper_init(self, serialisation_object):
        question = "Choose wether you start from bliss files or nexus one: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "b":
            if self.wf == "nabu":
                # let inode=0 here to reset the counter. This can run on already setted tree
                self.b_set_or_create_member(inode=0, cls=DatasetLocation, parent_serialiser=serialisation_object)
            else:
                # let inode=0 here to reset the counter. This can run on already setted tree
                self.b_set_or_create_member(
                    inode=0, cls=DatasetLocationForAspect, parent_serialiser=serialisation_object
                )
        if self.value == "n":
            if self.wf == "nabu":
                # let inode=0 here to reset the counter. This can run on already setted tree
                self.b_set_or_create_member(inode=0, cls=NexusLocation, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ComesFromAspectScheme, parent_serialiser=serialisation_object)
            else:
                # let inode=0 here to reset the counter. This can run on already setted tree
                self.b_set_or_create_member(inode=0, cls=NexusLocationForAspect, parent_serialiser=serialisation_object)


class DatasetLocationForAspectScheme(DatasetLocationScheme):
    possible_choices = ["from [B]liss files", "from [N]exus file"]
    value = "b"
    wf = "aspect"

class NumAngularSectorsRefPar(night_rail.TypedPar):
    value = "30"
    b_my_histories_add_to = ["num_angular_sectors"]
    b_my_histories_take_from = ["num_angular_sectors"]
    asked_question = "In how many sectors do you want to partially average the reference scans:"
    typed_default = "30"
    my_type = int


class ProjectionsStartPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["projections_start"]
    b_my_histories_take_from = ["projections_start"]
    asked_question = "For huge reference scans you can increase this number. It is the starting projection number for the interation range"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)    
    punctiliousness_handle = "data_selection"
    punctiliousness_offset = 1
    
class ProjectionsEndPar(night_rail.TypedPar):
    value = "-1"
    b_my_histories_add_to = ["projections_stop"]
    b_my_histories_take_from = ["projections_stop"]
    asked_question = "For huge reference scans you can use this number. It is the end projection number for the integration range"
    typed_default = "-1"
    my_type = int
    punctiliousness_handle = "data_selection"
    punctiliousness_offset = 1
   
class ProjectionsStepPar(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["projections_step"]
    b_my_histories_take_from = ["projections_step"]
    asked_question = "For huge reference scans you can use this number. It is the step in the integration range"
    typed_default = "1"
    my_type = night_rail.limited_typed_class(int, 1, None)
    punctiliousness_handle = "data_selection"
    punctiliousness_offset = 1

    
class SlurmFractionsNumberPar(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["slurm_fractions_number"]
    b_my_histories_take_from = ["slurm_fractions_number"]
    asked_question = "In case slurm is used in  how many jobs do you want to split the final reconstruction "
    typed_default = "1"
    my_type = night_rail.limited_typed_class(int, 0, None)


# class TargetNumProjectionsBySectorsPar(night_rail.TypedPar):
#     value = "12000"
#     b_my_histories_add_to = ["TargetNumProjectionsBySectorsPar"]
#     b_my_histories_take_from = ["TargetNumProjectionsBySectorsPar"]
#     asked_question = "How many projection in the target scan:"
#     typed_default = "1200"
#     my_type = int
    
class FrequencySeparationRadiusPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["FrequencySeparationRadius"]
    b_my_histories_take_from = ["FrequencySeparationRadius"]
    asked_question = "Gaussian sigma for frequency separation in angular averaging of the reference:"
    typed_default = "0"
    my_type = int

    
class ReferenceBySectorScheme(night_rail.BasePar):
    possible_choices = [" [Y]es ", " [N]o "]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "Do you want to do averaging by angular sectors" + " ".join(
            self.possible_choices
        )

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "y":
            # let inode=0 here to reset the counter. This can run on already setted tree
        
            self.b_set_or_create_member( inode=0, cls=NumAngularSectorsRefPar         ,  parent_serialiser=serialisation_object, )
#            self.b_set_or_create_member( inode=self.b_next_inode_num(), cls=TargetNumProjectionsBySectorsPar,  parent_serialiser=serialisation_object, )
            self.b_set_or_create_member( inode=self.b_next_inode_num(), cls=FrequencySeparationRadiusPar,  parent_serialiser=serialisation_object, )
            
            self.b_set_or_create_member( inode=self.b_next_inode_num(), cls=ProjectionsStartPar,  parent_serialiser=serialisation_object, )
            self.b_set_or_create_member( inode=self.b_next_inode_num(), cls=ProjectionsEndPar,  parent_serialiser=serialisation_object, )
            self.b_set_or_create_member( inode=self.b_next_inode_num(), cls=ProjectionsStepPar,  parent_serialiser=serialisation_object, )


        
class DatasetLocation(night_rail.BasePar):
    value = "p"
    my_workflow = "nabu"
    from_nexus = False

    def proper_init(self, serialisation_object):
        if not self.from_nexus:
            possible_localisation_schemes = [
                "from [P]ath",
                "from [M]ultiple paths",
                "From multiple path and [R]eference scans",
            ]
        else:
            possible_localisation_schemes = ["from [P]ath", "from [M]ultiple paths"]

        question = "Choose dataset localisation scheme: " + (", ".join(possible_localisation_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if not self.from_nexus:
            UniqueScan, FirstScan, LastScan, RefBeginScan, RefEndScan = (
                BlissUniqueScan,
                BlissFirstScan,
                BlissLastScan,
                BlissRefBeginScan,
                BlissRefEndScan,
            )
        else:
            UniqueScan, FirstScan, LastScan = NexusUniqueScan, NexusFirstScan, NexusLastScan

        if self.value == "p":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=UniqueScan, parent_serialiser=serialisation_object)

        elif self.value in ["m", "r"]:
            # let inode=0 here to reset the counter. This can run on already setted tree
            
            self.b_set_or_create_member(inode=0, cls=FirstScan, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=LastScan, parent_serialiser=serialisation_object)

            if self.value == "r":
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=RefBeginScan, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=RefEndScan, parent_serialiser=serialisation_object)
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(),
                    cls=RefExtractionTargetDir,
                    parent_serialiser=serialisation_object,
                )
                
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(),
                    cls=ReferenceBySectorScheme,
                    parent_serialiser=serialisation_object,
                )

            if self.my_workflow != "aspect":
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(),
                    cls=MultiZRecScheme,
                    parent_serialiser=serialisation_object,
                )
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """
        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=EntryNamePar, parent_serialiser=serialisation_object
        )

        if not self.from_nexus:
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(),
                cls=RoughVoxelCorrectionPar,
                parent_serialiser=serialisation_object,
            )


class DatasetLocationForAspect(DatasetLocation):
    my_workflow = "aspect"


class NexusLocation(DatasetLocation):
    from_nexus = True


class NexusLocationForAspect(DatasetLocation):
    my_workflow = "aspect"
    from_nexus = True


class EntryNamePar(night_rail.TypedPar):
    value = "entry0000"
    b_my_histories_add_to = ["entry_name"]
    b_my_histories_take_from = ["entry_name"]
    asked_question = "The entry name is in general entry0000 ( default). But you can possibly change it here : "
    typed_default = "entry0000"
    my_type = str
    punctiliousness_handle = "generic"
    punctiliousness_offset = 1


class ScintillatorDiffusionCorrectionSchemePar(night_rail.BasePar):
    possible_choices = ["[N]o scintillator diffusion correction", "following [E]PR paper"]
    value = "n"
    punctiliousness_handle = "scintillator"
    punctiliousness_offset = 0
    def proper_init(self, serialisation_object):
        question = "Choose scintillator diffusion correction scheme: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "e":
            # let inode=0 here to reset the counter. This can run on already setted tree
            
            self.b_set_or_create_member(inode=0, cls=ScintillatorDiffusionFractionPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorDiffusionLenghtPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorDiffusionBinProjectionPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorDiffusionBinPixelPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorDiffusionBorderInPixelsPar, parent_serialiser=serialisation_object)

    
class DetectorCorrectionSchemePar(night_rail.BasePar):
    possible_choices = ["[N]o correction", "from [M]ap file"]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "Choose detector correction scheme: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "m":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=MapXzPathPar, parent_serialiser=serialisation_object)


class DoubleFlatsFilterMethodPar(night_rail.BasePar):
    possible_choices = ["[G]aussian filter", "[M]edian filter"]
    value = "g"
    def proper_init(self, serialisation_object):
        question = "How do you filter the averages? " + " ".join(
            self.possible_choices
        )
        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        
class DoubleFlatsPeriodNturns(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["DoubleFlatsPeriodNturns"]
    b_my_histories_take_from = ["DoubleFlatsPeriodNturns"]
    asked_question = "On how many turns do you do the average: "
    typed_default = "1"
    my_type = night_rail.limited_typed_class(int, 1, None)

class DoubleFlatsNsectorsPerTurn(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["DoubleFlatsNsectorsPerTurn"]
    b_my_histories_take_from = ["DoubleFlatsNsectorsPerTurn"]
    asked_question = "How many sectors for a turn: "
    typed_default = "1"
    my_type = night_rail.limited_typed_class(int, 1, None)

class DoubleFlatsProjStep(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["DoubleFlatsProjStep"]
    b_my_histories_take_from = ["DoubleFlatsProjStep"]
    asked_question = "Step between considered radios: "
    typed_default = "1"
    my_type = night_rail.limited_typed_class(int, 1, None)

class DoubleFlatsSigmaPix(night_rail.TypedPar):
    value = "100"
    b_my_histories_add_to = ["DoubleFlatsSigmaPix"]
    b_my_histories_take_from = ["DoubleFlatsSigmaPix"]
    asked_question = "The sigma for the filter : "
    typed_default = "100"
    my_type = night_rail.limited_typed_class(int, 1, None)
    
class DoubleFlatsRadiusPixBasic(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["DoubleFlatsRadiusPix"]
    b_my_histories_take_from = ["DoubleFlatsRadiusPix"]
    asked_question = "The radius for the filter : "
    typed_default = "10"
    my_type = night_rail.limited_typed_class(int, 1, 37)

class DoubleFlatsRadiusPix(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["DoubleFlatsRadiusPix"]
    b_my_histories_take_from = ["DoubleFlatsRadiusPix"]
    asked_question = "The radius for the filter : "
    typed_default = "10"
    my_type = night_rail.limited_typed_class(int, 1, None)




class DoubleFlatsMultiAngleMedScheme(night_rail.BasePar):
    possible_choices = ["[N]o multi angle med", " [A]pply multi angle med"]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "Choose double_flats averaging/filtering scheme: " + (", ".join(self.possible_choices))
        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


    
class DoubleFlatsMethodScheme(night_rail.BasePar):
    possible_choices = ["[B]asic multiplicative scheme", " [V]ariation on the additive scheme"]
    value = "v"
    def proper_init(self, serialisation_object):
        question = "Choose double_flats averaging/filtering scheme: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "v":
            self.b_set_or_create_member(inode=0, cls=DoubleFlatsMultiAngleMedScheme, parent_serialiser=serialisation_object)

    
    
class DoubleFlatsScheme(night_rail.BasePar):
    possible_choices = ["[N]o double flats", "apply [D]ouble flats"]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "Choose double_flats scheme: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "d":
            # let inode=0 here to reset the counter. This can run on already setted tree
            method_object = self.b_set_or_create_member(inode=0, cls=DoubleFlatsFilterMethodPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsPeriodNturns, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsNsectorsPerTurn, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsProjStep, parent_serialiser=serialisation_object)
            df_method = self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsMethodScheme, parent_serialiser=serialisation_object)

            if method_object.value == "g":
                self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsSigmaPix, parent_serialiser=serialisation_object)
            else:
                if df_method.value == "b":
                    self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsRadiusPixBasic, parent_serialiser=serialisation_object)
                else:
                    self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DoubleFlatsRadiusPix, parent_serialiser=serialisation_object)




class ChebychevStraighteningPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["n_cheb"]
    b_my_histories_take_from = ["n_cheb"]
    asked_question = "Chebychev Straightening level , zero(default: no straightning) or 1"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, 1)
    punctiliousness_handle = "generic"
    punctiliousness_offset = 0


class MaxDarkPar(night_rail.TypedPar):
    value = "0.0"
    b_my_histories_add_to = ["max_dark"]
    b_my_histories_take_from = ["max_dark"]
    asked_question = "Set a number different from zero if you want to limit the dark to such value (So far this is available only with the 'concatenation' option) "
    typed_default = "0.0"
    my_type = night_rail.limited_typed_class(float, 0, None)
    
            
class MultiZRecScheme(night_rail.BasePar):
    possible_choices = ["[S]eparately", "[A]fter concatenation"]
    value = "a"

    global_value = "a"
    
    def proper_init(self, serialisation_object):
        question = "Choose scheme for reconstructing the whole stack: " + (", ".join(self.possible_choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        MultiZRecScheme.global_value = self.value
        if self.value == "a":
            self.b_set_or_create_member(inode=0, cls=MaxDarkPar, parent_serialiser=serialisation_object)
        

class MapXzPathPar(night_rail.BasePar):
    punctiliousness_handle = "data_selection"
    b_my_histories_add_to = ["mapxz_path"]
    b_my_histories_take_from = ["bliss_path", "mapxz_path"]
    path_is_file = True

    def proper_init(self, serialisation_object):
        question = "Choose mapxz file by input and autocompletion: "

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
            autocompletion=True,
        )
        
        if self.value:
            self.value = os.path.realpath(self.value)

        self.question_answer_dict[  list(self.question_answer_dict.keys() )[0]   ] = self.value

        
class VoxelSchemePar(night_rail.BasePar):
    possible_determination_schemes = [
        "Voxel size: Use the [B]liss provided value",
        "from [U]ser provided  value",
        "from [A]utomatic estimation by correlations",
    ]
    value = "b"

    def proper_init(self, serialisation_object):
        question = "Choose voxel setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "b":
            pass
        # let inode=0 here to reset the counter. This can run on already setted tree
        elif self.value == "u":
            self.b_set_or_create_member(inode=0, cls=UserVoxelSizePar, parent_serialiser=serialisation_object)
        elif self.value == "a":
            self.b_set_or_create_member(
                inode=0, cls=CompositeVoxelSizeFinderPar, parent_serialiser=serialisation_object
            )
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class UserVoxelSizePar(night_rail.TypedPar):
    value = "1.0"
    b_my_histories_add_to = ["voxel"]
    b_my_histories_take_from = ["voxel"]
    asked_question = "Voxel size in micron"
    typed_default = "1.0"
    my_type = float


class RoughVoxelCorrectionPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["voxel"]
    b_my_histories_take_from = ["voxel"]
    asked_question = "Let this value to zero if you want to start from the bliss voxel size. If not zero it will be the initial size in the nexus files. In microns:"
    typed_default = "0"
    my_type = float
    punctiliousness_handle = "generic"
    punctiliousness_offset = 0


class CorValuePar(night_rail.TypedPar):
    value = "0.0"
    b_my_histories_add_to = ["cors"]
    b_my_histories_take_from = ["cors"]
    asked_question = "input the center of rotation"
    typed_default = "0.0"
    my_type = float

    def proper_init(self, serialisation_object):
        # this is a derivation of TypedPar which already is
        # a specialisation of BasePar.
        # No need to define the proper_init in this case.
        # If the one defined in TypedPar is enough we
        # simple call the parent class proper_init
        # This method could be removed. Left here as a template
        super().proper_init(serialisation_object)


class CorSchemePar(night_rail.BasePar):
    possible_determination_schemes = ["from [U]ser value", "from [A]utomatic estimation", "from [D]iagnostics"]
    value = "a"
    punctiliousness_handle = "cor_search"
    
    def proper_init(self, serialisation_object):
        question = "Choose cor setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=WobbleCorrectionScheme , parent_serialiser=serialisation_object)

        if self.value == "u":
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CorValuePar, parent_serialiser=serialisation_object)
            
        elif self.value in ["a", "d"]:
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CompositeCorFinderPar, parent_serialiser=serialisation_object)
            
            if self.value == "d":
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(), cls=CorInterpolationSchemePar, parent_serialiser=serialisation_object
                )
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(), cls=DiagRotRangeFractionStartPar, parent_serialiser=serialisation_object
                )
                self.b_set_or_create_member(
                    inode=self.b_next_inode_num(), cls=DiagRotRangeFractionEndPar, parent_serialiser=serialisation_object
                )

        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """



class CorWeightSchemePar(night_rail.BasePar):
    possible_determination_schemes = ["do [N]ot use the weigth map", "use the [W]eight map in the loss function"]
    value = "w"
    punctiliousness_handle = "cor_search"
    
    def proper_init(self, serialisation_object):
        question = "Choose cor setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
            
class CorInterpolationSchemePar(night_rail.BasePar):
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1

    possible_determination_schemes = ["apply [L]inear interpolation", "[N]o interpolation "]
    value = "l"

    def proper_init(self, serialisation_object):
        question = "Choose cor setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class AngularToleranceInStepsPar(night_rail.TypedPar):
    value = "5"
    b_my_histories_add_to = ["angular_tolerance"]
    b_my_histories_take_from = ["angular_tolerancel"]
    asked_question = "How many steps may be missing from the scan that can be tolerated? ( sometimes scan stop short of making a full turn ) "
    typed_default = "5"
    my_type = night_rail.limited_typed_class(int, 0, None)
    punctiliousness_handle = "calculation"
    punctiliousness_offset = 0


class NearCorValueScheme(night_rail.BasePar):
    possible_determination_schemes = [
        "[U]ser setted near value", "from [Y]rot in bliss file"
    ]
    value = "y"
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1

    def proper_init(self, serialisation_object):
        question = "Choose near value scheme (center of the search region): " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "u":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=NearCorValuePar, parent_serialiser=serialisation_object)


            
class NearCorValuePar(night_rail.TypedPar):
    value = "0.0"
    b_my_histories_add_to = ["cors"]
    b_my_histories_take_from = ["cors"]
    asked_question = "input the near value for the center of rotation"
    typed_default = "0.0"
    my_type = float


class NearWidthValuePar(night_rail.TypedPar):
    value = "40"
    b_my_histories_add_to = ["near_widths"]
    b_my_histories_take_from = ["near_widths"]
    asked_question = "input the width value for the near around the center of rotation"
    typed_default = "40"
    my_type = int
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 0


class VerticalErrorWindowValuePar(night_rail.TypedPar):
    value = "20.0"
    b_my_histories_add_to = ["vertical_turn_error_width"]
    b_my_histories_take_from = ["vertical_turn_error_width"]
    asked_question = "The maximum error, in pixels,  that you expect after one turn in vertical translation due to the error in the value voxel size"
    typed_default = "20.0"
    my_type = float


class LowPassValuePar(night_rail.TypedPar):
    value = "0.4"
    b_my_histories_add_to = ["low_pass"]
    b_my_histories_take_from = ["low_pass"]
    asked_question = "input, in pixel, the size beyond shorter than which signal is filtered"
    typed_default = "0.4"
    my_type = float
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1


class HighPassValuePar(night_rail.TypedPar):
    value = "10"
    b_my_histories_add_to = ["high_pass"]
    b_my_histories_take_from = ["high_pass"]
    asked_question = "input, in pixel, the size beyond longer than which signal is filtered"
    typed_default = "10"
    my_type = int
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1


class CompCorThetaIntervalValuePar(night_rail.TypedPar):
    value = "5"
    b_my_histories_add_to = ["theta_interval"]
    b_my_histories_take_from = ["theta_interval"]
    asked_question = "The interval in degree between considered radios"
    typed_default = "5"
    my_type = float
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1


class DiagPixThetaIntervalValuePar(night_rail.TypedPar):
    value = "90"
    b_my_histories_add_to = ["theta_interval"]
    b_my_histories_take_from = ["theta_interval"]
    asked_question = "The interval in degree between considered radios"
    typed_default = "90"
    my_type = float


class NSubsamplingValuePar(night_rail.TypedPar):
    value = "40"
    b_my_histories_add_to = ["n_subsampling_y"]
    b_my_histories_take_from = ["n_subsampling_y"]
    asked_question = "How many lines from each radio are taken"
    typed_default = "40"
    my_type = int
    punctiliousness_handle = "cor_search"
    punctiliousness_offset = 1


class DeltaBetaPar(night_rail.TypedPar):
    value = "1000.0"
    b_my_histories_add_to = ["deltabeta"]
    b_my_histories_take_from = ["deltabeta"]
    asked_question = "the delta/beta ratio for the phase filter. ZERO to disactivate filtering."
    typed_default = "1000.0"
    my_type = float

    punctiliousness_handle = "paganin_phase"


class RangeFractionStartPar(night_rail.TypedPar):
    value = "0.5"
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = "Give start fractional position of the starting slice between 0.0 and 1.0"
    typed_default = 0.5
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class RangeFractionEndPar(night_rail.TypedPar):
    value = "0.5"
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = "Give end fractional position of the starting slice between 0.0 and 1.0. Possibly equal to start"
    typed_default = 0.5
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class DiagPixRangeFractionStartPar(night_rail.TypedPar):
    value = "0."
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = (
        "For pixel diagnostics collection give start fractional position of the starting slice between 0.0 and 1.0"
    )
    typed_default = 0.0
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class DiagPixRangeFractionEndPar(night_rail.TypedPar):
    value = "1."
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = (
        "For pixel diagnostics collection give end fractional position of the starting slice between 0.0 and 1.0"
    )
    typed_default = 1.0
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class DiagRotRangeFractionStartPar(night_rail.TypedPar):
    value = "0."
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = (
        "For cor diagnostics collection give start fractional position of the starting slice between 0.0 and 1.0"
    )
    typed_default = 0.0
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class DiagRotRangeFractionEndPar(night_rail.TypedPar):
    value = "1."
    b_my_histories_add_to = ["fraction"]
    b_my_histories_take_from = ["fraction"]
    asked_question = (
        "For cor diagnostics collection give end fractional position of the starting slice between 0.0 and 1.0"
    )
    typed_default = 1.0
    my_type = night_rail.limited_typed_class(float, 0.0, 1.0)


class WeightVerticalTransitionLengthPar(night_rail.TypedPar):
    value = "15.0"
    b_my_histories_add_to = ["weight_vertical_transition_length"]
    b_my_histories_take_from = ["weight_vertical_transition_length"]
    asked_question = "Give a lenght > 2.0 for the vertical transition at the map borders  "
    typed_default = 15.0
    my_type = night_rail.limited_typed_class(float, 2.0, 100000)


class CompositeCorFinderPar(night_rail.BasePar):
    def proper_init(self, serialisation_object):
        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=NearCorValueScheme, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NearWidthValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CorWeightSchemePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=LowPassValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=HighPassValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CompCorThetaIntervalValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NSubsamplingValuePar, parent_serialiser=serialisation_object)


class CompositeVoxelSizeFinderPar(night_rail.BasePar):
    def proper_init(self, serialisation_object):
        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=VerticalErrorWindowValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DiagPixThetaIntervalValuePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DiagPixRangeFractionStartPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DiagPixRangeFractionEndPar, parent_serialiser=serialisation_object)


class StartZPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["slice_index"]
    b_my_histories_take_from = ["slice_index"]
    asked_question = "give start_z (0 is the first doable slice, near the the acquisition beginning)"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)


class EndZPar(StartZPar):
    asked_question = "give end_z"


class StartZMmPar(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["slice_index"]
    b_my_histories_take_from = ["slice_index"]
    asked_question = "give start_z_mm (millimiters above the sample stage)"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(float, 0, None)


class EndZMmPar(night_rail.TypedPar):
    asked_question = "give end_z_mm (millimiters above the sample stage)"


class VerticalRangeSchemeNabuStandardPar(night_rail.BasePar):
    possible_determination_schemes = [" from slices [I]ndexes"]
    value = "i"


class VerticalRangeSchemePar(night_rail.BasePar):
    possible_determination_schemes = [
        "A slice at a [F]raction of the range","  from slices [I]ndexes, [M]iddle slice, middle of the [L]ast turn"
        " [A]ll",
    ]
    value = "m"

    def proper_init(self, serialisation_object):
        question = "Choose vertical range setting scheme: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "f":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=RangeFractionStartPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=RangeFractionEndPar, parent_serialiser=serialisation_object)

        elif self.value == "i":
        # let inode=0 here to reset the counter. This can run on already setted tree
             self.b_set_or_create_member(inode=0, cls=StartZPar, parent_serialiser=serialisation_object)
             self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=EndZPar, parent_serialiser=serialisation_object)        
        # elif self.value == "m":
        # let inode=0 here to reset the counter. This can run on already setted tree
        #     self.b_set_or_create_member(inode=0, cls=StartZMmPar, parent_serialiser=serialisation_object)
        #     self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=EndZMmPar, parent_serialiser=serialisation_object)
        
        elif self.value == "a":
            
            if MultiZRecScheme.global_value == "a" : # not excplicitely separately. If separately there is already spawning
                # let inode=0 here to reset the counter. This can run on already setted tree
                self.b_set_or_create_member(inode=0, cls=SlurmFractionsNumberPar, parent_serialiser=serialisation_object)
        elif self.value in ["m","l"]:
            pass
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class ReconstructionSchemePar(night_rail.BasePar):
    value="h" # for helical. Could be removed but in this case the check in scripts activation
              # for the reconstruction must be forced to true independently
    def proper_init(self, serialisation_object):

        
        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=DeltaBetaPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=UnsharpScheme, parent_serialiser=serialisation_object)

        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=VerticalRangeSchemePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ChunkSchemePar, parent_serialiser=serialisation_object)
        
        # self.b_set_or_create_member(
        #     inode=self.b_next_inode_num(), cls=WeightVerticalTransitionLengthPar, parent_serialiser=serialisation_object
        # )

        if ComesFromAspectScheme.global_value == "n":
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=ScintillatorStripesWeightingScheme , parent_serialiser=serialisation_object
            )
        else:
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=WeightFileOtherPath , parent_serialiser=serialisation_object
            )
        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=HierarchicalOrStandardBackprojectionPar, parent_serialiser=serialisation_object
        )
        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=ExtraPaddingFactorPar, parent_serialiser=serialisation_object
        )

        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=DoubleFlatsScheme, parent_serialiser=serialisation_object
        )
 
        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=ChebychevStraighteningPar, parent_serialiser=serialisation_object
        )
        
class WeightFileOtherPath(night_rail.LocationByPath):
    question = "As the scan come from aspect, the flat field is missing, which should be used to detemine the weight map. Provide here an external weight map "
    path_is_file = True

        
class ChunkSizePar(night_rail.TypedPar):    
    value = "100"
    b_my_histories_add_to = ["chunk_size"]
    b_my_histories_take_from = ["chunk_size"]
    asked_question = "Give the max chunk size for Nabu reconstruction: "
    typed_default = "100"
    my_type = int
    punctiliousness_handle = "calculation"
    punctiliousness_offset = 0


class PagopagChunkSizePar(night_rail.TypedPar):
    value = "500"
    b_my_histories_add_to = ["pagopag_chunk_size"]
    b_my_histories_take_from = ["pagopag_chunk_size"]
    asked_question = "Give the max chunk size for Pagopag 3D chunked subvolumes: "
    typed_default = "500"
    my_type = int


class ChunkSchemePar(night_rail.BasePar):
    possible_schemes = ["[N]o chunk limitation", "[C]hunk limited size"]
    value = "c"
    punctiliousness_handle = "calculation"
    punctiliousness_offset = 0

    def proper_init(self, serialisation_object):
        question = "Choose chunk size: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "n":
            pass
        elif self.value == "c":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=ChunkSizePar, parent_serialiser=serialisation_object)
        else:
            assert False, f""" Choice {self.value}
            has not been properly processed. This should not happen.
            """


class FilteredStartZ(StartZPar):
    asked_question = "give start_z "

class FilteredEndZ(EndZPar):
    asked_question = "give end_z "



class FilteredStartP(night_rail.TypedPar):
    value = "0"
    b_my_histories_add_to = ["projection_number"]
    b_my_histories_take_from = ["projection_number"]
    asked_question = "give start projection"
    typed_default = "0"
    my_type = night_rail.limited_typed_class(int, 0, None)

class FilteredEndP(FilteredStartP):
    asked_question = "give end projection"


    
class VerticalSubRangeScheme(night_rail.BasePar):
    possible_schemes = ["[A]ll", "[F]raction"]
    value = "a"

    def proper_init(self, serialisation_object):
        question = "vertical filtered radiography range: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "a":
            pass
        elif self.value == "f":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=FilteredStartZ, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=FilteredEndZ, parent_serialiser=serialisation_object)

class AngularSubRangeScheme(night_rail.BasePar):
    possible_schemes = ["[A]ll projection", "from a [P]rojection to another projection"]
    value = "a"

    def proper_init(self, serialisation_object):
        question = "angular filtered radiography range: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "a":
            pass
        elif self.value == "p":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=FilteredStartP, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=FilteredEndP, parent_serialiser=serialisation_object)


class UnsharpCoeffPar(night_rail.TypedPar):
    value = "4.0"
    b_my_histories_add_to = ["unsharp_coeff"]
    b_my_histories_take_from = ["unsharp_coeff"]
    asked_question = "Unsharp coefficient"
    typed_default = "4.0"
    my_type = float
    punctiliousness_handle = "unsharp"


class UnsharpSigmaPar(night_rail.TypedPar):
    value = "1.2"
    b_my_histories_add_to = ["unsharp_sigma"]
    b_my_histories_take_from = ["unsharp_sigma"]
    asked_question = "Unsharp sigma"
    typed_default = "1.2"
    my_type = float
    punctiliousness_handle = "unsharp"


class UnsharpScheme(night_rail.BasePar):
    punctiliousness_handle = "unsharp"
    possible_schemes = ["[N]o unsharp", "[U]nsharp"]
    value = "u"

    def proper_init(self, serialisation_object):
        question = "Unsharp: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "n":
            pass
        elif self.value == "u":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=UnsharpCoeffPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=UnsharpSigmaPar, parent_serialiser=serialisation_object)


class WeightFileWithStripesPath(night_rail.LocationByPath):
    question = "As you have choosed to consider stripes in the deconvolution, please provide the file name generated by the weight map procedure ( the one standard for nabu pipeline) "
    path_is_file = True

class  StripesLightTransmissionPar(night_rail.TypedPar):
    value = "0.44"
    b_my_histories_add_to = ["stripeslighttransmission"]
    b_my_histories_take_from = ["stripeslighttransmission"]
    asked_question = "How much light leaks through the stipes interfaces (from 0 to 1) "
    typed_default = "0.44"
    my_type = night_rail.limited_typed_class(float, 0, 1.0)

class DeconvStripesScheme(night_rail.BasePar):
    possible_schemes = ["[N]o stripe considered in deconvolution", "consider [S]tripes"]
    value = "u"

    def proper_init(self, serialisation_object):
        question = "Stripes: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )
        if self.value == "n":
            pass
        elif self.value == "s":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=WeightFileWithStripesPath, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=StripesLightTransmissionPar, parent_serialiser=serialisation_object)

            

class WorkFlowScheme(night_rail.BasePar):
    __version__ = night_rail_bm18.__version__
    b_is_root = True

    possible_schemes = ["[N]abu pipeline", "[A]spect preliminary phase"]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "Choose pipeline kind: " + (", ".join(self.possible_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "n":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=DatasetLocationScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=DetectorCorrectionSchemePar, parent_serialiser=serialisation_object
            )
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=ScintillatorDiffusionCorrectionSchemePar, parent_serialiser=serialisation_object
            )
            
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=VoxelSchemePar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=CorSchemePar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ReconstructionSchemePar, parent_serialiser=serialisation_object)
            
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=PagOPagScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(),
                cls=AngularToleranceInStepsPar,
                parent_serialiser=serialisation_object,
            )
            self.b_set_or_create_member(
                inode=self.b_next_inode_num(), cls=OutputFormatPar, parent_serialiser=serialisation_object
            )
        else:
            self.b_set_or_create_member(
                # let inode=0 here to reset the counter. This can run on already setted tree
                inode=0, cls=DatasetLocationForAspectScheme, parent_serialiser=serialisation_object
            )
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=BeamProfileScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=GeometryScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=NJobsPerStage, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=VerticalSubRangeScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=AngularSubRangeScheme, parent_serialiser=serialisation_object)
            # self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=UnsharpScheme, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=DeconvStripesScheme, parent_serialiser=serialisation_object)

        self.b_set_or_create_member(
            inode=self.b_next_inode_num(),
            cls=night_rail.SessionNamePar,
            parent_serialiser=serialisation_object,
        )


class NPointsSpectra(night_rail.TypedPar):
    value = "5"
    b_my_histories_add_to = ["spectra_n_points"]
    b_my_histories_take_from = ["spectra_n_points"]
    asked_question = "Give the number of subdivisions of the spectra energy range: "
    typed_default = "5"
    my_type = night_rail.limited_typed_class(int, 1, None)


class EnergyIntervalsKev(night_rail.TypedPar):
    value = "50 160"
    b_my_histories_add_to = ["spectra_intervals"]
    b_my_histories_take_from = ["spectra_intervals"]
    asked_question = "Give the  intervals of the spectra energy range. At least two energies in KeV: "
    typed_default = "50 160"
    my_type = night_rail.sequence_of_typed(float, 2, None)


class BeamEnergyPar(night_rail.TypedPar):
    value = "100.0"
    b_my_histories_add_to = ["energy"]
    b_my_histories_take_from = ["enerfy"]
    asked_question = "Beam energy in KeV "
    typed_default = "100.0"
    my_type = float


class BeamProfileFile(night_rail.LocationByPath):
    question = "Choose he excel file for beam profile : "
    path_is_file = True


class BeamSubdivisionScheme(night_rail.BasePar):
    choices = [" [N]umber of intervals", " by user provided [I]ntervals"]
    value = "n"

    def proper_init(self, serialisation_object):
        question = "split the energy range by: " + (", ".join(self.choices))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "n":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=NPointsSpectra, parent_serialiser=serialisation_object)
        else:
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=EnergyIntervalsKev, parent_serialiser=serialisation_object)


class MaterialScheme(night_rail.BasePar):
    possible_localisation_schemes = ["[B]one in alcohol", "[S]and in water"]
    value = "b"

    def proper_init(self, serialisation_object):
        question = "Choose composition: " + (", ".join(self.possible_localisation_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )


class SourceSampleDistPar(night_rail.TypedPar):
    value = "175.0"
    b_my_histories_add_to = ["source_distance_m"]
    b_my_histories_take_from = ["source_distance_m"]
    asked_question = "Source distance in meter"
    typed_default = "175.0"
    my_type = float


class SampleDetectorDistPar(night_rail.TypedPar):
    value = "15.0"
    b_my_histories_add_to = ["propagation_distance_m"]
    b_my_histories_take_from = ["propagation_distance_m"]
    asked_question = "Propagation distance in meter"
    typed_default = "15.0"
    my_type = float


class ScintillatorRPar(night_rail.TypedPar):
    value = "85.0"
    b_my_histories_add_to = ["scintillator_r"]
    b_my_histories_take_from = ["scintillator_r"]
    asked_question = "scintillator r"
    typed_default = "85.0"
    my_type = float


class ScintillatorAlphaPar(night_rail.TypedPar):
    value = "0.3"
    b_my_histories_add_to = ["scintillator_alpha"]
    b_my_histories_take_from = ["scintillator_alpha"]
    asked_question = "scintillator alpha"
    typed_default = "0.3"
    my_type = float


class ScintillatorBorderPar(night_rail.TypedPar):
    value = "100"
    b_my_histories_add_to = ["scintillator_border"]
    b_my_histories_take_from = ["scintillator_border"]
    asked_question = "scintillator border"
    typed_default = "100"
    my_type = int


class GeometryScheme(night_rail.BasePar):
    def proper_init(self, serialisation_object):
        # let inode=0 here to reset the counter. This can run on already setted tree
        self.b_set_or_create_member(inode=0, cls=SourceSampleDistPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=SampleDetectorDistPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=UserVoxelSizePar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorRPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorAlphaPar, parent_serialiser=serialisation_object)
        self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=ScintillatorBorderPar, parent_serialiser=serialisation_object)


class NJobsPerStage(night_rail.TypedPar):
    value = "1"
    b_my_histories_add_to = ["n_jobs_per_stage"]
    b_my_histories_take_from = ["n_jobs_per_stage"]
    asked_question = "How many slurm jobs per stage"
    typed_default = "1"
    my_type = int


class BeamProfileScheme(night_rail.BasePar):
    possible_determination_schemes = [" [S]ingle energy", " beam [P]rofile"]
    value = "s"

    def proper_init(self, serialisation_object):
        question = "chose beam shape: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "s":
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=BeamEnergyPar, parent_serialiser=serialisation_object)
        else:
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=BeamProfileFile, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=BeamSubdivisionScheme, parent_serialiser=serialisation_object)

        self.b_set_or_create_member(
            inode=self.b_next_inode_num(), cls=MaterialScheme, parent_serialiser=serialisation_object
        )


class SoftDeltaBetaPar(DeltaBetaPar):
    asked_question = "the delta/beta ratio for the soft matter. To be used in PagOPag"


class SegmentationThresholdPar(night_rail.TypedPar):
    value = "0.23"
    b_my_histories_add_to = ["segmentation_threshold"]
    b_my_histories_take_from = ["segmentation_threshold"]
    asked_question = "the threshold for PagOPag"
    typed_default = "0.23"
    my_type = float


class PagOPagScheme(night_rail.BasePar):
    possible_determination_schemes = [" [N]o PagOPag", " [P]agOPag"]
    value = "n"
    punctiliousness_handle = "generic"
    punctiliousness_offset = 1

    def proper_init(self, serialisation_object):
        question = "chose post treatement multi-phase: " + (", ".join(self.possible_determination_schemes))

        self.value = self.b_ask_input_from_question(
            question=question,
            serialisation_object=serialisation_object,
            answer_dict=self.question_answer_dict,
            default=self.value,
        )

        if self.value == "n":
            pass
        else:
            # let inode=0 here to reset the counter. This can run on already setted tree
            self.b_set_or_create_member(inode=0, cls=SourceSampleDistPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=SampleDetectorDistPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=UserVoxelSizePar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=SoftDeltaBetaPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=SegmentationThresholdPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=BeamEnergyPar, parent_serialiser=serialisation_object)
            self.b_set_or_create_member(inode=self.b_next_inode_num(), cls=PagopagChunkSizePar, parent_serialiser=serialisation_object)
            # beam energy already fro paganin


def install_defaults(json_filename):

    if not isinstance( json_filename, dict):
        global_dict = json.load(open(json_filename,"r"))
    else:
        global_dict = json_filename
        
    default_collector = DefaultCollector( )
    default_collector.install_defaults( global_dict )
    
class DefaultCollector:
    def __init__(self):
        my_classes = clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)

        self.handle2classes = {}
        for cname, cls in my_classes:
            
            if hasattr(cls, "punctiliousness_handle"):
                print(cname, cls)
                if not cls.punctiliousness_handle.startswith("no_punctiliousness_control_"):
                     handle_load = self.handle2classes.get(cls.punctiliousness_handle,{})
                     handle_load[cname]= cls
                     print(handle_load)
                     self.handle2classes[cls.punctiliousness_handle] = handle_load
                     
        print(" available handles : ",  list(self.handle2classes.keys())  )

    def get_handle_defaults(self, handle, global_dict = None):
        if global_dict is None:
            global_dict = {handle : {}}
        defaults = global_dict.get(handle, {})

        for cname, cls in self.handle2classes[handle].items():
            asker = cls()
            serialiser = night_rail.FromUser(check_for_path=False, just_one_shot=True)
            print(" CONFIGURING CLASS:", cname)
            asker.proper_init(serialiser)
            defaults[cname] = asker.value            
        global_dict[handle] = defaults
        return  global_dict

    def install_defaults(self, global_dict):
        for handle, defaults in global_dict.items():
            name2class = self.handle2classes[handle]
            for cname, value in defaults.items():
                if cname not in name2class:
                    raise RuntimeError("receiving the default for a class which is not internally listed by the default manager mechanism. Contact the developer. This should not happen")
                
                name2class[cname].value = str(value)
                name2class[cname].typed_default = str(value)

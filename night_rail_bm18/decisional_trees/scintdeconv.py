import night_rail
import os
import textwrap
import math
import shutil
import json
import h5py
from . import utils
from . import rec_tree

class ScintDeconvScript(night_rail.BaseScript):
    """ """
    def proper_init(self):
        classes_of_interest = [
            rec_tree.ScintillatorDiffusionCorrectionSchemePar,
            rec_tree.ScintillatorDiffusionFractionPar     ,
            rec_tree.ScintillatorDiffusionLenghtPar       ,
            rec_tree.ScintillatorDiffusionBinProjectionPar,
            rec_tree.ScintillatorDiffusionBinPixelPar     ,
            rec_tree.ScintillatorDiffusionBorderInPixelsPar     ,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissUniqueScan,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.NexusUniqueScan,
            rec_tree.EntryNamePar,
            rec_tree.MultiZRecScheme,
            rec_tree.SlurmFractionsNumberPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)  # beside setting the listening to them,
        # this also sets self.near_cor_value..


    def get_input_filename(self):
        ## Generate the output file in line with original bliss files
        
        input_nx_filename, _ =utils.get_input_filename_and_output_prefix_from_scan_names(  bliss_first_scan   = self.bliss_first_scan   ,
                                                                                          bliss_last_scan    = self.bliss_last_scan    ,
                                                                                          nexus_first_scan   = self.nexus_first_scan   ,
                                                                                          nexus_last_scan    = self.nexus_last_scan    ,
                                                                                          entry_name_par     = self.entry_name_par     ,
                                                                                          multi_z_rec_scheme = self.multi_z_rec_scheme ,
                                                                                          bliss_unique_scan  = self.bliss_unique_scan  ,
                                                                                          nexus_unique_scan  = self.nexus_unique_scan  ,
                                                                                           fractions_number   = self.get_slurm_fractions_number()   , 
        )
        return input_nx_filename

        
    def check_if_im_used(self):
        return  (self.scintillator_diffusion_correction_scheme_par == "e")


    def get_slurm_fractions_number(self):

        if self.multi_z_rec_scheme != "s":  # no explicitely separately (could be None
            slurm_fractions_number_par = int(self.slurm_fractions_number_par or 1)
        else:
            slurm_fractions_number_par = 1
        return int(slurm_fractions_number_par)

    
    def b_finalise_script(self):
        if not self.check_if_im_used():
            return
        self.b_class_set_used(True)

        input_nx_filename_list = self.get_input_filename()

        if not isinstance( input_nx_filename_list , (list, tuple) ):
            input_nx_filename_list = [ input_nx_filename_list ]
        
        print(input_nx_filename_list )
        
        self.b_extend_required_inputs(input_nx_filename_list)
        
        if not isinstance( input_nx_filename_list,  (tuple, list) ):
            input_nx_filename_list  = (input_nx_filename_list, )

        output_dir = self.b_compose_output_name("output_dir")
                
        diffusion_digests = []
        command = ""
        for input_nx in input_nx_filename_list:
            
            digest_file = os.path.join( output_dir, "diffusion_digest_" + os.path.splitext(os.path.basename(input_nx))[0]+".h5")

            diffusion_digests.append(digest_file)


            original_flats = os.path.splitext(input_nx)[0]+"_flats.hdf5"
            original_darks = os.path.splitext(input_nx)[0]+"_darks.hdf5"

            new_flats = os.path.join( output_dir, os.path.basename(  original_flats   )   )
            
            
            command += textwrap.dedent(
                rf"""
                set -e
                export SKIP_TOMOSCAN_CHECK="1"
                export TOMOTOOLS_SKIP_DET_CHECK="1"
                export MEAN_FOR_FLATS="1"

                aspect-create-deconv-correction-target  --bin_projections {self.scintillator_diffusion_bin_projection_par} \
                                                        --bin_x {self.scintillator_diffusion_bin_pixel_par} \
                                                        --original_file {input_nx} \
                                                        --target_file {digest_file} \
                                                        --entry_name  {self.entry_name_par}  

                deconvolve-z-stage-nx  \
                               --original_file {input_nx} \
                               --target_file    {digest_file}  \
                               --entry_name {self.entry_name_par}  \
                               --diffuse_scale_l {self.scintillator_diffusion_lenght_par} \
                               --diffuse_fraction {self.scintillator_diffusion_fraction_par} \
                               --diffuse_mask_border {self.scintillator_diffusion_border_in_pixels_par}  \
                               --bin_projections  {self.scintillator_diffusion_bin_projection_par}   \
                               --bin_x   {self.scintillator_diffusion_bin_pixel_par}
                
                cp {original_darks}  {output_dir}
                     
                deconvolve-z-stage-nx  \
                --original_file  {original_flats} \
                --target_file    {new_flats}  \
                --entry_name {self.entry_name_par} \
                --diffuse_scale_l     {self.scintillator_diffusion_lenght_par}  \
                --diffuse_fraction    {self.scintillator_diffusion_fraction_par}  \
                --diffuse_mask_border {self.scintillator_diffusion_border_in_pixels_par}   \
                --is_flat
                
                """
            )

        
        self.b_extend_provided_outputs(diffusion_digests)
        self.b_class_register_key_val("diffusion_digests", diffusion_digests )
        self.b_class_register_key_val("new_darks_flats_directory", output_dir )
        self.b_add_script_line(command)


    def configure_output_list(self, output_prefix_list):
        assert len(output_prefix_list) == 1, " The base version of the nabu run class works on one reconstruction only"

        output_file_list = [tok + ".hdf5" for tok in output_prefix_list]

        self.b_extend_provided_outputs(output_file_list)

        self.b_class_set_relevant_output_file(output_file_list[0])
        self.b_class_register_key_val("output_file", output_file_list[0])

        return output_file_list


import night_rail
import os
import textwrap
import math
import shutil
import json
import h5py
from namedlist import namedlist
from nabu.app.nx2etf import main as nx2etf
from . import utils
from . import rec_tree

from .scintdeconv import ScintDeconvScript

night_rail.environment_variables.update({"SKIP_TOMOSCAN_CHECK": "1"})
night_rail.environment_variables.update({"TOMOTOOLS_SKIP_DET_CHECK": "1"})
night_rail.environment_variables.update({"MEAN_FOR_FLATS": "1"})

def get_all_scripts():
    result = night_rail.BaseScriptsContainer()
    result.extend(
        [
            # raw reading
            Bliss2NxScript(),
            Bliss2NxMultiZScript(),

            CreateWeightMapScript(),
            
            # Cor finding which can be used when the first part does not z-translate
            CompositeCorSimpleScript(),
              # concatenation
            ConcatenateToHeliScanScript(),
              # Pixel size correction from helical diagnostics
            CreateNabuDiagToPixConfScript(),
            NabuDiagToPixRunScript(),
            CorrectPixFromDiagScript(),
              # Rot correction for helical diagnostics
            CreateNabuDiagToRotConfScript(),
            NabuDiagToRotRunScript(),
            RotCorrectionFromDiagScript(),
            ApplyRotCorrectionScript(),
            # final reconstruction
            CreateDoubleFlatsScript(),
            ScintDeconvScript(),
            CreateNabuFinalConfScript(),
            NabuFinalRunScript(),
            PagOPagScript(),
            
        ]
    )

    result.extend(
        [
            # raw reading
            # AspectBliss2NxScript(),
            AspectBliss2NxScript(),
            AspectBliss2NxMultiZScript(),
            AspectDuplicateNxToTargetScript(),
            AspectCreateBeamProfile(),
            AspectApply(),
        ]
    )

    return result


class DiagnosticNabuRunCorScript(night_rail.BaseScript):
    pass


class DiagToCorScript(night_rail.BaseScript):
    pass


class DiagnosticNabuRunPixScript(night_rail.BaseScript):
    pass


class DiagToPixScript(night_rail.BaseScript):
    pass


class Bliss2NxScript(night_rail.BaseScript):
    """This script performs the first phase : from bliss scan(s)
    to nexus scan(s)
    """

    im_for_aspect = False

    def proper_init(self):
        classes_of_interest = [
            rec_tree.WorkFlowScheme,
            rec_tree.BlissUniqueScan,
            rec_tree.NexusUniqueScan,
            rec_tree.RoughVoxelCorrectionPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        # respect the wf choice
        if {True: "a", False: "n"}[self.im_for_aspect] != self.work_flow_scheme:
            return

        if self.bliss_unique_scan or self.nexus_unique_scan:
            self.b_class_set_used(True)
            self.finalise_unique_scan()
        else:
            """this script is  not used because BlissUniqueScan was not set.
            This is a simple example of triggering a script.
            Another way would be triggering the neuralgic_id of the DatasetLocation class.
            So the finalisation will not be done, the script will remain empty, and therefore
            not considered.
            """
            pass

    def finalise_unique_scan(self):
        if self.bliss_unique_scan:
            output_file = self.b_compose_output_name("output.nx")
            self.b_class_set_relevant_output_file(output_file)
            self.b_extend_required_inputs([self.bliss_unique_scan])
            self.b_extend_provided_outputs([output_file])

            if self.rough_voxel_correction_par and float(self.rough_voxel_correction_par):
                # h52nx does not consider 0 as the default non effective value, therefore here above we have checked the float value
                #  of self.rough_voxel_correction_par. Which otherwise is a tring"
                extra_for_voxel = f"--set-params x_pixel_size {self.rough_voxel_correction_par} y_pixel_size {self.rough_voxel_correction_par}"
            else:
                extra_for_voxel = ""

            s = f"nxtomomill h52nx {self.bliss_unique_scan}  {output_file}  {extra_for_voxel} --overwrite"
        elif self.nexus_unique_scan:
            self.b_class_set_relevant_output_file(self.nexus_unique_scan)
            self.b_extend_provided_outputs([self.nexus_unique_scan])
            s = f""" echo 'nothing to be done. Just passing nexus filename'   """
        self.b_reset_script_empty()
        self.b_add_script_line(s)


class Bliss2NxMultiZScript(night_rail.BaseScript):
    """This script performs the first phase : from bliss scan(s)
    to nexus scan(s) in the more complicated case of multiple scans
    """

    im_for_aspect = False

    def proper_init(self):
        classes_of_interest = [
            rec_tree.WorkFlowScheme,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissRefBeginScan,
            rec_tree.BlissRefEndScan,
            rec_tree.RefExtractionTargetDir,
            rec_tree.EntryNamePar,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.RoughVoxelCorrectionPar,
            rec_tree.ReferenceBySectorScheme,
            rec_tree.NumAngularSectorsRefPar,
            # rec_tree.TargetNumProjectionsBySectorsPar,
            rec_tree.FrequencySeparationRadiusPar,
            rec_tree.DatasetLocationScheme,
            rec_tree.DatasetLocationForAspectScheme,
            rec_tree.ProjectionsStartPar,
            rec_tree.ProjectionsEndPar,
            rec_tree.ProjectionsStepPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if {True: "a", False: "n"}[self.im_for_aspect] != self.work_flow_scheme:
            return

        if (not self.bliss_first_scan) and (not self.nexus_first_scan):
            # because we need the first the last and optionally ref begin ref end
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan:
            first_scan = self.bliss_first_scan
            last_scan = self.bliss_last_scan
        else:
            first_scan = self.nexus_first_scan
            last_scan = self.nexus_last_scan

        filename_template, first_num, last_num, n_tot_avail_z = utils.count_z_stage(first_scan, last_scan)

        if first_scan == last_scan:
            filename_template = first_scan

        if self.bliss_first_scan and (first_scan != last_scan):
            if "b" in [self.dataset_location_scheme, self.dataset_location_for_aspect_scheme ] : # bliss transformed files
                output_filename_template = self.utility_get_output_filename_template()
            else: # from already existing nexus files
                output_filename_template = filename_template
               
        else:
            if not self.bliss_first_scan:
                output_filename_template = filename_template  # just passing existing nexus through
            else:
                # single file
                output_filename_template = os.path.join(
                    self.b_compose_output_name("output_dir"),
                    os.path.splitext(os.path.basename(filename_template))[0] + ".nx",
                )
        self.b_class_set_relevant_output_file((output_filename_template, first_num, last_num))

        if first_scan != last_scan:
            outputted_files = self.utility_get_outputted_filenames(first_num, last_num, output_filename_template)
        else:
            outputted_files = [output_filename_template]

        if self.rough_voxel_correction_par:
            # the pars are string. So this will be written even if self.rough_voxel_correction_par is "0"
            # However in zstages2nxs this has the meaning that its redefinition  is not enabled
            extra_for_voxel = f"--voxel_size   {self.rough_voxel_correction_par} "
        else:
            extra_for_voxel = ""

        self.b_extend_provided_outputs(outputted_files)

        self.b_class_register_key_val("nexus_files", outputted_files)
        self.b_reset_script_empty()

        if self.bliss_first_scan:
            template_or_not = {False: "", True: "_template"}[(first_scan != last_scan)]

            s = textwrap.dedent(
                rf"""
            nxtomomill zstages2nxs \
            --output_filename{template_or_not} {output_filename_template} \
            --filename{template_or_not} {filename_template} \
            --entry_name {self.entry_name_par} \
            --total_nstages {n_tot_avail_z} \
            --first_stage {first_num} \
            --last_stage {last_num} \
            """
            ).strip("\n")

            if self.bliss_ref_begin_scan is not None:
                s += textwrap.dedent(
                    rf"""
                --do_references True \
                --ref_scan_begin {self.bliss_ref_begin_scan} \
                --ref_scan_end {self.bliss_ref_end_scan} \
                --extracted_reference_target_dir {self.ref_extraction_target_dir} \
                {extra_for_voxel}
                """
                ).strip("\n")

            if self.reference_by_sector_scheme == "y":
                s += textwrap.dedent(
                    rf"""
                --by_angular_sectors y \
                --num_angular_sectors    {self.num_angular_sectors_ref_par} \
                --frequency_separation_radius  {self.frequency_separation_radius_par} \
                --projections_start   {self.projections_start_par} \
                --projections_stop    {self.projections_end_par } \
                --projections_step    {self.projections_step_par } 
                """
                ).strip("\n")

                # --nprojs_sectors_target  {self.target_num_projections_by_sectors_par} \


                
        else:
            s = """  echo 'nothing to be done. Just passing nexus filename'  """

        self.b_add_script_line(s)

    @classmethod
    def utility_get_nexus_refs_filenames(cls):
        output_dir = cls.b_compose_output_name("output_dir")
        res = [
            os.path.join(output_dir, cls.b_compose_output_name(f"{what}"))
            for what in ("output_begin.nx", "output_end.nx")
        ]
        return res

    @classmethod
    def utility_get_output_filename_template(cls):
        output_dir = cls.b_compose_output_name("output_dir")
        output_filename_template = os.path.join(output_dir, cls.b_compose_output_name("output_XXXX.nx"))
        return output_filename_template

    @classmethod
    def utility_get_outputted_filenames(cls, first_num, last_num, output_filename_template=None):
        if not output_filename_template:
            output_filename_template = cls.utility_get_output_filename_template()

        output_filename_template_numeric = utils.template_to_format_string(output_filename_template)
        names_list = []
        for i_stage in range(first_num, last_num + 1):
            names_list.append(output_filename_template_numeric.format(i_stage=i_stage))
        return names_list


class AspectBliss2NxMultiZScript(Bliss2NxMultiZScript):
    im_for_aspect = True


class AspectBliss2NxScript(Bliss2NxScript):
    im_for_aspect = True


class CompositeCorSimpleScript(night_rail.BaseScript):
    """Called to find the cor in the standard way.
    This means that the scan is at constant z_translation or it has at least
    a first part during which the z is constant for more than 180 degree,
    not necessarily 360 but more than 180
    """

    # reimplemented __init__
    def proper_init(self):
        self.nx_from_first_h52nx_filename = None

        classes_of_interest = [
            rec_tree.NearCorValueScheme,
            rec_tree.NearCorValuePar,
            rec_tree.NearWidthValuePar,
            rec_tree.LowPassValuePar,
            rec_tree.HighPassValuePar,
            rec_tree.CompCorThetaIntervalValuePar,
            rec_tree.NSubsamplingValuePar,
            rec_tree.EntryNamePar,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.CorSchemePar,
            rec_tree.CorWeightSchemePar,
            rec_tree.MapXzPathPar,
            rec_tree.DatasetLocationScheme,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)  # beside setting the listening to them,
        # this also sets self.near_cor_value..

    def b_finalise_script(self):
        if self.cor_scheme_par != "a":
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan or self.nexus_first_scan:
            self.finalise_for_multiple_scans()
        else:
            self.simple_finalise()

    def finalise_for_multiple_scans(self):
        if self.bliss_first_scan:
            first_scan, last_scan = self.bliss_first_scan, self.bliss_last_scan
        else:
            first_scan, last_scan = self.nexus_first_scan, self.nexus_last_scan

        template_tmp, first_num, last_num, _ = utils.count_z_stage(first_scan, last_scan)
        if self.bliss_first_scan:
            required_files = Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")
            # required_files = Bliss2NxMultiZScript.utility_get_outputted_filenames(first_num, last_num)
        else:
            file_template_numeric = utils.template_to_format_string(template_tmp)
            required_files = [file_template_numeric.format(i_stage=iz) for iz in range(first_num, last_num + 1)]

        self.b_extend_required_inputs(required_files)

        if len(required_files) > 1:
            if self.dataset_location_scheme == "b" : # get the template of bliss transformed files, outputted by Bliss2NxMultiZScript
                filename_template = Bliss2NxMultiZScript.utility_get_output_filename_template()
                numerical_scan_number_inset = f""" --first_stage {first_num}  --num_of_stages {last_num - first_num +1} """
            else:
                filename_template = template_tmp
                numerical_scan_number_inset = f""" --first_stage {first_num}  --num_of_stages {last_num - first_num +1} """
        else:
            filename_template = required_files[0]
            numerical_scan_number_inset = " "

        output_file = self.b_compose_output_name("cors.json")
        self.b_class_set_relevant_output_file(output_file)
        self.b_register_for_processed_dir([output_file])

        self.b_extend_provided_outputs([output_file])

        self.b_reset_script_empty()


        weight_map_file_name =  CreateWeightMapScript.b_class_get_relevant_output_file()

        
        cor_inset = ""
        if self.near_cor_value_par is not None:
            cor_inset = f"near_pos = {self.near_cor_value_par};"
            
        mapxz_inset = ""
        if self.map_xz_path_par is not None:
            mapxz_inset = f"--map_xz  {self.map_xz_path_par}"

        correlation_options = f""" "side='near'; {cor_inset} near_width = {self.near_width_value_par}; low_pass = {self.low_pass_value_par} ; high_pass={self.high_pass_value_par}  "  """

        if self.cor_weight_scheme_par == "w":
            weight_inset = f"  --weight_map  {weight_map_file_name} "
        else:
            weight_inset = ""
       
        command_line = textwrap.dedent(
            rf"""
            nabu-composite-cor --filename_template {filename_template} \
                 --cor_options  {correlation_options}  {numerical_scan_number_inset} \
                  --n_subsampling_y {self.n_subsampling_value_par} \
                  --theta_interval {self.comp_cor_theta_interval_value_par} \
                  --output_file {output_file}   {mapxz_inset}   \
                  --entry_name {self.entry_name_par}  {weight_inset}
            """
        ).strip("\n")

        self.b_add_script_line(command_line)

    def simple_finalise(self):
        input_nx_filename = Bliss2NxScript.b_class_get_relevant_output_file()
        self.b_extend_required_inputs([input_nx_filename])
        output_file = self.b_compose_output_name("cors.json")
        self.b_class_set_relevant_output_file(output_file)

        self.b_extend_provided_outputs([output_file])
        self.b_reset_script_empty()

        cor_inset = ""
        if self.near_cor_value_par is not None:
            cor_inset = f"near_pos = {self.near_cor_value_par};"

        weight_map_file_name =  CreateWeightMapScript.b_class_get_relevant_output_file()
        
        correlation_options = f""" "side='near'; {cor_inset} near_width = {self.near_width_value_par}; low_pass = {self.low_pass_value_par} ; high_pass={self.high_pass_value_par}  "  """

        mapxz_inset = ""
        if self.map_xz_path_par is not None:
            mapxz_inset = f"--map_xz  {self.map_xz_path_par}"

        if self.cor_weight_scheme_par == "w":
            weight_inset = f"  --weight_map  {weight_map_file_name} "
        else:
            weight_inset = ""
        
        command_line = textwrap.dedent(
            rf"""
             nabu-composite-cor --filename_template {input_nx_filename} \
                     --cor_options  {correlation_options} \
                     --n_subsampling_y {self.n_subsampling_value_par}  \
                     --theta_interval {self.comp_cor_theta_interval_value_par}  \
                     --output_file {output_file}   {mapxz_inset}   \
                     --entry_name {self.entry_name_par}  {weight_inset}
             """
        ).strip("\n")
        self.b_add_script_line(command_line)


class CreateNabuConfScript_base_class(night_rail.BaseScript):
    """Build Reconstruct conf file for Nabu."""

    b_im_python_script = True

    do_output_sinogram = 0
    
    vertical_range_automatic_setting = 0
    
    def proper_init(self):
        classes_of_interest = [
            # for Paganin
            rec_tree.DeltaBetaPar,
            # for reconstruction range
            rec_tree.VerticalRangeSchemePar,
            rec_tree.RangeFractionStartPar,
            rec_tree.RangeFractionEndPar,
            rec_tree.StartZPar,
            rec_tree.EndZPar,
            # rec_tree.StartZMmPar,
            # rec_tree.EndZMmPar,
            rec_tree.VoxelSchemePar,
            rec_tree.UserVoxelSizePar,
            rec_tree.CorSchemePar,
            rec_tree.CorValuePar,
            rec_tree.DetectorCorrectionSchemePar,
            rec_tree.MapXzPathPar,
            rec_tree.EntryNamePar,
            rec_tree.MultiZRecScheme,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissUniqueScan,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.NexusUniqueScan,
            rec_tree.ReconstructionSchemePar,
            rec_tree.VoxelSchemePar,
            rec_tree.CorSchemePar,
            rec_tree.UnsharpCoeffPar,
            rec_tree.UnsharpSigmaPar,
            rec_tree.OutputFormatPar,
            rec_tree.AngularToleranceInStepsPar,
            rec_tree.DiagPixRangeFractionStartPar,
            rec_tree.DiagPixRangeFractionEndPar,
            rec_tree.DiagRotRangeFractionStartPar,
            rec_tree.DiagRotRangeFractionEndPar,
            rec_tree.HierarchicalOrStandardBackprojectionPar,
            rec_tree.SlurmFractionsNumberPar,
            rec_tree.ChebychevStraighteningPar,
            rec_tree.ExtraPaddingFactorPar,
            rec_tree.WobbleCorrectionScheme,
            rec_tree.WeightHorizontalMendingFractionalThreshold,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)


    def get_slurm_fractions_number(self):
        return 1
        
    def get_output_format(self):
        # by default hdf5 always for the base class
        return "hdf5"
    
    def chek_deconvolution():
        return None, None
    
    def check_if_im_used(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def get_input_filename_and_output_prefix(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def get_cor_finder_class(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def set_vertical_range(self):
        # The range. This function is redefined in the diagnostic runner to get automatically the whole range
        # If too much ( for the diagnostic runners)  then extra range parameters will have to be asked in the dialog. Not yet done

        self.vertical_range_automatic_setting = 0

        start_z, end_z = 0, -1
        start_z_mm, end_z_mm = 0, 0  # *_z_mm has the priority unless it is zero
        start_z_fract, end_z_fract = (0, 0)

        if self.vertical_range_scheme_par == "a":
            is_single_slice = False
        elif self.vertical_range_scheme_par == "f":
            start_z_fract, end_z_fract = (self.range_fraction_start_par, self.range_fraction_end_par)
            is_single_slice = start_z_fract == end_z_fract
        elif self.vertical_range_scheme_par  in ["m","l"]:
            self.vertical_range_automatic_setting = {"m":1, "l":2}[self.vertical_range_scheme_par]
            is_single_slice = True
        elif self.vertical_range_scheme_par == "i":
             start_z, end_z = self.start_z_par, self.end_z_par
             start_z_fract, end_z_fract = (0, 0)
             start_z_mm, end_z_mm = 0, 0
             is_single_slice = (int(end_z) - int(start_z)) == 1
        # elif self.vertical_range_scheme_par == "m":
        #     start_z, end_z = 0, -1
        #     start_z_mm, end_z_mm = self.start_z_mm_par, self.end_z_mm_par
        #     start_z_fract, end_z_fract = (0, 0)
        #     is_single_slice = False

        else:
            raise ValueError(f"""  scheme {self.vertical_range_scheme_par} not yet implemented""")

        self.start_z = start_z
        self.start_z_mm = start_z_mm
        self.start_z_fract = start_z_fract
        self.end_z = end_z
        self.end_z_mm = end_z_mm
        self.end_z_fract = end_z_fract

        return is_single_slice

    def tune_bp_method(self):
        pass

    def b_set_divers(self):
        pass
    
    def get_deconvolution_output(self):
        return None,None
        

    def b_finalise_script(self):
        if not self.check_if_im_used():
            return

        self.b_class_set_used(True)

        input_nx_filename, output_prefix = self.get_input_filename_and_output_prefix()

        if isinstance(input_nx_filename, (list, tuple)):
            from_many_separately = True
            if self.get_slurm_fractions_number() > 1:
                do_extra_split = True
            else:
                do_extra_split = False
        else:
            from_many_separately = False
            do_extra_split = False

        self.b_class_register_key_val("input_nx_filename", input_nx_filename)
        self.b_class_register_key_val("output_prefix", output_prefix)

        if from_many_separately or do_extra_split:
            if from_many_separately:
                self.b_extend_required_inputs(input_nx_filename)
                conf_file_list = [
                    self.b_compose_output_name(f"nabuinput_{i_stage:03d}.conf") for i_stage in range(len(input_nx_filename))
                ]
                self.b_class_set_relevant_output_file(conf_file_list)
                self.b_extend_provided_outputs(conf_file_list)
                
                input_nx_list = input_nx_filename
                output_prefix_list = output_prefix
            else:
                self.b_extend_required_inputs([input_nx_filename])
                conf_file_list = [
                    self.b_compose_output_name(f"nabuinput_{i_split:03d}.conf") for i_split in range( self.get_slurm_fractions_number())
                ]
                self.b_class_set_relevant_output_file(conf_file_list)
                self.b_extend_provided_outputs(conf_file_list)
                
                input_nx_list = [input_nx_filename] * self.get_slurm_fractions_number()
                output_prefix_list = output_prefix

                
        else:
            self.b_extend_required_inputs([input_nx_filename])
            conf_file = self.b_compose_output_name("nabuinput.conf")
            self.b_class_set_relevant_output_file(conf_file)
            self.b_extend_provided_outputs([conf_file])

            input_nx_list = [input_nx_filename]
            conf_file_list = [conf_file]
            output_prefix_list = [output_prefix]

        script_to_create_conf = ""


        if not do_extra_split:
            i_split_list = [0]*len(output_prefix_list)
            i_rec_list = list(range(len(output_prefix_list)))
        else:
            i_rec_list = [0]*self.get_slurm_fractions_number()
            i_split_list = list(range(self.get_slurm_fractions_number()))

        if CreateDoubleFlatsScript.b_class_is_used():
            double_flats_file_list = CreateDoubleFlatsScript.b_class_get_relevant_output_file()
        else:
            double_flats_file_list = [None]*len(i_rec_list)

        scint_provided_files, scint_darks_flats_dir = self.get_deconvolution_output()
        if scint_provided_files is None:
            scint_provided_files = [""] * len(conf_file_list)
            scint_darks_flats_dir = ""

        for (i_rec, i_split, my_input_nx, my_conf, my_output_prefix, double_flat_file, scint_correction ) in zip(i_rec_list, i_split_list, input_nx_list, conf_file_list, output_prefix_list, double_flats_file_list, scint_provided_files):
            
            if double_flat_file is not None:
                double_flat_file_str = double_flat_file
            else:
                double_flat_file_str = ""

            etf_inset = rf"""
                      from namedlist import namedlist
                      from nabu.app.nx2etf import main as nx2etf

                      # preparing the argument for the etf creation script
                      Nx2EtfArguments = namedlist("Nx2EtfArguments",
                                         [
                                             ("bliss_file", None),
                                             ("nexus_file", None),
                                             ("target_name", None),
                                             ("darks_flats_dir", None),
                                             ("weight_map_file", None),
                                             ("distortion_correction_file", None),
                                                    ("entry_name", "entry0000"),
                                                       ("target_dir", None),
                                                       ("double_flats_file", None),
                                                       ("diffusion_correction_file", None)
                                                ]
                      )
                      nx2etf_arguments = Nx2EtfArguments()
                      nx2etf_arguments.nexus_file = "{my_input_nx}"
                      nx2etf_arguments.target_name = "{my_conf}"+".etf"

                      # The deconvolution produces modified darks and flats which are then taken from here
                      if "{scint_darks_flats_dir}":
                         nx2etf_arguments.darks_flats_dir = "{scint_darks_flats_dir}"

                      if "{double_flat_file_str}":
                          nx2etf_arguments.double_flats_file = "{double_flat_file_str}"
                               
                      if "{scint_correction}":
                          nx2etf_arguments.diffusion_correction_file = "{scint_correction}"
                               
                      nx2etf_arguments.weight_map_file = "{CreateWeightMapScript.b_class_get_relevant_output_file()}"
                      if "{self.detector_correction_scheme_par}" == "m":
                         map_file = "{self.map_xz_path_par}"
                         if map_file[0] != "/":
                            map_file = "./" + map_file
                         nx2etf_arguments.distortion_correction_file = map_file
                      # creating the etf directory
                      nx2etf( nx2etf_arguments )
            """

    
            ## COR parameterget
            inset_for_rotation_axis = ""

            cor_finder_cls = self.get_cor_finder_class()
            if cor_finder_cls is not None:
                if cor_finder_cls.__class__ == type:
                    cor_data_file = cor_finder_cls.b_class_get_relevant_output_file()
                    self.b_extend_required_inputs([cor_data_file])
                    #  keeping the indentation which will be removed by dedent
                    inset_for_rotation_axis = rf"""
                      import os
                      import h5py
                      import json
                      if os.path.splitext("{cor_data_file}")[1] == ".json":
                        cor_dict = json.load( open("{cor_data_file}","r")    )

                        if "{self.multi_z_rec_scheme}" != "a" :  # multi_z_rec_scheme != [a]fter concatenation :   we are building each stage separately, each one with its own cor
                             rotation_axis_position = cor_dict["rotation_axis_position_list"][{i_rec}]
                        else:
                             rotation_axis_position = cor_dict["rotation_axis_position"]

                      elif os.path.splitext("{cor_data_file}")[1] == ".hdf5":
                        rotation_axis_position = h5py.File("{cor_data_file}","r")["cor0"][()]
                      else:
                          raise f" the cor correction filename must have for extension either json or hdf5. The name was    '{cor_data_file}'   "
                    """
                else:
                    # for diagnostics we dont need to do the reconstruction, we just pass a float number for the core ( the diagnostic derivation returns a float for cor_finder_cls)
                    # So the diagnostic derived classes can just return a float or whatever is not a class for the cor finding, then we simply put 2000 as place holder
                    # Infacts for the specialisation to diagnostics the get_cor_finder_class returns a float instead of a cor finder class
                    inset_for_rotation_axis = rf"""
                      rotation_axis_position = 2000.0
                    """
            else:
                if not self.cor_scheme_par == "u":
                    raise ValueError(
                        f""" No automatic procedure for cor finding is active and there is no user selected value in class {self.__class__}"""
                    )
                rotation_axis_position = self.cor_value_par

                inset_for_rotation_axis = rf"""
                      rotation_axis_position = {float(rotation_axis_position):15.4f}
                """

            ## Distortion parameter
            if self.detector_correction_scheme_par == "m":
                do_detector_correction = "map_xz"
                map_file = self.map_xz_path_par
                
                if map_file and map_file[0] != "/":
                    map_file = "./" + map_file
                    
                detector_correction_url = rf""" map_x="silx:{map_file}?path=/coords_source_x" ; map_z="silx:{map_file}?path=/coords_source_z" """
                self.b_consider_external_file_for_storing(map_file)

            else:
                do_detector_correction = """  ""  """
                detector_correction_url = """  ""   """
            # Voxel parameter
            if self.voxel_scheme_par == "u":
                overwrite_metadata = rf""" " pixel_size = {self.user_voxel_size_par} um " """
            else:
                overwrite_metadata = """  ""  """
            # weight map
            weights_file =    CreateWeightMapScript.b_class_get_relevant_output_file()

            self.b_extend_required_inputs([weights_file])
            # the range
            is_single_slice = self.set_vertical_range()

            if is_single_slice:
                output_prefix = my_output_prefix + "_single_slice"

            # divers
            self.b_set_divers()

            # Unsharp
            if None in (self.unsharp_coeff_par, self.unsharp_sigma_par):  # pylint: disable=locally-disabled, E0203
                self.unsharp_coeff_par, self.unsharp_sigma_par = 0, 0

            # Phase Filter
            if float(self.delta_beta_par) == 0:
                phase_filter = ""
            else:
                phase_filter = "Paganin"

            output_format = self.get_output_format()

            should_i_normalize_srcurrent = 1

            # The range
            # do_extra_split = True
            if not do_extra_split:
                my_start_z        =  self.start_z        
                my_end_z          =  self.end_z          
                my_start_z_fract  =  self.start_z_fract  
                my_end_z_fract    =  self.end_z_fract    
                my_start_z_mm     =  self.start_z_mm     
                my_end_z_mm       =  self.end_z_mm
            else:
                my_start_z        =  self.start_z        
                my_end_z          =  self.end_z          
                my_start_z_fract  =  i_split /  self.get_slurm_fractions_number()
                if i_split < self.get_slurm_fractions_number():
                    my_end_z_fract    =  (i_split +0.999999) /  self.get_slurm_fractions_number()
                else:
                    my_end_z_fract    = 1
                    
                my_start_z_mm     =  0
                my_end_z_mm       =  0


            self.tune_bp_method()

            if double_flat_file :
                inset_for_double_flats = f"""angular_doubles_file = "{double_flat_file}"  """
            else:
                inset_for_double_flats = ""

            if self.wobble_correction_scheme == "w":
                wobble_correction = "1"
            else:
                wobble_correction = "0"



            fractional_threshold = self.weight_horizontal_mending_fractional_threshold
                
            # THE SCRIPT
            script_to_create_conf += textwrap.dedent(
                rf"""
                      from nabu.pipeline.config import generate_nabu_configfile
                      from nabu.pipeline.helical.nabu_config import nabu_config as helical_fullfield_config
                      {etf_inset} 
                      {inset_for_rotation_axis}

                      prefilled_values = dict(
                        dataset = dict(location = "{my_input_nx}",  etf_location = nx2etf_arguments.target_name,
                                       overwrite_metadata =  {overwrite_metadata}, hdf5_entry='{self.entry_name_par}',
                                       scintillator_diffusion_correction = "{scint_correction}", darks_flats_dir = "{scint_darks_flats_dir}" ),
                        phase   = dict(method = "{phase_filter}", delta_beta = {self.delta_beta_par},  unsharp_coeff = {self.unsharp_coeff_par} , unsharp_sigma = {self.unsharp_sigma_par}),
                        reconstruction = dict( rotation_axis_position = rotation_axis_position,
                              start_z = {my_start_z}, end_z = {my_end_z},
                              start_z_fract = {float(my_start_z_fract):16.10f},
                              end_z_fract = {float(my_end_z_fract):16.10f},
                              fbp_extra_padding_factor = {self.extra_padding_factor_par},
                              start_z_mm = {my_start_z_mm}, end_z_mm = {my_end_z_mm}, clip_outer_circle=1, angular_tolerance_steps={self.angular_tolerance_in_steps_par},
                              vertical_range_automatic_setting = {self.vertical_range_automatic_setting},
                              wobble_correction= {wobble_correction},
                              method="{dict(b="FBP",g="GHBP")[self.hierarchical_or_standard_backprojection_par]}"
                           ),
                        preproc = dict( detector_distortion_correction = "{do_detector_correction}", 
                                        detector_distortion_correction_options= '{detector_correction_url}',
                                        processes_file = "{weights_file}",
                                        normalize_srcurrent = "{should_i_normalize_srcurrent}",
                                        chebychev_straightening = "{self.chebychev_straightening_par}",
                                        fractional_threshold = {fractional_threshold},
                                        {inset_for_double_flats}
                                   ),

                        output = dict(  location = "./", file_prefix = "{my_output_prefix}", file_format = "{output_format}" ) ,
                        postproc = dict(  output_histogram = {self.do_output_sinogram} ) ,
                
                      )
                      generate_nabu_configfile(
                        "{my_conf}",
                        helical_fullfield_config,
                        comments=True,
                        options_level="advanced",
                        prefilled_values=prefilled_values,
                      )

            """
            )
        ### "#!/usr/bin/env python\n" +
        script_to_create_conf = night_rail.get_python_preamble() + script_to_create_conf

        self.b_set_python_script(script_to_create_conf)


class CreateNabuFinalConfScript(CreateNabuConfScript_base_class):
    """Build Reconstruct conf file for Nabu. Final reconstruction"""

    do_output_sinogram = 1

    def get_slurm_fractions_number(self):

        if self.multi_z_rec_scheme != "s":  # no explicitely separately (could be None
            slurm_fractions_number_par = int(self.slurm_fractions_number_par or 1)
        else:
            slurm_fractions_number_par = 1
        return int(slurm_fractions_number_par)
    
    def check_if_im_used(self):
        return self.reconstruction_scheme_par in ["h", "u"]


    def get_deconvolution_output(self):
        
        if ScintDeconvScript.b_class_is_used():
            scint_provided_files = ScintDeconvScript.b_class_retrieve_val( "diffusion_digests" )
            new_darks_flats_dir  = ScintDeconvScript.b_compose_output_name("output_dir")
            self.b_extend_required_inputs( scint_provided_files )
        else:
            scint_provided_files = None
            new_darks_flats_dir  = None
            
        return scint_provided_files, new_darks_flats_dir 
    
    def get_output_format(self):
        if self.output_format_par == "h":
            return "hdf5"
        elif self.output_format_par == "t":
            return "tiff"
        else:
            return "edf"
    
    def get_cor_finder_class(self):
        if self.cor_scheme_par == "a":
            return CompositeCorSimpleScript
        elif self.cor_scheme_par == "d":
            return RotCorrectionFromDiagScript
        else:
            return None

    def get_input_filename_and_output_prefix(self):
        ## Generate the output file in line with original bliss files
        
        return utils.get_input_filename_and_output_prefix_from_scan_names(  bliss_first_scan   = self.bliss_first_scan   ,
                                                                            bliss_last_scan    = self.bliss_last_scan    ,
                                                                            nexus_first_scan   = self.nexus_first_scan   ,
                                                                            nexus_last_scan    = self.nexus_last_scan    ,
                                                                            entry_name_par     = self.entry_name_par     ,
                                                                            multi_z_rec_scheme = self.multi_z_rec_scheme ,
                                                                            bliss_unique_scan  = self.bliss_unique_scan  ,
                                                                            nexus_unique_scan  = self.nexus_unique_scan  ,
                                                                            fractions_number   = self.get_slurm_fractions_number()   ,                                               
        )

class CreateNabuDiagToPixConfScript(CreateNabuConfScript_base_class):
    """Build Reconstruct conf file for Nabu. Diagnostics collection for pixel calibration"""

    def tune_bp_method(self):
        self.hierarchical_or_standard_backprojection_par = "b"
    
    def check_if_im_used(self):
        return self.voxel_scheme_par == "a"

    def get_cor_finder_class(self):
        """return a float instead of a class just to signal to the base class that cor has not to be searched"""
        return 2.0

    def get_input_filename_and_output_prefix(self):
        output_file = self.b_compose_output_name("pix_diag_run.hdf5")

        self.b_class_set_relevant_output_file(output_file)

        output_prefix = os.path.splitext(output_file)[0]

        input_nx_filename, _ = utils.find_top_level_output_file_from_classes(
            [ConcatenateToHeliScanScript, Bliss2NxScript]
        )

        return input_nx_filename, output_prefix

    def set_vertical_range(self):
        # This function is redefined in the diagnostic runner to get automatically the whole range
        # If too much ( for the diagnostic runners)  then extra range parameters will have to be asked in the dialog. Not yet done
        self.start_z = 0
        self.start_z_mm = 0
        self.start_z_fract = self.diag_pix_range_fraction_start_par

        self.end_z = -1
        self.end_z_mm = 0
        self.end_z_fract = self.diag_pix_range_fraction_end_par

        is_single_slice = False
        return is_single_slice

    def b_set_divers(self):
        self.delta_beta_par = 0


class CreateNabuDiagToRotConfScript(CreateNabuConfScript_base_class):
    """Build Reconstruct conf file for Nabu. Collection of Rot diagnostics reconstruction"""

    def tune_bp_method(self):
        self.hierarchical_or_standard_backprojection_par = "b"
    
    def check_if_im_used(self):
        return self.cor_scheme_par == "d"

    def get_cor_finder_class(self):
        """return a float instead of a class just to signal to the base class that cor has not to be searched with a possible finder"""
        return 2.0

    def set_vertical_range(self):
        # This function is redefined in the diagnostic runner to get automatically the whole range
        # If too much ( for the diagnostic runners)  then extra range parameters will have to be asked in the dialog. Not yet done
        self.start_z = 0
        self.start_z_mm = 0
        self.start_z_fract = self.diag_rot_range_fraction_start_par

        self.end_z = -1
        self.end_z_mm = 0
        self.end_z_fract = self.diag_rot_range_fraction_end_par

        is_single_slice = False
        return is_single_slice

    def b_set_divers(self):
        self.delta_beta_par = 0

    def get_input_filename_and_output_prefix(self):
        output_file = self.b_compose_output_name("rot_diag_run.hdf5")
        self.b_class_set_relevant_output_file(output_file)
        output_prefix = os.path.splitext(output_file)[0]

        input_nx_filename, _ = utils.find_top_level_output_file_from_classes(
            [CorrectPixFromDiagScript, ConcatenateToHeliScanScript, Bliss2NxScript]
        )

        return input_nx_filename, output_prefix


class PagOPagScript(night_rail.BaseScript):
    """ """

    def proper_init(self):
        classes_of_interest = [
            rec_tree.PagOPagScheme,
            rec_tree.SourceSampleDistPar,
            rec_tree.SampleDetectorDistPar,
            rec_tree.UserVoxelSizePar,
            rec_tree.DeltaBetaPar,
            rec_tree.SoftDeltaBetaPar,
            rec_tree.SegmentationThresholdPar,
            rec_tree.BeamEnergyPar,
            rec_tree.EntryNamePar,
            rec_tree.PagopagChunkSizePar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if not (NabuFinalRunScript.b_class_is_used() and self.pag_o_pag_scheme == "p"):
            return
        self.b_class_is_used()

        input_rec_filename_list = NabuFinalRunScript.b_class_get_relevant_output_file()
        self.b_extend_required_inputs(input_rec_filename_list)

        soft_rec_filename_list = [
            os.path.join(os.path.dirname(input_rec_filename), "soft_" + os.path.basename(input_rec_filename))
            for input_rec_filename in input_rec_filename_list
        ]
        pagopag_rec_filename_list = [
            os.path.join(os.path.dirname(input_rec_filename), "pagopag_" + os.path.basename(input_rec_filename))
            for input_rec_filename in input_rec_filename_list
        ]

        self.b_extend_provided_outputs(soft_rec_filename_list + pagopag_rec_filename_list)

        self.b_register_for_processed_dir(pagopag_rec_filename_list)

        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")

        slurm_string = textwrap.dedent(
            rf"""
        #SBATCH --nodes=1
        #SBATCH --tasks-per-node=1
        #SBATCH --cpus-per-task=32
        #SBATCH --mem=850G
        #SBATCH --time=2000
        #SBATCH --output=pagopag-%A.%a.out
        #SBATCH --job-name="pagopag"
        #SBATCH --gres=gpu:1
        #SBATCH -C a40
        #SBATCH -p bm18
        #SBATCH --array 0-{len(pagopag_rec_filename_list)-1}
        #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
        source {activation_script}
        """
        )

        self.b_set_slurm_string(slurm_string)

        input_rec_filename_string = " ".join(input_rec_filename_list)
        soft_rec_filename_string = " ".join(soft_rec_filename_list)
        pagopag_rec_filename_string = " ".join(pagopag_rec_filename_list)

        command = textwrap.dedent(
            rf"""
        input_rec_filename_list=( {input_rec_filename_string} )
        soft_rec_filename_list=( {soft_rec_filename_string} )
        pagopag_rec_filename_list=( {pagopag_rec_filename_string} )
        n_vols_per_task=1
        if [ -z ${{SLURM_ARRAY_TASK_ID+x}} ]; then
            echo "SLURM_ARRAY_TASK_ID    is unset "
            SLURM_ARRAY_TASK_ID=0
            n_vols_per_task={len(input_rec_filename_list)}
        fi

        for (( i=0; i<$(( $n_vols_per_task  )); i++ ))
        do

             k=$(( $SLURM_ARRAY_TASK_ID * $n_vols_per_task + $i  ))

             input_rec_filename=${{input_rec_filename_list[$k]}} 
             soft_rec_filename=${{soft_rec_filename_list[$k]}} 
             pagopag_rec_filename=${{pagopag_rec_filename_list[$k]}} 

             nabu-pagopag \
             --original_file  $input_rec_filename \
             --target_file $soft_rec_filename \
             --old_dbr {self.delta_beta_par} \
             --new_dbr {self.soft_delta_beta_par} \
             --ene_kev {self.beam_energy_par} \
             --threshold 1.0e9 \
             --distance  {self.sample_detector_dist_par}   \
             --source_distance {self.source_sample_dist_par} \
             --voxel_size  {self.user_voxel_size_par} \
             --entry_name  {self.entry_name_par} \
             --chunk_size  {self.pagopag_chunk_size_par}


             nabu-pagopag \
             --original_file  $input_rec_filename \
             --target_file $pagopag_rec_filename \
             --old_dbr {self.delta_beta_par} \
             --new_dbr {self.soft_delta_beta_par} \
             --ene_kev {self.beam_energy_par} \
             --threshold {self.segmentation_threshold_par} \
             --distance  {self.sample_detector_dist_par}   \
             --source_distance {self.source_sample_dist_par} \
             --voxel_size  {self.user_voxel_size_par} \
             --entry_name  {self.entry_name_par} \
             --file_for_segmentation $soft_rec_filename \
             --chunk_size  {self.pagopag_chunk_size_par}

        done
        """
        )

        self.b_reset_script_empty()
        self.b_add_script_line(command)


## nabu-pagopag --original_file  013_RD_11416_3_OL_227F_003__rec.hdf5 --target_file result.hdf5 --old_dbr 79.0 --new_dbr  400 --ene_kev 35.0 --threshold 0.23
## --distance_m  11 --source_distance_m 1000 --voxel_size_um 6.9  --chunk_size 500 --entry_name entry --file_for_segmentation rec_soft.hdf5


class NabuRunScript_base_class(night_rail.BaseScript):
    """ """

    i_am_final = False
    remove_master_rec_file = "False"
    # for bash shell. Let this as a string. To remove the master output before the run
    # set it to "True"

    def proper_init(self):
        classes_of_interest = [
            rec_tree.PagOPagScheme,
            rec_tree.ChunkSchemePar,
            rec_tree.ChunkSizePar,
            rec_tree.DatasetLocation,
            rec_tree.ReconstructionSchemePar,
            rec_tree.CompCorThetaIntervalValuePar,
            rec_tree.DiagPixThetaIntervalValuePar,
            rec_tree.OutputFormatPar,
            night_rail.SessionNamePar,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)  # beside setting the listening to them,
        # this also sets self.near_cor_value..


        
    def check_if_im_used(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def configure_output_list(self, output_prefix_list):
        assert len(output_prefix_list) == 1, " The base version of the nabu run class works on one reconstruction only"

        output_file_list = [tok + ".hdf5" for tok in output_prefix_list]

        self.b_extend_provided_outputs(output_file_list)

        self.b_class_set_relevant_output_file(output_file_list[0])
        self.b_class_register_key_val("output_file", output_file_list[0])

        return output_file_list

    def b_finalise_script(self):
        if not self.check_if_im_used():
            return
        self.b_class_set_used(True)

        input_nx_filename = self.get_dataset_location()

        if isinstance(input_nx_filename, (list, tuple)):
            input_nx_filename_list = input_nx_filename

        else:
            input_nx_filename_list = [input_nx_filename]

            self.b_class_register_key_val("input_file", input_nx_filename_list[0])

        self.b_extend_required_inputs(input_nx_filename_list)

        weights_file = CreateWeightMapScript.b_class_get_relevant_output_file()
        self.b_extend_required_inputs([weights_file])

        conf_cls = self.get_conf_creator_class()

        conf_file = conf_cls.b_class_get_relevant_output_file()

        if isinstance(conf_file, (list, tuple)):
            self.b_extend_required_inputs(conf_file)
            conf_file_list = conf_file
        else:
            self.b_extend_required_inputs([conf_file])
            conf_file_list = [conf_file]

        ## this for hdf5. For tiff, intercept par for tiff if any, and change accordingly

        output_prefix = conf_cls.b_class_retrieve_val("output_prefix")


        if isinstance(output_prefix, (list, tuple)):
            output_prefix_list = output_prefix
        else:
            output_prefix_list = [output_prefix]

        output_file_list = self.configure_output_list(output_prefix_list)
        
        if self.has_to_be_moved():
            if self.pag_o_pag_scheme == "n":
                to_be_moved = set()
                for a, b in zip(output_prefix_list, output_file_list):
                    to_be_moved.update((a, b))
                self.b_register_for_processed_dir(list(to_be_moved))

        run_options = self.get_run_options()

        self.b_reset_script_empty()

        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")

        slurm_string = textwrap.dedent(
            rf"""
        #SBATCH --nodes=1
        #SBATCH --tasks-per-node=1
        #SBATCH --cpus-per-task=32
        #SBATCH --mem=850G
        #SBATCH --time=2000
        #SBATCH --output=nabu-%A.%a.out
        #SBATCH --job-name="night_rail"
        #SBATCH --gres=gpu:1
        #SBATCH -C a40
        #SBATCH -p bm18
        #SBATCH --array 0-{len(conf_file_list)-1}
        #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
        source {activation_script}
        """
        )

        self.b_set_slurm_string(slurm_string)

        conf_files_string = " ".join(conf_file_list)

        command = textwrap.dedent(
            rf"""
        conf_file_list=( {" ".join( conf_file_list  )} )
        output_file_list=( {" ".join( [ prefix+""  for prefix in  output_file_list]  )} )
        n_vols_per_task=1
        if [ -z ${{SLURM_ARRAY_TASK_ID+x}} ]; then
            echo "SLURM_ARRAY_TASK_ID    is unset "
            SLURM_ARRAY_TASK_ID=0
            n_vols_per_task={len(conf_file_list)}
        fi

        for (( i=0; i<$(( $n_vols_per_task  )); i++ ))
        do
             k=$(( $SLURM_ARRAY_TASK_ID * $n_vols_per_task + $i  ))
             if [ {self.remove_master_rec_file} == True    ] ; then
                 rm -f ${{output_file_list[$k]}}
             fi 

             nabu-helical  ${{conf_file_list[$k]}} {run_options}
        done
        """
        )

        mdata_name = f"{self.session_name_par}_nr_metadata.json"
        
        if self.i_am_final:
            command += textwrap.dedent(
                rf"""
            for (( i=0; i<$(( $n_vols_per_task  )); i++ ))
            do
              k=$(( $SLURM_ARRAY_TASK_ID * $n_vols_per_task + $i  ))
              if [ $k == 0 ] ; then
                night_rail_bm18_nexus_juicer --log_on_book  ${{conf_file_list[$k]}} ${{conf_file_list[$k]}}_nexus_metadata.json {mdata_name} 
              else
                night_rail_bm18_nexus_juicer  ${{conf_file_list[$k]}} ${{conf_file_list[$k]}}_nexus_metadata.json 
              fi
            done
            """
            )
            for conf_name in conf_file_list:
                self.b_register_for_processed_dir([conf_name + "_nexus_metadata.json"])

        self.b_add_script_line(command)


class NabuFinalRunScript(NabuRunScript_base_class):
    """ """

    i_am_final = True
    
    def configure_output_list(self, output_prefix_list):
        if self.output_format_par == "h":
            output_file_list = [tok + ".hdf5" for tok in output_prefix_list]
        else:
            output_file_list = output_prefix_list

        self.b_extend_provided_outputs(output_file_list)
        self.b_class_set_relevant_output_file(output_file_list)
        self.b_class_register_key_val("output_file", output_file_list)

        return output_file_list

    def check_if_im_used(self):
        return CreateNabuFinalConfScript.b_class_is_used()

    def get_dataset_location(self):
        return CreateNabuFinalConfScript.b_class_retrieve_val("input_nx_filename")

    def get_conf_creator_class(self):
        return CreateNabuFinalConfScript

    def has_to_be_moved(self):
        return True

    def get_run_options(self):
        if self.chunk_scheme_par == "n":
            return ""
        elif self.chunk_scheme_par == "c":
            return f"--max_chunk_size {self.chunk_size_par}"


class NabuDiagToPixRunScript(NabuRunScript_base_class):
    """ """

    remove_master_rec_file = "True"
    # for bash shell. Let this as a string. To remove the master output before the run
    # set it to "True"

    def check_if_im_used(self):
        return CreateNabuDiagToPixConfScript.b_class_is_used()

    def get_dataset_location(self):
        return CreateNabuDiagToPixConfScript.b_class_retrieve_val("input_nx_filename")

    def get_conf_creator_class(self):
        return CreateNabuDiagToPixConfScript

    def has_to_be_moved(self):
        return False

    def get_run_options(self):
        dtheta_deg = float(self.diag_pix_theta_interval_value_par)
        n_in_0_180 = int(math.ceil(180 / dtheta_deg))
        return f"--diag_zpro {n_in_0_180}"


class NabuDiagToRotRunScript(NabuRunScript_base_class):
    """ """

    remove_master_rec_file = "True"
    # for bash shell. Let this as a string. To remove the master output before the run
    # set it to "True"

    def check_if_im_used(self):
        return CreateNabuDiagToRotConfScript.b_class_is_used()

    def get_dataset_location(self):
        return CreateNabuDiagToRotConfScript.b_class_retrieve_val("input_nx_filename")

    def get_conf_creator_class(self):
        return CreateNabuDiagToRotConfScript

    def has_to_be_moved(self):
        return False

    def get_run_options(self):
        dtheta_deg = float(self.comp_cor_theta_interval_value_par)
        n_in_0_180 = int(math.ceil(180 / dtheta_deg))
        return f"--diag_zpro {n_in_0_180}"


class CorrectPixFromDiagScript(night_rail.BaseScript):
    """ """

    def proper_init(self):
        self.b_add_to_classes_of_interest([])

    def b_finalise_script(self):
        if not NabuDiagToPixRunScript.b_class_is_used():
            return

        self.b_class_set_used(True)

        diag_file = NabuDiagToPixRunScript.b_class_retrieve_val("output_file")
        to_be_corrected_file = NabuDiagToPixRunScript.b_class_retrieve_val("input_file")
        corrected_file = self.b_compose_output_name("pix_corrected.nx")

        self.b_class_set_relevant_output_file(corrected_file)

        self.b_extend_required_inputs([to_be_corrected_file, diag_file])
        self.b_extend_provided_outputs([corrected_file])

        command_line = f"""nabu-diag2pix --diag_file {diag_file}  --nexus_source {to_be_corrected_file} --nexus_target  {corrected_file}  """
        self.b_add_script_line(command_line)

        # transferring reduced flats darks from source nexus data file
        for what in "_darks.hdf5", "_flats.hdf5":
            source = os.path.splitext(to_be_corrected_file)[0] + what
            target = os.path.splitext(corrected_file)[0] + what
            command_line = f"""ln -sf {source}   {target}  """
            self.b_add_script_line(command_line)


class RotCorrectionFromDiagScript(night_rail.BaseScript):
    """ """



    
    def proper_init(self):
        self.b_add_to_classes_of_interest(
            [
                rec_tree.NearCorValuePar,
                rec_tree.EntryNamePar,
                rec_tree.NearWidthValuePar,
                rec_tree.CorInterpolationSchemePar,
            ]
        )

    def b_finalise_script(self):
        if not NabuDiagToRotRunScript.b_class_is_used():
            return

        self.b_class_set_used(True)

        diag_file = NabuDiagToRotRunScript.b_class_retrieve_val("output_file")
        correction_file = self.b_compose_output_name("rot_correction.hdf5")
        self.b_class_set_relevant_output_file(correction_file)
        self.b_register_for_processed_dir([correction_file])
        self.b_extend_required_inputs([diag_file])
        self.b_extend_provided_outputs([correction_file])


        cor_inset = ""
        if self.near_cor_value_par is not None:
            cor_inset = f"--near {self.near_cor_value_par} "
        else:
            original_file = CreateNabuDiagToRotConfScript.b_class_retrieve_val("input_nx_filename")
            if self.entry_name_par is None:
                raise ValueError("When the approximated cor is taken from the scan, entry_name must be specified")

            cor_inset = f"--original_scan {original_file}  "
            

        command_line = f"""nabu-diag2rot --diag_file {diag_file} {cor_inset}  --near_width {self.near_width_value_par}  --linear_interpolation {self.cor_interpolation_scheme_par=="l"}  --cor_file {correction_file} --entry_name {self.entry_name_par}  """
        self.b_add_script_line(command_line)


##            nabu-helical-correct-rot --cor_file test.hdf5 --nexus_source ../only_three_stages_bis/concat_corrected_pixel.nx  --nexus_target ../only_three_stages_bis/corrected.nx


class ApplyRotCorrectionScript(night_rail.BaseScript):
    """ """

    def b_finalise_script(self):
        if not RotCorrectionFromDiagScript.b_class_is_used():
            return

        self.b_class_set_used(True)

        correction_file = RotCorrectionFromDiagScript.b_class_get_relevant_output_file()
        input_nx_filename, _ = utils.find_top_level_output_file_from_classes(
            [CorrectPixFromDiagScript, ConcatenateToHeliScanScript, Bliss2NxScript]
        )

        self.b_extend_required_inputs([correction_file, input_nx_filename])

        corrected_file = self.b_compose_output_name("rot_corrected.nx")
        self.b_class_set_relevant_output_file(corrected_file)
        self.b_extend_provided_outputs([corrected_file])

        input_nx_filename, _ = utils.find_top_level_output_file_from_classes(
            [CorrectPixFromDiagScript, ConcatenateToHeliScanScript, Bliss2NxScript]
        )

        command_line = f"""nabu-helical-correct-rot --cor_file {correction_file} --nexus_source {input_nx_filename}  --nexus_target {corrected_file}"""
        self.b_add_script_line(command_line)

        # transferring reduced flats darks from source nexus data file
        for what in "_darks.hdf5", "_flats.hdf5":
            source = os.path.splitext(input_nx_filename)[0] + what
            target = os.path.splitext(corrected_file)[0] + what
            command_line = f"""ln -sf {source}   {target}  """
            self.b_add_script_line(command_line)


class CreateWeightMapScript(night_rail.BaseScript):
    """ """
    b_im_python_script = True

    def proper_init(self):
        self.b_add_to_classes_of_interest(
            [
                rec_tree.WeightHorizontalTransitionLengthPar,
                rec_tree.WeightVerticalTransitionLengthPar,
                rec_tree.EntryNamePar,
                rec_tree.BlissFirstScan,
                rec_tree.NexusFirstScan,
                rec_tree.ReconstructionSchemePar,
                rec_tree.MultiZRecScheme,
                rec_tree.CorValuePar,                
                rec_tree.CorSchemePar,
                rec_tree.NumberOfScintillatorStripes  ,
                rec_tree.StripeWidth  ,
                rec_tree.StripesShearMin  ,
                rec_tree.StripesShearMax  ,
                rec_tree.WeightFileOtherPath,
                rec_tree.NearCorValuePar,
            ]
        )

        
    def b_finalise_script(self):
        if self.reconstruction_scheme_par != "h":
            return

        self.b_class_set_used(True)

        if self.weight_file_other_path:
            return self.b_finalise_already_done_script()
        
        if self.bliss_first_scan is not None or self.nexus_first_scan:
            if self.multi_z_rec_scheme == "a":
                # in principle the commented line below (#####) could be used,
                # this if the present script is placed after ConcatenateToHeliScanScript
                # If we want to keep the freedom to keep it before then we proced otherwise
                ###### input_nx_filename = ConcatenateToHeliScanScript.b_class_get_relevant_output_file()

                nexus_files, bliss_nx_cls = utils.find_top_level_key_val_from_classes(
                    "nexus_files", [Bliss2NxMultiZScript], must_exist=False
                )

                if nexus_files is None:
                    message =""" Was not able to retrieve the nexus files names that are used to the concatenation step.
                    This is the CreateWeightMapScript talking, which is trying to determine which scans are used in 
                    the concatenation, in order to use their flats for the weights map
                    """
                    raise RuntimeError(message)
                
                input_nx_filenames = nexus_files
                
            elif self.multi_z_rec_scheme == "s":
                input_nx_filenames = [Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")[0]]
        else:
            input_nx_filenames = [Bliss2NxScript.b_class_get_relevant_output_file()]

        self.b_extend_required_inputs(input_nx_filenames)

        output_file = self.b_compose_output_name("weight_map.h5")
        self.b_class_set_relevant_output_file(output_file)
        self.b_extend_provided_outputs([output_file])

        self.b_reset_script_empty()

        files_inset = ","
        for tok in input_nx_filenames:
            files_inset += f"""  "{tok}"   , """
        
#         "--scan_files", "{" ".join(input_nx_filenames)}",

            # "--scan_files", "{" ".join(input_nx_filenames)}",
        
        script_to_create_map = textwrap.dedent(
            rf"""
            from nabu.app.prepare_weights_double import main as prepare_map

            import os
            import h5py
            import json
            from nxtomo.application.nxtomo import NXtomo
        
            scan = NXtomo()
            scan.load(file_path="{input_nx_filenames[0]}", data_path="{self.entry_name_par}")

            user_provided_near={self.near_cor_value_par}
            
            if user_provided_near:
               estimated_near = user_provided_near
            else:
               estimated_near = scan.instrument.detector.estimated_cor_from_motor
    
            arguments = [  
            "--scan_files"   {files_inset} 
            "--entry_name" , "{self.entry_name_par}",
            "--target_file", "{output_file}",
            "--rotation_axis_position", str(estimated_near),
            "--transition_width_horizontal", str({self.weight_horizontal_transition_length_par}),
            "--transition_width_vertical", str({self.weight_vertical_transition_length_par}),
            ]     
            if {self.number_of_scintillator_stripes}:
              arguments += [
                  "--n_stripes", str({self.number_of_scintillator_stripes }),
                  "--stripe_shear_min", str({self.stripes_shear_min }) ,
                  "--stripe_shear_max", str({self.stripes_shear_max }) ,
                  "--stripe_width",     str({self.stripe_width }) 
                ]


            prepare_map(arguments)

            """)

        script_to_create_map = night_rail.get_python_preamble() + script_to_create_map

        self.b_set_python_script(script_to_create_map)
        

    def b_finalise_already_done_script(self):

        output_file = self.weight_file_other_path
        
        self.b_class_set_relevant_output_file(output_file)
        self.b_extend_provided_outputs([output_file])
        self.b_class_set_relevant_output_file(output_file)
        

        self.b_reset_script_empty()

        script_to_create_map = textwrap.dedent(
            rf"""
            print(f" This script of the class of the class CreateWeightMapScript has nothing to do because an already done map is used : {self.weight_file_other_path}")
            """)

        script_to_create_map = night_rail.get_python_preamble() + script_to_create_map

        self.b_set_python_script(script_to_create_map)

        
class ConcatenateToHeliScanScript(night_rail.BaseScript):
    """This script transforms z-scans from separate nexus files to one filer for helical."""

    def proper_init(self):
        classes_of_interest = [
            rec_tree.MultiZRecScheme,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissRefBeginScan,
            rec_tree.BlissRefEndScan,
            rec_tree.EntryNamePar,
            rec_tree.CorSchemePar,
            rec_tree.UserVoxelSizePar,
            rec_tree.RefExtractionTargetDir,
            rec_tree.ComesFromAspectScheme,
            rec_tree.MaxDarkPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def get_source_class(self):
        return Bliss2NxMultiZScript

    def b_finalise_script(self):
        if self.multi_z_rec_scheme:
            if self.multi_z_rec_scheme == "a":
                # After concatenation
                self.b_finalise_script_in_case()
            elif self.multi_z_rec_scheme == "s":
                # Separately
                pass
            else:
                raise ValueError(
                    f" The containt for MultiZRecScheme is supposed to be either a or s but was {self.multi_z_rec_scheme}"
                )

    def b_finalise_script_in_case(self):
        nexus_files, bliss_nx_cls = utils.find_top_level_key_val_from_classes(
            "nexus_files", [self.get_source_class()], must_exist=False
        )

        if nexus_files is None:
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan:
            first_scan, last_scan = self.bliss_first_scan, self.bliss_last_scan
        else:
            first_scan, last_scan = self.nexus_first_scan, self.nexus_last_scan

        _, first_num, last_num, n_tot_avail_z = utils.count_z_stage(first_scan, last_scan)


        if self.bliss_ref_begin_scan is not None:
            nexus_files_ref = []
            for tok in [self.bliss_ref_begin_scan, self.bliss_ref_end_scan]:
                nexus_ref = (
                    os.path.join(self.ref_extraction_target_dir, os.path.splitext(os.path.basename(tok))[0]) + ".nx"
                )
                nexus_files_ref.append(nexus_ref)

        nexus_template, _, _, _ = utils.count_z_stage(nexus_files[0], nexus_files[-1])

        self.b_extend_required_inputs(nexus_files)


        my_output = self.b_compose_output_name("concatenated.nx")
        self.b_class_set_relevant_output_file(my_output)
        self.b_extend_provided_outputs([my_output])

        self.b_reset_script_empty()

        s = textwrap.dedent(
            rf"""
        nxtomomill z-concatenate-scans \
        --filename_template {nexus_template} \
        --target_file {my_output} \
        --entry_name {self.entry_name_par} \
        --total_nstages {n_tot_avail_z} \
        --first_stage {first_num} \
        --last_stage {last_num} \
        --max_dark {self.max_dark_par} \
        """
        ).strip("\n")

        if self.user_voxel_size_par is not None:
            s += textwrap.dedent(
                rf"""
            --pixel_size_m  {(float(self.user_voxel_size_par)*1.0e-6):12.6e} \
            """
            ).strip("\n")

        if (self.cor_scheme_par is not None) and (self.cor_scheme_par == "a"):
            json_cors_file = CompositeCorSimpleScript.b_class_get_relevant_output_file()
            self.b_extend_required_inputs([json_cors_file])
            s += textwrap.dedent(
                f"""
            --cors_file  {json_cors_file} \
            """
            ).strip("\n")

        if self.bliss_ref_begin_scan is not None:
            s += textwrap.dedent(
                rf"""
            --flats_from_before_after yes \
            --scan_before {nexus_files_ref[0]} \
            --scan_after {nexus_files_ref[1]} \
            """
            ).strip("\n")

        if self.comes_from_aspect_scheme == "y":
            s += textwrap.dedent(
                rf"""
            --neutral_flat  yes \
            """
            ).strip("\n")

        self.b_add_script_line(s)


class AspectDuplicateNxToTargetScript(night_rail.BaseScript):
    """ """

    def proper_init(self):
        classes_of_interest = [
            rec_tree.EntryNamePar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if (not AspectBliss2NxScript.b_class_is_used()) and (not AspectBliss2NxMultiZScript.b_class_is_used()):
            return

        self.b_class_set_used(True)

        if AspectBliss2NxScript.b_class_is_used():
            self.finalise_mono()
        else:
            self.finalise_multi()

    def finalise_mono(self):
        original_file = AspectBliss2NxScript.b_class_get_relevant_output_file()
        output_dir = self.b_compose_output_name("aspect_targets_dir")
        target_file = os.path.join(output_dir, os.path.basename(original_file))

        self.b_extend_required_inputs([original_file])
        self.b_extend_provided_outputs([target_file])

        self.b_class_register_key_val("source_files", [original_file])
        self.b_class_set_relevant_output_file([target_file])

        s = textwrap.dedent(
            rf"""
        aspect-create-target --original_file {original_file} \
            --target_file {target_file} --entry_name  {self.entry_name_par}
        """
        ).strip("\n")

        self.b_reset_script_empty()
        self.b_add_script_line(s)

    def finalise_multi(self):
        original_file_template, first_num, last_num = AspectBliss2NxMultiZScript.b_class_get_relevant_output_file()
        output_dir = self.b_compose_output_name("aspect_targets_dir")
        target_file_template = os.path.join(output_dir, os.path.basename(original_file_template))

        original_file_template_numeric = utils.template_to_format_string(original_file_template)
        target_file_template_numeric = utils.template_to_format_string(target_file_template)

        source_files = [original_file_template_numeric.format(i_stage=iz) for iz in range(first_num, last_num + 1)]

        self.b_extend_required_inputs(source_files)

        target_files = [target_file_template_numeric.format(i_stage=iz) for iz in range(first_num, last_num + 1)]

        self.b_extend_provided_outputs(target_files)

        self.b_class_register_key_val("source_files", source_files)
        self.b_class_set_relevant_output_file(target_files)

        s = textwrap.dedent(
            rf"""
        aspect-create-target --original_file {original_file_template} \
            --target_file {target_file_template} \
            --first {first_num} \
            --last {last_num}
        """
        ).strip("\n")

        self.b_reset_script_empty()
        self.b_add_script_line(s)


class AspectCreateBeamProfile(night_rail.BaseScript):
    """This script writes beam profile for aspect"""

    def proper_init(self):
        classes_of_interest = [
            rec_tree.BeamEnergyPar,
            rec_tree.BeamProfileFile,
            rec_tree.MaterialScheme,
            rec_tree.BeamSubdivisionScheme,
            rec_tree.NPointsSpectra,
            rec_tree.EnergyIntervalsKev,
            rec_tree.BeamProfileScheme,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if not AspectDuplicateNxToTargetScript.b_class_is_used():
            return

        self.b_class_set_used(True)

        target_profile_file = self.b_compose_output_name("beam_profile.txt")
        self.b_class_set_relevant_output_file(target_profile_file)

        self.b_extend_provided_outputs([target_profile_file])

        s = textwrap.dedent(
            rf"""
        aspect-beam-energy-profiler \
        """
        )

        s += textwrap.dedent(
            rf"""
        --target_file {target_profile_file} \
        """
        )

        if self.beam_profile_file:
            s += textwrap.dedent(
                rf"""
            --beam_file {self.beam_profile_file} \
            """
            )
            self.b_consider_external_file_for_storing(self.beam_profile_file)

        if self.material_scheme == "b":
            hard_matter, soft_matter = "bone", "ethanol"
        else:
            hard_matter, soft_matter = "sand", "water"

        s += textwrap.dedent(
            rf"""
        --hard_matter {hard_matter} \
        """
        )

        s += textwrap.dedent(
            rf"""
        --soft_matter {soft_matter} \
        """
        )

        if self.beam_profile_scheme == "p":
            if self.n_points_spectra is not None:
                s += textwrap.dedent(
                    rf"""
                --n_points {self.n_points_spectra} \
                """
                )
            elif self.energy_intervals_kev is not None:
                s += textwrap.dedent(
                    rf"""
                --interval {self.energy_intervals_kev} \
                """
                )
            else:
                raise ValueError(
                    f"This should not happen, one and only one between NPointsSpectra and EnergyIntervalsKev has to be provided"
                )
        else:
            s += textwrap.dedent(
                rf"""
            --interval {self.beam_energy_par}  {self.beam_energy_par} \
            """
            )

        profile_img_file = self.b_compose_output_name("beam_profile_img.png")

        s += textwrap.dedent(
            rf"""
        --display 0 \
        --img_prefix  {profile_img_file}
        """
        )

        s = os.linesep.join([tok for tok in s.splitlines() if tok.strip()])

        self.b_reset_script_empty()
        self.b_add_script_line(s)


class AspectApply(night_rail.BaseScript):
    """This script runs aspect_phase"""

    def proper_init(self):
        classes_of_interest = [
            rec_tree.FilteredStartZ,
            rec_tree.FilteredEndZ,
            rec_tree.FilteredStartP,
            rec_tree.FilteredEndP,
            rec_tree.NJobsPerStage,
            rec_tree.EntryNamePar,
            rec_tree.SourceSampleDistPar,
            rec_tree.SampleDetectorDistPar,
            rec_tree.UserVoxelSizePar,
            rec_tree.ScintillatorRPar,
            rec_tree.ScintillatorAlphaPar,
            rec_tree.ScintillatorBorderPar,
            rec_tree.DeconvStripesScheme,
            rec_tree.WeightFileWithStripesPath,
            rec_tree.StripesLightTransmissionPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if not AspectDuplicateNxToTargetScript.b_class_is_used():
            return

        self.b_class_set_used(True)

        target_files = AspectDuplicateNxToTargetScript.b_class_get_relevant_output_file()
        assert isinstance(target_files, (list, tuple))
        self.b_extend_required_inputs(target_files)

        source_files = AspectDuplicateNxToTargetScript.b_class_retrieve_val("source_files")


        assert isinstance(source_files, (list, tuple))

        beam_file = AspectCreateBeamProfile.b_class_get_relevant_output_file()
        self.b_extend_required_inputs([beam_file])

        sequence_file = self.b_compose_output_name("sequence.json")
        sequence_list = [
            dict(expansion=7, niters=20, deriv_step=0.3, n_repets=1, poly=0),
            dict(expansion=6, niters=20, deriv_step=0.2, n_repets=1, poly=0),
            dict(expansion=5, niters=20, deriv_step=0.3, n_repets=1, poly=0),
            dict(expansion=4, niters=20, deriv_step=0.2, n_repets=1, poly=0),
            dict(expansion=3, niters=20, deriv_step=0.2, n_repets=1, poly=0),
            dict(expansion=2, niters=60, deriv_step=0.15, n_repets=1, poly=0),
            dict(expansion=1, niters=20, deriv_step=0.03, n_repets=5, poly=1),
            dict(expansion=0, niters=10, deriv_step=0.02, n_repets=5, poly=1),
        ]
        with open(sequence_file, "w") as fp:
            json.dump(sequence_list, fp)

        n_jobs = len(source_files) * int(self.n_jobs_per_stage)

        source_files_string = " ".join(source_files)
        target_files_string = " ".join(target_files)

        files_string = textwrap.dedent(
            rf"""
        source_list=( {source_files_string} ) 
        target_list=( {target_files_string} ) 

        n_jobs_per_stage={self.n_jobs_per_stage}

        if [ -z ${{SLURM_ARRAY_TASK_ID+x}} ]; then
            echo "SLURM_ARRAY_TASK_ID    is unset "
            SLURM_ARRAY_TASK_ID=0
            n_jobs_per_stage=1
        fi

        file_number=$((  $SLURM_ARRAY_TASK_ID / $n_jobs_per_stage   ))

        source_file=${{source_list[$file_number]}}
        target_file=${{target_list[$file_number]}}
        """
        )

        chunk_string = textwrap.dedent(
            rf"""
        chunk_id=$((  $SLURM_ARRAY_TASK_ID % $n_jobs_per_stage   ))
        """
        )

        s = files_string + chunk_string


        print(" QUI dist ", self.sample_detector_dist_par)
        if float(self.sample_detector_dist_par):
            script_name = "aspect-z-stage-nx"
        else:
            script_name = "deconvolve-z-stage-nx"
            
        s += textwrap.dedent(
            rf"""
        {script_name}  \
        --original_file  $source_file \
        --target_file    $target_file  \
        --entry_name {self.entry_name_par} \
        --n_chunks  $n_jobs_per_stage  \
        --chunk_id  $chunk_id \
        --skip_existing 1 \
        --diffuse_mask_border {self.scintillator_border_par}\
        --diffuse_scale_l {self.scintillator_r_par}\
        --diffuse_fraction {self.scintillator_alpha_par} \
        --diffuse_mask_border {self.scintillator_border_par}\
        """
        )

        if float(self.sample_detector_dist_par):
            s += textwrap.dedent(
                rf"""
                --source_dist_m  {self.source_sample_dist_par}   \
                --detector_dist_m  {self.sample_detector_dist_par}  \
                --pixel_size_um   {self.user_voxel_size_par} \
                --spectra_data_file {beam_file} \
                --pathological_threshold 2 \
                --sequence_file {sequence_file} \
                """
            )
            if self.filtered_start_z and self.filtered_end_z:
                s += textwrap.dedent(
                    rf"""
                    --iz_start  {self.filtered_start_z} \
                    --iz_end    {self.filtered_end_z} \
                    """
                )
            if self.filtered_start_p and self.filtered_end_p:
                s += textwrap.dedent(
                    rf"""
                    --projection_num_start  {max(0,int(self.filtered_start_p)-10)} \
                    --projection_num_end    {int(self.filtered_end_p) + 10} \
                    """
                )

        if self.deconv_stripes_scheme != "n":
            s += textwrap.dedent(
                rf"""
            --stripes_file {self.weight_file_with_stripes_path} \
            --stripes_light_transmission  {self.stripes_light_transmission_par} \
            """
            )

        s = os.linesep.join([tok for tok in s.splitlines() if tok.strip()])

        self.b_reset_script_empty()

        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")
        
        if float(self.sample_detector_dist_par):
            slurm_string = textwrap.dedent(
                rf"""
                #SBATCH --nodes=1
                #SBATCH --tasks-per-node=1
                #SBATCH --cpus-per-task=10
                #SBATCH --mem=100G
                #SBATCH --time=300
                #SBATCH --output=phase-%A.%a.out
                #SBATCH --job-name="phase-poly"
                #SBATCH --gres=gpu:1
                #SBATCH -C a40
                #SBATCH -p low-gpu
                #SBATCH --array 0-{n_jobs-1}
                #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
                source {activation_script}
                """
            )
        else:
            slurm_string = textwrap.dedent(
                rf"""
                #SBATCH --tasks-per-node=1
                #SBATCH --cpus-per-task=1
                #SBATCH --mem=28G
                #SBATCH --time=700
                #SBATCH --output=phase-%A.%a.out
                #SBATCH --job-name="deconvolve"
                #  SBATCH --gres=gpu:1
                #  SBATCH  --nodelist=gpbm18-02
                #  SBATCH -C a4
                #SBATCH -p low-gpu
                #SBATCH --array 0-{n_jobs-1}
                source {activation_script}
                """
            )
            
        self.b_set_slurm_string(slurm_string)
        self.b_add_script_line(s)



class CreateDoubleFlatsScript(night_rail.BaseScript):
    """Prepare the double flats to be used in the reconstructions"""

    def proper_init(self):
        classes_of_interest = [
            rec_tree.DoubleFlatsScheme,
            rec_tree.DoubleFlatsFilterMethodPar,
            rec_tree.DoubleFlatsPeriodNturns,
            rec_tree.DoubleFlatsNsectorsPerTurn,
            rec_tree.DoubleFlatsSigmaPix,
            rec_tree.DoubleFlatsRadiusPix,
            rec_tree.DoubleFlatsRadiusPixBasic,
            rec_tree.DoubleFlatsProjStep ,            

            # all the following one just in order of being able to compose the files that are used by the reconstruction process
            rec_tree.  BlissFirstScan , 
            rec_tree.  BlissLastScan , 
            rec_tree.  NexusFirstScan , 
            rec_tree.  NexusLastScan , 
            rec_tree.  EntryNamePar , 
            rec_tree.  MultiZRecScheme , 
            rec_tree.  BlissUniqueScan , 
            rec_tree.  NexusUniqueScan , 
            rec_tree.  SlurmFractionsNumberPar ,            
            rec_tree.  DoubleFlatsMethodScheme,
            rec_tree.  DoubleFlatsMultiAngleMedScheme,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)


    def b_finalise_script(self):
        if self.double_flats_scheme != "d":
            return
        self.b_class_set_used(True)

        # if self.double_flats_filter_method_par == "g":
        self.finalise_for_gaussian_or_median_filter()


    def finalise_for_gaussian_or_median_filter(self):

        # get the nx scan(s) that will be reconstructed by nabu-helical, possibly more than one

        slurm_fractions_number_par = 1
        if self.multi_z_rec_scheme == "a":  # [A]fter concatenation
            slurm_fractions_number_par = self.slurm_fractions_number_par or 1
        
        input_nx_filename, _ = utils.get_input_filename_and_output_prefix_from_scan_names( bliss_first_scan   = self.bliss_first_scan   ,        
                                                                                           bliss_last_scan    = self.bliss_last_scan    ,        
                                                                                           nexus_first_scan   = self.nexus_first_scan   ,        
                                                                                           nexus_last_scan    = self.nexus_last_scan    ,        
                                                                                           entry_name_par     = self.entry_name_par     ,        
                                                                                           multi_z_rec_scheme = self.multi_z_rec_scheme ,        
                                                                                           bliss_unique_scan  = self.bliss_unique_scan  ,        
                                                                                           nexus_unique_scan  = self.nexus_unique_scan  ,        
                                                                                           fractions_number   = slurm_fractions_number_par
        )

        if isinstance(input_nx_filename, (list, tuple)):
            all_inputs_list = input_nx_filename
        else:
            all_inputs_list = [input_nx_filename]

        radius = self.double_flats_sigma_pix
        if not radius:
            radius = self.double_flats_radius_pix or  self.double_flats_radius_pix_basic
            
        all_inputs_set = list(set(all_inputs_list))
            
        all_outputs_list = [ f"{os.path.splitext(scan_file)[0]}_{self.double_flats_nsectors_per_turn}_{self.double_flats_period_nturns}_{radius}_{self.double_flats_filter_method_par}_DoubleFlats.h5"  for scan_file in all_inputs_list]
        all_outputs_set  = [ f"{os.path.splitext(scan_file)[0]}_{self.double_flats_nsectors_per_turn}_{self.double_flats_period_nturns}_{radius}_{self.double_flats_filter_method_par}_DoubleFlats.h5"  for scan_file in all_inputs_set]

        self.b_extend_required_inputs( all_inputs_set)
        self.b_extend_provided_outputs(all_outputs_list)
 
        self.b_class_set_relevant_output_file(  all_outputs_list  )


        self.b_class_is_used()
        
        # building script
        input_filenames_string  = " ".join(all_inputs_set )
        output_filenames_string = " ".join(all_outputs_set)


        if self.double_flats_filter_method_par == "g":
            inset_for_median = ""
        else:
            inset_for_median = " --do_median "

        if self.double_flats_method_scheme == "b": # basic
            multi_angle_inset = ""
            method = 1
        else:  # Vincent
            angle_med = 0
            if self.double_flats_multi_angle_med_scheme == "a":  # apply
                angle_med=1
            multi_angle_inset = f" --multi_angle_med  {angle_med}"
            method = 2

            
        command = textwrap.dedent(
            rf"""
        input_list=( {input_filenames_string} )
        output_list=( {output_filenames_string} )

        n_scans={len(all_inputs_set)}

        for (( i=0; i<$(( $n_scans  )); i++ ))
        do

            input=${{input_list=[$k]}} 
            output=${{output_list=[$k]}} 

            get_double_flats_mean \
            --scan_file  $input \
            --output_file $output \
            --method {method}   {multi_angle_inset}  \
            --entry_name {self.entry_name_par} \
            --period_nturns   {self.double_flats_period_nturns}   \
            --sigma_pix  {self.double_flats_sigma_pix  or self.double_flats_radius_pix or self.double_flats_radius_pix_basic} \
            --nsectors_per_turn {self.double_flats_nsectors_per_turn}   \
            --ncpus 32   {inset_for_median} --proj_step {self.double_flats_proj_step}
        done
        """
        )
        
        
        self.b_reset_script_empty()
        self.b_add_script_line(command)

import night_rail
import os
import textwrap
import math
import shutil
import json
import h5py

from . import utils

from . import distortion_calibration_rec_tree as rec_tree

night_rail.environment_variables.update({"SKIP_TOMOSCAN_CHECK": "1"})
night_rail.environment_variables.update({"TOMOTOOLS_SKIP_DET_CHECK": "1"})
night_rail.environment_variables.update({"MEAN_FOR_FLATS": "1"})

def get_all_scripts():
    result = night_rail.BaseScriptsContainer()
    result.extend(
        [
            Bliss2NxScript(),
            Bliss2NxMultiZScript(),
            CompositeCorSimpleScript(),
            CreateWeightMapScript(),
            CreateNabuFinalConfScript(),
            NabuFinalRunScript(),
        ]
    )

    return result


class Bliss2NxScript(night_rail.BaseScript):
    """This script performs the first phase : from bliss scan(s)
    to nexus scan(s)
    """

    im_for_aspect = False

    def proper_init(self):
        classes_of_interest = [
            rec_tree.WorkFlowScheme,
            rec_tree.BlissUniqueScan,
            rec_tree.NexusUniqueScan,
            rec_tree.RoughVoxelCorrectionPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if self.bliss_unique_scan or self.nexus_unique_scan:
            self.b_class_set_used(True)
            self.finalise_unique_scan()
        else:
            """this script is  not used because BlissUniqueScan was not set.
            This is a simple example of triggering a script.
            Another way would be triggering the neuralgic_id of the DatasetLocation class.
            So the finalisation will not be done, the script will remain empty, and therefore
            not considered.
            """
            pass

    def finalise_unique_scan(self):
        if self.bliss_unique_scan:
            output_file = self.b_compose_output_name("output.nx")
            self.b_class_set_relevant_output_file(output_file)
            self.b_extend_required_inputs([self.bliss_unique_scan])
            self.b_extend_provided_outputs([output_file])

            if self.rough_voxel_correction_par and float(self.rough_voxel_correction_par):
                extra_for_voxel = f"--set-params x_pixel_size {self.rough_voxel_correction_par} y_pixel_size {self.rough_voxel_correction_par}"
            else:
                extra_for_voxel = ""

            s = f"nxtomomill h52nx {self.bliss_unique_scan}  {output_file}  {extra_for_voxel} --overwrite"
        elif self.nexus_unique_scan:
            self.b_class_set_relevant_output_file(self.nexus_unique_scan)
            self.b_extend_provided_outputs([self.nexus_unique_scan])
            s = f""" echo 'nothing to be done. Just passing nexus filename'   """
        self.b_reset_script_empty()
        self.b_add_script_line(s)


class Bliss2NxMultiZScript(night_rail.BaseScript):
    """This script performs the first phase : from bliss scan(s)
    to nexus scan(s) in the more complicated case of multiple scans
    """

    im_for_aspect = False

    def proper_init(self):
        classes_of_interest = [
            rec_tree.WorkFlowScheme,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissRefBeginScan,
            rec_tree.BlissRefEndScan,
            rec_tree.RefExtractionTargetDir,
            rec_tree.EntryNamePar,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.RoughVoxelCorrectionPar,
        ]
        self.b_add_to_classes_of_interest(classes_of_interest)

    def b_finalise_script(self):
        if (not self.bliss_first_scan) and (not self.nexus_first_scan):
            # because we need the first the last and optionally ref begin ref end
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan:
            first_scan = self.bliss_first_scan
            last_scan = self.bliss_last_scan
        else:
            first_scan = self.nexus_first_scan
            last_scan = self.nexus_last_scan

        filename_template, first_num, last_num, n_tot_avail_z = utils.count_z_stage(first_scan, last_scan)

        if first_scan == last_scan:
            filename_template = first_scan

        if self.bliss_first_scan and (first_scan != last_scan):
            output_filename_template = self.utility_get_output_filename_template()
        else:
            if not self.bliss_first_scan:
                output_filename_template = filename_template  # just passing existing nexus through
            else:
                # single file
                output_filename_template = os.path.join(
                    self.b_compose_output_name("output_dir"),
                    os.path.splitext(os.path.basename(filename_template))[0] + ".nx",
                )
        self.b_class_set_relevant_output_file((output_filename_template, first_num, last_num))

        if first_scan != last_scan:
            outputted_files = self.utility_get_outputted_filenames(first_num, last_num, output_filename_template)
        else:
            outputted_files = [output_filename_template]

        if self.rough_voxel_correction_par   and float(self.rough_voxel_correction_par):
            extra_for_voxel = f"--voxel_size   {self.rough_voxel_correction_par} "
        else:
            extra_for_voxel = ""

        self.b_extend_provided_outputs(outputted_files)

        self.b_class_register_key_val("nexus_files", outputted_files)
        self.b_reset_script_empty()

        if self.bliss_first_scan:
            template_or_not = {False: "", True: "_template"}[(first_scan != last_scan)]

            s = textwrap.dedent(
                rf"""
            nxtomomill zstages2nxs \
            --output_filename{template_or_not} {output_filename_template} \
            --filename{template_or_not} {filename_template} \
            --entry_name {self.entry_name_par} \
            --total_nstages {n_tot_avail_z} \
            --first_stage {first_num} \
            --last_stage {last_num} \
            """
            ).strip("\n")

            if self.bliss_ref_begin_scan is not None:
                s += textwrap.dedent(
                    rf"""
                --do_references True \
                --ref_scan_begin {self.bliss_ref_begin_scan} \
                --ref_scan_end {self.bliss_ref_end_scan} \
                --extracted_reference_target_dir {self.ref_extraction_target_dir} \
                {extra_for_voxel}
                """
                ).strip("\n")

        else:
            s = """  echo 'nothing to be done. Just passing nexus filename'  """

        self.b_add_script_line(s)

    @classmethod
    def utility_get_nexus_refs_filenames(cls):
        output_dir = cls.b_compose_output_name("output_dir")
        res = [
            os.path.join(output_dir, cls.b_compose_output_name(f"{what}"))
            for what in ("output_begin.nx", "output_end.nx")
        ]
        return res

    @classmethod
    def utility_get_output_filename_template(cls):
        output_dir = cls.b_compose_output_name("output_dir")
        output_filename_template = os.path.join(output_dir, cls.b_compose_output_name("output_XXXX.nx"))
        return output_filename_template

    @classmethod
    def utility_get_outputted_filenames(cls, first_num, last_num, output_filename_template=None):
        if not output_filename_template:
            output_filename_template = cls.utility_get_output_filename_template()

        output_filename_template_numeric = utils.template_to_format_string(output_filename_template)
        names_list = []
        for i_stage in range(first_num, last_num + 1):
            names_list.append(output_filename_template_numeric.format(i_stage=i_stage))
        return names_list


class CompositeCorSimpleScript(night_rail.BaseScript):
    """Called to find the cor in the standard way.
    This means that the scan is at constant z_translation or it has at least
    a first part during which the z is constant for more than 180 degree,
    not necessarily 360 but more than 180
    """

    # reimplemented __init__
    def proper_init(self):
        self.nx_from_first_h52nx_filename = None

        classes_of_interest = [
            rec_tree.NearCorValueScheme,
            rec_tree.NearCorValuePar,
            rec_tree.NearWidthValuePar,
            rec_tree.LowPassValuePar,
            rec_tree.HighPassValuePar,
            rec_tree.CompCorThetaIntervalValuePar,
            rec_tree.NSubsamplingValuePar,
            rec_tree.EntryNamePar,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.CorSchemePar,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)  # beside setting the listening to them,
        # this also sets self.near_cor_value..

    def b_finalise_script(self):
        if self.cor_scheme_par != "a":
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan or self.nexus_first_scan:
            self.finalise_for_multiple_scans()
        else:
            self.simple_finalise()

    def finalise_for_multiple_scans(self):
        if self.bliss_first_scan:
            first_scan, last_scan = self.bliss_first_scan, self.bliss_last_scan
        else:
            first_scan, last_scan = self.nexus_first_scan, self.nexus_last_scan

        template_tmp, first_num, last_num, _ = utils.count_z_stage(first_scan, last_scan)

        if self.bliss_first_scan:
            required_files = Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")
            # required_files = Bliss2NxMultiZScript.utility_get_outputted_filenames(first_num, last_num)
        else:
            file_template_numeric = utils.template_to_format_string(template_tmp)
            required_files = [file_template_numeric.format(i_stage=iz) for iz in range(first_num, last_num + 1)]

        self.b_extend_required_inputs(required_files)

        if len(required_files) > 1:
            filename_template = Bliss2NxMultiZScript.utility_get_output_filename_template()
            numerical_scan_number_inset = f""" --first_stage {first_num}  --num_of_stages {last_num - first_num +1} """
        else:
            filename_template = required_files[0]
            numerical_scan_number_inset = " "

        output_file = self.b_compose_output_name("cors.json")
        self.b_class_set_relevant_output_file(output_file)
        self.b_register_for_processed_dir([output_file])

        self.b_extend_provided_outputs([output_file])

        self.b_reset_script_empty()

        cor_inset = ""
        if self.near_cor_value_par is not None:
            cor_inset = f"near_pos = {self.near_cor_value_par};"
        
        correlation_options = f""" "side='near'; {cor_inset} near_width = {self.near_width_value_par}; low_pass = {self.low_pass_value_par} ; high_pass={self.high_pass_value_par}  "  """

        command_line = textwrap.dedent(
            rf"""
            nabu-composite-cor --filename_template {filename_template} \
                 --cor_options  {correlation_options}  {numerical_scan_number_inset} \
                  --n_subsampling_y {self.n_subsampling_value_par} \
                  --theta_interval {self.comp_cor_theta_interval_value_par} \
                  --output_file {output_file} \
                  --entry_name {self.entry_name_par} 
            """
        ).strip("\n")

        self.b_add_script_line(command_line)

    def simple_finalise(self):
        input_nx_filename = Bliss2NxScript.b_class_get_relevant_output_file()
        self.b_extend_required_inputs([input_nx_filename])
        output_file = self.b_compose_output_name("cors.json")
        self.b_class_set_relevant_output_file(output_file)

        self.b_extend_provided_outputs([output_file])
        self.b_reset_script_empty()


        cor_inset = ""
        if self.near_cor_value_par is not None:
            cor_inset = f"near_pos = {self.near_cor_value_par};"
        
        correlation_options = f""" "side='near'; {cor_inset} near_width = {self.near_width_value_par}; low_pass = {self.low_pass_value_par} ; high_pass={self.high_pass_value_par}  "  """

        command_line = textwrap.dedent(
            rf"""
             nabu-composite-cor --filename_template {input_nx_filename} \
                     --cor_options  {correlation_options} \
                     --n_subsampling_y {self.n_subsampling_value_par}  \
                     --theta_interval {self.comp_cor_theta_interval_value_par}  \
                     --output_file {output_file} \
                     --entry_name {self.entry_name_par} """
        ).strip("\n")
        self.b_add_script_line(command_line)


class CreateNabuConfScript_base_class(night_rail.BaseScript):
    """Build Reconstruct conf file for Nabu."""

    b_im_python_script = True

    def proper_init(self):
        classes_of_interest = [
            # for Paganin
            rec_tree.DeltaBetaPar,
            # for reconstruction range
            rec_tree.VerticalRangeSchemePar,
            rec_tree.VoxelSchemePar,
            rec_tree.UserVoxelSizePar,
            rec_tree.CorSchemePar,
            rec_tree.CorValuePar,
            rec_tree.EntryNamePar,
            rec_tree.BlissFirstScan,
            rec_tree.BlissLastScan,
            rec_tree.BlissUniqueScan,
            rec_tree.BlissUniqueScan,
            rec_tree.NexusFirstScan,
            rec_tree.NexusLastScan,
            rec_tree.NexusUniqueScan,
            rec_tree.NexusUniqueScan,
            rec_tree.ReconstructionSchemePar,
            rec_tree.VoxelSchemePar,
            rec_tree.CorSchemePar,
            rec_tree.OutputFormatPar,
            rec_tree.AngularToleranceInStepsPar,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)

    def get_output_format(self):
        # by default hdf5 always for the base class
        return "hdf5"

    def check_if_im_used(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def get_input_filename_and_output_prefix(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def get_cor_finder_class(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def set_vertical_range(self):
        # The range. This function is redefined in the diagnostic runner to get automatically the whole range
        # If too much ( for the diagnostic runners)  then extra range parameters will have to be asked in the dialog. Not yet done
        if self.vertical_range_scheme_par == "a":
            start_z, end_z = 0, -1
            start_z_mm, end_z_mm = 0, 0  # *_z_mm has the priority unless it is zero
            start_z_fract, end_z_fract = (0, 0)
            is_single_slice = False
        elif self.vertical_range_scheme_par == "f":
            start_z, end_z = 0, -1
            start_z_fract, end_z_fract = (self.range_fraction_start_par, self.range_fraction_end_par)
            start_z_mm, end_z_mm = 0, 0
            is_single_slice = start_z_fract == end_z_fract

        elif self.vertical_range_scheme_par == "i":
            start_z, end_z = self.start_z_par, self.end_z_par
            start_z_fract, end_z_fract = (0, 0)
            start_z_mm, end_z_mm = 0, 0

            is_single_slice = (end_z - start_z) == 1

        elif self.vertical_range_scheme_par == "m":
            start_z, end_z = 0, -1
            start_z_mm, end_z_mm = self.start_z_mm_par, self.end_z_mm_par
            start_z_fract, end_z_fract = (0, 0)

            is_single_slice = False

        else:
            raise ValueError(f"""  scheme {self.vertical_range_scheme_par} not yet implemented""")

        self.start_z = start_z
        self.start_z_mm = start_z_mm
        self.start_z_fract = start_z_fract
        self.end_z = end_z
        self.end_z_mm = end_z_mm
        self.end_z_fract = end_z_fract

        return is_single_slice

    def b_set_divers(self):
        pass

    def b_finalise_script(self):
        if not self.check_if_im_used():
            return

        self.b_class_set_used(True)

        n_stripes = 3
        stripe_border = 10
        stripe_width = 2 * stripe_border + 1

        input_nx_filename, output_prefix = self.get_input_filename_and_output_prefix()

        if isinstance(input_nx_filename, (list, tuple)):
            from_many_separately = True
        else:
            from_many_separately = True
            input_nx_filename = [input_nx_filename]

        self.b_class_register_key_val("input_nx_filename", input_nx_filename)
        self.b_class_register_key_val("output_prefix", output_prefix)

        if from_many_separately:
            self.b_extend_required_inputs(input_nx_filename)
            conf_file = self.b_compose_output_name("nabuinput.conf")

            input_nx_list = input_nx_filename
            conf_file_list = [(conf_file + f"_stripe_{i_rec}") for i_rec in range(n_stripes)]
            output_prefix_list = [(output_prefix[0] + f"_stripe_{i_rec}") for i_rec in range(n_stripes)]

            self.b_class_set_relevant_output_file(conf_file_list)
            self.b_extend_provided_outputs(conf_file_list)

        else:
            self.b_extend_required_inputs([input_nx_filename])
            conf_file = self.b_compose_output_name("nabuinput.conf")
            self.b_class_set_relevant_output_file(conf_file)
            self.b_extend_provided_outputs([conf_file])

            input_nx_list = [input_nx_filename]
            conf_file_list = [(conf_file + f"_stripe_{i_rec}") for i_rec in range(n_stripes)]
            output_prefix_list = [(output_prefix + f"_stripe_{i_rec}") for i_rec in range(n_stripes)]

        script_to_create_conf = ""

        if len(input_nx_list) > 1:
            message = f"""  the distortion calibration retrieval is expected to operate on one scan only.
            I got instead this list of scans: {input_nx_list,} """
            raise ValueError(message)

        radio_height, radio_width = utils.get_radio_shape(
            [self.bliss_first_scan, self.bliss_unique_scan, self.nexus_first_scan, self.nexus_unique_scan]
        )

        if radio_height < stripe_width + (n_stripes - 1):
            message = f""" Radios are too thin in height, I expect an height of {stripe_width+(n_stripes-1)} at least
            """
            raise ValueError(message)

        h_range = radio_height - 2 * stripe_border

        step_short = h_range // (n_stripes + 1)

        step_big = max(1, step_short)

        info_for_retriever = {}

        info_for_retriever.update(
            dict(
                n_stripes=n_stripes,
                radio_height=radio_height,
                radio_width=radio_width,
            )
        )

        for i_rec in range(n_stripes):
            info_for_retriever.update({i_rec: {}})

            my_input_nx, my_conf, my_output_prefix = input_nx_list[0], conf_file_list[i_rec], output_prefix_list[i_rec]

            line_position_z = stripe_border + step_short + (i_rec) * step_big
            start_z = line_position_z - stripe_border
            end_z = line_position_z + stripe_border

            info_for_retriever[i_rec].update(dict(start_z=start_z, end_z=end_z + 1))
            info_for_retriever[i_rec].update(dict(sino_file="sinogram_" + output_prefix_list[i_rec] + ".hdf5"))

            inset_for_rotation_axis = ""

            cor_finder_cls = self.get_cor_finder_class()
            if cor_finder_cls is not None:
                cor_data_file = cor_finder_cls.b_class_get_relevant_output_file()

                self.b_extend_required_inputs([cor_data_file])
                #  keeping the indentation which will be removed by dedent
                inset_for_rotation_axis = rf"""
                  import os
                  import h5py
                  import json
                  if os.path.splitext("{cor_data_file}")[1] == ".json":
                    cor_dict = json.load( open("{cor_data_file}","r")    )
                    rotation_axis_position = cor_dict["rotation_axis_position_list"][0]
                  elif os.path.splitext("{cor_data_file}")[1] == ".hdf5":
                    message = "The distortion retriever  works with single scan for which the cor is expected from the compositecorfinder procedure for non helical scans"
                    messagge += " it seems instead that the output from a diagnostica analysis is plugged. This should not happen "
                    raise ValueError(message)
                  else:
                      raise f" the cor correction filename must have for extension either json or hdf5. The name was    '{cor_data_file}'   "
                """

                info_for_retriever.update(dict(cor_data_file=cor_data_file))

            else:
                if not self.cor_scheme_par == "u":
                    raise ValueError(
                        f""" No automatic procedure for cor finding is active and there is no user selected value in class {self.__class__}"""
                    )
                rotation_axis_position = self.cor_value_par

                info_for_retriever.update(dict(rotation_axis_position=rotation_axis_position))

                inset_for_rotation_axis = rf"""
                      rotation_axis_position = {rotation_axis_position:15.4f}
                """

            ## Distortion parameter
            do_detector_correction = """  ""  """
            detector_correction_url = """  ""   """
            # Voxel parameter
            if self.voxel_scheme_par == "u":
                overwrite_metadata = rf""" pixel_size = {self.user_voxel_size_par} um ; """
            else:
                overwrite_metadata = """  ""  """
            # weight map
            weights_file = CreateWeightMapScript.b_compose_output_name("weight_map.h5")
            self.b_extend_required_inputs([weights_file])

            output_prefix = my_output_prefix

            # divers
            self.b_set_divers()

            # Unsharp

            # Phase Filter
            if float(self.delta_beta_par) == 0:
                phase_filter = ""
            else:
                phase_filter = "Paganin"

            output_format = self.get_output_format()

            should_i_normalize_srcurrent = 1
            # THE SCRIPT
            script_to_create_conf += textwrap.dedent(
                rf"""
                  from nabu.pipeline.config import generate_nabu_configfile
                  from nabu.pipeline.helical.nabu_config import nabu_config as helical_fullfield_config
                  {inset_for_rotation_axis}

                  prefilled_values = dict(
                    dataset = dict(location = "{my_input_nx}",  overwrite_metadata =  {overwrite_metadata}, hdf5_entry='{self.entry_name_par}' ),
                    phase   = dict(method = "{phase_filter}", delta_beta = {self.delta_beta_par} ),
                    reconstruction = dict( rotation_axis_position = rotation_axis_position, start_z = {start_z}, end_z = {end_z}, start_z_fract = {0}, end_z_fract = {0},
                          start_z_mm = {0}, end_z_mm = {0}, clip_outer_circle=1, angular_tolerance_steps={self.angular_tolerance_in_steps_par}
                       ),
                    preproc = dict( detector_distortion_correction = "{do_detector_correction}", 
                                    detector_distortion_correction_options= '{detector_correction_url}',
                                    processes_file = "{weights_file}" , 
                                    normalize_srcurrent = "{should_i_normalize_srcurrent}",   
                               ),

                    output = dict(  location = "./", file_prefix = "{my_output_prefix}", file_format = "{output_format}" ) ,
                    pipeline = dict (save_steps = "sinogram"    ),
                  )
                  generate_nabu_configfile(
                    "{my_conf}",
                    helical_fullfield_config,
                    comments=True,
                    options_level="advanced",
                    prefilled_values=prefilled_values,
                  )

            """
            )
        ### "#!/usr/bin/env python\n" +
        script_to_create_conf = night_rail.get_python_preamble() + script_to_create_conf

        self.b_set_python_script(script_to_create_conf)

        stripes_file = self.b_compose_output_name("stripes.json")

        with open(stripes_file, "w") as f:
            json.dump(info_for_retriever, f, indent=4)


class CreateNabuFinalConfScript(CreateNabuConfScript_base_class):
    """Build Reconstruct conf file for Nabu. Final reconstruction"""

    def check_if_im_used(self):
        return self.reconstruction_scheme_par in ["h", "u"]

    def get_output_format(self):
        if self.output_format_par == "h":
            return "hdf5"
        else:
            return "tiff"

    def get_cor_finder_class(self):
        if self.cor_scheme_par == "a":
            return CompositeCorSimpleScript
        else:
            return None

    def get_input_filename_and_output_prefix(self):
        ## Generate the output file in line with original bliss files

        from_many_separately = False

        if self.bliss_first_scan or self.nexus_first_scan:
            if self.bliss_first_scan:
                first_scan = self.bliss_first_scan
                last_scan = self.bliss_last_scan
                first_scan_bliss = first_scan
                last_scan_bliss = last_scan
            else:
                first_scan = self.nexus_first_scan
                last_scan = self.nexus_last_scan
                first_scan_bliss = first_scan
                last_scan_bliss = last_scan

                url = "/" + self.entry_name_par + "/bliss_original_files"
                with h5py.File(first_scan, "r") as f:
                    if url in f:
                        first_scan_bliss = str(f[url][0])

                with h5py.File(last_scan, "r") as f:
                    if url in f:
                        last_scan_bliss = str(f["/" + self.entry_name_par + "/bliss_original_files"][-1])

            filename_template_bliss, first_num_bliss, last_num_bliss, _ = utils.count_z_stage(
                first_scan_bliss, last_scan_bliss
            )  # the original total number
            filename_template, first_num, last_num, _ = utils.count_z_stage(
                first_scan, last_scan
            )  # just the ones that are found there when nexus is used

            if True:
                from_many_separately = True
                original_file_template_numeric = filename_template  # distortion will work on one file only
                # utils.template_to_format_string(filename_template)

                output_prefix = [
                    os.path.splitext(os.path.basename(original_file_template_numeric.format(i_stage=i_z)))[0]
                    for i_z in range(first_num, last_num + 1)  # no substitution will be done here
                ]

        else:
            if self.bliss_unique_scan:
                unique_scan = self.bliss_unique_scan
            else:
                unique_scan = self.nexus_unique_scan
            output_prefix = os.path.splitext(os.path.basename(unique_scan))[0]

        if not from_many_separately:
            # let us see who is producing the nexus for us
            input_nx_filename, _ = utils.find_top_level_output_file_from_classes([Bliss2NxScript])
        else:
            input_nx_filename = Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")
        return input_nx_filename, output_prefix


class NabuRunScript_base_class(night_rail.BaseScript):
    """ """

    i_am_final = False

    def proper_init(self):
        classes_of_interest = [
            rec_tree.ChunkSchemePar,
            rec_tree.ChunkSizePar,
            rec_tree.DatasetLocation,
            rec_tree.ReconstructionSchemePar,
            rec_tree.CompCorThetaIntervalValuePar,
            rec_tree.OutputFormatPar,
        ]

        self.b_add_to_classes_of_interest(classes_of_interest)  # beside setting the listening to them,
        # this also sets self.near_cor_value..

    def check_if_im_used(self):
        raise RuntimeError("This function should be redefined in the derived classes")

    def configure_output_list(self, output_prefix_list):
        assert len(output_prefix_list) == 1, " The base version of the nabu run class works on one reconstruction only"

        output_file_list = [tok + ".hdf5" for tok in output_prefix_list]

        self.b_extend_provided_outputs(output_file_list)

        self.b_class_set_relevant_output_file(output_file_list[0])
        self.b_class_register_key_val("output_file", output_file_list[0])

        return output_file_list

    def b_finalise_script(self):
        if not self.check_if_im_used():
            return
        self.b_class_set_used(True)

        input_nx_filename = self.get_dataset_location()

        if isinstance(input_nx_filename, (list, tuple)):
            input_nx_filename_list = input_nx_filename

        else:
            input_nx_filename_list = [input_nx_filename]

            self.b_class_register_key_val("input_file", input_nx_filename_list[0])

        self.b_extend_required_inputs(input_nx_filename_list)

        weights_file = CreateWeightMapScript.b_class_get_relevant_output_file()
        self.b_extend_required_inputs([weights_file])

        conf_cls = self.get_conf_creator_class()

        conf_file = conf_cls.b_class_get_relevant_output_file()

        if isinstance(conf_file, (list, tuple)):
            self.b_extend_required_inputs(conf_file)
            conf_file_list = conf_file
        else:
            self.b_extend_required_inputs([conf_file])
            conf_file_list = [conf_file]

        ## this for hdf5. For tiff, intercept par for tiff if any, and change accordingly

        output_prefix = conf_cls.b_class_retrieve_val("output_prefix")

        print(" OUTPUT ", output_prefix)
        print(" CONF ", conf_file)

        if isinstance(output_prefix, (list, tuple)):
            output_prefix_list = output_prefix
        else:
            output_prefix_list = [output_prefix]

        output_file_list = self.configure_output_list(output_prefix_list)

        if self.has_to_be_moved():
            to_be_moved = set()
            for a, b in zip(output_prefix_list, output_file_list):
                to_be_moved.update((a, b))
            self.b_register_for_processed_dir(list(to_be_moved))

        run_options = self.get_run_options()

        self.b_reset_script_empty()

        activation_script = os.path.join(os.path.dirname(shutil.which("python")), "activate")

        slurm_string = textwrap.dedent(
            rf"""
        #SBATCH --nodes=1
        #SBATCH --tasks-per-node=1
        #SBATCH --cpus-per-task=32
        #SBATCH --mem=100G
        #SBATCH --time=300
        #SBATCH --output=phase-%A.%a.out
        #SBATCH --job-name="phase-poly"
        #SBATCH --gres=gpu:1
        #SBATCH -C a40
        #SBATCH -p gpu
        #SBATCH --array 0-{len(conf_file_list)-1}
        #  SBATCH   --exclude=gpbm18-01,gpbm18-02,gpbm18-03,gpbm18-04
        source {activation_script}
        module load cuda/11
        """
        )

        self.b_set_slurm_string(slurm_string)

        conf_files_string = " ".join(conf_file_list)

        command = textwrap.dedent(
            rf"""
        conf_file_list=( {" ".join( conf_file_list  )} )
        n_vols_per_task=1
        if [ -z ${{SLURM_ARRAY_TASK_ID+x}} ]; then
            echo "SLURM_ARRAY_TASK_ID    is unset "
            SLURM_ARRAY_TASK_ID=0
            n_vols_per_task={len(conf_file_list)}
        fi

        for (( i=0; i<$(( $n_vols_per_task  )); i++ ))
        do
             k=$(( $SLURM_ARRAY_TASK_ID * $n_vols_per_task + $i  ))
             nabu-helical  ${{conf_file_list[$k]}} {run_options}
        done
        """
        )
        if self.i_am_final:
            command += textwrap.dedent(
                rf"""
            for (( i=0; i<$(( $n_vols_per_task  )); i++ ))
            do
              k=$(( $SLURM_ARRAY_TASK_ID * $n_vols_per_task + $i  ))
              night_rail_bm18_nexus_juicer  ${{conf_file_list[$k]}} ${{conf_file_list[$k]}}_nexus_metadata.json 
            done
            """
            )
            for conf_name in conf_file_list:
                self.b_register_for_processed_dir([conf_name + "_nexus_metadata.json"])

        self.b_add_script_line(command)


class NabuFinalRunScript(NabuRunScript_base_class):
    """ """

    i_am_final = True

    def configure_output_list(self, output_prefix_list):
        if self.output_format_par == "h":
            output_file_list = [tok + ".hdf5" for tok in output_prefix_list]
        else:
            output_file_list = output_prefix_list

        self.b_extend_provided_outputs(output_file_list)
        self.b_class_set_relevant_output_file(output_file_list)
        self.b_class_register_key_val("output_file", output_file_list)

        return output_file_list

    def check_if_im_used(self):
        return CreateNabuFinalConfScript.b_class_is_used()

    def get_dataset_location(self):
        return CreateNabuFinalConfScript.b_class_retrieve_val("input_nx_filename")

    def get_conf_creator_class(self):
        return CreateNabuFinalConfScript

    def has_to_be_moved(self):
        return True

    def get_run_options(self):
        if self.chunk_scheme_par == "n":
            return ""
        elif self.chunk_scheme_par == "c":
            return f"--max_chunk_size {self.chunk_size_par}"



class CreateWeightMapScript(night_rail.BaseScript):
    """ """
    b_im_python_script = True

    def proper_init(self):
        self.b_add_to_classes_of_interest(
            [
                rec_tree.WeightVerticalTransitionLengthPar,
                rec_tree.WeightHorizontalTransitionLengthPar,
                rec_tree.EntryNamePar,
                rec_tree.BlissFirstScan,
                rec_tree.NexusFirstScan,
                rec_tree.ReconstructionSchemePar,
                rec_tree.CorValuePar,                
                rec_tree.CorSchemePar,
                rec_tree.NumberOfScintillatorStripes  ,
                rec_tree.StripeWidth  ,
                rec_tree.StripesShearMin  ,
                rec_tree.StripesShearMax  ,
            ]
        )

        
    def b_finalise_script(self):
        if self.reconstruction_scheme_par != "h":
            return

        self.b_class_set_used(True)

        if self.bliss_first_scan is not None or self.nexus_first_scan:
            input_nx_filename = Bliss2NxMultiZScript.b_class_retrieve_val("nexus_files")[0]
        else:
            input_nx_filename = Bliss2NxScript.b_class_get_relevant_output_file()

        self.b_extend_required_inputs([input_nx_filename])


            
        self.b_extend_required_inputs([input_nx_filename])

        output_file = self.b_compose_output_name("weight_map.h5")
        self.b_class_set_relevant_output_file(output_file)
        self.b_extend_provided_outputs([output_file])

        self.b_reset_script_empty()
    
        script_to_create_map = textwrap.dedent(
            rf"""
            from nabu.app.prepare_weights_double import main as prepare_map

            import os
            import h5py
            import json
            from nxtomo.application.nxtomo import NXtomo
        
            scan = NXtomo()
            scan.load(file_path="{input_nx_filename}", data_path="{self.entry_name_par}")
            estimated_near = scan.instrument.detector.estimated_cor_from_motor
    
            arguments = [  
            "--scan_file", "{input_nx_filename}",
            "--entry_name" , "{self.entry_name_par}",
            "--target_file", "{output_file}",
            "--rotation_axis_position", str(estimated_near),
            "--transition_width_horizontal", str({self.weight_horizontal_transition_length_par}),
            "--transition_width_vertical", str({self.weight_vertical_transition_length_par}),
            ]     
            if {self.number_of_scintillator_stripes}:
              arguments += [
                  "--n_stripes", str({self.number_of_scintillator_stripes }),
                  "--stripe_shear_min", str({self.stripes_shear_min }) ,
                  "--stripe_shear_max", str({self.stripes_shear_max }) ,
                  "--stripe_width",     str({self.stripe_width }) 
                ]


            prepare_map(arguments)

            """)

        script_to_create_map = night_rail.get_python_preamble() + script_to_create_map

        self.b_set_python_script(script_to_create_map)
        


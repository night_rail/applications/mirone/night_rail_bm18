#!/bin/bash
set -e

TAG=24_02_08

# INST_DIR=/data/scisofttmp/mirone/environments
# ENV_DIR=${INST_DIR}/epr_${TAG} 

# INST_DIR=/scisoft/tomotools/x86_64/cuda11/
# INST_DIR=/tmp_14_days/mirone/silx

ENV_DIR=${INST_DIR}/bm18_test

# INST_DIR=/data/scisofttmp/mirone/aspect_bis
# ENV_DIR=${INST_DIR}/env



python3 -m venv $ENV_DIR

sed -i '3s/^/module load cuda\/11 \n/' $ENV_DIR/bin/activate

sed -i '3s/^/export SKIP_TOMOSCAN_CHECK=\"1\"  \n/' $ENV_DIR/bin/activate
sed -i '3s/^/export TOMOTOOLS_SKIP_DET_CHECK=\"1\"  \n/' $ENV_DIR/bin/activate


source $ENV_DIR/bin/activate
pip install pip --upgrade

pip install pyopencl

python3 -m pip install git+https://github.com/lebedov/scikit-cuda.git


git clone https://gitlab.esrf.fr/mirone/nabu nabu_tmp
cd nabu_tmp
git checkout $TAG
pip install .[full]
cd ..

# python3 -m pip install git+https://gitlab.esrf.fr/mirone/nabu@${TAG}

python3 -m pip install git+https://gitlab.esrf.fr/mirone/nxtomomill@${TAG}
python3 -m pip install git+https://gitlab.esrf.fr/mirone/nxtomo@${TAG}
python3 -m pip install git+https://gitlab.esrf.fr/mirone/tomoscan@${TAG}

python3 -m pip install git+https://gitlab.esrf.fr/night_rail/night_rail@${TAG}
python3 -m pip install git+https://gitlab.esrf.fr/night_rail/applications/mirone/night_rail_bm18@${TAG}

python3 -m pip install git+https://gitlab.esrf.fr/tomotools/nabuxx@${TAG}

pip install ewoks
pip install python-dateutil
pip install pyperclip
pip install PyQt5
pip install matplotlib

python3 -m pip install git+https://gitlab.esrf.fr/mirone/aspect_phase@release_1

pip install filelock
pip install pyfftw
pip install xrt
pip install pandas

pip install silx==1.1.2


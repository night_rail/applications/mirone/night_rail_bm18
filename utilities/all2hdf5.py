from h5py import *
import h5py
import fabio
import numpy

templatein="helical_HA1200_33.31um_violin-Paolasini-Guarneri_OYCAM_{pid:06d}.edf"

shape = fabio.open(templatein.format(pid=0)).data.shape
size=4*shape[0]*shape[1]
offset = 512

nz = 14643

with h5py.File("stack.h5", "w") as f:
    gf = f.create_group("files")
    for i in list(range(nz)):
        print (i)
        dataset = gf.create_dataset(f"edffile{i:06d}",
                                   shape=shape,
                                   dtype=numpy.float32,
                                    external=((templatein.format(pid=i), offset, size),))

        
with h5py.File("stack.h5", "r+") as f:
    layout = h5py.VirtualLayout( (nz,) + shape , dtype='f')

    for i in list(range(nz)):
        print (i)
        vsource = h5py.VirtualSource(f[f"files/edffile{i:06d}"])
        layout[i] = vsource

    f.create_virtual_dataset('stack', layout, fillvalue=0)


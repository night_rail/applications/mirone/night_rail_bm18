stemp=open("input_template.conf","r").read()

Njobs = 100

for i in range(Njobs):
    fstart = i*1.0/Njobs
    fend = (i+0.9999)*1.0/Njobs

    s=stemp.format(FRACT_START=fstart, FRACT_END=fend)
    open("input_%d.conf"%i,"w").write(s)
